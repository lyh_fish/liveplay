//
//  TiUI.m
//  TiSDK
//
//  Created by Cat66 on 2018/5/15.
//  Copyright © 2018年 Tillusory Tech. All rights reserved.
//

#import "TiUIView.h"
#import "TiSaveData.h"
#import "TiUICell.h"
#import "TiStickerManager.h"
#import "TiStickerCell.h"
#import "TiStickerDownloadManager.h"
#import "TiGiftManager.h"
#import "TiGiftCell.h"
#import "TiGiftDownloadManager.h"
#import "TiWatermarkManager.h"
#import "TiWatermarkCell.h"
#import "UIImageView+WebCache.h"

#define SkinWhiteningSliderMax 100 // 美白拉条最大值，如果需要修改美白效果上限，可自行设置
#define  SkinBlemishRemovalSliderMax 100 // 磨皮拉条最大值，如果需要修改磨皮效果上限，可自行设置
#define SkinTendernessSliderMax 100 // 粉嫩拉条最大值，如果需要修改粉嫩效果上限，可自行设置
#define EyeMagnifyingSliderMax 100 // 大眼拉条最大值，如果需要修改大眼效果上限，可自行设置
#define ChinSlimmingSliderMax 100 // 瘦脸拉条最大值，如果需要修改大眼效果上限，可自行设置

#define SkinWhiteningValue 65 // 美白拉条默认参数
#define SkinBlemishRemovalValue 80 // 磨皮拉条默认参数
#define SkinTendernessValue 60 // 粉嫩拉条默认参数
#define SkinSaturationValue 50 // 饱和拉条默认参数，50表示无饱和效果，[0, 50]表示降低饱和度，[50, 100]表示提升饱和度
#define EyeMagnifyingValue 80 // 大眼拉条默认参数
#define ChinSlimmingValue 80 // 瘦脸拉条默认参数

@interface TiUIView () <UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, strong) TiSDKManager *tiSDKManager;
@property(nonatomic, weak) id <TiUIViewDelegate> delegate;
@property(nonatomic, strong) UIView *superView;

@property(nonatomic, weak) UIView *currentPopView;
@property(nonatomic, weak) UIView *currentPopViewBeautySlider;
@property(nonatomic, weak) UIView *currentPopViewRenderEnable;


@property(nonatomic, weak) UIView *nextPopView;
@property(nonatomic, weak) UIView *nextPopViewBeautySlider;
@property(nonatomic, weak) UIView *nextPopViewRenderEnable;

@property(nonatomic, strong) UILabel *beautyLabel;


@property(nonatomic, strong) UIView *currentMainMenuPopView;

@property(nonatomic, assign) BOOL isSystemEnglish; // 系统是否为英文


@property(nonatomic, strong) UIButton *mainSwitchButton; // 总开关



@property(nonatomic, strong) UIScrollView *mainMenuView; // 菜单栏
@property(nonatomic, strong) UIButton *selectBeautyButton; // 美颜-选择按钮
@property(nonatomic, strong) UIButton *selectFaceTrimButton; // 美型-选择按钮
@property(nonatomic, strong) UIButton *selectStickerButton; // 贴纸-选择按钮
@property(nonatomic, strong) UIButton *selectFilterButton; // 滤镜-选择按钮
@property(nonatomic, strong) UIButton *selectRockButton; // Rock-选择按钮
@property(nonatomic, strong) UIButton *selectDistortionButton; // 哈哈镜-选择按钮
@property(nonatomic, strong) UIButton *selectGiftButton; // 礼物-选择按钮
@property(nonatomic, strong) UIButton *selectWatermarkButton; // 水印-选择按钮

@property(nonatomic, strong) UIView *beautySliderView; // 美颜拉条窗口
@property(nonatomic, strong) UIView *renderEnableView; // 原图按钮窗口
@property(nonatomic, strong) UIButton *setRenderEnableButton; // 原图按钮开关



@property(nonatomic, assign) NSInteger beautyEnum; //全局美颜类型
@property(nonatomic, assign) NSInteger trimEnum; //全局美颜类型

@property(nonatomic, strong) UIView *beautyView; // 美颜窗口
@property(nonatomic, strong) UIView *faceTrimView; // 美型窗口
@property(nonatomic, strong) UIView *stickerView; // 贴纸窗口
@property(nonatomic, strong) UIView *filterView; // 滤镜窗口
@property(nonatomic, strong) UIView *giftView; // 礼物窗口
@property(nonatomic, strong) UIView *rockView; // Rock窗口
@property(nonatomic, strong) UIView *distortionView; // 哈哈镜窗口
@property(nonatomic, strong) UIView *watermarkView; // 水印窗口

@property(nonatomic, strong) UIButton *beautySwitchButton; // 美颜开关
@property(nonatomic, strong) UILabel *beautySwitchButtonLabel; // 美颜开关标签
@property(nonatomic, strong) UISlider *beautySkinWhiteningSlider; // 美颜-美白拉条
@property(nonatomic, strong) UILabel *beautySkinWhiteningLabel; // 美颜-美白标签
@property(nonatomic, strong) UISlider *beautySkinBlemishRemovalSlider; // 美颜-磨皮拉条
@property(nonatomic, strong) UILabel *beautySkinBlemishRemovalLabel; // 美颜-磨皮标签
@property(nonatomic, strong) UISlider *beautySkinSaturationSlider; // 美颜-饱和拉条
@property(nonatomic, strong) UILabel *beautySkinSaturationLabel; // 美颜-饱和标签
@property(nonatomic, strong) UISlider *beautySkinTendernessSlider; // 美颜-粉嫩拉条
@property(nonatomic, strong) UILabel *beautySkinTendernessLabel; // 美颜-粉嫩标签

@property(nonatomic, strong) UIButton *faceTrimSwitchButton; // 美型开关
@property(nonatomic, strong) UILabel *faceTrimSwitchButtonLabel; // 美型开关
@property(nonatomic, strong) UISlider *faceTrimEyeMagnifyingSlider; // 美型-大眼拉条
@property(nonatomic, strong) UILabel *faceTrimEyeMagnifyingLabel; // 美型-大眼标签
@property(nonatomic, strong) UISlider *faceTrimChinSlimmingSlider; // 美型-瘦脸拉条
@property(nonatomic, strong) UILabel *faceTrimChinSlimmingLabel; // 美型-瘦脸标签

@property(nonatomic, strong) UISlider *faceTrimJawTransformingSlider; // 美型-下巴拉条
@property(nonatomic, strong) UISlider *faceTrimForeheadTransformingSlider; // 美型-额头拉条
@property(nonatomic, strong) UISlider *faceTrimMouthTransformingSlider; // 美型-嘴型拉条
@property(nonatomic, strong) UISlider *faceTrimNoseSlimmingSlider; // 美型-瘦鼻拉条
@property(nonatomic, strong) UISlider *faceTrimTeethWhiteningSlider; // 美型-美牙拉条


@property(nonatomic, strong) UICollectionView *beautyCollectionView; // 美颜选择窗口
@property(nonatomic, strong) UICollectionView *faceTrimCollectionView; // 美颜选择窗口
@property(nonatomic, strong) UICollectionView *stickerCollectionView; // 贴纸选择窗口
@property(nonatomic, strong) UICollectionView *filterCollectionView; // 滤镜选择窗口
@property(nonatomic, strong) UICollectionView *rockCollectionView; // Rock选择窗口
@property(nonatomic, strong) UICollectionView *distortionCollectionView; // 哈哈镜选择窗口
@property(nonatomic, strong) UICollectionView *giftCollectionView; // 礼物选择窗口
@property(nonatomic, strong) UICollectionView *watermarkCollectionView; // 水印选择窗口


@property(nonatomic, assign) NSInteger rockEnumIndex; // rock特效索引
@property(nonatomic, assign) NSInteger filterEnumIndex; // 滤镜特效索引
@property(nonatomic, assign) NSInteger distortionEnumIndex; // 哈哈镜特效索引

@property(nonatomic, assign) NSInteger currentBeautyIndex; // 美颜特效索引
@property(nonatomic, assign) NSInteger currentTrimIndex; // 美型特效索引

@property(nonatomic, strong) NSMutableArray<TiSticker *> *stickerArray;
@property(nonatomic) NSInteger currentStickerIndex;

@property(nonatomic, strong) NSMutableArray<TiGift *> *giftArray;
@property(nonatomic) NSInteger currentGiftIndex;

@property(nonatomic, strong) NSMutableArray<TiWatermark *> *watermarkArray;
@property(nonatomic) NSInteger currentWatermarkIndex;

@end

@implementation TiUIView

UIView *tapView;

- (instancetype)initTiUIViewWith:(TiSDKManager *)tiSDKManager delegate:(id<TiUIViewDelegate>)delegate superView:(UIView *)superView {
    if (self = [super init]) {
        self.tiSDKManager = tiSDKManager;
        self.delegate = delegate;
        self.superView = superView;
    }
    
    // 判定系统语言是否为英文
    if ([[[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2] isEqual: @"en"]) {
        _isSystemEnglish = true;
    } else {
        _isSystemEnglish = false;
    }
    
    [self.tiSDKManager setBeautyEnable:YES];
    [self.tiSDKManager setFaceTrimEnable: YES];
    _rockEnumIndex = -1;
    _filterEnumIndex = -1;
    _distortionEnumIndex = -1;
    _currentStickerIndex = -1;
    _currentGiftIndex = -1;
    _currentWatermarkIndex = -1;
    _currentBeautyIndex = -1;
    _currentTrimIndex = -1;
    
    __weak typeof(self) __weakSelf = self;
    [[TiStickerManager sharedManager] loadStickersWithCompletion:^(NSMutableArray<TiSticker *> *stickerArray) {
        __weakSelf.stickerArray = stickerArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Ti_STICKERSLOADED_COMPLETE" object:nil];
        });
    }];
    
    [[TiGiftManager sharedManager] loadGiftsWithCompletion:^(NSMutableArray<TiGift *> *giftArray) {
        __weakSelf.giftArray = giftArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Ti_GIFTSLOADED_COMPLETE" object:nil];
        });
    }];
    
    [[TiWatermarkManager init] loadWatermarkWithCompletion:^(NSMutableArray<TiWatermark *> *watermarkArray) {
        __weakSelf.watermarkArray = watermarkArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Ti_WATERMARKSLOADED_COMPLETE" object:nil];
        });
    }];
    
    return self;
}


- (void)createTiUIView {
    if (self.superView != nil) {
        if (self.isClearOldUI) {
            [self clearAllViews];
        }
        
        tapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, TiScreenWidth, TiScreenHeight)];
        tapView.userInteractionEnabled = YES;
        [UIApplication sharedApplication].statusBarHidden = YES;
//        [tapView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)]];
        [self.superView addSubview:tapView];
        [tapView setHidden:YES];
        
       // [self.superView addSubview:self.mainSwitchButton];
        [self.superView addSubview:self.setRenderEnableButton];

        [self.superView addSubview:self.mainMenuView];
        
        [self.superView addSubview:self.beautyView];
        [self.superView addSubview:self.faceTrimView];
        [self.superView addSubview:self.stickerView];
//        [self.superView addSubview:self.giftView];
        [self.superView addSubview:self.filterView];
        [self.superView addSubview:self.rockView];
        [self.superView addSubview:self.distortionView];
        [self.superView addSubview:self.watermarkView];
        
        [self.superView addSubview:self.beautySliderView];
        [self.superView addSubview:self.renderEnableView];
        
        
        self.tapView = tapView;
        
         [self pushMainMenuView:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stickersLoadedComplete:) name:@"Ti_STICKERSLOADED_COMPLETE" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(giftsLoadedComplete:) name:@"Ti_GIFTSLOADED_COMPLETE" object:nil];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watermarksLoadedComplete:) name:@"Ti_WATERMARKSLOADED_COMPLETE" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resourceURLLoadedComplete:) name:TiResourceURLNotificationName object:nil];
    }
}

- (void)stickersLoadedComplete:(NSNotification *)noti {
    [self.stickerCollectionView reloadData];
    [self.stickerCollectionView scrollsToTop];
}

- (void)giftsLoadedComplete:(NSNotification *)noti {
    [self.giftCollectionView reloadData];
    [self.giftCollectionView scrollsToTop];
}

- (void)watermarksLoadedComplete:(NSNotification *)noti {
    [self.watermarkCollectionView reloadData];
    [self.watermarkCollectionView scrollsToTop];
}

- (void)resourceURLLoadedComplete:(NSNotification *)noti {
    [self.stickerCollectionView reloadData];
    [self.giftCollectionView reloadData];
    [self.watermarkCollectionView reloadData];
}
/////////////////////// UI绘制 开始 ///////////////////////
// 绘制总开关
- (UIButton *)mainSwitchButton {
    if (!_mainSwitchButton) {
        _mainSwitchButton = [[UIButton alloc] initWithFrame:CGRectMake(50, TiScreenHeight/2, 36, 36)];
        [_mainSwitchButton setImage:[UIImage imageNamed:@"ic_beauty_unselected"] forState:UIControlStateNormal];
        [_mainSwitchButton setImage:[UIImage imageNamed:@"ic_beauty_selected"] forState:UIControlStateSelected];
        [_mainSwitchButton addTarget:self action:@selector(onMainSwitchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _mainSwitchButton;
}

// 绘制主菜单栏
- (UIScrollView *)mainMenuView {
    if (!_mainMenuView) {
        _mainMenuView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 150)];
        [_mainMenuView setBackgroundColor:TiRGBA(0, 0, 0, 0.8)];
        _mainMenuView.contentSize = CGSizeMake(TiScreenWidth*2.5, 150);
        _mainMenuView.showsHorizontalScrollIndicator=YES; // 显示水平滚动条
        [_mainMenuView addSubview:self.selectBeautyButton];
        [_mainMenuView addSubview:self.selectFaceTrimButton];
        [_mainMenuView addSubview:self.selectStickerButton];
      //  [_mainMenuView addSubview:self.selectGiftButton];
        [_mainMenuView addSubview:self.selectFilterButton];
        [_mainMenuView addSubview:self.selectRockButton];
        [_mainMenuView addSubview:self.selectDistortionButton];
        [_mainMenuView addSubview:self.selectWatermarkButton];
    }
    return _mainMenuView;
}

// 绘制美颜选择按钮
- (UIButton *)selectBeautyButton {
    if (!_selectBeautyButton) {
        _selectBeautyButton =
        [[UIButton alloc] initWithFrame:CGRectMake((TiScreenWidth - 46 * 5) / 6 + 10, 8, 60, 25)];
        if (_isSystemEnglish) {
            [_selectBeautyButton setTitle:@"Beauty" forState:UIControlStateNormal];
        }
        else{
            [_selectBeautyButton setTitle:@"美颜" forState:UIControlStateNormal];
        }
        [_selectBeautyButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectBeautyButton addTarget:self action:@selector(onSelectBeautyButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _selectBeautyButton;
}

//  绘制美型选择按钮
- (UIButton *)selectFaceTrimButton {
    if (!_selectFaceTrimButton) {
        _selectFaceTrimButton =
        [[UIButton alloc] initWithFrame:CGRectMake((TiScreenWidth - 46 * 5) / 6 * 2 + 75, 8, 60, 25)];
        if (_isSystemEnglish) {
            [_selectFaceTrimButton setTitle:@"Face Trim" forState:UIControlStateNormal];
        }
        else{
            [_selectFaceTrimButton setTitle:@"美型" forState:UIControlStateNormal];
        }
        [_selectFaceTrimButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectFaceTrimButton addTarget:self action:@selector(onSelectFaceTrimButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectFaceTrimButton;
}

// 绘制贴纸选择按钮
- (UIButton *)selectStickerButton {
    if (!_selectStickerButton) {
        _selectStickerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectStickerButton.frame = CGRectMake((TiScreenWidth - 46 * 5) / 6 * 3 + 75 * 2, 8, 60, 25);
        if (_isSystemEnglish) {
            [_selectStickerButton setTitle:@"Sticker" forState:UIControlStateNormal];
        }
        else{
            [_selectStickerButton setTitle:@"贴纸" forState:UIControlStateNormal];
        }
        [_selectStickerButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectStickerButton addTarget:self action:@selector(onSelectStickerButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectStickerButton;
}

// 绘制礼物选择按钮
- (UIButton *)selectGiftButton {
    if (!_selectGiftButton) {
        _selectGiftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectGiftButton.frame = CGRectMake((TiScreenWidth - 46 * 5) / 6 * 4 + 75 * 3, 8, 60, 25);
        if (_isSystemEnglish) {
            [_selectGiftButton setTitle:@"Gift" forState:UIControlStateNormal];
        }
        else{
            [_selectGiftButton setTitle:@"礼物" forState:UIControlStateNormal];
        }
        [_selectGiftButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectGiftButton addTarget:self action:@selector(onSelectGiftButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectGiftButton;
}

// 绘制滤镜选择按钮
- (UIButton *)selectFilterButton {
    if (!_selectFilterButton) {
        _selectFilterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectFilterButton.frame =CGRectMake((TiScreenWidth - 46 * 5) / 6 * 4 +  75 * 3, 8, 60, 25);
        if (_isSystemEnglish) {
            [_selectFilterButton setTitle:@"Filter" forState:UIControlStateNormal];
        }
        else{
            [_selectFilterButton setTitle:@"滤镜" forState:UIControlStateNormal];
        }
        [_selectFilterButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectFilterButton addTarget:self action:@selector(onSelectFilterButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _selectFilterButton;
}

// 绘制Rock选择按钮
- (UIButton *)selectRockButton {
    if (!_selectRockButton) {
        _selectRockButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectRockButton.frame = CGRectMake((TiScreenWidth - 46 * 5) / 6 * 5 +  75 * 4, 8, 60, 25);
        
        if (_isSystemEnglish) {
            [_selectRockButton setTitle:@"Rock" forState:UIControlStateNormal];
        }
        else{
            [_selectRockButton setTitle:@"抖动" forState:UIControlStateNormal];
        }
        [_selectRockButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectRockButton addTarget:self action:@selector(onSelectRockButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectRockButton;
}

// 绘制哈哈镜选择按钮
- (UIButton *)selectDistortionButton {
    if (!_selectDistortionButton) {
        _selectDistortionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectDistortionButton.frame = CGRectMake((TiScreenWidth - 46 * 5) / 6 * 6 +  75 * 5, 8, 60, 25);
    
        if (_isSystemEnglish) {
            [_selectDistortionButton setTitle:@"Distortion" forState:UIControlStateNormal];
        }
        else{
            [_selectDistortionButton setTitle:@"哈哈镜" forState:UIControlStateNormal];
        }
        [_selectDistortionButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectDistortionButton addTarget:self action:@selector(onSelectDistortionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectDistortionButton;
}

// 绘制哈哈镜选择按钮
- (UIButton *)selectWatermarkButton {
    if (!_selectWatermarkButton) {
        _selectWatermarkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectWatermarkButton.frame = CGRectMake((TiScreenWidth - 46 * 5) / 6 * 7 +  75 * 6, 8, 60, 25);
        
        if (_isSystemEnglish) {
            [_selectWatermarkButton setTitle:@"Watermark" forState:UIControlStateNormal];
        }
        else{
            [_selectWatermarkButton setTitle:@"水印" forState:UIControlStateNormal];
        }
        [_selectWatermarkButton setTitleColor:TiRGBA(18, 150, 219, 0.9) forState:UIControlStateSelected];
        [_selectWatermarkButton addTarget:self action:@selector(onSelectWatermarkButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectWatermarkButton;
}


// 绘制窗口
- (UIView *)renderEnableView {
    if (! _renderEnableView) {
        _renderEnableView = [[UIView alloc] initWithFrame:CGRectMake(TiScreenWidth-40, TiScreenHeight, 40, 40)];
        
        _renderEnableView.userInteractionEnabled = YES;
        [_renderEnableView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        [_renderEnableView addSubview:_setRenderEnableButton];
    }
    return _renderEnableView;
}

// 绘制总开关
- (UIButton *)setRenderEnableButton {
    if (!_setRenderEnableButton) {
        _setRenderEnableButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [_setRenderEnableButton setImage:[UIImage imageNamed:@"ic_ti_render_disable"] forState:UIControlStateNormal];
        [_setRenderEnableButton setImage:[UIImage imageNamed:@"ic_ti_render_disable"] forState:UIControlStateSelected];
        
        [_setRenderEnableButton setSelected:NO];
        _setRenderEnableButton.layer.masksToBounds = NO;
        
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        
        longPress.minimumPressDuration = 0.0; //定义按的时间
        
        [_setRenderEnableButton addGestureRecognizer:longPress];
        
    }

    return _setRenderEnableButton;
}

-(void)longPress:(UILongPressGestureRecognizer*)gestureRecognizer{

    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        [self.tiSDKManager setRenderEnable:false];
    }
    else if([gestureRecognizer state] == UIGestureRecognizerStateEnded){
        [self.tiSDKManager setRenderEnable:true];
    }
    else{
        return;
    }
    
    
}

// 绘制美颜窗口
- (UIView *)beautyView {
    if (!_beautyView) {
        _beautyView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 200)];
        _beautyView.userInteractionEnabled = YES;
        [_beautyView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        [_beautyView addSubview:self.beautyCollectionView];
        [_beautyView addSubview:self.beautySwitchButton];
    }
    return _beautyView;
}

// 绘制美颜开关按钮
- (UIButton *)beautySwitchButton {
    if (!_beautySwitchButton) {
        _beautySwitchButton =
        [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 50, 50)];
        [_beautySwitchButton setImage:[UIImage imageNamed:@"ic_enable_unselected"] forState:UIControlStateNormal];
        [_beautySwitchButton setImage:[UIImage imageNamed:@"ic_enable_selected"] forState:UIControlStateSelected];
        [_beautySwitchButton setSelected:YES];
        _beautySwitchButton.layer.masksToBounds = YES;
        [_beautySwitchButton addTarget:self action:@selector(onBeautySwitchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _beautySwitchButton;
}

// 绘制美型窗口
- (UIView *)faceTrimView {
    if (!_faceTrimView) {
        _faceTrimView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 200)];
        _faceTrimView.userInteractionEnabled = YES;
        [_faceTrimView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        
        [_faceTrimView addSubview:self.faceTrimCollectionView];
        [_faceTrimView addSubview:self.faceTrimSwitchButton];
        
        
    }
    return _faceTrimView;
}

// 绘制美型开关按钮
- (UIButton *)faceTrimSwitchButton {
    if (!_faceTrimSwitchButton) {
        _faceTrimSwitchButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 50, 50)];
        [_faceTrimSwitchButton setImage:[UIImage imageNamed:@"ic_enable_unselected"] forState:UIControlStateNormal];
        [_faceTrimSwitchButton setImage:[UIImage imageNamed:@"ic_enable_selected"] forState:UIControlStateSelected];
        [_faceTrimSwitchButton setSelected:YES];
        _faceTrimSwitchButton.layer.masksToBounds = YES;
        [_faceTrimSwitchButton addTarget:self action:@selector(onFaceTrimSwitchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _faceTrimSwitchButton;
}

// 绘制美白拉条
- (UISlider *)beautySkinWhiteningSlider {
    
    if (!_beautySkinWhiteningSlider) {
        _beautySkinWhiteningSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //美白指定可变最小值
        _beautySkinWhiteningSlider.minimumValue = 0;
        //美白指定可变最大值
        _beautySkinWhiteningSlider.maximumValue = 100;
        //美白指定初始值
        if (![TiSaveData floatForKey:@"TiSliderWhitening"]) {
            _beautySkinWhiteningSlider.value = 65;
            [TiSaveData setFloat:65 forKey:@"TiSliderWhitening"];
        } else {
            _beautySkinWhiteningSlider.value = [TiSaveData floatForKey:@"TiSliderWhitening"];
        }
        
        [self.tiSDKManager setSkinWhitening:_beautySkinWhiteningSlider.value];
        [_beautySkinWhiteningSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_beautySkinWhiteningSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_beautySkinWhiteningSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_beautySkinWhiteningSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_beautySkinWhiteningSlider addTarget:self action:@selector(onBeautySkinWhiteningSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _beautySkinWhiteningSlider;
}

// 绘制美颜-磨皮拉条
- (UISlider *)beautySkinBlemishRemovalSlider {
    
    if (!_beautySkinBlemishRemovalSlider) {
        _beautySkinBlemishRemovalSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        //磨皮指定可变最小值
        _beautySkinBlemishRemovalSlider.minimumValue = 0;
        //磨皮指定可变最大值
        _beautySkinBlemishRemovalSlider.maximumValue = 100;
        if (![TiSaveData floatForKey:@"TiSliderMicrodermabrasion"]) {
            _beautySkinBlemishRemovalSlider.value = 70;
            [TiSaveData setFloat:70 forKey:@"TiSliderMicrodermabrasion"];
        } else {
            _beautySkinBlemishRemovalSlider.value = [TiSaveData floatForKey:@"TiSliderMicrodermabrasion"];
        }
        
        [self.tiSDKManager setSkinBlemishRemoval:_beautySkinBlemishRemovalSlider.value];
        [_beautySkinBlemishRemovalSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_beautySkinBlemishRemovalSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_beautySkinBlemishRemovalSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_beautySkinBlemishRemovalSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_beautySkinBlemishRemovalSlider addTarget:self action:@selector(onBeautySkinBlemishRemovalSliderClick:) forControlEvents:UIControlEventValueChanged];
        
    }
    return _beautySkinBlemishRemovalSlider;
}

// 绘制美颜-饱和拉条
- (UISlider *)beautySkinSaturationSlider {
    
    if (!_beautySkinSaturationSlider) {
        _beautySkinSaturationSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        //饱和指定可变最小值
        _beautySkinSaturationSlider.minimumValue = 0;
        //饱和指定可变最大值
        _beautySkinSaturationSlider.maximumValue = 100;
        //饱和指定初始值
        if (![TiSaveData floatForKey:@"TiSliderSaturation"]) {
            _beautySkinSaturationSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderSaturation"];
        } else {
            _beautySkinSaturationSlider.value = [TiSaveData floatForKey:@"TiSliderSaturation"];
        }
        
        [self.tiSDKManager setSkinSaturation:_beautySkinSaturationSlider.value];
        
        [_beautySkinSaturationSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_beautySkinSaturationSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        //注意这里要加UIControlStateHightlighted的状态，否则当拖动滑块时滑块将变成原生的控件
        [_beautySkinSaturationSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_beautySkinSaturationSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_beautySkinSaturationSlider addTarget:self action:@selector(onBeautySkinSaturationSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _beautySkinSaturationSlider;
}

// 绘制美颜-粉嫩拉条
- (UISlider *)beautySkinTendernessSlider {
    
    if (!_beautySkinTendernessSlider) {
        _beautySkinTendernessSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        //磨皮指定可变最小值
        _beautySkinTendernessSlider.minimumValue = 0;
        //磨皮指定可变最大值
        _beautySkinTendernessSlider.maximumValue = 100;
        //磨皮指定初始值
        if (![TiSaveData floatForKey:@"TiSliderPinkistender"]) {
            _beautySkinTendernessSlider.value = 80;
            [TiSaveData setFloat:80 forKey:@"TiSliderPinkistender"];
        } else {
            _beautySkinTendernessSlider.value = [TiSaveData floatForKey:@"TiSliderPinkistender"];
        }
        
        [self.tiSDKManager setSkinTenderness:_beautySkinTendernessSlider.value];
        
        [_beautySkinTendernessSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_beautySkinTendernessSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        //注意这里要加UIControlStateHightlighted的状态，否则当拖动滑块时滑块将变成原生的控件
        [_beautySkinTendernessSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_beautySkinTendernessSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_beautySkinTendernessSlider addTarget:self action:@selector(onBeautySkinTendernessSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _beautySkinTendernessSlider;
}

// 绘制美型-大眼拉条
- (UISlider *)faceTrimEyeMagnifyingSlider {
    if (!_faceTrimEyeMagnifyingSlider) {
        _faceTrimEyeMagnifyingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        _faceTrimEyeMagnifyingSlider.minimumValue = 0;
        _faceTrimEyeMagnifyingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderEyeMagnifying"]) {
            _faceTrimEyeMagnifyingSlider.value = 80;
            [TiSaveData setFloat:80 forKey:@"TiSliderEyeMagnifying"];
        } else {
            _faceTrimEyeMagnifyingSlider.value = [TiSaveData floatForKey:@"TiSliderEyeMagnifying"];
        }
        
        [self.tiSDKManager setEyeMagnifying:_faceTrimEyeMagnifyingSlider.value];
        
        [_faceTrimEyeMagnifyingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimEyeMagnifyingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimEyeMagnifyingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimEyeMagnifyingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimEyeMagnifyingSlider addTarget:self action:@selector(onFaceTrimEyeMagnifyingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimEyeMagnifyingSlider;
}

// 绘制美型-瘦脸拉条
- (UISlider *)faceTrimChinSlimmingSlider {
    if (!_faceTrimChinSlimmingSlider) {
        _faceTrimChinSlimmingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //瘦脸指定可变最小值
        _faceTrimChinSlimmingSlider.minimumValue = 0;
        //瘦脸指定可变最大值
        _faceTrimChinSlimmingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderChinSlimming"]) {
            _faceTrimChinSlimmingSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderChinSlimming"];
        } else {
            _faceTrimChinSlimmingSlider.value = [TiSaveData floatForKey:@"TiSliderChinSlimming"];
        }
        
        [self.tiSDKManager setChinSlimming:_faceTrimChinSlimmingSlider.value];
        
        [_faceTrimChinSlimmingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimChinSlimmingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimChinSlimmingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimChinSlimmingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimChinSlimmingSlider addTarget:self action:@selector(onFaceTrimChinSlimmingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimChinSlimmingSlider;
}

// 绘制美型-下巴拉条
- (UISlider *)faceTrimJawTransformingSlider {
    if (!_faceTrimJawTransformingSlider) {
        _faceTrimJawTransformingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //下巴指定可变最小值
        _faceTrimJawTransformingSlider.minimumValue = 0;
        //下巴指定可变最大值
        _faceTrimJawTransformingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderJawTransforming"]) {
            _faceTrimJawTransformingSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderJawTransforming"];
        } else {
            _faceTrimJawTransformingSlider.value = [TiSaveData floatForKey:@"TiSliderJawTransforming"];
        }
        
        [self.tiSDKManager setJawTransforming:_faceTrimJawTransformingSlider.value];
        
        [_faceTrimJawTransformingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimJawTransformingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimJawTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimJawTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimJawTransformingSlider addTarget:self action:@selector(onFaceTrimJawTransformingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimJawTransformingSlider;
}

// 绘制美型-额头拉条
- (UISlider *)faceTrimForeheadTransformingSlider {
    if (!_faceTrimForeheadTransformingSlider) {
        _faceTrimForeheadTransformingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //额头指定可变最小值
        _faceTrimForeheadTransformingSlider.minimumValue = 0;
        //额头指定可变最大值
        _faceTrimForeheadTransformingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderForeheadTransforming"]) {
            _faceTrimForeheadTransformingSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderForeheadTransforming"];
        } else {
            _faceTrimForeheadTransformingSlider.value = [TiSaveData floatForKey:@"TiSliderForeheadTransforming"];
        }
        
        [self.tiSDKManager setForeheadTransforming:_faceTrimForeheadTransformingSlider.value];
        
        [_faceTrimForeheadTransformingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimForeheadTransformingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimForeheadTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimForeheadTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimForeheadTransformingSlider addTarget:self action:@selector(onFaceTrimForeheadTransformingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimForeheadTransformingSlider;
}

// 绘制美型-嘴型拉条
- (UISlider *)faceTrimMouthTransformingSlider {
    if (!_faceTrimMouthTransformingSlider) {
        _faceTrimMouthTransformingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //嘴型指定可变最小值
        _faceTrimMouthTransformingSlider.minimumValue = 0;
        //嘴型指定可变最大值
        _faceTrimMouthTransformingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderMouthTransforming"]) {
            _faceTrimMouthTransformingSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderMouthTransforming"];
        } else {
            _faceTrimMouthTransformingSlider.value = [TiSaveData floatForKey:@"TiSliderMouthTransforming"];
        }
        
        [self.tiSDKManager setMouthTransforming:_faceTrimMouthTransformingSlider.value];
        
        [_faceTrimMouthTransformingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimMouthTransformingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimMouthTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimMouthTransformingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimMouthTransformingSlider addTarget:self action:@selector(onFaceTrimMouthTransformingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimMouthTransformingSlider;
}

// 绘制美型-瘦鼻拉条
- (UISlider *)faceTrimNoseSlimmingSlider {
    if (!_faceTrimNoseSlimmingSlider) {
        _faceTrimNoseSlimmingSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //瘦鼻指定可变最小值
        _faceTrimNoseSlimmingSlider.minimumValue = 0;
        //瘦鼻指定可变最大值
        _faceTrimNoseSlimmingSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderNoseSlimming"]) {
            _faceTrimNoseSlimmingSlider.value = 0;
            [TiSaveData setFloat:0 forKey:@"TiSliderNoseSlimming"];
        } else {
            _faceTrimNoseSlimmingSlider.value = [TiSaveData floatForKey:@"TiSliderNoseSlimming"];
        }
        
        [self.tiSDKManager setNoseMinifying:_faceTrimNoseSlimmingSlider.value];
        
        [_faceTrimNoseSlimmingSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimNoseSlimmingSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimNoseSlimmingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimNoseSlimmingSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimNoseSlimmingSlider addTarget:self action:@selector(onFaceTrimNoseSlimmingSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimNoseSlimmingSlider;
}

// 绘制美型-美牙拉条
- (UISlider *)faceTrimTeethWhiteningSlider {
    if (!_faceTrimTeethWhiteningSlider) {
        _faceTrimTeethWhiteningSlider =
        [[UISlider alloc] initWithFrame:CGRectMake(40, 10, TiScreenWidth-80, 20)];
        
        //美牙指定可变最小值
        _faceTrimTeethWhiteningSlider.minimumValue = 0;
        //美牙指定可变最大值
        _faceTrimTeethWhiteningSlider.maximumValue = 100;
        
        if (![TiSaveData floatForKey:@"TiSliderTeethWhitening"]) {
            _faceTrimTeethWhiteningSlider.value = 50;
            [TiSaveData setFloat:50 forKey:@"TiSliderTeethWhitening"];
        } else {
            _faceTrimTeethWhiteningSlider.value = [TiSaveData floatForKey:@"TiSliderTeethWhitening"];
        }
        
        [self.tiSDKManager setNoseMinifying:_faceTrimTeethWhiteningSlider.value];
        
        [_faceTrimTeethWhiteningSlider setMinimumTrackImage:[UIImage imageNamed:@"ic_wire"] forState:UIControlStateNormal];
        [_faceTrimTeethWhiteningSlider setMaximumTrackImage:[UIImage imageNamed:@"ic_wire_drk"] forState:UIControlStateNormal];
        [_faceTrimTeethWhiteningSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateHighlighted];
        [_faceTrimTeethWhiteningSlider setThumbImage:[UIImage imageNamed:@"ic_button"] forState:UIControlStateNormal];
        
        [_faceTrimTeethWhiteningSlider addTarget:self action:@selector(onFaceTrimTeethWhiteningSliderClick:) forControlEvents:UIControlEventValueChanged];
    }
    return _faceTrimTeethWhiteningSlider;
}


// 美白拉条响应事件
- (void)onBeautySkinWhiteningSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinWhiteningSlider.value]];
    
    [self.tiSDKManager setSkinWhitening:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderWhitening"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderWhitening"];
    }
}


// 美颜-磨皮拉条响应事件
- (void)onBeautySkinBlemishRemovalSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinBlemishRemovalSlider.value]];
    
    [self.tiSDKManager setSkinBlemishRemoval:sender.value];
    
    [TiSaveData setFloat:sender.value forKey:@"TiSliderMicrodermabrasion"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderMicrodermabrasion"];
    }
}

// 美颜-饱和拉条响应事件
- (void)onBeautySkinSaturationSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinSaturationSlider.value]];
    
    [self.tiSDKManager setSkinSaturation:sender.value];
    
    [TiSaveData setFloat:sender.value forKey:@"TiSliderSaturation"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderSaturation"];
    }
}

// 美颜-粉嫩拉条响应事件
- (void)onBeautySkinTendernessSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinTendernessSlider.value]];
    
    [self.tiSDKManager setSkinTenderness:sender.value];
    
    [TiSaveData setFloat:sender.value forKey:@"TiSliderPinkistender"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderPinkistender"];
    }
}

// 美型-大眼拉条响应事件
- (void)onFaceTrimEyeMagnifyingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimEyeMagnifyingSlider.value]];
    
    [self.tiSDKManager setEyeMagnifying:sender.value];
    
    [TiSaveData setFloat:sender.value forKey:@"TiSliderEyeMagnifying"];
    
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderEyeMagnifying"];
    }
}

// 美型-瘦脸拉条响应事件
- (void)onFaceTrimChinSlimmingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimChinSlimmingSlider.value]];
    
    [self.tiSDKManager setChinSlimming:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderChinSlimming"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderChinSlimming"];
    }
}

// 美型-下巴拉条响应事件
- (void)onFaceTrimJawTransformingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimJawTransformingSlider.value]];
    
    [self.tiSDKManager setJawTransforming:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderJawTransforming"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderJawTransforming"];
    }
}

// 美型-额头拉条响应事件
- (void)onFaceTrimForeheadTransformingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimForeheadTransformingSlider.value]];
    
    [self.tiSDKManager setForeheadTransforming:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderForeheadTransforming"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderForeheadTransforming"];
    }
}

// 美型-嘴型拉条响应事件
- (void)onFaceTrimMouthTransformingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimMouthTransformingSlider.value]];
    
    [self.tiSDKManager setMouthTransforming:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderMouthTransforming"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderMouthTransforming"];
    }
}

// 美型-瘦鼻拉条响应事件
- (void)onFaceTrimNoseSlimmingSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimNoseSlimmingSlider.value]];
    
    [self.tiSDKManager setNoseMinifying:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderNoseSlimming"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderNoseSlimming"];
    }
}

// 美型-美牙拉条响应事件
- (void)onFaceTrimTeethWhiteningSliderClick:(UISlider *)sender {
    [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimTeethWhiteningSlider.value]];
    
    [self.tiSDKManager setTeethWhitening:sender.value];
    [TiSaveData setFloat:sender.value forKey:@"TiSliderTeethWhitening"];
    if (sender.value == 0) {
        [TiSaveData setFloat:0.01 forKey:@"TiSliderTeethWhitening"];
    }
}


// 绘制全局美颜选择窗口
- (UICollectionView *)beautyCollectionView {
    if (!_beautyCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7 + 10, (TiScreenWidth) / 7);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 15;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 15;
        //定义每个UICollectionView 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(25, 16, 25, 21);//上左下右
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动方向
        
        _beautyCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(60, 0, TiScreenWidth-60, 100) collectionViewLayout:flowLayout];
        //注册cell
        [_beautyCollectionView registerClass:[TiUICell class] forCellWithReuseIdentifier:TiUICellIdentifier];
        
        //设置代理
        _beautyCollectionView.delegate = self;
        _beautyCollectionView.dataSource = self;
        
        //背景颜色
        _beautyCollectionView.backgroundColor = [UIColor clearColor];
        //自适应大小
        _beautyCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _beautyCollectionView;
}


// 绘制美型选择窗口
- (UICollectionView *)faceTrimCollectionView {
    if (!_faceTrimCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7 + 10, (TiScreenWidth) / 7);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 15;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 15;
        //定义每个UICollectionView 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(25, 16, 25, 21);//上左下右
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动方向
        
        _faceTrimCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(60, 0, TiScreenWidth-60, 100) collectionViewLayout:flowLayout];
        //注册cell
        [_faceTrimCollectionView registerClass:[TiUICell class] forCellWithReuseIdentifier:TiUICellIdentifier];
        
        //设置代理
        _faceTrimCollectionView.delegate = self;
        _faceTrimCollectionView.dataSource = self;
        
        //背景颜色
        _faceTrimCollectionView.backgroundColor = [UIColor clearColor];
        //自适应大小
        _faceTrimCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _faceTrimCollectionView;
}

// 绘制美颜拉条窗口
- (UIView *)beautySliderView {
    if (! _beautySliderView) {
        _beautySliderView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 40)];
        _beautySliderView.userInteractionEnabled = YES;
        [_beautySliderView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        
        
        [_beautySliderView addSubview:self.beautySkinWhiteningSlider];
        [_beautySliderView addSubview:self.beautySkinBlemishRemovalSlider];
        [_beautySliderView addSubview:self.beautySkinSaturationSlider];
        [_beautySliderView addSubview:self.beautySkinTendernessSlider];
        [_beautySliderView addSubview:self.faceTrimEyeMagnifyingSlider];
        [_beautySliderView addSubview:self.faceTrimChinSlimmingSlider];
        [_beautySliderView addSubview:self.faceTrimForeheadTransformingSlider];
        [_beautySliderView addSubview:self.faceTrimJawTransformingSlider];
        [_beautySliderView addSubview:self.faceTrimMouthTransformingSlider];
        [_beautySliderView addSubview:self.faceTrimNoseSlimmingSlider];
        [_beautySliderView addSubview:self.faceTrimTeethWhiteningSlider];
        
        
        
        _beautyLabel =
        [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 50, 20)];
        
        [_beautyLabel setTextColor:[UIColor whiteColor]];
        [_beautyLabel setFont:[UIFont systemFontOfSize:11.f]];
        [_beautyLabel setTextAlignment:NSTextAlignmentCenter];
        
        [_beautySliderView addSubview:_beautyLabel];
        
        
    }
    return _beautySliderView;
}


// 绘制贴纸窗口
- (UIView *)stickerView {
    if (!_stickerView) {
        _stickerView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_stickerView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        _stickerView.userInteractionEnabled = YES;
        [_stickerView addSubview:self.stickerCollectionView];
    }
    return _stickerView;
}

// 绘制贴纸选择窗口
- (UICollectionView *)stickerCollectionView {
    if (!_stickerCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionCell 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7, (TiScreenWidth) / 7);
        //定义每个UICollectionCell 横向的间距
        flowLayout.minimumLineSpacing = 10;
        //定义每个UICollectionCell 纵向的间距
        flowLayout.minimumInteritemSpacing = 10;
        //定义每个UICollectionCell 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 20, 40, 20);//上左下右
        
        _stickerCollectionView =
        [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, TiScreenWidth-10, 110) collectionViewLayout:flowLayout];
        
        //注册cell
        [_stickerCollectionView registerClass:[TiStickerCell class] forCellWithReuseIdentifier:TiStickerCellIdentifier];
        
        //设置代理
        _stickerCollectionView.delegate = self;
        _stickerCollectionView.dataSource = self;
        
        //背景颜色
        _stickerCollectionView.backgroundColor = [UIColor clearColor];
        
        //自适应大小
        _stickerCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return _stickerCollectionView;
}

// 绘制礼物窗口
- (UIView *)giftView {
    if (!_giftView) {
        _giftView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_giftView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        _giftView.userInteractionEnabled = YES;
        [_giftView addSubview:self.giftCollectionView];
    }
    return _giftView;
}

// 绘制礼物选择窗口
- (UICollectionView *)giftCollectionView {
    if (!_giftCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionCell 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7, (TiScreenWidth) / 7);
        //定义每个UICollectionCell 横向的间距
        flowLayout.minimumLineSpacing = 10;
        //定义每个UICollectionCell 纵向的间距
        flowLayout.minimumInteritemSpacing = 10;
        //定义每个UICollectionCell 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 20, 40, 20);//上左下右
        
        _giftCollectionView =
        [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, TiScreenWidth-10, 110) collectionViewLayout:flowLayout];
        
        //注册cell
        [_giftCollectionView registerClass:[TiGiftCell class] forCellWithReuseIdentifier:TiGiftCellIdentifier];
        
        //设置代理
        _giftCollectionView.delegate = self;
        _giftCollectionView.dataSource = self;
        
        //背景颜色
        _giftCollectionView.backgroundColor = [UIColor clearColor];
        
        //自适应大小
        _giftCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return _giftCollectionView;
}

// 绘制滤镜窗口
- (UIView *)filterView {
    if (!_filterView) {
        _filterView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_filterView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        [_filterView addSubview:self.filterCollectionView];
    }
    return _filterView;
}

// 绘制滤镜选择窗口
- (UICollectionView *)filterCollectionView {
    if (!_filterCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7 + 10, (TiScreenWidth) / 7);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 15;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 15;
        //定义每个UICollectionView 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(21, 16, 24, 21);//上左下右
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动方向
        _filterCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, TiScreenWidth-10, 100) collectionViewLayout:flowLayout];
        //注册cell
        [_filterCollectionView registerClass:[TiUICell class] forCellWithReuseIdentifier:TiUICellIdentifier];
        
        //设置代理
        _filterCollectionView.delegate = self;
        _filterCollectionView.dataSource = self;
        //背景颜色
        _filterCollectionView.backgroundColor = [UIColor clearColor];
        //自适应大小
        _filterCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _filterCollectionView;

}

// 绘制Rock 窗口
- (UIView *)rockView {
    if (!_rockView) {
        _rockView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_rockView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        [_rockView addSubview:self.rockCollectionView];
    }
    return _rockView;
}

// 绘制Rock选择窗口
- (UICollectionView *)rockCollectionView {
    if (!_rockCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7 + 10, (TiScreenWidth) / 7);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 15;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 15;
        //定义每个UICollectionView 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(21, 16, 24, 21);//上左下右
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动方向
        
        _rockCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, TiScreenWidth-10, 100) collectionViewLayout:flowLayout];
        //注册cell
        [_rockCollectionView registerClass:[TiUICell class] forCellWithReuseIdentifier:TiUICellIdentifier];
        
        //设置代理
        _rockCollectionView.delegate = self;
        _rockCollectionView.dataSource = self;
        
        //背景颜色
        _rockCollectionView.backgroundColor = [UIColor clearColor];
        //自适应大小
        _rockCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _rockCollectionView;
}

// 绘制哈哈镜窗口
- (UIView *)distortionView {
    if (!_distortionView) {
        _distortionView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_distortionView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        [_distortionView addSubview:self.distortionCollectionView];
    }
    return _distortionView;
}

// 绘制哈哈镜选择窗口
- (UICollectionView *)distortionCollectionView {
    if (!_distortionCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7 + 10, (TiScreenWidth) / 7);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 15;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 15;
        flowLayout.sectionInset = UIEdgeInsetsMake(21, 16, 24, 21);//上左下右
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动方向
        
        _distortionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, TiScreenWidth-10, 100) collectionViewLayout:flowLayout];
        //注册cell
        [_distortionCollectionView registerClass:[TiUICell class] forCellWithReuseIdentifier:TiUICellIdentifier];
        
        //设置代理
        _distortionCollectionView.delegate = self;
        _distortionCollectionView.dataSource = self;
        
        //背景颜色
        _distortionCollectionView.backgroundColor = [UIColor clearColor];
        //自适应大小
        _distortionCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _distortionCollectionView;
}

// 绘制水印窗口
- (UIView *)watermarkView {
    if (!_watermarkView) {
        _watermarkView = [[UIView alloc] initWithFrame:CGRectMake(0, TiScreenHeight, TiScreenWidth, 240)];
        [_watermarkView setBackgroundColor:TiRGBA(0, 0, 0, 0.0)];
        _watermarkView.userInteractionEnabled = YES;
        [_watermarkView addSubview:self.watermarkCollectionView];
    }
    return _watermarkView;
}


// 绘制水印选择窗口
- (UICollectionView *)watermarkCollectionView {
    if (!_watermarkCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        //定义每个UICollectionCell 的大小
        flowLayout.itemSize = CGSizeMake((TiScreenWidth) / 7, (TiScreenWidth) / 7);
        //定义每个UICollectionCell 横向的间距
        flowLayout.minimumLineSpacing = 10;
        //定义每个UICollectionCell 纵向的间距
        flowLayout.minimumInteritemSpacing = 10;
        //定义每个UICollectionCell 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 20, 40, 20);//上左下右
        
        _watermarkCollectionView =
        [[UICollectionView alloc] initWithFrame:CGRectMake(10, 25, TiScreenWidth-10, 110) collectionViewLayout:flowLayout];
        
        //注册cell
        [_watermarkCollectionView registerClass:[TiWatermarkCell class] forCellWithReuseIdentifier:TiWatermarkCellIdentifier];
        
        //设置代理
        _watermarkCollectionView.delegate = self;
        _watermarkCollectionView.dataSource = self;
        
        //背景颜色
        _watermarkCollectionView.backgroundColor = [UIColor clearColor];
        
        //自适应大小
        _watermarkCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return _watermarkCollectionView;
}
/////////////////////// UI绘制 开始 ///////////////////////

/////////////////////// 响应事件 开始 ///////////////////////
// tap手势响应事件
- (void)onTap:(UITapGestureRecognizer *)recognizer {
    [self popAllViews];
}

// 总开关响应事件
- (void)onMainSwitchButtonClick:(UIButton *)sender {
    [self pushMainMenuView:YES];
}

// 美颜选择按钮响应事件
- (void)onSelectBeautyButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 美型选择按钮响应事件
- (void)onSelectFaceTrimButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 贴纸选择按钮响应事件
- (void)onSelectStickerButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 礼物选择按钮响应事件
- (void)onSelectGiftButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 滤镜选择按钮响应事件
- (void)onSelectFilterButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// Rock选择按钮响应事件
- (void)onSelectRockButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 哈哈镜选择按钮响应事件
- (void)onSelectDistortionButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 水印选择按钮响应事件
- (void)onSelectWatermarkButtonClick:(UIButton *)sender {
    [self switchButtonInMainMenu:sender];
}

// 美颜开关按钮响应实现
- (void)onBeautySwitchButtonClick:(UIButton *)sender {
    [TiSaveData setValue:[NSNumber numberWithBool:sender.isSelected] forKey:@"TiEnableBeautyFilter"];
    
    [sender setSelected:!sender.isSelected];
    
    BOOL enable = sender.isSelected;
    
    [self.tiSDKManager setBeautyEnable:enable];
    [self.beautySkinWhiteningSlider setEnabled:enable];
    [self.beautySkinBlemishRemovalSlider setEnabled:enable];
    [self.beautySkinSaturationSlider setEnabled:enable];
    [self.beautySkinTendernessSlider setEnabled:enable];
}

// 美型开关按钮响应事件
- (void)onFaceTrimSwitchButtonClick:(UIButton *)sender {
    [TiSaveData setValue:[NSNumber numberWithBool:sender.isSelected] forKey:@"TiEnableBigEyeSlimChin"];
    
    [sender setSelected:!sender.isSelected];
    
    BOOL enable = sender.isSelected;
    
    [self.tiSDKManager setFaceTrimEnable:enable];
    [self.faceTrimEyeMagnifyingSlider setEnabled:enable];
    [self.faceTrimChinSlimmingSlider setEnabled:enable];
    [self.faceTrimJawTransformingSlider setEnabled:enable];
    [self.faceTrimForeheadTransformingSlider setEnabled:enable];
    [self.faceTrimMouthTransformingSlider setEnabled:enable];
    [self.faceTrimNoseSlimmingSlider setEnabled:enable];
    [self.faceTrimTeethWhiteningSlider setEnabled:enable];
    
}


/////////////////////// 响应事件 结束 ///////////////////////

/////////////////////// UI控制 开始 ///////////////////////
- (void)clearAllViews {
    for (UIView *subView in self.superView.subviews) {
        [subView removeFromSuperview];
    }
}

- (void)popAllViews {
    if (self.currentPopView != nil) {
        [self turnOffAllButtonsInMainMenu];
        
        [self hideSubview:self.currentPopView];
        [self hideSubview:self.currentPopViewBeautySlider];
        [self hideSubview:self.currentPopViewRenderEnable];
        
        
        self.currentPopView = nil;
        self.currentPopViewBeautySlider = nil;
        self.currentPopViewRenderEnable = nil;
    }
    
    if (self.currentMainMenuPopView != nil) {
        if ([self.currentMainMenuPopView isEqual:self.mainMenuView]) {
            [self pushMainMenuView:NO];
        } else {
            [self pushFilterView:NO];
        }
        
        [self havePushed:NO];
    }
}

- (void)turnOffAllButtonsInMainMenu {
    [self.selectBeautyButton setSelected:NO];
    [self.selectFaceTrimButton setSelected:NO];
    [self.selectStickerButton setSelected:NO];
    [self.selectGiftButton setSelected:NO];
    [self.selectFilterButton setSelected:NO];
    [self.selectRockButton setSelected:NO];
    [self.selectDistortionButton setSelected:NO];
    [self.selectWatermarkButton setSelected:NO];
}

- (void)hideSubview:(UIView *)view {
    CGRect frame = view.frame;
    frame.origin.y = TiScreenHeight;
    view.frame = frame;
}

- (void)showSubview:(UIView *)view {
    [view setHidden:NO];
    CGRect frame = view.frame;
    
    if ([view isEqual:self.beautyView]) {
        frame.origin.y = TiScreenHeight - 112 ;
    } else if([view isEqual:self.beautySliderView]){
        frame.origin.y = TiScreenHeight - 190;
    }else if([view isEqual:self.renderEnableView]){
        frame.origin.y = TiScreenHeight - 190;
    }else if ([view isEqual:self.faceTrimView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.stickerView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.giftView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.filterView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.rockView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.distortionView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else if ([view isEqual:self.watermarkView]) {
        frame.origin.y = TiScreenHeight - 112;
    } else {
        frame.origin.y = TiScreenHeight - 224;
    }
    
    view.frame = frame;
}

- (void)pushMainMenuView:(BOOL)isPop {
    [self havePushed:YES];
    
    if (isPop) {
        self.currentMainMenuPopView = self.mainMenuView;
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.mainMenuView.frame;
            frame.origin.y = TiScreenHeight - 150;
            self.mainMenuView.frame = frame;
            [self onSelectBeautyButtonClick:self.selectBeautyButton];
        }];
    } else {
        self.currentMainMenuPopView = nil;
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.mainMenuView.frame;
            frame.origin.y = TiScreenHeight;
            self.mainMenuView.frame = frame;
        }];
    }
}

- (void)pushFilterView:(BOOL)isPop {
    [self havePushed:YES];
    
    if (isPop) {
        self.currentMainMenuPopView = self.filterView;
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = self.filterView.frame;
            frame.origin.y = TiScreenHeight - 150;
            self.filterView.frame = frame;
        }];
    } else {
        self.currentMainMenuPopView = nil;
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame = self.filterView.frame;
            frame.origin.y = TiScreenHeight;
            self.filterView.frame = frame;
        }];
    }
}

- (void)havePushed:(BOOL)isPush {
    if (!isPush) {
        [self.mainSwitchButton setAlpha:0];
        [self.mainSwitchButton setHidden:isPush];
        [UIView animateWithDuration:0.6 animations:^{
            [self.mainSwitchButton setAlpha:1];
        }];
        
        [tapView setHidden:YES];
    } else {
        [self.mainSwitchButton setHidden:isPush];
        
        [tapView setHidden:NO];
        
    }
}

- (void)switchButtonInMainMenu:(UIButton *)sender {
    BOOL enable = sender.isSelected;
    
    [self turnOffAllButtonsInMainMenu];
    if (enable) {
        [sender setSelected:NO];
    } else {
        [sender setSelected:YES];
    }
    

    
    if ([sender isEqual:self.selectBeautyButton]) {
        [self setBeautyEnumByIndex:(int)_currentBeautyIndex+1];
        self.nextPopView = self.beautyView;
        self.nextPopViewBeautySlider = self.beautySliderView;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectFaceTrimButton]) {
        [self setTrimEnumByIndex:(int)_currentTrimIndex+1];
        self.nextPopView = self.faceTrimView;
        self.nextPopViewBeautySlider = self.beautySliderView;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectStickerButton]) {
        self.nextPopView = self.stickerView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectGiftButton]) {
        self.nextPopView = self.giftView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectFilterButton]) {
        self.nextPopView = self.filterView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectRockButton]) {
        self.nextPopView = self.rockView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectDistortionButton]) {
        self.nextPopView = self.distortionView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    } else if ([sender isEqual:self.selectWatermarkButton]) {
        self.nextPopView = self.watermarkView;
        self.nextPopViewBeautySlider = nil;
        self.nextPopViewRenderEnable = self.renderEnableView;
    }
    
    [self lockButtonsInMainMenu:NO];
    
    
    if (self.currentPopView != nil) {
        if (self.nextPopView != nil) {
            if (![self.currentPopView isEqual:self.nextPopView]) {
                [UIView animateWithDuration:0.1 animations:^{
                    [self hideSubview:self.currentPopView];
                    [self showSubview:self.nextPopView];
                } completion:^(BOOL finished) {
                    [self lockButtonsInMainMenu:YES];
                }];
                if(self.nextPopViewBeautySlider != nil){
                    [self showSubview:self.nextPopViewBeautySlider];
                }
                else{
                    [self hideSubview:self.currentPopViewBeautySlider];
                }
                if(self.nextPopViewRenderEnable != nil){
                    [self showSubview:self.nextPopViewRenderEnable];
                }
                else{
                    [self hideSubview:self.currentPopViewRenderEnable];
                }
                
                
            } else {
                //判断是否被选中 如果非选中状态 则隐藏当前窗口
                if (!sender.isSelected) {
                    [UIView animateWithDuration:0.1 animations:^{
                        [self hideSubview:self.currentPopView];
                    }                completion:^(BOOL finished) {
                        [self lockButtonsInMainMenu:YES];
                    }];
                } else {
                    [UIView animateWithDuration:0.1 animations:^{
                        [self showSubview:self.currentPopView];
                    }                completion:^(BOOL finished) {
                        [self lockButtonsInMainMenu:YES];
                    }];
                }
            }
        } else {
            [UIView animateWithDuration:0.1 animations:^{
                [self hideSubview:self.currentPopView];
            }                completion:^(BOOL finished) {
                [self lockButtonsInMainMenu:YES];
            }];
        }
    } else {
        if (self.nextPopView != nil) {
            [UIView animateWithDuration:0.1 animations:^{
                [self showSubview:self.nextPopView];
                if(self.nextPopViewBeautySlider != nil){
                    [self showSubview:self.nextPopViewBeautySlider];
                }
                else{
                    [self hideSubview:self.currentPopViewBeautySlider];
                }
                if(self.nextPopViewRenderEnable != nil){
                    [self showSubview:self.nextPopViewRenderEnable];
                }
                else{
                    [self hideSubview:self.currentPopViewRenderEnable];
                }
            }                completion:^(BOOL finished) {
                [self lockButtonsInMainMenu:YES];
            }];
        } else {
            [self lockButtonsInMainMenu:YES];
        }
    }
    
    self.currentPopView = self.nextPopView;
    self.currentPopViewBeautySlider = self.nextPopViewBeautySlider;
    self.currentPopViewRenderEnable = self.nextPopViewRenderEnable;
    self.nextPopView = nil;
    self.nextPopViewBeautySlider = nil;
    self.nextPopViewRenderEnable = nil;
}

- (void)lockButtonsInMainMenu:(BOOL)isEnable {
    [self.selectBeautyButton setEnabled:isEnable];
    [self.selectFaceTrimButton setEnabled:isEnable];
    [self.selectStickerButton setEnabled:isEnable];
    [self.selectGiftButton setEnabled:isEnable];
    [self.selectFilterButton setEnabled:isEnable];
    [self.selectRockButton setEnabled:isEnable];
    [self.selectDistortionButton setEnabled:isEnable];
    [self.selectWatermarkButton setEnabled:isEnable];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger count = 0;
    if(collectionView == self.beautyCollectionView){
        if (section == 0) {
            count = BEAUTY_CELL_COUNTER + 1;
        }
    } else if (collectionView == self.faceTrimCollectionView) {
        if (section == 0) {
            count = TRIM_CELL_COUNTER + 1;
        }
    } else if (collectionView == self.stickerCollectionView) {
        if (section == 0) {
            count = self.stickerArray.count + 1;
        }
    } else if (collectionView == self.giftCollectionView) {
        if (section == 0) {
            count = self.giftArray.count + 1;
        }
    } else if (collectionView == self.filterCollectionView) {
        if (section == 0) {
            count = FILTER_CELL_COUNTER + 1;
        }
    } else if (collectionView == self.rockCollectionView) {
        if (section == 0) {
            count = ROCK_CELL_COUNTER + 1;
        }
    } else if (collectionView == self.distortionCollectionView) {
        if (section == 0) {
            count = DISTORTION_CELL_COUNTER + 1;
        }
    } else if (collectionView == self.watermarkCollectionView) {
        if (section == 0) {
            count = self.watermarkArray.count + 1;
        }
    }
    
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    
    if (collectionView == self.beautyCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiUICellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        
        
        if (_currentBeautyIndex == indexPath.item - 1) {
            [((TiUICell *) cell) changeCellEffect:NO];
            [((TiUICell *) cell) setBeautyUICellByIndex:indexPath.item isSelected:NO];
            
            
        } else {
            [((TiUICell *) cell) changeCellEffect:YES];
            [((TiUICell *) cell) setBeautyUICellByIndex:indexPath.item isSelected:YES];
            
        }
        
    } else if (collectionView == self.faceTrimCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiUICellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        if (_currentTrimIndex == indexPath.item - 1) {
            [((TiUICell *) cell) changeCellEffect:NO];
            [((TiUICell *) cell) setTrimUICellByIndex:indexPath.item isSelected:NO];
            
        } else {
            [((TiUICell *) cell) changeCellEffect:YES];
            [((TiUICell *) cell) setTrimUICellByIndex:indexPath.item isSelected:YES];
        }
    }
    
    else if (collectionView == self.stickerCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiStickerCellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (indexPath.item <= 0) {
            [((TiStickerCell *) cell) setSticker:nil index:indexPath.item];
        } else {
            TiSticker *sticker = self.stickerArray[indexPath.item - 1];
            [((TiStickerCell *) cell) setSticker:sticker index:indexPath.item];
        }
        
        if (self.currentStickerIndex == indexPath.item - 1) {
            [((TiStickerCell *) cell) hideBackView:NO];
        } else {
            [((TiStickerCell *) cell) hideBackView:YES];
        }
    } else if (collectionView == self.giftCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiGiftCellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (indexPath.item <= 0) {
            [((TiGiftCell *) cell) setGift:nil index:indexPath.item];
        } else {
            TiGift *gift = self.giftArray[indexPath.item - 1];
            [((TiGiftCell *) cell) setGift:gift index:indexPath.item];
        }
        
        if (self.currentGiftIndex == indexPath.item - 1) {
            [((TiGiftCell *) cell) hideBackView:NO];
        } else {
            [((TiGiftCell *) cell) hideBackView:YES];
        }
    } else if (collectionView == self.filterCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiUICellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (_filterEnumIndex == indexPath.item - 1) {
            [((TiUICell *) cell) changeCellEffect:NO];
        } else {
            [((TiUICell *) cell) changeCellEffect:YES];
        }
        
        [((TiUICell *) cell) setFilterUICellByIndex:indexPath.item];
        
    } else if (collectionView == self.rockCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiUICellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (_rockEnumIndex == indexPath.item - 1) {
            [((TiUICell *) cell) changeCellEffect:NO];
        } else {
            [((TiUICell *) cell) changeCellEffect:YES];
        }
        
        [((TiUICell *) cell) setRockUICellByIndex:indexPath.item];
        
    }    else if (collectionView == self.distortionCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiUICellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (_distortionEnumIndex == indexPath.item - 1) {
            [((TiUICell *) cell) changeCellEffect:NO];
            [((TiUICell *) cell) setDistortionUICellByIndex:indexPath.item isSelected:NO];
            
        } else {
            [((TiUICell *) cell) changeCellEffect:YES];
            [((TiUICell *) cell) setDistortionUICellByIndex:indexPath.item isSelected:YES];
        }
    } else if (collectionView == self.watermarkCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:TiWatermarkCellIdentifier forIndexPath:indexPath];
        [cell sizeToFit];
        
        if (indexPath.item <= 0) {
            [((TiWatermarkCell *) cell) setWatermark:nil index:indexPath.item];
        } else {
            TiWatermark *watermark = self.watermarkArray[indexPath.item - 1];
            [((TiWatermarkCell *) cell) setWatermark:watermark index:indexPath.item];
        }
        
        if (self.currentWatermarkIndex == indexPath.item - 1) {
            [((TiWatermarkCell *) cell) hideBackView:NO];
        } else {
            [((TiWatermarkCell *) cell) hideBackView:YES];
        }
    }
    
    return cell;
}

- (void)removeAllSlider{
    [_beautySkinWhiteningSlider removeFromSuperview];
    [_beautySkinSaturationSlider removeFromSuperview];
    [_beautySkinTendernessSlider removeFromSuperview];
    [_beautySkinBlemishRemovalSlider removeFromSuperview];
    [_faceTrimEyeMagnifyingSlider removeFromSuperview];
    [_faceTrimChinSlimmingSlider removeFromSuperview];
    [_faceTrimForeheadTransformingSlider removeFromSuperview];
    [_faceTrimJawTransformingSlider removeFromSuperview];
    [_faceTrimMouthTransformingSlider removeFromSuperview];
    [_faceTrimNoseSlimmingSlider removeFromSuperview];
    [_faceTrimTeethWhiteningSlider removeFromSuperview];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.beautyCollectionView) {
        if (_currentBeautyIndex == indexPath.item - 1) {
            return;
        }
        //隐藏上次选中cell的背景
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_currentBeautyIndex +1 inSection:0];
        
        TiUICell *lastCell = (TiUICell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell changeCellEffect:YES];
        _currentBeautyIndex = indexPath.item -1;
        if (indexPath.item -1 >= -1) {
            [self setBeautyEnumByIndex:(int)indexPath.item];
        }
        
        TiUICell *cell = (TiUICell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell changeCellEffect:NO];
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    }
    
    else if (collectionView == self.faceTrimCollectionView) {
        if (_currentTrimIndex == indexPath.item - 1) {
            return;
        }
        //隐藏上次选中cell的背景
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_currentTrimIndex +1 inSection:0];
        
        TiUICell *lastCell = (TiUICell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell changeCellEffect:YES];
        _currentTrimIndex = indexPath.item -1;
        if (indexPath.item -1 >= -1) {
            [self setTrimEnumByIndex:(int)indexPath.item];
        }
        
        TiUICell *cell = (TiUICell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell changeCellEffect:NO];
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    }

    else if (collectionView == self.stickerCollectionView) {
        //选中同一个cell不做处理
        if (self.currentStickerIndex == indexPath.item - 1 ) {
            return;
        }
        
        if (indexPath.item > 0) {
            TiSticker *sticker = self.stickerArray[indexPath.item - 1];
            if (sticker.isDownload == NO) {
                [[TiStickerDownloadManager sharedInstance] downloadSticker:sticker index:indexPath.item  - 1 withAnimation:^(NSInteger index) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index  + 1 inSection:0];
                    TiStickerCell *cell = (TiStickerCell *) [self.stickerCollectionView cellForItemAtIndexPath:indexPath];
                    [cell startDownload];
                } successed:^(TiSticker *sticker, NSInteger index) {
                    sticker.downloadState = TiStickerDownloadStateDownoadDone;

                    [self.stickerArray replaceObjectAtIndex:index withObject:sticker];

                    //回到主线程刷新
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (collectionView) {
                            for (NSIndexPath *path in collectionView.indexPathsForVisibleItems) {
                                if (index == path.item) {
                                    [collectionView reloadData];
                                    break;
                                }
                            }
                        }
                    });
                } failed:^(TiSticker *sticker, NSInteger index) {
                    sticker.downloadState = TiStickerDownloadStateDownoadNot;
                    //回到主线程刷新
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [collectionView reloadData];
                        // TODO: 提示用户网络不给力
                    });
                }];
                return;
            }
        }
        
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:self.currentStickerIndex+1  inSection:0];
        TiStickerCell *lastCell = (TiStickerCell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell hideBackView:YES];
        
        //渲染指定贴纸
        // TODO
        self.currentStickerIndex = indexPath.item-1;
        
        if (self.currentStickerIndex >= 0 && self.currentStickerIndex < self.stickerArray.count + 1) {
             [self.tiSDKManager setStickerName:[self.stickerArray objectAtIndex:self.currentStickerIndex].stickerName];
        } else {
            [self.tiSDKManager setStickerName:@""];
        }
        
        //显示选中cell的背景
        TiStickerCell *cell = (TiStickerCell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell hideBackView:NO];
        
        //不刷新会导致背景框显示错误
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    } else if (collectionView == self.giftCollectionView) {
        //选中同一个cell不做处理
        if (self.currentGiftIndex == indexPath.item - 1 ) {
            return;
        }
        
        if (indexPath.item > 0) {
            TiGift *gift = self.giftArray[indexPath.item - 1];
            if (gift.isDownload == NO) {
                [[TiGiftDownloadManager sharedInstance] downloadGift:gift index:indexPath.item  - 1 withAnimation:^(NSInteger index) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index  + 1 inSection:0];
                    TiGiftCell *cell = (TiGiftCell *) [self.giftCollectionView cellForItemAtIndexPath:indexPath];
                    [cell startDownload];
                } successed:^(TiGift *gift, NSInteger index) {
                    gift.downloadState = TiGiftDownloadStateDownoadDone;
                    
                    [self.giftArray replaceObjectAtIndex:index withObject:gift];
                    
                    //回到主线程刷新
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (collectionView) {
                            for (NSIndexPath *path in collectionView.indexPathsForVisibleItems) {
                                if (index == path.item) {
                                    [collectionView reloadData];
                                    break;
                                }
                            }
                        }
                    });
                } failed:^(TiGift *gift, NSInteger index) {
                    gift.downloadState = TiGiftDownloadStateDownoadNot;
                    //回到主线程刷新
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [collectionView reloadData];
                        // TODO: 提示用户网络不给力
                    });
                }];
                return;
            }
        }
        
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:self.currentGiftIndex+1  inSection:0];
        TiGiftCell *lastCell = (TiGiftCell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell hideBackView:YES];
        
        //渲染指定贴纸
        // TODO
        self.currentGiftIndex = indexPath.item-1;
        
        if (self.currentGiftIndex >= 0 && self.currentGiftIndex < self.giftArray.count + 1) {
            [self.tiSDKManager setGift:[self.giftArray objectAtIndex:self.currentGiftIndex].giftName];
        } else {
            [self.tiSDKManager setGift:@""];
        }
        
        //显示选中cell的背景
        TiGiftCell *cell = (TiGiftCell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell hideBackView:NO];
        
        //不刷新会导致背景框显示错误
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    } else if (collectionView == self.filterCollectionView) {
        if (_filterEnumIndex == indexPath.item - 1) {
            return;
        }
        //隐藏上次选中cell的背景
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_filterEnumIndex + 1 inSection:0];
        TiUICell *lastCell = (TiUICell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell changeCellEffect:YES];
        _filterEnumIndex = indexPath.item -1;
        
        if (indexPath.item -1 >= -1) {
            [self.tiSDKManager setFilterEnum:[self setFilterEnumByIndex:(int)indexPath.item]];
        }
        
        TiUICell *cell = (TiUICell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell changeCellEffect:NO];
        
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
        
    } else if (collectionView == self.rockCollectionView) {
        if (_rockEnumIndex == indexPath.item - 1) {
            return;
        }
        //隐藏上次选中cell的背景
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_rockEnumIndex +1 inSection:0];
        
        TiUICell *lastCell = (TiUICell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell changeCellEffect:YES];
        _rockEnumIndex = indexPath.item -1;
        if (indexPath.item -1 >= -1) {
            [self.tiSDKManager setRockEnum:[self setRockEnumByIndex:(int)indexPath.item]];
        }
        TiUICell *cell = (TiUICell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell changeCellEffect:NO];
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
        
    } else if (collectionView == self.distortionCollectionView) {
        if (_distortionEnumIndex == indexPath.item - 1) {
            return;
        }
        //隐藏上次选中cell的背景
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_distortionEnumIndex +1 inSection:0];
        
        TiUICell *lastCell = (TiUICell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell changeCellEffect:YES];
        _distortionEnumIndex = indexPath.item -1;
        if (indexPath.item -1 >= -1) {
            [self.tiSDKManager setDistortionEnum:[self setDistortionEnumByIndex:(int)indexPath.item]];
        }
        TiUICell *cell = (TiUICell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell changeCellEffect:NO];
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    } else if (collectionView == self.watermarkCollectionView) {
        //选中同一个cell不做处理
        if (self.currentWatermarkIndex == indexPath.item - 1 ) {
            return;
        }
        
        if (indexPath.item > 0) {
            TiWatermark *watermark = self.watermarkArray[indexPath.item - 1];
            if (watermark.downloaded == NO) {
                TiWatermarkCell *cell = (TiWatermarkCell *) [self.watermarkCollectionView cellForItemAtIndexPath:indexPath];
                watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoading;
                [self.watermarkArray replaceObjectAtIndex:indexPath.item - 1 withObject:watermark];
                [((TiWatermarkCell *) cell) setWatermark:watermark index:indexPath.item];
                
                //                NSURL *imageURL = watermark.watermarkPictureURL;
                NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [TiSDK getWatermarkURL], watermark.watermarkNameString]];
                SDWebImageManager *manager = [SDWebImageManager sharedManager] ;
                [manager loadImageWithURL:imageURL options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                    
                } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                    if (image) {
                        NSString *path = [[TiWatermarkManager init] getSandboxPath];
                        NSString *imageFilePath = [path stringByAppendingPathComponent:watermark.watermarkNameString];
                        [UIImagePNGRepresentation(image) writeToFile:imageFilePath  atomically:YES];
                        watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoadDone;
                        [self.watermarkArray replaceObjectAtIndex:indexPath.item - 1 withObject:watermark];
                        //回到主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (collectionView) {
                                for (NSIndexPath *path in collectionView.indexPathsForVisibleItems) {
                                    if (indexPath.item - 1 == path.item) {
                                        [collectionView reloadData];
                                        break;
                                    }
                                }
                            }
                        });
                    } else {
                        watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoadNot;
                        //回到主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [collectionView reloadData];
                            // TODO: 提示用户网络不给力
                        });
                    }
                }];
//                return;
            }
        }
        
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:self.currentWatermarkIndex+1  inSection:0];
        TiWatermarkCell *lastCell = (TiWatermarkCell *) [collectionView cellForItemAtIndexPath:lastPath];
        [lastCell hideBackView:YES];
        
        //渲染指定贴纸
        // TODO
        self.currentWatermarkIndex = indexPath.item-1;
        
        if (self.currentWatermarkIndex >= 0 && self.currentWatermarkIndex < self.watermarkArray.count + 1) {
            [self.tiSDKManager setWatermark:TRUE Left:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex].xLocation.intValue Top:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex].yLocation.intValue Ratio:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex].ratio.intValue FileName:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex].watermarkNameString];
        } else {
            [self.tiSDKManager setWatermark:NO Left:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex + 1].xLocation.intValue Top:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex + 1].yLocation.intValue Ratio:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex + 1].ratio.intValue FileName:[self.watermarkArray objectAtIndex:self.currentWatermarkIndex + 1].watermarkNameString];
        }
        
        //显示选中cell的背景
        TiWatermarkCell *cell = (TiWatermarkCell *) [collectionView cellForItemAtIndexPath:indexPath];
        [cell hideBackView:NO];
        
        //不刷新会导致背景框显示错误
        [collectionView reloadItemsAtIndexPaths:@[indexPath, lastPath]];
    }
}

- (void)releaseTiUIView {
    self.superView = nil;
    self.currentPopView = nil;
    self.nextPopView = nil;
    self.currentPopViewBeautySlider = nil;
    self.currentPopViewRenderEnable = nil;
    self.currentMainMenuPopView = nil;
    self.nextPopViewBeautySlider = nil;
    self.nextPopViewRenderEnable = nil;
    self.beautyLabel = nil;
    self.mainSwitchButton = nil; // 总开关
    self.mainMenuView = nil; // 菜单栏
    self.selectBeautyButton = nil; // 美颜-选择按钮
    self.selectFaceTrimButton = nil; // 美型-选择按钮
    self.selectStickerButton = nil; // 贴纸-选择按钮
    self.selectGiftButton = nil; // 礼物-选择按钮
    self.selectFilterButton = nil; // 滤镜-选择按钮
    self.selectRockButton = nil; // Rock-选择按钮
    self.selectDistortionButton = nil; // 哈哈镜-选择按钮
    self.selectWatermarkButton = nil; // 水印-选择按钮
    self.beautyView = nil; // 美颜窗口
    self.faceTrimView = nil; // 美型窗口
    self.stickerView = nil; // 贴纸窗口
    self.giftView = nil; // 礼物窗口
    self.filterView = nil; // 滤镜窗口
    self.rockView = nil; // Rock窗口
    self.distortionView = nil; // 哈哈镜窗口
    self.watermarkView = nil; // 水印-选择按钮
    self.beautySwitchButton = nil; // 美颜开关
    self.beautySwitchButtonLabel = nil; // 美颜开关标签
    self.beautySkinWhiteningSlider = nil; // 美颜-美白拉条
    self.beautySkinWhiteningLabel = nil; // 美颜-美白标签
    self.beautySkinBlemishRemovalSlider = nil; // 美颜-磨皮拉条
    self.beautySkinBlemishRemovalLabel = nil; // 美颜-磨皮标签
    self.beautySkinSaturationSlider = nil; // 美颜-饱和拉条
    self.beautySkinSaturationLabel = nil; // 美颜-饱和标签
    self.beautySkinTendernessSlider = nil; // 美颜-粉嫩拉条
    self.beautySkinTendernessLabel = nil; // 美颜-粉嫩标签
    self.faceTrimSwitchButton = nil; // 美型开关
    self.faceTrimSwitchButtonLabel = nil; // 美型开关
    self.faceTrimEyeMagnifyingSlider = nil; // 美型-大眼拉条
    self.faceTrimEyeMagnifyingLabel = nil; // 美型-大眼标签
    self.faceTrimChinSlimmingSlider = nil; // 美型-瘦脸拉条
    self.faceTrimChinSlimmingLabel = nil; // 美型-瘦脸标签
    self.faceTrimJawTransformingSlider = nil;
    self.faceTrimForeheadTransformingSlider=nil;
    self.faceTrimMouthTransformingSlider = nil;
    self.faceTrimNoseSlimmingSlider = nil;
    self.faceTrimTeethWhiteningSlider = nil;
    self.beautyCollectionView = nil;
    self.faceTrimCollectionView = nil;
    self.stickerCollectionView = nil;
    self.giftCollectionView = nil; // 礼物选择窗口
    self.filterCollectionView = nil; // 滤镜选择窗口
    self.rockCollectionView = nil; // Rock选择窗口
    self.distortionCollectionView = nil; // 哈哈镜选择窗口
    self.watermarkCollectionView = nil; // 水印选择窗口
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Ti_STICKERSLOADED_COMPLETE" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Ti_GIFTSLOADED_COMPLETE" object:nil];
 
}

- (void)dealloc {
    [self popAllViews];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Ti_STICKERSLOADED_COMPLETE" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Ti_GIFTSLOADED_COMPLETE" object:nil];
    tapView = nil;
}


- (void)setBeautyEnumByIndex:(int)index {
    [self removeAllSlider];
    
    switch(index) {
        case 0:
            self.beautyEnum = 0;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinWhiteningSlider.value]];
            [_beautySliderView addSubview:self.beautySkinWhiteningSlider];
            break;
        case 1:
            self.beautyEnum = 1;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinBlemishRemovalSlider.value]];
            [_beautySliderView addSubview:self.beautySkinBlemishRemovalSlider];
            break;
        case 2:
            self.beautyEnum = 2;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinSaturationSlider.value]];
            [_beautySliderView addSubview:self.beautySkinSaturationSlider];
            break;
        case 3:
            self.beautyEnum = 3;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinTendernessSlider.value]];
            [_beautySliderView addSubview:self.beautySkinTendernessSlider];
            break;
        default:
            self.beautyEnum = 0;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.beautySkinWhiteningSlider.value]];
            [_beautySliderView addSubview:self.beautySkinWhiteningSlider];
            break;
    }
}


- (void)setTrimEnumByIndex:(int)index {
    [self removeAllSlider];
    
    switch(index) {
        case 0:
            self.trimEnum = 0;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimEyeMagnifyingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimEyeMagnifyingSlider];
            break;
        case 1:
            self.trimEnum = 1;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimChinSlimmingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimChinSlimmingSlider];
            break;
        case 2:
            self.trimEnum = 2;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimJawTransformingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimJawTransformingSlider];
            break;
        case 3:
            self.trimEnum = 3;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimForeheadTransformingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimForeheadTransformingSlider];
            break;
        case 4:
            self.trimEnum = 4;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimMouthTransformingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimMouthTransformingSlider];
            break;
        case 5:
            self.trimEnum = 5;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimNoseSlimmingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimNoseSlimmingSlider];
            break;
        case 6:
            self.trimEnum = 6;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimTeethWhiteningSlider.value]];
            [_beautySliderView addSubview:self.faceTrimTeethWhiteningSlider];
            break;
        default:
            self.trimEnum = 0;
            [_beautyLabel setText:[NSString stringWithFormat:@"%d", (int) self.faceTrimEyeMagnifyingSlider.value]];
            [_beautySliderView addSubview:self.faceTrimEyeMagnifyingSlider];
            break;
    }
}

- (TiFilterEnum)setFilterEnumByIndex:(int)index {
    switch(index) {
        case 0:
            return NO_FILTER;
            break;
        case 1:
            return SKETCH_FILTER;
            break;
        case 2:
            return SOBELEDGE_FILTER;
            break;
        case 3:
            return TOON_FILTER;
            break;
        case 4:
            return EMBOSS_FILTER;
            break;
        case 5:
            return FILM_FILTER;
            break;
        case 6:
            return PIXELLATION_FILTER;
            break;
        case 7:
            return HALFTONE_FILTER;
            break;
        case 8:
            return CROSSHATCH_FILTER;
            break;
        case 9:
            return NASHVILLE_FILTER;
            break;
        case 10:
            return COFFEE_FILTER;
            break;
        case 11:
            return CHOCOLATE_FILTER;
            break;
        case 12:
            return COCO_FILTER;
            break;
        case 13:
            return DELICIOUS_FILTER;
            break;
        case 14:
            return FIRSTLOVE_FILTER;
            break;
        case 15:
            return FOREST_FILTER;
            break;
        case 16:
            return GLOSSY_FILTER;
            break;
        case 17:
            return GRASS_FILTER;
            break;
        case 18:
            return HOLIDAY_FILTER;
            break;
        case 19:
            return KISS_FILTER;
            break;
        case 20:
            return LOLITA_FILTER;
            break;
        case 21:
            return MEMORY_FILTER;
            break;
        case 22:
            return MOUSSE_FILTER;
            break;
        case 23:
            return NORMAL_FILTER;
            break;
        case 24:
            return OXGEN_FILTER;
            break;
        case 25:
            return PLATYCODON_FILTER;
            break;
        case 26:
            return RED_FILTER;
            break;
        case 27:
            return SUNLESS_FILTER;
            break;
        case 28:
            return PINCH_DISTORTION_FILTER;
            break;
        case 29:
            return KUWAHARA_FILTER; // 油画
            break;
        case 30:
            return POSTERIZE_FILTER; // 分色
            break;
        case 31:
            return SWIRL_DISTORTION_FILTER; // 漩涡
            break;
        case 32:
            return VIGNETTE_FILTER; // 光晕
            break;
        case 33:
            return ZOOM_BLUR_FILTER; // 眩晕
            break;
        case 34:
            return POLKA_DOT_FILTER; // 圆点
            break;
        case 35:
            return POLAR_PIXELLATE_FILTER; // 极坐标
            break;
        case 36:
            return GLASS_SPHERE_REFRACTION_FILTER; // 水晶球
            break;
        case 37:
            return SOLARIZE_FILTER; // 曝光
            break;
        case 38:
            return INK_WASH_PAINTING_FILTER;
            break;
        case 39:
            return ARABICA_FILTER;
            break;
        case 40:
            return AVA_FILTER;
            break;
        case 41:
            return AZREAL_FILTER;
            break;
        case 42:
            return BOURBON_FILTER;
            break;
        case 43:
            return BYERS_FILTER;
            break;
        case 44:
            return CHEMICAL_FILTER;
            break;
        case 45:
            return CLAYTON_FILTER;
            break;
        case 46:
            return CLOUSEAU_FILTER;
            break;
        case 47:
            return COBI_FILTER;
            break;
        case 48:
            return CONTRAIL_FILTER;
            break;
        case 49:
            return CUBICLE_FILTER;
            break;
        case 50:
            return DJANGO_FILTER;
            break;
        default:
            return NO_FILTER;
            break;
    }
}

- (TiRockEnum)setRockEnumByIndex:(int)index {
    switch(index) {
        case 0:
            return NO_ROCK;
            break;
        case 1:
            return DAZZLED_COLOR_ROCK;
            break;
        case 2:
            return LIGHT_COLOR_ROCK;
            break;
        case 3:
            return DIZZY_GIDDY_ROCK;
            break;
        case 4:
            return ASTRAL_PROJECTION_ROCK;
            break;
        case 5:
            return BLACK_MAGIC_ROCK;
            break;
        case 6:
            return VIRTUAL_MIRROR_ROCK;
            break;
        case 7:
            return DYNAMIC_SPLIT_SCREEN_ROCK;
            break;
        case 8:
            return BLACK_WHITE_FILM_ROCK;
            break;
        case 9:
            return GRAY_PETRIFACTION_ROCK; // 瞬间石化
            break;
        case 10:
            return BULGE_DISTORTION__ROCK; // 魔法镜面
            break;
        default:
            return NO_ROCK;
            break;
    }
}

- (TiDistortionEnum)setDistortionEnumByIndex:(int)index {
    switch(index) {
        case 0:
            return NO_DISTORTION;
            break;
        case 1:
            return ET_DISTORTION;
            break;
        case 2:
            return PEAR_FACE_DISTORTION;
            break;
        case 3:
            return SLIM_FACE_DISTORTION;
            break;
        case 4:
            return SQUARE_FACE_DISTORTION;
            break;
        default:
            return NO_DISTORTION;
            break;
    }
}

/////////////////////// UI控制 结束 ///////////////////////

@end
