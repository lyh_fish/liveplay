//
//  TiUICell.h
//  TiLive
//
//  Created by Cat66 on 2018/5/8.
//  Copyright © 2018年 Tillurosy Tech. All rights reserved.
//

#import "TiUICell.h"

@interface TiUICell ()

@end

@implementation TiUICell


- (UIImageView *)bgView {
    if (!_bgView) {
        
        _bgView = [[UIImageView alloc] initWithFrame:self.bounds];
        [_bgView setImage:nil];
    }
    return _bgView;
    
}

- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor whiteColor];
        _label.font = [UIFont fontWithName:@"Helvetica" size:13.f];
        _label.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:_label];
    }
    return _label;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundView = self.bgView;
        self.bgView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame) - 20, CGRectGetWidth(self.frame) - 20);
        CGFloat margin = self.bgView.frame.size.height;
        self.label.frame = CGRectMake(-10, margin+15, margin+20, 5);
    }
    return self;
}

- (void)setRockUICellByIndex:(NSInteger)index{
    NSArray *rockNameCN = [[NSArray alloc] initWithObjects:@"无", @"炫彩抖动", @"轻彩抖动", @"头晕目眩", @"灵魂出窍",@"暗黑魔法", @"虚拟镜像",@"动感分屏", @"黑白电影", @"瞬间石化", @"魔法镜面", nil];
    self.label.text = [rockNameCN objectAtIndex:index];
    
    self.bgView.image = [UIImage imageNamed: [@"ic_ti_rock_" stringByAppendingString:[NSString stringWithFormat: @"%ld", (long)index]]] ;
    
}


- (void)setBeautyUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange{
    NSArray *beautyNameCN = [[NSArray alloc] initWithObjects:@"美白", @"磨皮",@"饱和",@"粉嫩", nil];
    NSArray *beautyImage = [[NSArray alloc] initWithObjects:@"ic_ti_whitening_unselected.png", @"ic_ti_blemish_removal_unselected.png", @"ic_ti_saturation_unselected.png", @"ic_ti_tenderness_unselected.png", nil];
    NSArray *beautyImageSelected = [[NSArray alloc] initWithObjects:@"ic_ti_whitening_selected.png", @"ic_ti_blemish_removal_selected.png", @"ic_ti_saturation_selected.png", @"ic_ti_tenderness_selected.png", nil];
    
    self.label.text = [beautyNameCN objectAtIndex:index];
    if (isChange) {
        self.bgView.image = [UIImage imageNamed: [beautyImage objectAtIndex:index]] ;
    }
    else{
        self.bgView.image = [UIImage imageNamed:[beautyImageSelected objectAtIndex:index]];
    }
}


- (void)setTrimUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange{
    NSArray *beautyNameCN = [[NSArray alloc] initWithObjects:@"大眼", @"瘦脸",@"下巴",@"额头", @"嘴型", @"瘦鼻", @"美牙",  nil];
    NSArray *beautyImage = [[NSArray alloc] initWithObjects:@"ic_ti_eye_magnifying_unselected.png", @"ic_ti_chin_slimming_unselected.png",@"ic_ti_jaw_transforming_unselected.png",@"ic_ti_forehead_transforming_unselected.png",@"ic_ti_mouth_transforming_unselected.png",@"ic_ti_nose_minifying_unselected.png",@"ic_ti_teeth_whitening_unselected.png",nil];
    NSArray *beautyImageSelected = [[NSArray alloc] initWithObjects:@"ic_ti_eye_magnifying_selected.png", @"ic_ti_chin_slimming_selected.png", @"ic_ti_jaw_transforming_selected.png",@"ic_ti_forehead_transforming_selected.png",@"ic_ti_mouth_transforming_selected.png",@"ic_ti_nose_minifying_selected.png",@"ic_ti_teeth_whitening_selected.png",nil];
    
    
    self.label.text = [beautyNameCN objectAtIndex:index];
    if (isChange) {
        self.bgView.image = [UIImage imageNamed: [beautyImage objectAtIndex:index]] ;
    }
    else{
        self.bgView.image = [UIImage imageNamed:[beautyImageSelected objectAtIndex:index]];
    }
}

- (void)setFilterUICellByIndex:(NSInteger)index {
    NSArray *filterNameCN = [[NSArray alloc] initWithObjects:@"无", @"素描",@"黑边", @"卡通",@"浮雕", @"胶片",@"马赛克", @"半色调",@"交叉线", @"那舍尔", @"咖啡", @"巧克力", @"可可", @"美味", @"初恋", @"森林", @"光泽", @"禾草", @"假日", @"初吻", @"洛丽塔", @"回忆", @"慕斯", @"标准", @"氧气", @"桔梗", @"赤红", @"冷日", @"扭曲", @"油画", @"分色", @"漩涡", @"光晕", @"眩晕", @"圆点", @"极坐标", @"水晶球", @"曝光", @"水墨", @"阿拉比卡", @"阿瓦", @"艾瑟瑞尔", @"波旁", @"拜尔斯", @"化学", @"克莱顿", @"克卢索", @"小新", @"凝迹", @"隔间", @"姜戈", nil];
    //    , @"多明戈", @"褪色", @"福尔杰", @"融合", @"雨蛙", @"柯本", @"雷诺克斯", @"幸运", @"麦金农", @"麦洛", @"霓虹", @"游侠", @"帕萨迪纳", @"火龙果", @"里夫", @"雷米", @"链轮", @"泰根", @"特伦特", @"花呢", @"绿鹃", @"Z字母", @"齐克", @"漂白", @"烛光", @"温暖", @"干冷", @"落蓝", @"琥珀", @"秋色", @"软片", @"雾夜", @"富士A", @"富士B", @"富士C", @"绮年华A", @"绮年华B", @"富士D", @"富士E", @"真实", @"暗淡", @"晦暗", @"日落", @"月光", @"白夜", @"暖意", @"剥离A", @"剥离B", @"青橘", @"青绿"
    self.label.text = [filterNameCN objectAtIndex:index];
    self.bgView.image = [UIImage imageNamed: [@"ic_ti_filter_" stringByAppendingString:[NSString stringWithFormat: @"%ld", (long)index]]] ;
    
}

- (void)setDistortionUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange{
    NSArray *distortionNameCN = [[NSArray alloc] initWithObjects:@"无", @"外星人", @"梨梨脸", @"瘦瘦脸", @"方方脸", nil];
    
    NSArray *distortionImage = [[NSArray alloc] initWithObjects:@"ic_ti_none.png", @"ic_ti_et_unselected.png", @"ic_ti_pear_face_unselected.png", @"ic_ti_slim_face_unselected.png", @"ic_ti_square_face_unselected.png",nil];
    NSArray *distortionImageSelected = [[NSArray alloc] initWithObjects:@"ic_ti_none.png", @"ic_ti_et_selected.png", @"ic_ti_pear_face_selected.png", @"ic_ti_slim_face_selected.png",@"ic_ti_square_face_unselected.png", nil];
    
    self.label.text = [distortionNameCN objectAtIndex:index];
    
    if (isChange) {
        self.bgView.image = [UIImage imageNamed: [distortionImage objectAtIndex:index]] ;
    }
    else{
        self.bgView.image = [UIImage imageNamed:[distortionImageSelected objectAtIndex:index]];
    }
    
    
    
}

- (void)setGreenScreenUICellByIndex:(NSInteger)index {
    NSArray *greenScreenNameCN = [[NSArray alloc] initWithObjects:@"无", @"星空", @"黑板", nil];
    self.label.text = [greenScreenNameCN objectAtIndex:index];
}

- (void)changeCellEffect:(BOOL)isChange {
    if (isChange) {
        [self.label setTextColor:TiRGBA(255, 255, 255, 255)];
    } else {
        [self.label setTextColor:TiRGBA(18, 150, 219, 0.9)];
    }
}


@end
