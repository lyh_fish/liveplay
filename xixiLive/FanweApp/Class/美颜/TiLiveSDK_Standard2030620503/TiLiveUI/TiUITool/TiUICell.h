//
//  TiUICell.h
//  TiLive
//
//  Created by Cat66 on 2018/5/8.
//  Copyright © 2018年 Tillurosy Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TiConst.h"

static NSString *TiUICellIdentifier = @"TiUICellIdentifier";

#define TiScreenHeight ([[UIScreen mainScreen] bounds].size.height)
#define TiScreenWidth  ([[UIScreen mainScreen] bounds].size.width)

#define BEAUTY_CELL_COUNTER 3 // 美颜特效数量
#define TRIM_CELL_COUNTER 6 // 美型特效数量
#define STICKER_CELL_COUNTER 19 // 贴纸特效数量
#define FILTER_CELL_COUNTER 50// 滤镜特效数量
#define ROCK_CELL_COUNTER 10 // Rock特效数量
#define DISTORTION_CELL_COUNTER 4 // 哈哈镜特效数量
#define GREEN_SCREEN_CELL_COUNTER 2 // 绿幕特效数量

@interface TiUICell : UICollectionViewCell

@property(nonatomic, strong) UIImageView *bgView;

@property(nonatomic, strong) UILabel *label;

// 初始化
- (instancetype)initWithFrame:(CGRect)frame;

// Rock特效
- (void)setRockUICellByIndex:(NSInteger)index;

// 滤镜特效
- (void)setFilterUICellByIndex:(NSInteger)index;

// 哈哈镜特效
- (void)setDistortionUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange;

// 绿幕特效
- (void)setGreenScreenUICellByIndex:(NSInteger)index;

// 切换cell时特效更改
- (void)changeCellEffect:(BOOL)isChange;

- (void)setBeautyUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange;

- (void)setTrimUICellByIndex:(NSInteger)index isSelected:(BOOL)isChange;



@end
