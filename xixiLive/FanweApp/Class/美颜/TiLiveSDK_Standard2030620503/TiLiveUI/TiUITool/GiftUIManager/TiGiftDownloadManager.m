//
//  TiGiftDownLoadManager.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiGiftDownloadManager.h"
#import "TiGiftItem.h"
#import "SSZipArchive.h"
#import "TiGiftManager.h"
#import "TiConst.h"
#import "TiSDKInterface.h"

@interface TiGiftDownloader : NSObject <SSZipArchiveDelegate, NSURLSessionDelegate>
@property(nonatomic, strong) NSURLSession *session;

@property(nonatomic, copy) void (^successedBlock)(TiGift *, NSInteger, TiGiftDownloader *);

@property(nonatomic, copy) void (^failedBlock)(TiGift *, NSInteger, TiGiftDownloader *);

@property(nonatomic, strong) TiGift *gift;

@property(nonatomic, strong) NSURL *url;

@property(nonatomic, assign) NSInteger index;

- (instancetype)initWithGift:(TiGift *)gift url:(NSURL *)url index:(NSInteger)index;

- (void)downloadSuccessed:(void (^)(TiGift *gift, NSInteger index, TiGiftDownloader *downloader))success failed:(void (^)(TiGift *gift, NSInteger index, TiGiftDownloader *downloader))failed;

@end

@implementation TiGiftDownloader

- (instancetype)initWithGift:(TiGift *)gift url:(NSURL *)url index:(NSInteger)index {
    if (self = [super init]) {
        
        self.gift = gift;
        self.index = index;
        self.url = url;
    }
    
    return self;
}

- (NSURLSession *)session {
    if (!_session) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session =
        [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[[NSOperationQueue alloc] init]];
    }
    return _session;
}

- (void)downloadSuccessed:(void (^)(TiGift *gift, NSInteger index, TiGiftDownloader *downloader))success failed:(void (^)(TiGift *gift, NSInteger index, TiGiftDownloader *downloader))failed {
    
    [[self.session downloadTaskWithURL:self.url completionHandler:^(NSURL *_Nullable location, NSURLResponse *_Nullable response, NSError *_Nullable error) {
        
        if (error) {
            failed(self.gift, self.index, self);
        } else {
            self.successedBlock = success;
            self.failedBlock = failed;
            // unzip
            [SSZipArchive unzipFileAtPath:location.path toDestination:[[TiGiftManager sharedManager] getGiftPath] delegate:self];
            
        }
    }] resume];
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *_Nullable))completionHandler {
    NSURLSessionAuthChallengeDisposition disposition = NSURLSessionAuthChallengePerformDefaultHandling;
    __block NSURLCredential *credential = nil;
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
        if (credential) {
            disposition = NSURLSessionAuthChallengeUseCredential;
        } else {
            disposition = NSURLSessionAuthChallengePerformDefaultHandling;
        }
    } else {
        disposition = NSURLSessionAuthChallengeCancelAuthenticationChallenge;
    }
    if (completionHandler) {
        completionHandler(disposition, credential);
    }
}

#pragma mark - Unzip complete callback

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    // update gift's download config
    [[TiGiftManager sharedManager] updateConfigJSON];
    
    NSString *dir =
    [NSString stringWithFormat:@"%@/%@/", [[TiGiftManager sharedManager] getGiftPath], self.gift.giftName];
    NSURL *url = [NSURL fileURLWithPath:dir];
    
    NSString *s =
    [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"img.jpeg"];
    
    [TiGift updateGiftAfterDownload:self.gift DirectoryURL:url sucess:^(TiGift *sucessGift) {
        
        self.successedBlock(sucessGift, self.index, self);
        
    }                                fail:^(TiGift *failGift) {
        
        self.failedBlock(failGift, self.index, self);
        
    }];
    
}

@end

@interface TiGiftDownloadManager ()

/**
 *   操作缓冲池
 */
@property(nonatomic, strong) NSMutableDictionary *downloadCache;

@end

@implementation TiGiftDownloadManager

+ (instancetype)sharedInstance {
    static id _sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [TiGiftDownloadManager new];
    });
    
    return _sharedManager;
}

- (NSMutableDictionary *)downloadCache {
    if (_downloadCache == nil) {
        _downloadCache = [[NSMutableDictionary alloc] init];
    }
    return _downloadCache;
}

- (void)downloadGift:(TiGift *)gift index:(NSInteger)index withAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiGift *gift, NSInteger index))success failed:(void (^)(TiGift *gift, NSInteger index))failed {
    NSString *zipName = [NSString stringWithFormat:@"%@.zip", gift.giftName];
    
    NSURL *downloadUrl = gift.downloadURL;
    
    if (gift.sourceType == TiGiftSourceTypeFromTi) {
        
        downloadUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [TiSDK getGiftURL], zipName]];
    }
    
    // 判断是否存在对应的下载操作
    if (self.downloadCache[downloadUrl] != nil) {
        return;
    }
    
    animating(index);
    
    TiGiftDownloader *downloader = [[TiGiftDownloader alloc] initWithGift:gift url:downloadUrl index:index];
    
    [self.downloadCache setObject:downloader forKey:downloadUrl];
    
    [downloader downloadSuccessed:^(TiGift *gift, NSInteger index, TiGiftDownloader *downloader) {
        
        [self.downloadCache removeObjectForKey:downloadUrl];
        downloader = nil;
        success(gift, index);
        
    }                      failed:^(TiGift *gift, NSInteger index, TiGiftDownloader *downloader) {
        
        [self.downloadCache removeObjectForKey:downloadUrl];
        downloader = nil;
        failed(gift, index);
        
    }];
    
}

- (void)downloadGifts:(NSArray *)gifts withAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiGift *gift, NSInteger index))success failed:(void (^)(TiGift *gift, NSInteger index))failed {
    
    for (TiGift *gift in gifts) {
        if (gift.isDownload == NO && gift.downloadState == TiGiftDownloadStateDownoadNot) {
            gift.downloadState = TiGiftDownloadStateDownoading;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self downloadGift:gift index:[gifts indexOfObject:gift] withAnimation:^(NSInteger index) {
                    
                    animating([gifts indexOfObject:gift]);
                    
                }           successed:^(TiGift *gift, NSInteger index) {
                    success(gift, index);
                    
                }              failed:^(TiGift *gift, NSInteger index) {
                    failed(gift, index);
                    
                }];
                
            });
        }
    }
}


@end

