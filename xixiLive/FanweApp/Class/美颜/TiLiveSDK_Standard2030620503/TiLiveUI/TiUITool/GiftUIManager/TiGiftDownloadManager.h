//
//  TiGiftDownLoadManager.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TiGift;

@interface TiGiftDownloadManager : NSObject

+ (instancetype)sharedInstance;

/**
 Download a single gift
 
 @param gift the gift to download
 @param index the index of the gift in the array
 @param animating the animation when downloading
 @param success download successed callback
 @param failed download failed callback
 */

- (void)downloadGift:(TiGift *)gift index:(NSInteger)index withAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiGift *gift, NSInteger index))success failed:(void (^)(TiGift *gift, NSInteger index))failed;

/**
 Download all unsaved gifts
 
 @param gifts the array of all gifts
 @param animating the animation when downloading
 @param success the gift download successed
 @param failed the gift download failed
 */

- (void)downloadGifts:(NSArray *)gifts withAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiGift *gift, NSInteger index))success failed:(void (^)(TiGift *gift, NSInteger index))failed;

@end

