//
//  TiGiftCell.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <UIKit/UIkit.h>

static NSString *TiGiftCellIdentifier = @"TiGiftCellIdentifier";

@class TiGift;

@interface TiGiftIndicatorView : UIView

- (id)initWithTintColor:(UIColor *)tintColor size:(CGFloat)size;

@property(nonatomic, strong) UIColor *tintColor;
@property(nonatomic) CGFloat size;

@property(nonatomic, readonly) BOOL animating;

- (void)startAnimating;

- (void)stopAnimating;

@end

@interface TiGiftCell : UICollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setGift:(TiGift *)gift index:(NSInteger)index;

- (void)hideBackView:(BOOL)hidden;

- (void)startDownload;

@end
