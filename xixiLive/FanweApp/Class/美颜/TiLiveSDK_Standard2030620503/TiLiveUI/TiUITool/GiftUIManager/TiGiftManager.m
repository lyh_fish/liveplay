//
//  TiGiftManager.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiGiftManager.h"

@implementation TiGiftManager
{
    dispatch_queue_t _ioQueue;
    NSString *_giftPath;
    NSMutableArray *_giftNames;
    NSMutableArray *gifts_json;
}

+ (instancetype)sharedManager {
    static id _giftsManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _giftsManager = [TiGiftManager new];
    });
    
    return _giftsManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _ioQueue = dispatch_queue_create("com.tillusory.gifts", DISPATCH_QUEUE_SERIAL);
        _giftPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"gift"];
        
        //To determine whether there gifts folder, if it does not exist, create a folder
        if (![[NSFileManager defaultManager] fileExistsAtPath:_giftPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:_giftPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        
    }
    return self;
}

- (void)loadGiftsWithCompletion:(void (^)(NSMutableArray<TiGift *> *gifts))completion {
    dispatch_async(_ioQueue, ^{
        if ([self getGiftsFromJson]) {
            completion(gifts_json);
        }
    });
}

//Update the download information for all gifts
- (void)updateConfigJSON {
    
    NSDictionary *oldDict =
    [NSDictionary dictionaryWithContentsOfFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"]];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *newArr = [[NSMutableArray alloc] init];
    NSArray *oldArr = [oldDict objectForKey:@"gifts"];
    for (NSDictionary *itemDict in oldArr) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        dic[@"name"] = [itemDict valueForKey:@"name"];
        dic[@"dir"] = [itemDict valueForKey:@"dir"];
        dic[@"category"] = [itemDict valueForKey:@"category"];
        dic[@"thumb"] = [itemDict valueForKey:@"thumb"];
        dic[@"voiced"] = [itemDict valueForKey:@"voiced"];
        dic[@"sourceType"] = [itemDict valueForKey:@"sourceType"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[_giftPath stringByAppendingPathComponent:[itemDict valueForKey:@"dir"]]]) {
            
            dic[@"downloaded"] = [NSNumber numberWithBool:YES];
        } else {
            
            dic[@"downloaded"] = [NSNumber numberWithBool:NO];
            
        }
        
        [newArr addObject:dic];
        
    }
    
    newDict[@"gifts"] = newArr;
    
    [newDict writeToFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"] atomically:NO];
    
}

//Update the download information for all gifts
- (NSMutableDictionary *)updateConfigJSONForDic {
    
    NSDictionary *oldDict =
    [NSDictionary dictionaryWithContentsOfFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"]];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *newArr = [[NSMutableArray alloc] init];
    NSArray *oldArr = [oldDict objectForKey:@"gifts"];
    for (NSDictionary *itemDict in oldArr) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        dic[@"name"] = [itemDict valueForKey:@"name"];
        dic[@"dir"] = [itemDict valueForKey:@"dir"];
        dic[@"category"] = [itemDict valueForKey:@"category"];
        dic[@"thumb"] = [itemDict valueForKey:@"thumb"];
        dic[@"voiced"] = [itemDict valueForKey:@"voiced"];
        dic[@"sourceType"] = [itemDict valueForKey:@"sourceType"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[_giftPath stringByAppendingPathComponent:[itemDict valueForKey:@"dir"]]]) {
            
            dic[@"downloaded"] = [NSNumber numberWithBool:YES];
        } else {
            
            dic[@"downloaded"] = [NSNumber numberWithBool:NO];
            
        }
        
        [newArr addObject:dic];
        
    }
    
    newDict[@"gifts"] = newArr;
    
    [newDict writeToFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"] atomically:NO];
    
    return newDict;
}

- (BOOL)getGiftsFromJson {
    BOOL isLoadSuccess = NO;
    
    // Read the config file in the resource directory
    NSString *configPath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"gifts.json"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:configPath isDirectory:NULL]) {
        NSLog(@"The general configuration file for the gift in the resource directory does not exist");
        return isLoadSuccess;
    }
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:configPath];
    NSDictionary *oldDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (error || !oldDict) {
        NSLog(@"Resource directory under the general configuration file to read the gifts failed:%@", error);
        return isLoadSuccess;
    }
    
    //Copy the config file in the resource directory to the gifts folder in the document directory
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[_giftPath stringByAppendingPathComponent:@"gifts.json"] isDirectory:NULL]) {
        [oldDict writeToFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"] atomically:NO];
    }
    
    //拷贝本地礼物到沙盒
    NSString *localPath =
    [[[NSBundle mainBundle] pathForResource:@"TiSDKResource" ofType:@"bundle"] stringByAppendingPathComponent:@"gift"];
    
    NSArray *dirArr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:localPath error:NULL];
    for (NSString *giftName in dirArr) {
        
        if (self.isLoadGiftsFromServer) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:[_giftPath stringByAppendingPathComponent:giftName]]) {
                
                [[NSFileManager defaultManager] copyItemAtPath:[localPath stringByAppendingPathComponent:giftName] toPath:[_giftPath stringByAppendingPathComponent:giftName] error:NULL];
                
            }
        } else {
            [[NSFileManager defaultManager] copyItemAtPath:[localPath stringByAppendingPathComponent:giftName] toPath:[_giftPath stringByAppendingPathComponent:giftName] error:NULL];
        }
        
    }
    
    //Update the gift download information first
    [self updateConfigJSON];
    
    NSDictionary *newDict =
    [NSDictionary dictionaryWithContentsOfFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"]];
    
    NSArray *newArr = [newDict objectForKey:@"gifts"];
    
    gifts_json = [NSMutableArray arrayWithCapacity:newArr.count];
    
    
    //删除gifts.json中不存在的贴纸
    NSMutableArray *giftNames = [NSMutableArray arrayWithCapacity:newArr.count];
    
    for (NSDictionary *itemDict in newArr) {
        [giftNames addObject:[itemDict valueForKey:@"name"]];
    }
    [giftNames addObject:@"gifts.json"];
    
    //    NSArray *existsFile = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_giftPath error:NULL];
    
    //    for (NSString *giftName in existsFile) {
    //        if (![giftNames containsObject:giftName]) {
    //            NSLog(@"delete:%@",giftName);
    //            [[NSFileManager defaultManager] removeItemAtPath:[_giftPath stringByAppendingPathComponent:giftName] error:NULL];
    //        }
    //    }
    //
    
    //遍历json返回gift数组
    for (NSDictionary *itemDict in newArr) {
        
        NSString *dir = [NSString stringWithFormat:@"%@/%@/", _giftPath, [itemDict valueForKey:@"dir"]];
        NSURL *url = [NSURL fileURLWithPath:dir];
        
        TiGift *gift =
        [[TiGift alloc] initWithName:[itemDict valueForKey:@"name"] thumbName:[itemDict valueForKey:@"thumb"] download:[[itemDict valueForKey:@"downloaded"] boolValue] DirectoryURL:url];
        if ([[itemDict valueForKey:@"sourceType"] intValue] == 0) {
            gift.sourceType = TiGiftSourceTypeFromTi;
        } else {
            gift.sourceType = TiGiftSourceTypeFromLocal;
        }
        
        if (gift) {
            [gifts_json addObject:gift];
        }
    }
    
    isLoadSuccess = YES;
    return isLoadSuccess;
}

/**
 Update the local gifts from the server
 */
- (void)updateGiftsJSONWithCompletion:(void (^)(BOOL isSuccess, NSMutableDictionary *dic))completion serverJson:(NSArray *)serverJson {
    BOOL isSuccess = NO;
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    
    [newDict setObject:serverJson forKey:@"gifts"];
    
    isSuccess = [newDict writeToFile:[_giftPath stringByAppendingPathComponent:@"gifts.json"] atomically:YES];
    
    newDict = [self updateConfigJSONForDic];
    
    completion(isSuccess, newDict);
}

- (NSString *)getGiftPath {
    return _giftPath;
}

@end

