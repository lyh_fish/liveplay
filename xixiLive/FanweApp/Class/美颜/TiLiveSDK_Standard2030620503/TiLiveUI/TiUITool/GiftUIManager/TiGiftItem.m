//
//  TiGiftItem.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiGiftItem.h"

@interface TiGiftItem ()

@property(nonatomic, strong) UIImage *currentFrame;

/**
 counter
 */

@property(nonatomic) NSTimeInterval interval;

@end

@implementation TiGiftItem
{
    NSArray *_imageFileURLs;
    //    NSMutableDictionary *_imagesDict;
    
    GLuint *_textureArr;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.type = [[dict objectForKey:@"type"] intValue];
        self.trigerType = [[dict objectForKey:@"trigerType"] intValue];
        
        self.itemDir = [dict objectForKey:@"frameFolder"];
        self.count = [[dict objectForKey:@"frameNum"] intValue];
        self.duration = [[dict objectForKey:@"frameDuration"] doubleValue] / 1000.;
        self.width = [[dict objectForKey:@"frameWidth"] floatValue];
        self.height = [[dict objectForKey:@"frameHeight"] floatValue];

        self.alignPos = [[dict objectForKey:@"alignPos"] intValue];
        
        self.scaleWidthOffset = [[dict objectForKey:@"scaleWidthOffset"] floatValue];
        self.scaleHeightOffset = [[dict objectForKey:@"scaleHeightOffset"] floatValue];
        self.scaleXOffset = [[dict objectForKey:@"scaleXOffset"] floatValue];
        self.scaleYOffset = [[dict objectForKey:@"scaleYOffset"] floatValue];
        
        self.accumulator = 0.;
        self.currentFrameIndex = 0;
        self.loopCountdown = NSIntegerMax;
        //        selfalloc.interval = self.duration / self.count;
    }
    
    return self;
}

- (UIImage *)currentFrame {
    return [self _imageAtIndex:self.currentFrameIndex];
}

- (UIImage *)nextImageForInterval:(NSTimeInterval)interval {
    if (self.loopCountdown == 0) {
        return self.currentFrame;
    }
    
    self.currentFrameIndex = [self _nextFrameIndexForInterval:interval];
    
    return [self _imageAtIndex:self.currentFrameIndex];
}

- (NSUInteger)_nextFrameIndexForInterval:(NSTimeInterval)interval {
    
    
    // This is where FLAnimatedImage loads the GIF
    
    NSUInteger nextFrameIndex = self.currentFrameIndex;
    self.accumulator += interval;
    
    //    if (self.isLastItem && self.loopCountdown == 0) {
    //
    //        if (interval > (self.count -1) * self.duration) {
    //            if (self.giftItemPlayOver) {
    //                self.giftItemPlayOver();
    //            }
    //        }
    //        //                    NSLog(@"self.isLastItem:%d",self.isLastItem);
    //    }
    
    
    while (self.accumulator > self.duration) {
        self.accumulator -= self.duration;
        nextFrameIndex++;
        if (nextFrameIndex >= self.count) {
            // If we've looped the number of times that this animated image describes, stop looping.
            self.loopCountdown--;
            
            if (self.loopCountdown == 0) {
                if (self.isLastItem && self.giftItemPlayOver) {
                    self.giftItemPlayOver();
                }
                nextFrameIndex = self.count - 1;
                //                self.accumulator = 0;
                //                nextFrameIndex = 0;
                break;
            } else {
                nextFrameIndex = 0;
            }
        }
    }
    
    return nextFrameIndex;
}

- (UIImage *)_imageAtIndex:(NSUInteger)index {
    if (_imageFileURLs.count <= 0) {
        [self _loadImages];
    }
    
    return [UIImage imageWithContentsOfFile:[[_imageFileURLs objectAtIndex:index] path]];
}

- (void)_loadImages {
    NSURL *diskCacheURL = [NSURL fileURLWithPath:self.itemDir isDirectory:YES];
    NSArray *resourceKeys = @[NSURLIsDirectoryKey, NSURLContentModificationDateKey, NSURLNameKey];
    
    // First get the cache file related properties
    NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtURL:diskCacheURL
                                                                 includingPropertiesForKeys:resourceKeys
                                                                                    options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                               errorHandler:^BOOL(NSURL *_Nonnull url, NSError *_Nonnull error) {
                                                                                   NSLog(@"error: %@", error);
                                                                                   return NO;
                                                                               }];
    
    NSMutableDictionary *imageFiles = [NSMutableDictionary dictionary];
    
    //Traverse all the files in the directory
    for (NSURL *fileURL in fileEnumerator) {
        NSDictionary *resourceValues = [fileURL resourceValuesForKeys:resourceKeys error:NULL];
        if ([resourceValues[NSURLIsDirectoryKey] boolValue]) {
            continue;
        }
        
        [imageFiles setObject:resourceValues forKey:fileURL];
    }
    
    _imageFileURLs = [imageFiles keysSortedByValueWithOptions:NSSortConcurrent
                                              usingComparator:^NSComparisonResult(id obj1, id obj2) {
                                                  return [obj1[NSURLNameKey] compare:obj2[NSURLNameKey] options:NSNumericSearch];
                                              }];
}

//- (GLuint)_textureWithImage:(UIImage *)image {
//    CGImageRef imageRef = image.CGImage;
//    NSAssert(imageRef, @"Failed to load image.");
//
//    size_t width = CGImageGetWidth(imageRef);
//    size_t height = CGImageGetHeight(imageRef);
//
//    GLubyte *imageData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
//
//    CGContextRef spriteContext =
//            CGBitmapContextCreate(imageData, width, height, 8, width * 4, CGImageGetColorSpace(imageRef), kCGImageAlphaPremultipliedLast);
//    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), imageRef);
//    CGContextRelease(spriteContext);
//
//    GLuint texture;
//    glGenTextures(1, &texture);
//    glBindTexture(GL_TEXTURE_2D, texture);
//
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
//
//    glBindTexture(GL_TEXTURE_2D, 0);
//    free(imageData);
//
//    return texture;
//}

//- (GLuint)_textureAtIndex:(NSUInteger)index {
//    if (_textureArr == NULL) {
//        _textureArr = malloc(sizeof(GLuint) * self.count);
//        if (_textureArr == NULL) {
//            // 分配内存失败
//            return 0;
//        }
//
//        for (int i = 0; i < self.count; i++) {
//            _textureArr[i] = 0;
//        }
//    }
//
//    GLuint texture = _textureArr[index];
//    if (texture == 0) {
//        texture = [self _textureWithImage:[self _imageAtIndex:index]];
//        _textureArr[index] = texture;
//    }
//
//    return texture;
//}

//- (GLuint)nextTextureForInterval:(NSTimeInterval)interval {
//    self.currentFrameIndex = [self _nextFrameIndexForInterval:interval];
//
//    return [self _textureAtIndex:self.currentFrameIndex];
//}

//- (void)deleteTextures {
//    if (_textureArr) {
//        glDeleteTextures((GLsizei) self.count, _textureArr);
//        free(_textureArr);
//        _textureArr = NULL;
//    }
//}

//- (void)dealloc {
//    [self deleteTextures];
//}


@end

@implementation TiGift

- (instancetype)initWithName:(NSString *)name thumbName:(NSString *)thumb download:(BOOL)download DirectoryURL:(NSURL *)dirurl {
    if (self = [super init]) {
        if (self.playGiftCount == 0) {
            self.playGiftCount = NSIntegerMax;
        }
        
        _giftName = name;
        _giftIcon = thumb;
        _isDownload = download;
        _downloadState = TiGiftDownloadStateDownoadNot;
        _giftDir = nil;
        _giftSound = nil;
        _items = nil;
        //Unloaded items do not initialize items, giftDir, giftSound, and so on
        if (download == YES) {
            NSString *dir = dirurl.path;
            _giftDir = dir;
            _downloadState = TiGiftDownloadStateDownoadDone;
            NSString *configFile = [dir stringByAppendingPathComponent:@"config.json"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:configFile isDirectory:NULL]) {
                [TiGift resetGift:self];
                return self;
            }
            
            NSError *error = nil;
            NSData *data = [NSData dataWithContentsOfFile:configFile];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if ([dict objectForKey:@"soundName"]) {
                _giftSound = dict[@"soundName"];
            }
            if (error || !dict) {
                _isDownload = NO;
                return self;
            }
            
            NSArray *itemsDict = [dict objectForKey:@"itemList"];
            NSMutableArray *items = [NSMutableArray arrayWithCapacity:itemsDict.count];
            
            NSInteger itemsFrameNum = 0;
            TiGiftItem *itemCopy;
            for (NSDictionary *itemDict in itemsDict) {
                TiGiftItem *item = [[TiGiftItem alloc] initWithJSONDictionary:itemDict];
                
                if (item.count >= itemsFrameNum) {
                    itemsFrameNum = item.count;
                    itemCopy = item;
                }
                item.itemDir = [_giftDir stringByAppendingPathComponent:item.itemDir];
                item.loopCountdown = self.playGiftCount;
                NSArray *dirArr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:item.itemDir error:NULL];
                NSInteger fileCount = dirArr.count;
                
                for (NSString *fileName in dirArr) {
                    if (![fileName hasSuffix:@".png"]) {
                        fileCount--;
                    }
                }
                
                //Check the gift file for missing gifts
//                if (fileCount != [[itemDict objectForKey:@"frameNum"] intValue]) {
//                    [TiGift resetGift:self];
//                    //                    break;
//                    return self;
//                }
                
                [items addObject:item];
            }
            itemCopy.isLastItem = YES;
            
            _items = items;
            
        }
    }
    return self;
}

- (void)setSourceType:(TiGiftSourceType)sourceType {
    _sourceType = sourceType;
    if (sourceType == TiGiftSourceTypeFromTi) {
        // if you want to use Ti's downloadURL, you donot need to do anything
        _downloadURL = nil;
    } else if (sourceType == TiGiftSourceTypeFromLocal) {
        
        // you can set your own downURL here
        _downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://在这拼接下载的URL%@.zip", _giftName]];
    }
    
}

+ (void)updateGiftAfterDownload:(TiGift *)gift DirectoryURL:(NSURL *)dirurl sucess:(void (^)(TiGift *))sucessed fail:(void (^)(TiGift *))failed {
    if (gift.playGiftCount == 0) {
        gift.playGiftCount = NSIntegerMax;
    }
    
    gift.isDownload = YES;
    NSString *dir = dirurl.path;
    gift.giftDir = dir;
    NSString *configFile = [dir stringByAppendingPathComponent:@"config.json"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:configFile isDirectory:NULL]) {
        [self resetGift:gift];
        failed(gift);
    }
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:configFile];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if ([dict objectForKey:@"soundName"]) {
        gift.giftSound = dict[@"soundName"];
    }
    if (error || !dict) {
        [self resetGift:gift];
        failed(gift);
    }
    
    NSArray *itemsDict = [dict objectForKey:@"itemList"];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:itemsDict.count];
    
    NSInteger itemsFrameNum = 0;
    TiGiftItem *itemCopy;
    for (NSDictionary *itemDict in itemsDict) {
        TiGiftItem *item = [[TiGiftItem alloc] initWithJSONDictionary:itemDict];
        if (item.count >= itemsFrameNum) {
            itemsFrameNum = item.count;
            itemCopy = item;
        }
        item.itemDir = [gift.giftDir stringByAppendingPathComponent:item.itemDir];
        item.loopCountdown = gift.playGiftCount;
        NSArray *dirArr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:item.itemDir error:NULL];
        NSInteger fileCount = dirArr.count;
        
        for (NSString *fileName in dirArr) {
            if (![fileName hasSuffix:@".png"]) {
                fileCount--;
            }
        }
        
        //Check the gift file for missing gifts
//        if (fileCount != [[itemDict objectForKey:@"frameNum"] intValue]) {
//
//            [self resetGift:gift];
//            failed(gift);
//
//            break;
//        }
        
        [items addObject:item];
    }
    itemCopy.isLastItem = YES;
    gift.items = items;
    
    sucessed(gift);
}

- (void)setPlayCount:(NSUInteger)count {
    self.playGiftCount = count;
    for (TiGiftItem *item in _items) {
        item.loopCountdown = count;
        if (count == 0) {
            item.accumulator = 0;
            item.currentFrameIndex = 0;
        }
    }
}

+ (void)resetGift:(TiGift *)gift {
    [[NSFileManager defaultManager] removeItemAtPath:gift.giftDir error:NULL];
    gift.giftDir = nil;
    gift.downloadState = TiGiftDownloadStateDownoadNot;
    gift.isDownload = NO;
    gift.giftSound = nil;
    gift.items = nil;
    
}

//- (void)dealloc {
//    for (TiGiftItem *item in _items) {
//        [item deleteTextures];
//    }
//    _items = nil;
//}

@end


