//
//  TiGiftManager.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TiGiftItem.h"

/**
 Gift operation management class
 */
@interface TiGiftManager : NSObject

+ (instancetype)sharedManager;

/**
 Whether through the network load gifts
 */
@property(nonatomic, assign) BOOL isLoadGiftsFromServer;

/**
 Asynchronous mode reads all the gift information from the file
 
 @param completion Read the callback after completion
 */
- (void)loadGiftsWithCompletion:(void (^)(NSMutableArray<TiGift *> *gifts))completion;

/*
 * Get the gift path
 */
- (NSString *)getGiftPath;

/**
 Update GiftConfig's gift download status
 */
- (void)updateConfigJSON;

/**
 Update the Json file and get new Json
 */
- (NSMutableDictionary *)updateConfigJSONForDic;

/**
 Update the json file of local gifts from the server
 completion:After the completion of the block of callback  {【isSuccess：Whether the update is successful】，【dic：The updated new json】}
 serverJson:The new gifts array from the server
 */
- (void)updateGiftsJSONWithCompletion:(void (^)(BOOL isSuccess, NSMutableDictionary *dic))completion serverJson:(NSArray *)serverJson;


@end
