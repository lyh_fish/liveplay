//
//  TiGiftItem.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/5.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <UIKit/UIkit.h>

typedef enum
{
    TiGiftItemTypeFullScreen,  // full-screen display
    TiGiftItemTypeFace,        // Face
    TiGiftItemTypeEdge         // edge
} TiGiftItemType;

typedef enum
{
    TiGiftAlignPositionTop,
    TiGiftAlignPositionLeft,
    TiGiftAlignPositionBottom,
    TiGiftAlignPositionRight,
} TiGiftAlignPosition;

typedef NS_ENUM(NSInteger, TiGiftDownloadState)
{
    TiGiftDownloadStateDownoadNot,//Not downloaded
    TiGiftDownloadStateDownoadDone,//Downloaded
    TiGiftDownloadStateDownoading,//downloading
};

typedef NS_ENUM(NSInteger, TiGiftSourceType)
{
    TiGiftSourceTypeFromTi,//0  Ti's downloadURL
    TiGiftSourceTypeFromLocal,//1 your own downloadURL
};

/**
 A set of gifts, all the information of a component (such as a nose).
 */
@interface TiGiftItem : NSObject

typedef void(^GiftItemPlayOver)(void);

@property(nonatomic, copy) GiftItemPlayOver giftItemPlayOver;

@property(nonatomic, assign) BOOL isLastItem;

@property(nonatomic) NSTimeInterval accumulator;

/**
 The type of location to display
 */
@property(nonatomic) TiGiftItemType type;

/**
 Trigger condition, default 0, always displayed
 */
@property(nonatomic) int trigerType;

/**
 The directory of resources (including a set of image sequence frames)
 */
@property(nonatomic, copy) NSString *itemDir;

/**
 Frames (a sequence of frames that make up an animation effect)
 */
@property(nonatomic) NSInteger count;

/**
 Interval of each frame, in seconds
 */
@property(nonatomic) NSTimeInterval duration;

/**
 The width of the image
 */
@property(nonatomic) float width;

/**
 The picture is high
 */
@property(nonatomic) float height;

/**
 The edge item parameter
 Edge position（top、bottom、left、right）
 */
@property(nonatomic) TiGiftAlignPosition alignPos;

/**
 Width of the zoom factor (for the face of the item to eye distance as a reference; for the edge of the item is the screen width and height as a reference, the same below)
 */
@property(nonatomic) float scaleWidthOffset;

/**
 Height scaling factor
 */
@property(nonatomic) float scaleHeightOffset;

/**
 Horizontal offset coefficient
 */
@property(nonatomic) float scaleXOffset;

/**
 Vertical offset coefficient
 */
@property(nonatomic) float scaleYOffset;

/**
 The index of the current frame
 */
@property(nonatomic) NSUInteger currentFrameIndex;

/**
 Number of remaining cycles
 */
@property(nonatomic) NSUInteger loopCountdown;

/**
 According to the current frame position and time interval to obtain the next frame picture, in order to adapt to different frame rates of video streaming, to ensure the effect of animation.
   The first time to use a picture from a file loaded, and cache, the next directly from the memory access
 
 @param interval The interval of each frame of the video stream
 @return This picture can be added to the current video frame
 */
- (UIImage *)nextImageForInterval:(NSTimeInterval)interval;

/**
 Similar to the -nextImageForInterval: method, but get a texture for OpenGL use
 
 @param interval The interval of each frame of the video stream
 @return textures
 */
- (GLuint)nextTextureForInterval:(NSTimeInterval)interval;

/**
 Deletes all loaded textures
 */
- (void)deleteTextures;

@end

/**
 A gift class
 */
@interface TiGift : NSObject

typedef void(^GiftPlayOver)(void);

@property(nonatomic, copy) GiftPlayOver giftPlayOver;

/**
 Contains all components of a gift resource
 */
@property(nonatomic, strong) NSArray<TiGiftItem *> *items;

/**
 Directory of a gift
 */
@property(nonatomic, copy) NSString *giftDir;

/**
 name of a gift
 */
@property(nonatomic, readonly) NSString *giftName;

/**
 The name of the file for the thumbnail of the gift
 */
@property(nonatomic, readonly) NSString *giftIcon;

/**
 The name of the file for the sound of the gift
 */
@property(nonatomic, copy) NSString *giftSound;

/**
 Gifts are downloaded
 */
@property(nonatomic, assign) BOOL isDownload;

@property(nonatomic, assign) NSUInteger playGiftCount;

/**
 The download status of the gift
 */
@property(nonatomic, assign) TiGiftDownloadState downloadState;

/**
 The download source type of the gift
 */
@property(nonatomic, assign) TiGiftSourceType sourceType;

/**
 The download URL of the gift
 */
@property(nonatomic, strong) NSURL *downloadURL;

/**
 Initialize a gift
 
 @param name the gift's name
 @param download the download status of the gift in the configuration file
 @param dirurl the path of gift
 @return a gift object
 */
- (instancetype)initWithName:(NSString *)name thumbName:(NSString *)thumb download:(BOOL)download DirectoryURL:(NSURL *)dirurl;

/**
 Update a gift
 
 @param gift the gift to update
 @param dirurl the path of gift
 @param sucessed update sucessed callback
 @param failed update failed callback
 */
+ (void)updateGiftAfterDownload:(TiGift *)gift DirectoryURL:(NSURL *)dirurl sucess:(void (^)(TiGift *))sucessed fail:(void (^)(TiGift *))failed;

- (void)setPlayCount:(NSUInteger)count;

@end
