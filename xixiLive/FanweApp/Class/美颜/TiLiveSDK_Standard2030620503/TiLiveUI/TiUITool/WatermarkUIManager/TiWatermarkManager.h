//
//  TiWatermarkManager.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <UIkit/UIkit.h>
#import "TiWatermark.h"

@interface TiWatermarkManager : NSObject

+ (TiWatermarkManager *)init;

- (void)loadWatermarkWithCompletion:(void (^)(NSMutableArray<TiWatermark *> *watermarks))completion;

- (void)downloadWatermark:(TiWatermark *)watermark Index:(NSInteger)index WithAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiWatermark *watermark, NSInteger index))success failed:(void (^)(TiWatermark *watermark, NSInteger index))failed;

- (NSString *)getSandboxPath;

@end


