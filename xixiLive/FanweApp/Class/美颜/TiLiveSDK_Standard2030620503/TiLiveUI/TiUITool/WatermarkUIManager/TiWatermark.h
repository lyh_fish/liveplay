//
//  TiWatermark.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <UIKit/UIkit.h>

typedef NS_ENUM(NSInteger, TiWatermarkDownloadState)
{
    TiWatermarkDownloadStateDownoadNot,//Not downloaded
    TiWatermarkDownloadStateDownoadDone,//Downloaded
    TiWatermarkDownloadStateDownoading,//downloading
};

@interface TiWatermark : NSObject

@property (nonatomic, strong) NSNumber *xLocation;
@property (nonatomic, strong) NSNumber *yLocation;
@property (nonatomic, strong) NSNumber *ratio;
@property (nonatomic, strong) NSString *watermarkNameString;
@property (nonatomic, strong) NSString *watermarkIconString;
@property (nonatomic, assign) BOOL downloaded;

@property (nonatomic, strong) NSURL *watermarkPictureURL;
@property (nonatomic, strong) NSURL *watermarkIconURL;
@property (nonatomic, assign) TiWatermarkDownloadState watermarkDownloadState;

- (instancetype)initWithJson:(NSDictionary *)jason DownloadState:(TiWatermarkDownloadState)state;

@end
