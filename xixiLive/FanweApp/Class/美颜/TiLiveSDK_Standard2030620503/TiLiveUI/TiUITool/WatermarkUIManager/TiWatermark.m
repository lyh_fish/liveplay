//
//  TiWatermark.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiWatermark.h"
#import "TiConst.h"
#import "TiSDKInterface.h"

@interface TiWatermark ()

@end

@implementation TiWatermark

- (instancetype)initWithJson:(NSDictionary *)jason DownloadState:(TiWatermarkDownloadState)state {
    if (self = [super init]) {
        self.xLocation = jason[@"x"];
        self.yLocation = jason[@"y"];
        self.ratio = jason[@"ratio"];
        self.watermarkNameString = jason[@"name"];
        self.watermarkIconString = jason[@"thumb"];
        NSNumber *downloadedNumber = jason[@"downloaded"];
        self.downloaded = downloadedNumber.boolValue;
        
        self.watermarkPictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [TiSDK getWatermarkURL], jason[@"name"]]];
        self.watermarkIconURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [TiSDK getWatermarkIconURL], jason[@"thumb"]]];
        self.watermarkDownloadState = state;
    }
    
    return self;
}

@end
