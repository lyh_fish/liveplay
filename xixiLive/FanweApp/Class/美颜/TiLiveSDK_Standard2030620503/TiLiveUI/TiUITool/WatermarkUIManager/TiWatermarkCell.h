//
//  TiWatermarkCell.h
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import <UIKit/UIkit.h>
#import "TiWatermark.h"

static NSString *TiWatermarkCellIdentifier = @"TiWatermarkCellIdentifier";

@class TiWatermark;

@interface TiWatermarkIndicatorView : UIView

@property(nonatomic, strong) UIColor *tintColor;
@property(nonatomic) CGFloat size;
@property(nonatomic, readonly) BOOL animating;

- (id)initWithTintColor:(UIColor *)tintColor size:(CGFloat)size;
- (void)startAnimating;
- (void)stopAnimating;

@end

@interface TiWatermarkCell : UICollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setWatermark:(TiWatermark *)watermark index:(NSInteger)index;

- (void)hideBackView:(BOOL)hidden;

- (void)startDownload;

@end
