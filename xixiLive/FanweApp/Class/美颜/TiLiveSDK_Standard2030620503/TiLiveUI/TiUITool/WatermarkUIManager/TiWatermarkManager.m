//
//  TiWatermarkManager.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiWatermarkManager.h"
#import "UIImageView+WebCache.h"

@interface TiWatermarkManager ()

@end

@implementation TiWatermarkManager {
    dispatch_queue_t _ioQueue;
    NSString *watermarkPath;
    NSMutableArray *watermarkArray;
}

static TiWatermarkManager *watermarkManager = nil;

+ (TiWatermarkManager *)init {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (watermarkManager == nil) {
            watermarkManager = [[self alloc] init];
        }
    });
    
    return watermarkManager;
}

+ (id)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (watermarkManager == nil) {
            watermarkManager = [super allocWithZone:zone];
        }
    });
    
    return watermarkManager;
}

- (void)loadWatermarkWithCompletion:(void (^)(NSMutableArray<TiWatermark *> *watermarks))completion {
    watermarkPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"watermark"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:watermarkPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:watermarkPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    _ioQueue = dispatch_queue_create("com.tillusory.watermarks", DISPATCH_QUEUE_SERIAL);
    dispatch_async(_ioQueue, ^{
        if ([self loadWatermark]) {
            completion(self->watermarkArray);
        }
    });
}

- (BOOL)loadWatermark {
    BOOL isLoadSuccess = FALSE;
    
    
    NSString *watermarksJsonPath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"watermarks.json"];
    
    // watermarks.json文件是否存在
    if (![[NSFileManager defaultManager] fileExistsAtPath:watermarksJsonPath isDirectory:NULL]) {
        NSLog(@"The general configuration file for the watermark in the resource directory does not exist");
        return isLoadSuccess;
    }
    
    // watermarks.json文件读取是否成功
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:watermarksJsonPath];
    NSDictionary *srcDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (error || !srcDict) {
        NSLog(@"Resource directory under the general configuration file to read the watermarks failed:%@", error);
        return isLoadSuccess;
    }
    
    // 写watermarks.json到沙盒中
    if (![[NSFileManager defaultManager] fileExistsAtPath:[watermarkPath stringByAppendingPathComponent:@"watermarks.json"] isDirectory:NULL]) {
        [srcDict writeToFile:[watermarkPath stringByAppendingPathComponent:@"watermarks.json"] atomically:NO];
    }

    // 拷贝本地水印文件到沙盒
    NSString *localPath =
    [[[NSBundle mainBundle] pathForResource:@"TiResource" ofType:@"bundle"] stringByAppendingPathComponent:@"watermark"];
    NSArray *dirArr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:localPath error:NULL];
    for (NSString *watermarkName in dirArr) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:[watermarkPath stringByAppendingPathComponent:watermarkName]]) {
            [[NSFileManager defaultManager] copyItemAtPath:[localPath stringByAppendingPathComponent:watermarkName] toPath:[watermarkPath stringByAppendingPathComponent:watermarkName] error:NULL];
        }
    }
    
    // 更新沙盒中watermarks.json文件
    [self updateConfigJson];
    
    //
    NSDictionary *newDict =
    [NSDictionary dictionaryWithContentsOfFile:[watermarkPath stringByAppendingPathComponent:@"watermarks.json"]];
    NSArray *newArr = [newDict objectForKey:@"watermarks"];
    watermarkArray = [NSMutableArray arrayWithCapacity:newArr.count];
    for (NSDictionary *itemDict in newArr) {
        TiWatermarkDownloadState downloadState = TiWatermarkDownloadStateDownoadNot;
        if ([[itemDict valueForKey:@"downloaded"] boolValue]) {
            downloadState = TiWatermarkDownloadStateDownoadDone;
        }
        
        TiWatermark *watermark = [[TiWatermark alloc] initWithJson:itemDict DownloadState:downloadState];
        if (watermark) {
            [watermarkArray addObject:watermark];
        }
    }
    
    isLoadSuccess = YES;
    return isLoadSuccess;
}

- (void)updateConfigJson {
    NSDictionary *oldDict = [NSDictionary dictionaryWithContentsOfFile:[watermarkPath stringByAppendingPathComponent:@"watermarks.json"]];
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *newArr = [[NSMutableArray alloc] init];
    NSArray *oldArr = [oldDict objectForKey:@"watermarks"];
    for (NSDictionary *itemDict in oldArr) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"x"] = [itemDict valueForKey:@"x"];
        dic[@"y"] = [itemDict valueForKey:@"y"];
        dic[@"ratio"] = [itemDict valueForKey:@"ratio"];
        dic[@"name"] = [itemDict valueForKey:@"name"];
        dic[@"thumb"] = [itemDict valueForKey:@"thumb"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[watermarkPath stringByAppendingPathComponent:[itemDict valueForKey:@"name"]]]) {
            dic[@"downloaded"] = [NSNumber numberWithBool:YES];
        } else {
            dic[@"downloaded"] = [NSNumber numberWithBool:NO];
        }
        [newArr addObject:dic];
    }

    newDict[@"watermarks"] = newArr;
    [newDict writeToFile:[watermarkPath stringByAppendingPathComponent:@"watermarks.json"] atomically:NO];
}

- (void)downloadWatermark:(TiWatermark *)watermark Index:(NSInteger)index WithAnimation:(void (^)(NSInteger index))animating successed:(void (^)(TiWatermark *watermark, NSInteger index))success failed:(void (^)(TiWatermark *watermark, NSInteger index))failed {
    animating(index);
    NSURL *imageURL = watermark.watermarkPictureURL;
    SDWebImageManager *manager = [SDWebImageManager sharedManager] ;
    [manager loadImageWithURL:imageURL options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        if (image) {
            NSString *imageFilePath = [self->watermarkPath stringByAppendingPathComponent:watermark.watermarkNameString];
            [UIImagePNGRepresentation(image) writeToFile:imageFilePath  atomically:YES];
            success(watermark, index);
        } else {
            failed(watermark, index);
        }
    }];
}

- (NSString *)getSandboxPath {
    return watermarkPath;
}

@end
