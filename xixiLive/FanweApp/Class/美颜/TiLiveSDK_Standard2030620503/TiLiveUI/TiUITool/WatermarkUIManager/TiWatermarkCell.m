//
//  TiWatermarkCell.m
//  TiFancy
//
//  Created by Husky Cooper on 2018/11/6.
//  Copyright © 2018 Tillusory Tech. All rights reserved.
//

#import "TiWatermarkCell.h"
#import "TiConst.h"
#import "UIImageView+WebCache.h"
#import "TiSDKInterface.h"

static NSString *Ti_WATERMARK_ICON_DOWNLOADING = @"ic_download";
static NSString *Ti_WATERMARK_ICON_SELECTED = @"ic_border_selected";
static NSString *Ti_WATERMARK_ICON_NONE = @"ic_none";
static NSString *Ti_WATERMARK_ICON_DOWNLOADALL = @"download.png";

@interface TiWatermarkIndicatorView ()
{
    CALayer *_animationLayer;
}

@end

@implementation TiWatermarkIndicatorView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        //        _tintColor = [UIColor whiteColor];
        _tintColor = TiRGBA(18, 150, 210, 255);
        _size = 25.0f;
        [self commonInit];
    }
    return self;
}

- (id)initWithTintColor:(UIColor *)tintColor size:(CGFloat)size {
    self = [super init];
    if (self) {
        _size = size;
        _tintColor = tintColor;
        [self commonInit];
    }
    return self;
    
}

- (void)commonInit {
    self.userInteractionEnabled = NO;
    self.hidden = YES;
    
    _animationLayer = [[CALayer alloc] init];
    [self.layer addSublayer:_animationLayer];
    
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

- (void)setupAnimation {
    _animationLayer.sublayers = nil;
    
    [self setupAnimationInLayer:_animationLayer withSize:CGSizeMake(_size, _size) tintColor:_tintColor];
    _animationLayer.speed = 0.0f;
    
}

- (void)setupAnimationInLayer:(CALayer *)layer withSize:(CGSize)size tintColor:(UIColor *)tintColor {
    CGFloat duration = 0.6f;
    
    // Rotate animation
    CAKeyframeAnimation *rotateAnimation = [self createKeyframeAnimationWithKeyPath:@"transform.rotation.z"];
    
    rotateAnimation.values = @[@0, @M_PI, @(2 * M_PI)];
    rotateAnimation.duration = duration;
    rotateAnimation.repeatCount = HUGE_VALF;
    
    
    // Draw ball clip
    CAShapeLayer *circle = [CAShapeLayer layer];
    UIBezierPath *circlePath =
    [UIBezierPath bezierPathWithArcCenter:CGPointMake(size.width / 2, size.height / 2) radius:size.width / 2 startAngle:1.5 * M_PI endAngle:M_PI clockwise:true];
    circle.path = circlePath.CGPath;
    circle.lineWidth = 3;
    circle.fillColor = nil;
    circle.strokeColor = tintColor.CGColor;
    circle.frame = CGRectMake((layer.bounds.size.width - size.width) / 2, (layer.bounds.size.height - size.height) / 2, size.width, size.height);
    [circle addAnimation:rotateAnimation forKey:@"animation"];
    [layer addSublayer:circle];
}

- (CAKeyframeAnimation *)createKeyframeAnimationWithKeyPath:(NSString *)keyPath {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:keyPath];
    animation.removedOnCompletion = NO;
    return animation;
}

- (void)startAnimating {
    if (!_animationLayer.sublayers) {
        [self setupAnimation];
    }
    self.hidden = NO;
    _animationLayer.speed = 1.0f;
    _animating = YES;
}

- (void)stopAnimating {
    _animationLayer.speed = 0.0f;
    _animating = NO;
    self.hidden = YES;
}

- (void)setSize:(CGFloat)size {
    if (_size != size) {
        _size = size;
        
        [self setupAnimation];
        [self invalidateIntrinsicContentSize];
    }
}

- (void)setTintColor:(UIColor *)tintColor {
    if (![_tintColor isEqual:tintColor]) {
        _tintColor = tintColor;
        
        CGColorRef tintColorRef = tintColor.CGColor;
        for (CALayer *sublayer in _animationLayer.sublayers) {
            sublayer.backgroundColor = tintColorRef;
            
            if ([sublayer isKindOfClass:[CAShapeLayer class]]) {
                CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
                shapeLayer.strokeColor = tintColorRef;
                shapeLayer.fillColor = tintColorRef;
            }
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _animationLayer.frame = self.bounds;
    
    BOOL animating = _animating;
    
    if (animating)
        [self stopAnimating];
    
    [self setupAnimation];
    
    if (animating)
        [self startAnimating];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(_size, _size);
}

@end

@interface TiWatermarkCell ()
// 水印Icon窗口
@property (nonatomic, strong) UIImageView *imageView;
// 下载标识图片窗口
@property (nonatomic, strong) UIImageView *downloadView;
// 选中背景框窗口
@property (nonatomic, strong) UIImageView *selectedView;
// 下载指示窗口
@property(nonatomic, strong) TiWatermarkIndicatorView *watermarkIndicatorView;
// 水印信息
@property(nonatomic, strong) TiWatermark *watermark;

@end

@implementation TiWatermarkCell

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
    
}

- (UIImageView *)downloadView {
    if (!_downloadView) {
        _downloadView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:Ti_WATERMARK_ICON_DOWNLOADING]];
        [self.contentView addSubview:_downloadView];
    }
    return _downloadView;
}

- (UIImageView *)selectedView {
    if (!_selectedView) {
        _selectedView = [[UIImageView alloc] initWithFrame:self.bounds];
        [_selectedView setImage:nil];
    }
    return _selectedView;
    
}

- (TiWatermarkIndicatorView *)watermarkIndicatorView {
    if (!_watermarkIndicatorView) {
        _watermarkIndicatorView = [[TiWatermarkIndicatorView alloc] initWithTintColor:TiRGBA(18, 150, 210, 255) size:self.imageView.frame.size.width - 8];
        
        [self.contentView addSubview:_watermarkIndicatorView];
        
    }
    return _watermarkIndicatorView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundView = self.selectedView;
        self.imageView.frame = CGRectMake(6, 6, (CGRectGetWidth(self.frame)) - 10, CGRectGetWidth(self.frame) - 10);
        self.downloadView.frame = CGRectMake(self.bounds.size.width - 13, self.bounds.size.height - 13, 13, 13);
        self.watermarkIndicatorView.center = self.imageView.center;
    }
    return self;
}

- (void)setWatermark:(TiWatermark *)watermark index:(NSInteger)index {
    self.watermark = watermark;
    ///[self.imageView sd_cancelCurrentImageLoad];
    if (index == 0) {
        self.imageView.image = [UIImage imageNamed:Ti_WATERMARK_ICON_NONE];
        self.downloadView.image = nil;
        if (self.watermarkIndicatorView.animating == YES) {
            [self.watermarkIndicatorView stopAnimating];
        }
    } else {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [TiSDK getWatermarkIconURL], watermark.watermarkIconString]]];
        self.downloadView.image = [UIImage imageNamed:Ti_WATERMARK_ICON_DOWNLOADING];
        if (watermark.downloaded == YES) {
            if (self.watermarkIndicatorView.animating == YES) {
                [self.watermarkIndicatorView stopAnimating];
            }
            watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoadDone;
            self.downloadView.hidden = YES;
        } else {
            if (watermark.watermarkDownloadState == TiWatermarkDownloadStateDownoading) {
                self.downloadView.hidden = YES;
                if (self.watermarkIndicatorView.animating != YES) {
                    [self.watermarkIndicatorView startAnimating];
                }
            } else {
                if (self.watermarkIndicatorView.animating == YES) {
                    [self.watermarkIndicatorView stopAnimating];
                }
                watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoadNot;
                self.downloadView.hidden = NO;
            }
        }
    }
    
}

- (void)startDownload {
    self.watermark.watermarkDownloadState = TiWatermarkDownloadStateDownoading;
    self.downloadView.hidden = YES;
    [self.watermarkIndicatorView startAnimating];
    
}

- (void)hideBackView:(BOOL)hidden {
    if (hidden) {
        self.selectedView.image = nil;
    } else {
        self.selectedView.image = [UIImage imageNamed:Ti_WATERMARK_ICON_SELECTED];
    }
}

@end
