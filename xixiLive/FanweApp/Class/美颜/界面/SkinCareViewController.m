//
//  SkinCareViewController.m
//  FanweApp
//
//  Created by lyh on 2018/12/12.
//  Copyright © 2018 xfg. All rights reserved.
//

#import "SkinCareViewController.h"
#import "PublishLiveViewModel.h"

@interface SkinCareViewController ()
@property (nonatomic, strong) UIButton *startButton;
@property (nonatomic, strong) UIButton *closeButton;
@end

@implementation SkinCareViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSkinView:self.view];
    [self.tiUIView createTiUIView];
    // 随意一个rtmp地址，只是预览，不推流
    self.pushUrlStr = @"rtmp://push.com";
    [self startPreview];
    
    self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.startButton.frame = CGRectMake( kScreenW - 80 -20, 30, 80, 30);
    CAShapeLayer *layer = [[CAShapeLayer alloc]init];
    layer.frame = self.startButton.bounds;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.startButton.bounds cornerRadius:22.5];
    layer.path = path.CGPath;
    layer.fillColor = [UIColor blackColor].CGColor;
    layer.strokeColor = [UIColor blackColor].CGColor;
    [self.startButton.layer addSublayer:layer];
    [self.startButton addTarget:self action:@selector(startbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.startButton setTitle:@"开始直播" forState:UIControlStateNormal];
    self.startButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.startButton setTitleColor:kWhiteColor forState:UIControlStateNormal];
    [self.view addSubview:self.startButton];
    [self.startButton bringSubviewToFront:self.view];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(10, 30, 60, 45);
    self.closeButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.closeButton addTarget:self action:@selector(closebuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton setTitle:@"关闭" forState:UIControlStateNormal];
    [self.closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:self.closeButton];
    [self.closeButton bringSubviewToFront:self.view];
}

- (void)startbuttonAction {

    [self.startButton setTitle:@"正在努力配置直播..." forState:UIControlStateNormal];
    
    __weak UIButton *btn = self.startButton;
    
    [PublishLiveViewModel beginLive:[self.liveDic mutableCopy] vc:self block:^(AppBlockModel *blockModel) {
        
        btn.userInteractionEnabled = YES;
        [btn setTitle:@"开始直播" forState:UIControlStateNormal];
        
    }];
}

- (void)closebuttonAction {
    [self dismissViewControllerAnimated:true completion:^{
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
