//
//  TCShowLivePhotoView.h
//  FanweApp
//
//  Created by lyh on 2019/3/4.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TCShowLivePhotoViewDelegate <NSObject>

- (void)tCShowLivePhotoViewClicked;

@end

@interface TCShowLivePhotoView : UIView

@property (nonatomic,weak) id<TCShowLivePhotoViewDelegate> delegate;

/**
 请求完接口后，刷新直播间相关信息
 
 @param liveItem 视频Item
 @param liveInfo get_video2接口获取下来的数据实体
 */
- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem;

@end

NS_ASSUME_NONNULL_END
