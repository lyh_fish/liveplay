//
//  TCShowLiveKTVTopView.h
//  FanweApp
//
//  Created by lyh on 2019/5/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class TCShowLiveKTVTopView;
@protocol TCShowLiveKTVTopViewDelegate <NSObject>

- (void)showTickWithKTVTopView:(TCShowLiveKTVTopView *)TopView;

@end

@interface TCShowLiveKTVTopView : UIView

{
    __weak id<FWShowLiveRoomAble>      _liveItem;           // 开启、观看直播传入的实体
}

@property (weak, nonatomic) IBOutlet UILabel *numLabel;

@property (weak, nonatomic) IBOutlet UIView *numBackView;

@property (nonatomic, weak) id <TCShowLiveKTVTopViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *focusButton;

- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem liveInfo:(CurrentLiveInfo *)liveInfo;

@end

NS_ASSUME_NONNULL_END
