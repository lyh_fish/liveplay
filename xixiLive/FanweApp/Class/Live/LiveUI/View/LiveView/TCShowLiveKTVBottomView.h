//
//  TCShowLiveKTVBottomView.h
//  FanweApp
//
//  Created by lyh on 2019/5/9.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCShowLiveKTVBottomView;
@protocol TCShowLiveKTVBottomViewDelegate <NSObject>

- (void)collectionBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)photoBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)giftBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)messageBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)shareBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)zanBottomView:(TCShowLiveKTVBottomView *)bottomView;

- (void)tapBottomView:(TCShowLiveKTVBottomView *)bottomView;

@end

@protocol TCShowLiveKTVBottomViewToDelegate <NSObject>

// 点击用户头像
- (void)onBottomView:(TCShowLiveKTVBottomView *)bottomView userModel:(UserModel *)userModel;

@end

NS_ASSUME_NONNULL_BEGIN

@interface TCShowLiveKTVBottomView : UIView

@property (weak, nonatomic) IBOutlet UIView *infoView;

@property (weak, nonatomic) IBOutlet UIView *actionView;

@property (nonatomic, weak) id<TCShowLiveKTVBottomViewDelegate> delegate;

@property (nonatomic, weak) id<TCShowLiveKTVBottomViewToDelegate> toDelegate;

/**
 初始化房间信息等
 
 @param liveItem 房间信息
 @param liveController 直播VC
 @return self
 */
- (instancetype)initWith:(id<FWShowLiveRoomAble>)liveItem liveController:(id<FWLiveControllerAble>)liveController;

/**
 请求完接口后，刷新直播间相关信息
 
 @param liveItem 视频Item
 @param liveInfo get_video2接口获取下来的数据实体
 */
- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem  liveInfo:(CurrentLiveInfo *)liveInfo;

@end

NS_ASSUME_NONNULL_END
