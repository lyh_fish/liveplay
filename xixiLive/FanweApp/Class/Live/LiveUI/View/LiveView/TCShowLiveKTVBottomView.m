//
//  TCShowLiveKTVBottomView.m
//  FanweApp
//
//  Created by lyh on 2019/5/9.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "TCShowLiveKTVBottomView.h"
#import "KTVHomePlayView.h"
#import "GKMusic.h"
#import "KTVInteractiveViewController.h"
#import "KTVLikesViewController.h"
#import "KTVHotSongModel.h"
@interface TCShowLiveKTVBottomView()

{
@protected
    __weak id<FWShowLiveRoomAble>   _liveItem;
}

@property (nonatomic, strong) KTVHomePlayView *playView;

@property (weak, nonatomic) IBOutlet UIView *playSubView;

@property (weak, nonatomic) IBOutlet UIButton *photoButton;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIButton *playNumLabel;

@property (weak, nonatomic) IBOutlet UIImageView *vipImageView;

@property (weak, nonatomic) IBOutlet UILabel *giftLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UILabel *shareLabel;

@property (weak, nonatomic) IBOutlet UILabel *zanLabel;

@property (weak, nonatomic) IBOutlet UIButton *collectionButton;

@property (weak, nonatomic) IBOutlet UIButton *zanImgButton;

@property (weak, nonatomic) IBOutlet UIButton *zanButton;

@property (weak, nonatomic) IBOutlet UIButton *focusButton;

@property (nonatomic, assign) BOOL isShowInfo;

@end

@implementation TCShowLiveKTVBottomView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addSubview];
    [self updateUI];
    self.collectionButton.hidden = true;
    self.focusButton.hidden = true;
    self.focusButton.layer.cornerRadius = CGRectGetHeight(self.focusButton.frame)/2;
    _isShowInfo = true;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self addGestureRecognizer:tap];
}

- (void)addSubview {
    [self.playSubView addSubview:self.playView];
}

- (void)updateUI {
    [self.playView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.playSubView);
    }];
    
    self.photoButton.layer.cornerRadius = CGRectGetHeight(self.photoButton.frame)/2;
    self.photoButton.clipsToBounds = true;
}

- (void)tapAction {
    if ([self.delegate respondsToSelector:@selector(tapBottomView:)]) {
        [self.delegate tapBottomView:self];
    }
    if (self.isShowInfo) {
        [UIView animateWithDuration:1.0 animations:^{
            self.infoView.transform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(self.infoView.frame));
        }];
        
        [UIView animateWithDuration:1.0 animations:^{
            self.actionView.transform = CGAffineTransformMakeTranslation(0, -CGRectGetHeight(self.infoView.frame));
        }];
        self.isShowInfo = !self.isShowInfo;
    }else {
        [UIView animateWithDuration:1.0 animations:^{
            self.infoView.transform = CGAffineTransformIdentity;
        }];
        
        [UIView animateWithDuration:1.0 animations:^{
            self.actionView.transform = CGAffineTransformIdentity;
        }];
        self.isShowInfo = !self.isShowInfo;
    }
    
}

- (void)followAchorButtonAction:(UIButton *)sender
{
    sender.userInteractionEnabled = false;
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"user" forKey:@"ctl"];
    [mDict setObject:@"follow" forKey:@"act"];
    [mDict setObject:@(_liveItem.liveAVRoomId) forKey:@"room_id"];
    if ([[_liveItem liveHost] imUserId])
    {
        [mDict setObject:[[_liveItem liveHost] imUserId] forKey:@"to_user_id"];
        
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson){
            sender.userInteractionEnabled = true;
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                self.collectionButton.selected = [responseJson toInt:@"has_focus"] == 1;
            }
        } FailureBlock:^(NSError *error) {
            sender.userInteractionEnabled = true;
        }];
    }
}


- (IBAction)photoButtonAction:(UIButton *)sender {
    UserModel *userModel = [[UserModel alloc]init];
    userModel.user_id = [[_liveItem liveHost] imUserId];
    userModel.nick_name = [[_liveItem liveHost] imUserName];
    userModel.head_image = [[_liveItem liveHost] imUserIconUrl];
    if ([self.toDelegate respondsToSelector:@selector(onBottomView:userModel:)]) {
        [self.toDelegate onBottomView:self userModel:userModel];
    }
}

- (IBAction)collectionButtonAction:(UIButton *)sender {
    KTVHotSong *song = self.playView.music.other;
    sender.userInteractionEnabled = false;
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"audio" forKey:@"ctl"];
    [mDict setObject:@"is_collection" forKey:@"act"];
    [mDict setObject:song.id forKey:@"id"];
    if ([[_liveItem liveHost] imUserId])
    {
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson){
            sender.userInteractionEnabled = true;
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                [FanweMessage alertTWMessage:@"收藏成功"];
                sender.hidden = [responseJson toInt:@"iscollection"] == 1;
            }
        } FailureBlock:^(NSError *error) {
            sender.userInteractionEnabled = true;
        }];
    }
}

- (IBAction)giftButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(giftBottomView:)]) {
        [self.delegate giftBottomView:self];
    }
}

- (IBAction)messageButtonAction:(UIButton *)sender {
    KTVInteractiveViewController * interactiveViewController  = [[KTVInteractiveViewController alloc] init];
    [[AppDelegate sharedAppDelegate] pushViewController:interactiveViewController];
}

- (IBAction)shareButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(shareBottomView:)]) {
        [self.delegate shareBottomView:self];
    }
}

- (IBAction)focusButtonAction:(UIButton *)sender {
    sender.userInteractionEnabled = false;
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"user" forKey:@"ctl"];
    [mDict setObject:@"follow" forKey:@"act"];
    [mDict setObject:@(_liveItem.liveAVRoomId) forKey:@"room_id"];
    if ([[_liveItem liveHost] imUserId])
    {
        [mDict setObject:[[_liveItem liveHost] imUserId] forKey:@"to_user_id"];
        
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson){
            sender.userInteractionEnabled = true;
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                [FanweMessage alertTWMessage:@"关注成功"];
                sender.hidden = [responseJson toInt:@"has_focus"] == 1;
            }
        } FailureBlock:^(NSError *error) {
            sender.userInteractionEnabled = true;
        }];
    }
}

- (IBAction)zanButtonAction:(UIButton *)sender {
    sender.userInteractionEnabled = false;
    NSString *ID = [[NSUserDefaults standardUserDefaults] stringForKey:KTV_SONG_ID];
    if (ID) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"user" forKey:@"ctl"];
        [parmDict setObject:@"publish_comment" forKey:@"act"];
        [parmDict setObject:ID forKey:@"audio_user_id"];
        [parmDict setObject:@"2" forKey:@"type"];
        [parmDict setObject:@"xr" forKey:@"itype"];
        
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             sender.userInteractionEnabled = true;
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 self.zanImgButton.selected = [responseJson toInt:@"has_digg"] == 1;
                 if ([responseJson toInt:@"has_digg"] == 1) {
                     [FanweMessage alertTWMessage:@"点赞成功"];
                 }else {
                     [FanweMessage alertTWMessage:@"取消点赞"];
                 }
                 [self refresh];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
         }];
    }else {
        sender.userInteractionEnabled = true;
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem liveInfo:(CurrentLiveInfo *)liveInfo{
    TCShowLiveListItem *item = (TCShowLiveListItem *)liveItem;
    _liveItem = item;
    if ([item.type isEqualToString:@"2"]) {
        self.hidden = false;
    }else {
        self.hidden = true;
    }
    NSString *url = [[liveItem liveHost] imUserIconUrl];
    [self.photoButton sd_setBackgroundImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:kDefaultPreloadHeadImg];
    
    self.vipImageView.hidden = liveInfo.is_vip == 0;
    
    if (![[[IMAPlatform sharedInstance].host imUserId] isEqualToString:[[liveItem liveHost] imUserId]]) {
        self.collectionButton.hidden = false;
        self.collectionButton.selected = liveInfo.podcast.has_focus != 0;
        self.focusButton.hidden = liveInfo.podcast.has_focus != 0;;
    }
    self.nameLabel.text = liveInfo.podcast.user.nick_name;
    self.zanImgButton.selected = liveInfo.has_digg == 1;
    [self.playNumLabel setTitle:liveInfo.play_times forState:UIControlStateNormal];
    self.giftLabel.text = liveInfo.gift_num;
    self.messageLabel.text = [NSString stringWithFormat:@"%d",liveInfo.comment_num];
    self.shareLabel.text = [NSString stringWithFormat:@"%d",liveInfo.share_num];
    self.zanLabel.text = [NSString stringWithFormat:@"%d",liveInfo.digg_num];
}

- (void)refresh {
    [[NSNotificationCenter defaultCenter] postNotificationName:ROOM_UPDATE object:nil];
}

- (KTVHomePlayView *)playView {
    if (!_playView) {
        _playView = [[KTVHomePlayView alloc] init];
        GKMusic *music = [GKMusicTool sharedInstance].music;
        [_playView prepareWithMusic:music];
    }
    return _playView;
}





@end
