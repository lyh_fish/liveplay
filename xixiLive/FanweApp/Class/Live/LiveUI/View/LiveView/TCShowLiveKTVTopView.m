//
//  TCShowLiveKTVTopView.m
//  FanweApp
//
//  Created by lyh on 2019/5/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "TCShowLiveKTVTopView.h"
#import "MZTimerLabel.h"

@implementation TCShowLiveKTVTopView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.focusButton.hidden = true;
    self.focusButton.layer.cornerRadius = CGRectGetHeight(self.focusButton.frame)/2;
    self.focusButton.clipsToBounds = true;
    
    self.numBackView.layer.cornerRadius = CGRectGetHeight(self.numBackView.frame)/2;
    self.numBackView.clipsToBounds = true;

    MZTimerLabel *timerLabel = [[MZTimerLabel alloc] init];
    timerLabel.timeFormat = @"HH:mm:ss";
    timerLabel.font = [UIFont systemFontOfSize:10];
    timerLabel.textColor = [UIColor whiteColor];
    [self addSubview:timerLabel];
    timerLabel.hidden = true;
    [timerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.numLabel);
        make.top.equalTo(self.numBackView.mas_bottom).offset(4);
    }];
    [timerLabel start];
}

- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem liveInfo:(CurrentLiveInfo *)liveInfo{
    _liveItem = liveItem;
//    if (![[[IMAPlatform sharedInstance].host imUserId] isEqualToString:[[liveItem liveHost] imUserId]]) {
//        self.focusButton.hidden = liveInfo.podcast.has_focus != 0;
//    }
}

- (IBAction)buttonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(showTickWithKTVTopView:)]) {
        [self.delegate showTickWithKTVTopView:self];
    }
}

- (IBAction)focusButtonAction:(UIButton *)sender {
    sender.userInteractionEnabled = false;
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"user" forKey:@"ctl"];
    [mDict setObject:@"follow" forKey:@"act"];
    [mDict setObject:@(_liveItem.liveAVRoomId) forKey:@"room_id"];
    if ([[_liveItem liveHost] imUserId])
    {
        [mDict setObject:[[_liveItem liveHost] imUserId] forKey:@"to_user_id"];
        
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson){
            sender.userInteractionEnabled = true;
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                sender.hidden = [responseJson toInt:@"has_focus"] == 1;
            }
        } FailureBlock:^(NSError *error) {
            sender.userInteractionEnabled = true;
        }];
    }

}



@end
