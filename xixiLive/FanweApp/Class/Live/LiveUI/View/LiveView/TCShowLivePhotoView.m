//
//  TCShowLivePhotoView.m
//  FanweApp
//
//  Created by lyh on 2019/3/4.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "TCShowLivePhotoView.h"
#import "SDCycleScrollView.h"

@interface TCShowLivePhotoView()<SDCycleScrollViewDelegate>
@property (nonatomic,strong) SDCycleScrollView *cycleScrollView;
@end

@implementation TCShowLivePhotoView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:self.frame delegate:self placeholderImage:nil];
        self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        self.cycleScrollView.currentPageDotColor = [UIColor redColor]; // 自定义分页控件小圆标颜色
        self.cycleScrollView.backgroundColor = [UIColor clearColor];
        self.hidden = YES;
        [self addSubview:self.cycleScrollView];
        
        GKMusic *music = [GKMusicTool sharedInstance].music;
        NSString *path = music.cover_path;
        NSData * jsonData = [path dataUsingEncoding:NSUTF8StringEncoding];
        if (jsonData) {
            //json解析
            id obj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            if ([obj isKindOfClass:[NSArray class]]) {
                NSArray *paths = [NSArray arrayWithArray:obj];
                self.cycleScrollView.imageURLStringsGroup = paths;
                self.cycleScrollView.autoScrollTimeInterval = self.cycleScrollView.imageURLStringsGroup.count*1.5;
            }else {
                
            }
        }
        
        UISwipeGestureRecognizer *upRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(upSwipeAction)];
        [self addGestureRecognizer:upRecognizer];
        
        UISwipeGestureRecognizer *dowmRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dowmRecognizer)];
        [self addGestureRecognizer:dowmRecognizer];
        
    }
    return self;
}

- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem {
    TCShowLiveListItem *item = (TCShowLiveListItem *)liveItem;
    if ([item.type isEqualToString:@"2"]) {
        self.hidden = false;
    }else {
        self.hidden = true;
    }
}

- (void)upSwipeAction {
    NSLog(@"上滑");
}

- (void)dowmRecognizer {
    NSLog(@"下滑");
}

#pragma mark  SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(tCShowLivePhotoViewClicked)]) {
        [self.delegate tCShowLivePhotoViewClicked];
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    
}

@end
