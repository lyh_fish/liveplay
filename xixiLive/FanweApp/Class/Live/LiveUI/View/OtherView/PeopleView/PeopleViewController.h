//
//  PeopleViewController.h
//  FanweApp
//
//  Created by lyh on 2019/4/12.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@protocol PeopleViewControllerDelegate <NSObject>

- (void)showUserInfoWithUser:(UserModel *)user;

@end

NS_ASSUME_NONNULL_BEGIN

@interface PeopleViewController : UIViewController

@property (nonatomic,strong) NSArray *people;

@property (nonatomic,weak) id <PeopleViewControllerDelegate> delegate;

-(void)updateUIWithPeople:(NSArray *)people;

@end

NS_ASSUME_NONNULL_END
