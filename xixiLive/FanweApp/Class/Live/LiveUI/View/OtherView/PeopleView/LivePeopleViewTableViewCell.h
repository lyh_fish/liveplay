//
//  LivePeopleViewTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/4/11.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LivePeopleViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *firstImageView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (nonatomic,strong) UserModel *model;
@end

NS_ASSUME_NONNULL_END
