//
//  LivePeopleView.h
//  FanweApp
//
//  Created by lyh on 2019/4/11.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LivePeopleView;
@protocol LivePeopleViewDelegate <NSObject>

- (void)showPeopleWithUser:(UserModel *)user;

- (void)closeWithLivePeopleView:(LivePeopleView *)peopleView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface LivePeopleView : UIView<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *people;
@property (nonatomic,weak) id<LivePeopleViewDelegate> delegate;

- (void)updateUIWithPeople:(NSArray *)people;

@end

NS_ASSUME_NONNULL_END
