//
//  LivePeopleView.m
//  FanweApp
//
//  Created by lyh on 2019/4/11.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "LivePeopleView.h"
#import "LivePeopleViewTableViewCell.h"
#import "UserModel.h"

@implementation LivePeopleView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.people = [NSArray array];
    self.layer.cornerRadius = 8;
    self.clipsToBounds = true;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"LivePeopleViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"LivePeopleViewTableViewCell"];
}

- (void)updateUIWithPeople:(NSArray *)people {
    self.people = [NSArray arrayWithArray:people];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LivePeopleViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LivePeopleViewTableViewCell" forIndexPath:indexPath];
    cell.firstImageView.hidden = indexPath.row > 0;
    cell.model = self.people[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.people.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 84;
    }
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if ([self.delegate respondsToSelector:@selector(showPeopleWithUser:)]) {
        [self.delegate showPeopleWithUser:self.people[indexPath.row]];
    }
}

- (IBAction)closeButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(closeWithLivePeopleView:)]) {
        [self.delegate closeWithLivePeopleView:self];
    }
}


@end
