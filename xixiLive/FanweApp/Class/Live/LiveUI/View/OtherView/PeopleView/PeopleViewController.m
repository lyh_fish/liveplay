//
//  PeopleViewController.m
//  FanweApp
//
//  Created by lyh on 2019/4/12.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "PeopleViewController.h"
#import "LivePeopleView.h"

@interface PeopleViewController ()<LivePeopleViewDelegate>

@property (nonatomic,strong) LivePeopleView *peopleView;

@property (nonatomic,strong) UIView *clearView;

@end

@implementation PeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.clearView];
    [self.view addSubview:self.peopleView];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.clearView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

-(void)updateUIWithPeople:(NSArray *)people {
    [self.peopleView updateUIWithPeople:people];
}

- (void)tapAction {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark LivePeopleViewDelegate

- (void)showPeopleWithUser:(UserModel *)user {
    if ([self.delegate respondsToSelector:@selector(showUserInfoWithUser:)]) {
        [self.delegate showUserInfoWithUser:user];
    }
}

- (LivePeopleView *)peopleView {
    if (!_peopleView) {
        _peopleView = [[NSBundle mainBundle] loadNibNamed:@"LivePeopleView" owner:self options:nil].lastObject;
        _peopleView.frame = CGRectMake(0, kScreenH-360, kScreenW, 360);
        _peopleView.delegate = self;
    }
    return _peopleView;
}

- (UIView *)clearView {
    if (!_clearView) {
        _clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
        _clearView.backgroundColor = RGBA(0, 0, 0, 0);
    }
    return _clearView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
