//
//  LivePeopleViewTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/4/11.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "LivePeopleViewTableViewCell.h"

@implementation LivePeopleViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.photoImageView.layer.cornerRadius = 44/2;
    self.photoImageView.clipsToBounds = true;
    // Initialization code
}

- (void)setModel:(UserModel *)model {
    _model = model;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:model.head_image] placeholderImage:kDefaultPreloadHeadImg];
    self.nameLabel.text = model.nick_name;
    self.numberLabel.text = [NSString stringWithFormat:@"ID:%@",model.user_id];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
