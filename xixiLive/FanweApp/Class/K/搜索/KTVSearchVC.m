//
//  KTVSearchVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/18.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVSearchVC.h"
#import "KTVPointMusicTableViewCell.h"
#import "KTVSongModel.h"

@interface KTVSearchVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, assign) NSInteger inputCount;     //用户输入次数，用来控制延迟搜索请求

@property (nonatomic, strong) NSMutableArray *songs;

@end

@implementation KTVSearchVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [self.navigationController setNavigationBarHidden:true animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.searchBar becomeFirstResponder];
    
    self.tableview.tableFooterView = [[UIView alloc] init];
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    [self.tableview registerNib:[UINib nibWithNibName:@"KTVPointMusicTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableview reloadData];
    
    self.searchBar.delegate = self;
    
    [self setupBackBtnWithBlock:nil];
    // Do any additional setup after loading the view.
}

- (void)loadNetData
{
    if ([FWUtils isBlankString:self.searchBar.text]) {
        [self.songs removeAllObjects];
        [self.tableview  reloadData];
        return;
    }
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"song_by_search" forKey:@"act"];
    [parmDict setObject:self.searchBar.text forKey:@"keyword"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             self.songs = [KTVSongModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
             [self.tableview  reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
     }];
}

- (void)requestKeyWorld:(NSNumber *)count {
    if (self.inputCount == [count integerValue]) {
        //说明用户停止输入超过了一秒，发起网络请求
        [self loadNetData];
    }
}

#pragma mark ----  UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.inputCount ++;
    [self performSelector:@selector(requestKeyWorld:) withObject:@(self.inputCount) afterDelay:1.0f];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark ----  UITableViewDelegate,UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVPointMusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.song = self.songs[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.songs.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:true];
}


- (NSMutableArray *)songs {
    if (!_songs) {
        _songs = [NSMutableArray array];
    }
    return _songs;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
