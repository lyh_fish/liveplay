//
//  LyricsTool.h
//  KTV
//
//  Created by lyh on 2019/1/7.
//  Copyright © 2019 Sinopec. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^lyricAnalyzeBlock)(NSArray *lyricModels);

@interface LyricsTool : NSObject

@property (nonatomic, copy) lyricAnalyzeBlock analyzeBlock;

- (void)lyricAnalyzeWithLyric:(NSString *)lyric;
@end

NS_ASSUME_NONNULL_END
