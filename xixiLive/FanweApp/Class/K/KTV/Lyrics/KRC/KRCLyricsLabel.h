//
//  LXMLyricsLabel.h
//  LXMLyricsLabel
//
//  Created by luxiaoming on 15/9/8.
//  Copyright (c) 2015年 luxiaoming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRCLyricsLabel : UIView

@property (nonatomic, strong, readonly) UILabel *textLabel;
@property (nonatomic, strong, readonly) UILabel *maskLabel;


- (void)setFont:(UIFont *)font;

- (void)setText:(NSString *)text;

- (void)setTextAlignment:(NSTextAlignment)textAlignment;


- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray Duration:(NSTimeInterval)duration;

- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray Duration:(NSTimeInterval)duration Location:(NSTimeInterval)location;

/**
 暂停动画
 */
- (void)pauseAnimation;

/**
 继续动画
 */
- (void)continueAnimation;
/**
 停止动画
 */
- (void)stopAnimation;

@end
