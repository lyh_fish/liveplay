//
//  NSData+STUnzipArchive.h
//  高仿酷狗音乐歌词逐渐字播放
//
//  Created by lyh on 2019/1/27.
//  Copyright © 2019 ly. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (STUnzipArchive)
// ZLIB
- (NSData *) zlibInflate;
- (NSData *) zlibDeflate;

// GZIP
- (NSData *) gzipInflate;
- (NSData *) gzipDeflate;
@end

NS_ASSUME_NONNULL_END
