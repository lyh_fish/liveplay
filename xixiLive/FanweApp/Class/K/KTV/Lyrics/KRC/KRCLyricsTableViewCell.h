//
//  KRCLyricsTableViewCell.h
//  高仿酷狗音乐歌词逐渐字播放
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRCLyricsLabel.h"
#import "KTVLyricsPointView.h"

NS_ASSUME_NONNULL_BEGIN

@interface KRCLyricsTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *lyric;

@property (nonatomic, strong) KTVLyricsPointView *pointView;

- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray andLocationArray:(NSArray *)locationArray;

- (void)pauseLyricsAnimation;

- (void)continueLyricsAnimation;

- (void)stopLyricsAnimation;

@end

NS_ASSUME_NONNULL_END
