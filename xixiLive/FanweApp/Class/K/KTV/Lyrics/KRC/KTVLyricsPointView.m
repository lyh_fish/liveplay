//
//  KTVLyricsPointView.m
//  FanweApp
//
//  Created by lyh on 2019/2/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLyricsPointView.h"

@interface KTVLyricsPointView()<CAAnimationDelegate>

{
    BOOL isInterval;
}

@property (nonatomic, strong) CALayer *maskLayer;//用来控制maskLabel渲染的layer

@property (nonatomic, strong) CAKeyframeAnimation *animation;//用来控制maskLabel渲染的layer



@end

@implementation KTVLyricsPointView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CALayer *maskLayer = [CALayer layer];
    maskLayer.anchorPoint = CGPointMake(0, 0.5);//注意，按默认的anchorPoint，width动画是同时像左右扩展的
    maskLayer.position = CGPointMake(0, CGRectGetHeight(self.bounds)/ 2);
    maskLayer.bounds = CGRectMake(0, 0, 0, CGRectGetHeight(self.bounds));
    maskLayer.backgroundColor = [UIColor whiteColor].CGColor;
    self.layer.mask = maskLayer;
    self.maskLayer = maskLayer;
}

- (void)startAnimation {
    self.animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    self.animation.delegate = self;
    self.animation.values = @[@(self.frame.size.width),@(0)];
    self.animation.duration = self.animationDuration;
    self.animation.calculationMode = kCAAnimationLinear;
    self.animation.fillMode = kCAFillModeForwards;
    self.animation.repeatCount = 0;
    self.animation.removedOnCompletion = NO;
    [self.maskLayer addAnimation:self.animation forKey:@"KTVLyricsPointViewAnimation"];
}

- (void)startAnimationWithDuration:(double)duration {
    self.animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    self.animation.delegate = self;
    self.animation.values = @[@(self.frame.size.width),@(0)];
    self.animation.duration = duration;
    self.animation.calculationMode = kCAAnimationLinear;
    self.animation.fillMode = kCAFillModeForwards;
    self.animation.repeatCount = 0;
    self.animation.removedOnCompletion = NO;
    [self.maskLayer addAnimation:self.animation forKey:@"intervalAnimation"];
}

- (void)pauseAnimation {
    CFTimeInterval pausedTime = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.maskLayer.speed = 0.0;
    self.maskLayer.timeOffset = pausedTime;
}

- (void)continueAnimation {
    CFTimeInterval pausedTime = [self.maskLayer timeOffset];
    self.maskLayer.speed = 1.0;
    self.maskLayer.timeOffset = 0.0;
    self.maskLayer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.maskLayer.beginTime = timeSincePause;
}

- (void)stopAnimation {
    [self.maskLayer removeAllAnimations];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        if ([self.delegate respondsToSelector:@selector(endPointWithKTVLyricsPointView:)]) {
            [self.delegate endPointWithKTVLyricsPointView:self];
        }
    }
}


@end
