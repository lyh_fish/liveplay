//
//  LXMLyricsLabel.m
//  LXMLyricsLabel
//
//  Created by luxiaoming on 15/9/8.
//  Copyright (c) 2015年 luxiaoming. All rights reserved.
//

#import "KRCLyricsLabel.h"
#import "JXBorderLabel.h"

@interface KRCLyricsLabel ()

{
    CFTimeInterval pausedTime;
}

@property (nonatomic, strong, readwrite) UILabel *textLabel;
@property (nonatomic, strong, readwrite) JXBorderLabel *maskLabel;
@property (nonatomic, strong) CALayer *maskLayer;//用来控制maskLabel渲染的layer

@end

@implementation KRCLyricsLabel

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.textLabel];
        [self addSubview:self.maskLabel];
        [self setupDefault];
    }
    return self;
}

- (void)setupDefault {
    
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];

    [self.maskLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.backgroundColor = [UIColor clearColor];
//
//    self.maskLabel.textColor = [UIColor redColor];
//    self.maskLabel.backgroundColor = [UIColor clearColor];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    [self.maskLabel setNeedsDisplay];
    
    CALayer *maskLayer = [CALayer layer];
    maskLayer.anchorPoint = CGPointMake(0, 0.5);//注意，按默认的anchorPoint，width动画是同时像左右扩展的
    maskLayer.position = CGPointMake(0, CGRectGetHeight(self.frame) / 2);
    maskLayer.bounds = CGRectMake(0, 0, 0, CGRectGetHeight(self.frame));
    maskLayer.backgroundColor = [UIColor whiteColor].CGColor;
    self.maskLabel.layer.mask = maskLayer;
    self.maskLayer = maskLayer;
}

#pragma mark - publicMethod


- (void)setFont:(UIFont *)font {
    self.textLabel.font = font;
    self.maskLabel.font = font;
}

- (void)setText:(NSString *)text {
    self.textLabel.text = text;
    self.maskLabel.text = text;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment {
    self.textLabel.textAlignment = textAlignment;
    self.maskLabel.textAlignment = textAlignment;
}

- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray Duration:(NSTimeInterval)duration{
    CGFloat width = CGRectGetWidth(self.frame);
    long count = timeArray.count;
    if (count == 0) {
        return;
    }
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    NSMutableArray *keyTimeArray = [NSMutableArray array];
    NSMutableArray *widthArray = [NSMutableArray array];
    CGFloat tempWidth = width/count;
    for (int i = 0 ; i < timeArray.count; i++) {
        int time = [timeArray[i] intValue];
        CGFloat tempTime = time / duration;
        [keyTimeArray addObject:@(tempTime)];
        [widthArray addObject:@(tempWidth*i)];
    }
    [widthArray removeObject:@(0)];
    [widthArray addObject:@(width)];
    animation.values = widthArray;
    animation.keyTimes = keyTimeArray;
    animation.duration = duration/1000.0;
    animation.calculationMode = kCAAnimationLinear;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.maskLayer addAnimation:animation forKey:@"kLyrcisAnimation"];
}

- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray Duration:(NSTimeInterval)duration Location:(NSTimeInterval)location{
    CGFloat width = CGRectGetWidth(self.frame);
    long count = timeArray.count;
    if (count == 0) {
        return;
    }
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    NSMutableArray *keyTimeArray = [NSMutableArray array];
    NSMutableArray *widthArray = [NSMutableArray array];
    [keyTimeArray addObject:@(0)];
    CGFloat tempWidth = width/count;
    for (int i = 0 ; i < timeArray.count; i++) {
        int time = [timeArray[i] intValue];
        CGFloat tempTime = time / duration;
        if (!(location > time)) {
            if (tempTime != 0) {
                 [keyTimeArray addObject:@(tempTime)];
            }
            [widthArray addObject:@(tempWidth*i)];
        }
    }
//    if (![keyTimeArray containsObject:@(0)]) {
//        [keyTimeArray addObject:@(0)];
//    }
    [widthArray removeObject:@(0)];
    [widthArray addObject:@(width)];
    animation.values = widthArray;
    animation.keyTimes = keyTimeArray;
    animation.duration = (duration - location)/1000.0;
    animation.calculationMode = kCAAnimationLinear;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.maskLayer addAnimation:animation forKey:@"kLyrcisAnimation"];
}



- (void)pauseAnimation {
    CFTimeInterval pausedTime = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.maskLayer.speed = 0.0;
    self.maskLayer.timeOffset = pausedTime;
}

- (void)continueAnimation {
    CFTimeInterval pausedTime = [self.maskLayer timeOffset];
    self.maskLayer.speed = 1.0;
    self.maskLayer.timeOffset = 0.0;
    self.maskLayer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.maskLayer.beginTime = timeSincePause;
}

- (void)stopAnimation {
    [self.maskLayer removeAllAnimations];
}

#pragma mark - property

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
    }
    return _textLabel;
}

- (UILabel *)maskLabel {
    if (!_maskLabel) {
        _maskLabel = [[JXBorderLabel alloc] init];
    }
    return _maskLabel;
}

@end
