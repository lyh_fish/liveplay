//
//  KRCLyricsShowView.h
//  高仿酷狗音乐歌词逐渐字播放
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 ly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class KRCLyricsShowView;
@protocol KRCLyricsShowViewDelegate <NSObject>

/**
 歌词开始滑动

 @param showView showView
 */
- (void)beginScrollWithKRCLyricsShowView:(KRCLyricsShowView *)showView;


/**
 歌词结束滑动,开始....

 @param showView showView
 @param time 滑动到某句歌词的开始时间
 */
- (void)endScrollStartPointWithKRCLyricsShowView:(KRCLyricsShowView *)showView Time:(double)time;

/**
 歌词结束滑动,结束....
 
 @param showView showView
 @param time 滑动到某句歌词的开始时间
 */
- (void)endScrollendPointWithKRCLyricsShowView:(KRCLyricsShowView *)showView Time:(double)time;

@end

@interface KRCLyricsShowView : UIView

@property (nonatomic,assign)double currentTime;

@property (nonatomic,copy) NSString *lyricPath;

@property (nonatomic,weak) id<KRCLyricsShowViewDelegate> delegate;


- (void)pauseLyricAnimation;

- (void)continueLyricAnimation;

- (void)clearLyricAnimation;


@end

NS_ASSUME_NONNULL_END
