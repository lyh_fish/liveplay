//
//  KRCLyricsShowView.m
//  高仿酷狗音乐歌词逐渐字播放
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 ly. All rights reserved.
//

#import "KRCLyricsShowView.h"
#import "KRCLyricsTableViewCell.h"
#import "LyricsFocusView.h"
#import "KRC.h"
#import "KRCLyricsUtil.h"
#import "LyricModel.h"
#import "LyricsTool.h"
#import "WZBCountdownLabel.h"

@interface KRCLyricsShowView ()<UITableViewDelegate,UITableViewDataSource,KTVLyricsPointViewDelegate>
{
    NSInteger pointIndex;///....的位置
    
    BOOL intervalAnimation;
    /// 是否解析歌曲成功
    BOOL isParse;
}
/** tableView */
@property (strong ,nonatomic) UITableView * tableView;

@property (assign,nonatomic) int currentRow;

@property (nonatomic, strong) NSArray *animationTimesArray;

@property (nonatomic, strong) NSArray *animationlocationsArray;

/**焦点线*/
@property (weak ,nonatomic) LyricsFocusView * focusView;

/** 无歌词提示信息 */
@property (strong ,nonatomic) UILabel * noticeLabel;

@property (nonatomic,strong) NSArray *lyrics;

@property (nonatomic,strong) NSArray *lyricTimes;

@property (nonatomic,strong) NSArray *starsTimes;

@property (nonatomic,strong) NSArray *durationTimes;

@property (nonatomic,strong) NSMutableArray *intervalAnimationRows;

@property (nonatomic,strong) NSMutableArray *intervalBeginTimes;

@property (nonatomic,assign) NSInteger intervalAnimationRow;

@property (nonatomic,assign) double intervalBeginTime;

@property (nonatomic,strong) UIButton *skipButton;
@end

static int BLACKININT = 3;

@implementation KRCLyricsShowView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setUpTableView];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setUpTableView];
    }
    return self;
}

- (void)setLyricPath:(NSString *)lyricPath {
    _lyricPath = lyricPath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:lyricPath]) {
        if ([lyricPath containsString:@".krc"]) {
            KRC * krc = [KRC new];
            NSString * lyric = [krc Decode:lyricPath];
            self.lyrics = [KRCLyricsUtil getLyricSArrayWithLyric:lyric];
            self.lyricTimes = [KRCLyricsUtil timeArrayWithLineLyric:lyric];
            self.starsTimes = [KRCLyricsUtil startTimeArrayWithLineLyric:lyric];
            self.durationTimes = [KRCLyricsUtil durationTimeArrayWithLineLyric:lyric];
            [self reloadData];
        }else if ([lyricPath containsString:@".lrc"]) {
            NSError *error;
            NSString *lyric = [NSString stringWithContentsOfFile:lyricPath encoding:NSUTF8StringEncoding error:&error];
            LyricsTool *tool = [[LyricsTool alloc] init];
            [tool lyricAnalyzeWithLyric:lyric];
            [tool setAnalyzeBlock:^(NSArray * _Nonnull lyricModels) {
                NSMutableArray *lyrics = [NSMutableArray array];
                NSMutableArray *lyricTimes = [NSMutableArray array];
                NSMutableArray *starsTimes = [NSMutableArray array];
                for (LyricModel *model in lyricModels) {
                    [lyrics addObject:model.lyric];
                    NSMutableArray *times = [NSMutableArray array];
                    for (int i=0; i<model.lyric.length; i++) {
                        [times addObject:@((int)(model.duration*1000*i/model.lyric.length))];
                    }
                    [lyricTimes addObject:times];
                    [starsTimes addObject:@((int)(model.start*1000))];
                }
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    self.lyrics = [NSArray arrayWithArray:lyrics];
                    self.lyricTimes = [NSArray arrayWithArray:lyricTimes];
                    self.starsTimes = [NSArray arrayWithArray:starsTimes];
                    [self reloadData];
                    [self.tableView reloadData];
                }];
            }];
        }else {
            self.noticeLabel.hidden = NO;
            NSLog(@"歌词不存在");
        }
    }
    else {
        self.noticeLabel.hidden = NO;
        NSLog(@"歌词不存在");
    }
}

- (void)setLyrics:(NSArray *)lyrics {
    _lyrics = lyrics;
    self.noticeLabel.hidden = lyrics.count != 0;
    if (_lyrics.count > 0) {
        [self addSkipButton];
    }
}

- (void)reloadData {

    self.intervalAnimationRows = [NSMutableArray array];
    self.intervalBeginTimes = [NSMutableArray array];
    
    NSMutableArray *animationTimesArray = [NSMutableArray array];
    NSMutableArray *animationlocationsArray = [NSMutableArray array];
    
    NSSet *counts = [NSSet setWithObjects:@(self.lyrics.count),@(self.lyricTimes.count),@(self.starsTimes.count), @(self.durationTimes.count), nil];
    
    if (counts.count != 1) { ///歌曲解析失败
        self.noticeLabel.hidden = false;
        self.tableView.scrollEnabled = false;
        isParse = false;
        return;
    }else {
        isParse = true;
    }
    
    /// 准备数据
    for (int i=0; i< MIN(self.lyrics.count, self.lyricTimes.count); i++) {
        NSMutableArray *timesArr = [NSMutableArray array];
        NSMutableArray *locationsArr = [NSMutableArray array];
        NSArray *times = self.lyricTimes[i];
        double length = times.count;
        for (int j=0;  j< times.count; j++) {
            int index = [times[j] intValue];
            [timesArr addObject:@(index/1000.0)];
            [locationsArr addObject:@(j/length)];
        }
        [animationTimesArray addObject:timesArr];
        [animationlocationsArray addObject:locationsArr];
    }
    
    for (int i=0; i<self.starsTimes.count - 2; i++) {
        NSInteger start1 = [self.starsTimes[i] integerValue];
        NSInteger duration1 = [self.durationTimes[i] integerValue];
        
        NSInteger start2 = [self.starsTimes[i+1] integerValue];
        NSInteger duration2 = [self.durationTimes[i+1] integerValue];
    
        NSInteger interval = start2 + duration2 - start1 - duration1;
        
        if (i==0&&start1> 1000*5) {
            [self.intervalAnimationRows addObject:@(i)];
            [self.intervalBeginTimes addObject:@((start1 - 4000)/1000)];
        }
        
        if (interval > 1000 * 10) {
            [self.intervalAnimationRows addObject:@(i)];
            [self.intervalBeginTimes addObject:@((start2 - 4000)/1000)];
        }
    }
    
    self.animationTimesArray = [NSArray arrayWithArray:animationTimesArray];
    self.animationlocationsArray = [NSArray arrayWithArray:animationlocationsArray];
}

- (void)setUpTableView {
    UITableView * tableView = [[UITableView alloc]init];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    /// 取消弹簧效果
//    tableView.bounces = NO;
    [tableView registerClass:[KRCLyricsTableViewCell class] forCellReuseIdentifier:@"cell"];
    [tableView registerClass:[KRCLyricsTableViewCell class] forCellReuseIdentifier:@"blackCell"];
    [self addSubview:tableView];
    self.tableView = tableView;
    
    self.currentRow = -1;
    pointIndex = -2;
    
    /// 焦点线
    [self addSubview:self.focusView];
    [self bringSubviewToFront:self.focusView];
    // 无歌词显示
    [self addSubview:self.noticeLabel];
    [self bringSubviewToFront:self.noticeLabel];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.focusView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(tableView);
        make.width.equalTo(self.tableView);
        make.height.equalTo(@(36));
    }];
    
    [self.noticeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(tableView);
    }];
}

- (void)addSkipButton {
    [self addSubview:self.skipButton];
    
    [self.skipButton  mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.top.equalTo(self).offset(66);
        make.height.equalTo(@(30));
        make.width.equalTo(@(60));
    }];
}

- (void)skipButtonAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:BLACKININT inSection:0];
    [self beginPointWithIndexPath:indexPath];
}

- (void)setCurrentTime:(double)currentTime
{
    _currentTime = currentTime;
    
    if (!isParse){
        return;
    }
    if (self.currentRow == pointIndex) {
        return;
    }
    
    if  (self.starsTimes.count > 0) {
        double firsttime = [self.starsTimes.firstObject doubleValue]/1000.0;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.skipButton.hidden = currentTime > firsttime;
        });
    }
    
    if (currentTime == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //启动之后，第一句歌词就居中显示
            NSIndexPath * middleIndexPath = [NSIndexPath indexPathForRow:BLACKININT inSection:0];
            if (self.lyrics.count != 0) {
                [self.tableView scrollToRowAtIndexPath:middleIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            }
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.focusView.isHidden) {
            return;
        }
    });
    if (!intervalAnimation) {
        for (int i = 0; i < self.intervalBeginTimes.count ; i++)
        {
            double time = [self.intervalBeginTimes[i] doubleValue];
            double durationTime = time + 4.0;
            if ((currentTime > time || currentTime == time) && durationTime > currentTime) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.currentRow == -1) {
                        KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:BLACKININT inSection:0]];
                        [cell.pointView startAnimationWithDuration:4.0];
                        intervalAnimation = true;
                    }else {
                        KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow + 1 inSection:0]];
                        [cell.pointView startAnimationWithDuration:4.0];
                        intervalAnimation = true;
                    }
                    NSLog(@"开始间隔动画%d",self.currentRow+1);
                });
                return;
            }
        }
    }
    NSInteger count = self.starsTimes.count;
    //拿到当前行
    for (int i = 0; i < count ; i++)
    {
        double time = [self.starsTimes[i] doubleValue]/1000.0;
        double durationTime = 0.0;
        if (i == count - 1) {
            durationTime = MAXFLOAT;
        }else {
            double duration = [self.durationTimes[i] doubleValue]/1000.0;
            durationTime = time + duration;
        }
        if ((currentTime > time  || currentTime == time)  && durationTime > currentTime && self.currentRow != (i + BLACKININT)) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self startAnimationWithRow:i  + BLACKININT];
            });
        }
    }
    
//
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//
//
//    });
}

- (void)startAnimationWithRow:(int)row {
    if (self.currentRow > -1 ) {
        [self clearLyricAnimation];
    }
    self.currentRow = row;
    KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    [cell startLyricsAnimationWithTimeArray:self.animationTimesArray[row  - BLACKININT] andLocationArray:self.animationlocationsArray[row  - BLACKININT]];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    NSLog(@"滚动到中间");
}

#pragma mark - 暂停/继续/清除歌词动画

- (void)pauseLyricAnimation {
    KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
    [cell pauseLyricsAnimation];
    [cell.pointView pauseAnimation];
    NSLog(@"暂停%@",cell.pointView);
}

- (void)continueLyricAnimation {
    KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
    [cell continueLyricsAnimation];
    [cell.pointView continueAnimation];
}

- (void)clearLyricAnimation {
    KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
    [cell stopLyricsAnimation];
    [cell.pointView stopAnimation];
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.lyrics.count + BLACKININT *2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < BLACKININT || indexPath.row > (self.lyrics.count + BLACKININT) || indexPath.row == (self.lyrics.count + BLACKININT)) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"blackCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }else {
        KRCLyricsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.pointView.delegate = self;
        cell.pointView.indexPath = indexPath;
        if (indexPath.row - BLACKININT > 0) {
            NSArray *times = self.animationTimesArray[indexPath.row - BLACKININT-1];
            cell.pointView.animationDuration = [times.lastObject doubleValue];
        }
        cell.lyric = self.lyrics[indexPath.row - BLACKININT];
        if (self.currentRow != indexPath.row) {
            [cell stopLyricsAnimation];
        }
        return cell;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark ----- KTVLyricsPointViewDelegate

- (void)endPointWithKTVLyricsPointView:(KTVLyricsPointView *)pointView {
    NSLog(@"结束.......");
    if (intervalAnimation) {
        intervalAnimation = false;
    }else {
        pointIndex = -2;
        if ([self.delegate respondsToSelector:@selector(endScrollendPointWithKRCLyricsShowView:Time:)]) {
            int start = [self.starsTimes[pointView.indexPath.row  - BLACKININT] intValue];
            [self startAnimationWithRow:(int)pointView.indexPath.row];
            double time = start/1000.0;
            NSLog(@"开始唱从%f唱",time);
            [self.delegate endScrollendPointWithKRCLyricsShowView:self Time:time];
        }
    }
}

/// scrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSIndexPath *middleIndexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(0, scrollView.contentOffset.y+self.tableView.frame.size.height/2)];
    if (middleIndexPath.row < BLACKININT || middleIndexPath.row > (self.lyrics.count + BLACKININT) || middleIndexPath.row == (self.lyrics.count + BLACKININT)) {
        return;
    }
    if (!_focusView.hidden) {
        int start = [self.starsTimes[middleIndexPath.row - BLACKININT] intValue];
        _focusView.timeLabel.text = [self musicTime:start/1000];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self clearLyricAnimation];
    _focusView.hidden = false;
    intervalAnimation = false;
    if ([self.delegate respondsToSelector:@selector(beginScrollWithKRCLyricsShowView:)]) {
        [self.delegate beginScrollWithKRCLyricsShowView:self];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
     NSIndexPath *middleIndexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(0, scrollView.contentOffset.y+self.tableView.frame.size.height/2)];
    if (middleIndexPath.row < BLACKININT || middleIndexPath.row > (self.lyrics.count + BLACKININT) || middleIndexPath.row == (self.lyrics.count + BLACKININT)) {
        return;
    }
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.isTracking && !scrollView.isDragging && !scrollView.isDecelerating;
        if (dragToDragStop) {
            [self beginPointWithIndexPath:middleIndexPath];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSIndexPath *middleIndexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(0, scrollView.contentOffset.y+self.tableView.frame.size.height/2)];
    if (middleIndexPath.row < BLACKININT || middleIndexPath.row > (self.lyrics.count + BLACKININT) || middleIndexPath.row == (self.lyrics.count + BLACKININT)) {
        return;
    }
    BOOL dragToDragStop = !scrollView.isTracking && !scrollView.isDragging && !scrollView.isDecelerating;
    if (dragToDragStop) {
        [self beginPointWithIndexPath:middleIndexPath];
    }
}

/// 开始point.... 正常
- (void)beginPointWithIndexPath:(NSIndexPath *)indexPath {
     NSLog(@"开始.......");
    _focusView.hidden = true;
    pointIndex = indexPath.row;
    KRCLyricsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell.pointView startAnimation];
    self.currentRow = indexPath.row;
    if ([self.delegate respondsToSelector:@selector(endScrollStartPointWithKRCLyricsShowView:Time:)]) {
        int lastTimeIndex = indexPath.row  - BLACKININT - 1;
        if (lastTimeIndex > 0) {
            int start = [self.starsTimes[lastTimeIndex] intValue];
            double time = start/1000.0;
            [self.delegate endScrollStartPointWithKRCLyricsShowView:self Time:time];
        }

    }
}

- (LyricsFocusView *)focusView {
    if (!_focusView) {
        _focusView = (LyricsFocusView *) [[NSBundle mainBundle] loadNibNamed:@"LyricsFocusView" owner:self options:nil].firstObject;
        _focusView.hidden = YES;
        _focusView.backgroundColor = [UIColor clearColor];
    }
    return _focusView;
}

- (UILabel *)noticeLabel {
    if (!_noticeLabel) {
        _noticeLabel = [[UILabel alloc] init];
        _noticeLabel.text = @"当前歌曲解析失败,请联系管理员解决";
        [_noticeLabel setFont:[UIFont systemFontOfSize:14]];
        _noticeLabel.textColor = [UIColor darkGrayColor];
        [_noticeLabel setTextAlignment:NSTextAlignmentCenter];
    }
    return _noticeLabel;
}

- (UIButton *)skipButton {
    if (!_skipButton) {
        _skipButton = [[UIButton alloc] init];
        [_skipButton setTitle:@"跳过伴奏" forState:UIControlStateNormal];
        _skipButton.titleLabel.font = [UIFont systemFontOfSize:10];
        [_skipButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        _skipButton.backgroundColor = [UIColor colorWithHexString:@"#151e25"];
        _skipButton.layer.cornerRadius = 15;
        [_skipButton addTarget:self action:@selector(skipButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _skipButton;
}

- (NSArray *)animationlocationsArray {
    if (!_animationlocationsArray) {
        _animationlocationsArray = [NSArray array];
    }
   return _animationlocationsArray;
}

- (NSArray *)animationTimesArray {
    if (!_animationTimesArray) {
        _animationTimesArray = [NSArray array];
    }
    return _animationTimesArray;
}


- (NSString *)musicTime:(int)time {
    int h = time/3600;
    NSString * hstr = [[NSString alloc] initWithFormat:@"%02d",h];
    NSString * mstr = [[NSString alloc] initWithFormat:@"%02d",time%3600/60];
    NSString * sstr = [[NSString alloc] initWithFormat:@"%02d",time%60];
    if (h==0) {
        return [NSString stringWithFormat:@"%@:%@",mstr,sstr];
    }else {
        return [NSString stringWithFormat:@"%@:%@:%@",hstr,mstr,sstr];
    }
}

@end
