//
//  KTVLyricsPointView.h
//  FanweApp
//
//  Created by lyh on 2019/2/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KTVLyricsPointView;
@protocol KTVLyricsPointViewDelegate <NSObject>

- (void)endPointWithKTVLyricsPointView:(KTVLyricsPointView *)pointView;

- (void)intervalWithKTVLyricsPointView:(KTVLyricsPointView *)pointView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVLyricsPointView : UIView

@property (nonatomic,weak) id<KTVLyricsPointViewDelegate> delegate;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic, assign) double animationDuration;

- (void)startAnimation;
- (void)startAnimationWithDuration:(double)duration;
- (void)pauseAnimation;
- (void)continueAnimation;
- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
