//
//  KRCLyricsTableViewCell.m
//  高仿酷狗音乐歌词逐渐字播放
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 ly. All rights reserved.
//

#import "KRCLyricsTableViewCell.h"


@interface KRCLyricsTableViewCell ()

@property (nonatomic, strong, readwrite) UILabel *lyricLabel;
@property (nonatomic, strong, readwrite) UILabel *maskLabel;
@property (nonatomic, strong) CALayer *maskLayer;//用来控制maskLabel渲染的layer
@end

@implementation KRCLyricsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.lyricLabel];
        [self addSubview:self.maskLabel];
        [self addSubview:self.pointView];
        [self setupDefault];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setLyric:(NSString *)lyric {
    _lyric = lyric;
    _lyricLabel.text = lyric;
    _maskLabel.text = lyric;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CALayer *maskLayer = [CALayer layer];
    maskLayer.anchorPoint = CGPointMake(0, 0.5);//注意，按默认的anchorPoint，width动画是同时像左右扩展的
    maskLayer.position = CGPointMake(0, CGRectGetHeight(self.maskLabel.frame) / 2);
    maskLayer.bounds = CGRectMake(0, 0, 0, CGRectGetHeight(self.maskLabel.frame));
    maskLayer.backgroundColor = [UIColor whiteColor].CGColor;
    self.maskLabel.layer.mask = maskLayer;
    self.maskLayer = maskLayer;
}

- (void)setupDefault {
    
    [self.pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self.maskLabel);
        make.height.equalTo(@12);
        make.width.equalTo(@60);
    }];
    
    [self.maskLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.centerX.equalTo(self.contentView);
        make.width.lessThanOrEqualTo(self.contentView);
    }];
    
    [self.lyricLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.maskLabel);
    }];
    
}

- (void)startLyricsAnimationWithTimeArray:(NSArray *)timeArray andLocationArray:(NSArray *)locationArray {
    
    CGFloat totalDuration = [timeArray.lastObject floatValue];
   
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    NSMutableArray *keyTimeArray = [NSMutableArray array];
    NSMutableArray *widthArray = [NSMutableArray array];
    for (int i = 0 ; i < timeArray.count; i++) {
        CGFloat tempTime = [timeArray[i] floatValue] / totalDuration;
        [keyTimeArray addObject:@(tempTime)];
        CGFloat tempWidth = [locationArray[i] floatValue] * CGRectGetWidth(self.maskLabel.bounds);
        [widthArray addObject:@(tempWidth)];
    }
    [widthArray removeObject:@(0)];
    [widthArray addObject:@(CGRectGetWidth(self.maskLabel.bounds))];
    animation.values = widthArray;
    animation.keyTimes = keyTimeArray;
    animation.duration = totalDuration;
    animation.calculationMode = kCAAnimationLinear;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.maskLayer addAnimation:animation forKey:@"kLyrcisAnimation"];
}

- (void)pauseLyricsAnimation {
    CFTimeInterval pausedTime = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.maskLayer.speed = 0.0;
    self.maskLayer.timeOffset = pausedTime;
}

- (void)continueLyricsAnimation {
    CFTimeInterval pausedTime = [self.maskLayer timeOffset];
    self.maskLayer.speed = 1.0;
    self.maskLayer.timeOffset = 0.0;
    self.maskLayer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.maskLayer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.maskLayer.beginTime = timeSincePause;
}

- (void)stopLyricsAnimation {
    [self.maskLayer removeAllAnimations];
}

#pragma mark - property

- (UILabel *)lyricLabel {
    if (!_lyricLabel) {
        _lyricLabel = [[UILabel alloc] init];
        _lyricLabel.textColor = [UIColor lightTextColor];
        _lyricLabel.backgroundColor = [UIColor clearColor];
        _lyricLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    return _lyricLabel;
}

- (UILabel *)maskLabel {
    if (!_maskLabel) {
        _maskLabel = [[UILabel alloc] init];
        _maskLabel.textColor = [UIColor orangeColor];
        _maskLabel.backgroundColor = [UIColor clearColor];
        _maskLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    return _maskLabel;
}


- (KTVLyricsPointView *)pointView {
    if (!_pointView) {
        _pointView = [[NSBundle mainBundle] loadNibNamed:@"KTVLyricsPointView" owner:self options:nil].lastObject;
    }
    return _pointView;
}
@end
