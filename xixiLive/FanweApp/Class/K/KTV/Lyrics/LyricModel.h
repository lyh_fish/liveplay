//
//  LyricModel.h
//  KTV
//
//  Created by lyh on 2019/1/7.
//  Copyright © 2019 Sinopec. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricModel : NSObject
/**歌词*/
@property (nonatomic, copy) NSString *lyric;
/**显示焦点的时间*/
@property (nonatomic, copy) NSString *focustime;
/**这一行歌词播放开始的时间*/
@property (nonatomic, assign) double start;
/**持续时间*/
@property (nonatomic, assign) double duration;
@end

NS_ASSUME_NONNULL_END
