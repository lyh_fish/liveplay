//
//  LyricsTool.m
//  KTV
//
//  Created by lyh on 2019/1/7.
//  Copyright © 2019 Sinopec. All rights reserved.
//

#import "LyricsTool.h"
#import "LyricModel.h"
#import <UIKit/UIKit.h>

@implementation LyricsTool

- (void)lyricAnalyzeWithLyric:(NSString *)lyric{
    NSMutableArray *tempArr = [NSMutableArray array];
    //这里先将每句歌词分割
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *arr = [lyric componentsSeparatedByString:@"\n"];
        for (int i = 0; i < arr.count; i++) {
            NSString *lrc = arr[i];
            //如果该行为空不继续解析
            if ([self IsNilWithStr:lrc]) {continue;}
            //开始解析（这里只解析时间信息，不解析音频头部信息，如：ar：ti：等）
            NSArray *lineArray = [NSArray array];
            if ([lrc rangeOfString:@"]"].location != NSNotFound) {
                lineArray = [lrc componentsSeparatedByString:@"]"];
                if (lineArray.count > 2) {//多个时间
//                    NSMutableArray *tempTimeArray = [NSMutableArray array];
                    for (int j = 0; j < lineArray.count-1; j++) {
                        CGFloat seconds = [self getLyricTimeWithTimeStr:lineArray[j]];
                        NSString *focustime = [self getFocusLyricTimeWithTimeStr:lineArray[j]];
                        LyricModel *model = [[LyricModel alloc] init];
                        model.lyric = lineArray.lastObject;
                        model.start = seconds;
                        model.focustime = focustime;
                        [tempArr addObject:model];
                        
//                        if (seconds >= 0) {
//                            [tempTimeArray addObject:[NSNumber numberWithFloat:seconds]];
//                        }
                    }
//                    if (tempTimeArray.count > 0) {
//                        for (NSNumber *number in tempTimeArray) {
//                            LyricModel *model = [[LyricModel alloc] init];
//                            model.lyric = lineArray.lastObject;
//                            model.start = [number intValue];
//                            [tempArr addObject:model];
//                        }
//                    }
                }else{//单个时间
                    CGFloat seconds = [self getLyricTimeWithTimeStr:lineArray.firstObject];
                    NSString *focustime = [self getFocusLyricTimeWithTimeStr:lineArray.firstObject];
                    if (seconds >= 0) {
                        LyricModel *model = [[LyricModel alloc] init];
                        model.lyric = lineArray.lastObject;
                        model.start = (double)seconds;
                        model.focustime = focustime;
                        [tempArr addObject:model];
                    }
                }
            }
        }
        /// 排序
        [tempArr sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            LyricModel *model1 = obj1;
            LyricModel *model2 = obj2;
            return model1.start > model2.start;
        }];
        
        for (int i=0; i< tempArr.count-1; i++) {
            LyricModel *model1 = tempArr[i];
            LyricModel *model2 = tempArr[i+1];
            model1.duration = model2.start - model1.start;
        }
        
        if (self.analyzeBlock) {
            self.analyzeBlock(tempArr);
        }
    });
}

/**时间转换*/
- (CGFloat)getLyricTimeWithTimeStr:(NSString *)timeStr{
    if ([self isHasLetterWithStr:timeStr]) {
        return -1;
    }
    if ([timeStr rangeOfString:@"["].location != NSNotFound) {
        timeStr = [timeStr componentsSeparatedByString:@"["].lastObject;
        timeStr = [self removeNilWithStr:timeStr];
    }
    //时间转换成秒
    CGFloat second = -1.0;
    //[00:00.00]和[00:00:00]（分钟:秒.毫秒）
    if (timeStr.length == 8) {
        NSString *str = [timeStr substringWithRange:NSMakeRange(5, 1)];
        if ([str isEqualToString:@":"]) {
            timeStr = [timeStr stringByReplacingOccurrencesOfString:@":" withString:@"." options:(NSAnchoredSearch) range:(NSMakeRange(5, 1))];
        }
        NSString *minutes = [timeStr substringWithRange:NSMakeRange(0, 2)];
        NSString *seconds = [timeStr substringWithRange:NSMakeRange(3, 2)];
        NSString *msec = [timeStr substringWithRange:NSMakeRange(6, 2)];
        second = minutes.floatValue*60 + seconds.floatValue + msec.floatValue/1000;
    }
    //[00:00]（分钟:秒）
    if (timeStr.length == 6) {
        NSString *minutes = [timeStr substringWithRange:NSMakeRange(0, 2)];
        NSString *seconds = [timeStr substringWithRange:NSMakeRange(3, 2)];
        second = minutes.floatValue*60 + seconds.floatValue;
    }
    return second;
}

/**焦点时间转换*/
- (NSString *)getFocusLyricTimeWithTimeStr:(NSString *)timeStr{
    if ([self isHasLetterWithStr:timeStr]) {
        return @"";
    }
    if ([timeStr rangeOfString:@"["].location != NSNotFound) {
        timeStr = [timeStr componentsSeparatedByString:@"["].lastObject;
        timeStr = [self removeNilWithStr:timeStr];
    }
    //时间转换成秒
    NSString *time = @"";
    //[00:00.00]和[00:00:00]（分钟:秒.毫秒）
    if (timeStr.length == 8) {
        NSString *str = [timeStr substringWithRange:NSMakeRange(5, 1)];
        if ([str isEqualToString:@":"]) {
            timeStr = [timeStr stringByReplacingOccurrencesOfString:@":" withString:@"." options:(NSAnchoredSearch) range:(NSMakeRange(5, 1))];
        }
        NSString *minutes = [timeStr substringWithRange:NSMakeRange(0, 2)];
        NSString *seconds = [timeStr substringWithRange:NSMakeRange(3, 2)];
      ///  NSString *msec = [timeStr substringWithRange:NSMakeRange(6, 2)];
        time = [NSString stringWithFormat:@"%@:%@", minutes,seconds];
    }
    //[00:00]（分钟:秒）
    if (timeStr.length == 6) {
        time = timeStr;
    }
    return time;
}

/**判断是否为空*/
- (BOOL)IsNilWithStr:(NSString *)str{
    NSString *string = [self removeNilWithStr:str];
    if (string.length == 0)
        return YES;
    return NO;
}

/**是否包含字母*/
- (BOOL)isHasLetterWithStr:(NSString *)str{
    NSRegularExpression *numberRegular = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSInteger count = [numberRegular numberOfMatchesInString:str options:NSMatchingReportProgress range:NSMakeRange(0, str.length)];
    if (count > 0) {
        return YES;
    }
    return NO;
}

//字符串去空字符
- (NSString *)removeNilWithStr:(NSString *)str{
    NSString *string = [NSString stringWithFormat:@"%@",str];
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""];
}

@end
