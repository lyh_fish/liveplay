//
//  LyricsFocusView.h
//  KTV
//
//  Created by lyh on 2019/1/7.
//  Copyright © 2019 Sinopec. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricsFocusView : UIView
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

NS_ASSUME_NONNULL_END
