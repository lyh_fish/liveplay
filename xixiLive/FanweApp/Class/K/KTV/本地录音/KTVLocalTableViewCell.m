//
//  KTVLocalTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLocalTableViewCell.h"

@implementation KTVLocalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.uploadButton.layer.borderWidth = 0.5;
    self.uploadButton.layer.cornerRadius = CGRectGetHeight(self.uploadButton.frame)/2;
}

- (IBAction)uploadButtonAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(uploadWithSong:)]) {
        [self.delegate uploadWithSong:self.song];
    }
}

- (void)setSong:(KTVSong *)song {
    _song = song;
    
    self.musicLabel.text = song.title;
    self.timeLabel.text = [song.local_time timeTextOfDate];
    
    if (song.isRelease) {
        [self.uploadButton setTitle:@"已发布" forState:UIControlStateNormal];
        [self.uploadButton setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        self.uploadButton.layer.borderColor = self.uploadButton.titleLabel.textColor.CGColor;
    }else {
        [self.uploadButton setTitle:@"发布" forState:UIControlStateNormal];
        [self.uploadButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.uploadButton.layer.borderColor = self.uploadButton.titleLabel.textColor.CGColor;
    }
    self.durationLabel.text = [self toMusicDurationWithDuration:song.duration];
}

- (NSString *)toMusicDurationWithDuration:(double)duration {
    int time = (int)duration;
    int m = time%3600/60;
    int s = time%60;
    NSString *mStr;
    NSString *sstr;
    if (m < 10) {
        mStr = [NSString stringWithFormat:@"0%d",time%3600/60];
    }else {
        mStr = [NSString stringWithFormat:@"%d",time%3600/60];
    }
    
    if (s < 10) {
        sstr = [NSString stringWithFormat:@"0%d",time%60];
    }else {
        sstr = [NSString stringWithFormat:@"%d",time%60];
    }
    return [NSString stringWithFormat:@"%@分%@秒",mStr,sstr];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
