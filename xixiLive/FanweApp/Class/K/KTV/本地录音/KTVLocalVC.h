//
//  KTVLocalVC.h
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseViewController.h"
#import "KTVSong.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVLocalVC : FWBaseViewController

@property (nonatomic,strong) KTVSong *song;

@end

NS_ASSUME_NONNULL_END
