//
//  KTVLocalTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVSong.h"

@protocol KTVLocalTableViewCellDelegate <NSObject>

- (void)uploadWithSong:(KTVSong *)song;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVLocalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *musicLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

@property (nonatomic, weak) id <KTVLocalTableViewCellDelegate> delegate;

@property (nonatomic, strong) KTVSong *song;

@end

NS_ASSUME_NONNULL_END
