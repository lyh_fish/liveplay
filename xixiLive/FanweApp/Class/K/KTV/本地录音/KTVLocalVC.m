//
//  KTVLocalVC.m
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLocalVC.h"
#import "KTVLocalTableViewCell.h"
#import "KTVSong.h"
#import "KTVUploadVC.h"
#import <Realm/Realm.h>
#import "KTVHomePlayController.h"

@interface KTVLocalVC ()<UITableViewDelegate,UITableViewDataSource,KTVLocalTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *musics;

@property RLMResults *results;

@end

@implementation KTVLocalVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.results = [[KTVSong allObjects] sortedResultsUsingKeyPath:@"local_time" ascending:false];
    
    self.title = [NSString stringWithFormat:@"本地录音(%ld)",self.results.count];
    
    for (KTVSong *song in self.results) {
        GKMusic *music = [[GKMusic alloc] init];
        music.title = song.title;
        music.subtitle = song.title;
        music.song_path = song.local_path;
        music.lyric_path = song.lyrics_path;
        music.other = song;
        [self.musics addObject:music];
    }
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"KTVLocalTableViewCell" bundle:nil] forCellReuseIdentifier:@"KTVLocalTableViewCell"];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.width.top.bottom.equalTo(self.view);
    }];
    
    [self setupBackBtnWithBlock:^{
        if (self.song) {
            [self showBackAlert];
        }else {
            [self pop];
        }
    }];
    
    if (self.song) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
       {
           KTVUploadVC *vc = [[KTVUploadVC alloc] init];
           vc.song = weakSelf.song;
           vc.backRoot = true;
           [[AppDelegate sharedAppDelegate] pushViewController:vc withBackTitle:nil];
       });
    }
    // Do any additional setup after loading the view.
}

- (void)showBackAlert {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"作品未发布,确定退出吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self pop];
    }];
    [alertVC addAction:action1];
    [alertVC addAction:action2];
    [self presentViewController:alertVC animated:true completion:^{
        
    }];
}

-  (void)pop {
    [[GKMusicTool sharedInstance] stopMusic];
    if (self.song) {
        [[AppDelegate sharedAppDelegate] popToRootViewController];
    }else {
        [[AppDelegate sharedAppDelegate] popViewController];
    }
}

#pragma mark ----- UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVLocalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KTVLocalTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.song = self.results[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.results.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    KTVHomePlayController *homePlayVC = [[KTVHomePlayController alloc] init];
    homePlayVC.music = self.musics[indexPath.row];
    [[AppDelegate sharedAppDelegate] pushViewController:homePlayVC];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定删除该作品吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES)lastObject];
            KTVSong *song = self.results[indexPath.row];
            NSString *filePath = [NSString stringWithFormat:@"%@/%@",documentsPath,song.local_path];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:filePath]) {
                BOOL isSuccess = [fileManager removeItemAtPath:filePath error:nil];
                if (isSuccess) {
                    NSLog(@"文件删除成功");
                }else {
                    NSLog(@"文件删除失败");
                }
            }
            [[RLMRealm defaultRealm] transactionWithBlock:^{
                [[RLMRealm defaultRealm] deleteObject:song];
            }];
            [tableView reloadData];
            self.title = [NSString stringWithFormat:@"本地录音(%ld)",self.results.count];
        }];
        [alertVC addAction:action1];
        [alertVC addAction:action2];
        [self presentViewController:alertVC animated:true completion:^{
            
        }];
    }
}

#pragma mark ----- KTVLocalTableViewCellDelegate

- (void)uploadWithSong:(KTVSong *)song {
    if (!song.isRelease) {
        KTVUploadVC *vc = [[KTVUploadVC alloc] init];
        vc.backRoot = false;
        vc.song = song;
        [[AppDelegate sharedAppDelegate] pushViewController:vc withBackTitle:nil];
    }
}

- (void)uploadWithKTVLocalTableViewCell:(KTVLocalTableViewCell *)cell {
   
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)musics {
    if (!_musics) {
        _musics = [NSMutableArray array];
    }
    return _musics;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
