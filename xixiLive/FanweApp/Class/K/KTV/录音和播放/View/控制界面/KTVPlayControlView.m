//
//  GKWYMusicControlView.m
//  GKWYMusic
//
//  Created by gaokun on 2018/4/20.
//  Copyright © 2018年 gaokun. All rights reserved.
//

#import "KTVPlayControlView.h"

@interface KTVPlayControlView()<GKSliderViewDelegate>


@property (nonatomic, strong) UIButton  *playBtn;

@property (nonatomic, strong) UILabel   *timeLabel;

@end

@implementation KTVPlayControlView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        
        // 滑杆
        [self addSubview:self.slider];
        [self addSubview:self.timeLabel];
        [self addSubview:self.playBtn];
        
        // 底部
        [self addSubview:self.playBtn];
        
        [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(0);
            make.centerY.equalTo(self);
            make.height.equalTo(self);
            make.width.equalTo(self.playBtn.mas_height);
        }];
        
        [self.slider mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.playBtn.mas_right);
            make.width.equalTo(@(kScreenW - 40 -100));
            make.height.equalTo(@(30));
        }];
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.slider.mas_right);
            make.height.equalTo(self.slider);
        }];
        
        [self initialData];
    }
    return self;
}

- (void)setProgress:(float)progress {
    _progress = progress;
    
    if (progress < 0) {
        progress = 0;
    }
    
    self.slider.value = progress;
    
    [self.slider layoutIfNeeded];
}


- (void)updateTime {
     self.timeLabel.text = [NSString stringWithFormat:@"%@/%@",[self toMusicDurationWithDuration:self.currentTime],[self toMusicDurationWithDuration:self.totalTime]];
}

#pragma mark - Public Methods
- (void)initialData {
    self.progress       = 0;
    [self updateTime];
}

- (void)setupPlayBtn {
    [self.playBtn setImage:[UIImage imageNamed:@"KTV_recorder_pause"] forState:UIControlStateNormal];
}

- (void)setupPauseBtn {
    [self.playBtn setImage:[UIImage imageNamed:@"KTV_recorder_play"] forState:UIControlStateNormal];
}

#pragma mark - UserAction

- (void)playBtnClick:(id)sender {
    self.playBtn.selected = !self.playBtn.selected;
    if (self.playBtn.selected) {
        [self setupPlayBtn];
    }else {
        [self setupPauseBtn];
    }
    
    if ([self.delegate respondsToSelector:@selector(controlView:didClickPlay:)]) {
        [self.delegate controlView:self didClickPlay:sender];
    }
}

#pragma mark - GKSliderViewDelegate
- (void)sliderTouchBegin:(float)value {
    if ([self.delegate respondsToSelector:@selector(controlView:didSliderTouchBegan:)]) {
        [self.delegate controlView:self didSliderTouchBegan:value];
    }
}

- (void)sliderTouchEnded:(float)value {
    if ([self.delegate respondsToSelector:@selector(controlView:didSliderTouchEnded:)]) {
        [self.delegate controlView:self didSliderTouchEnded:value];
    }
}

- (void)sliderTapped:(float)value {
    if ([self.delegate respondsToSelector:@selector(controlView:didSliderTapped:)]) {
        [self.delegate controlView:self didSliderTapped:value];
    }
}

- (void)sliderValueChanged:(float)value {
    if ([self.delegate respondsToSelector:@selector(controlView:didSliderValueChange:)]) {
        [self.delegate controlView:self didSliderValueChange:value];
    }
}

#pragma mark - 转换时间方法

- (NSString *)toMusicDurationWithDuration:(double)duration {
    int time = (int)duration;
    int m = time%3600/60;
    int s = time%60;
    NSString *mStr;
    NSString *sstr;
    if (m < 10) {
        mStr = [NSString stringWithFormat:@"0%d",time%3600/60];
    }else {
        mStr = [NSString stringWithFormat:@"%d",time%3600/60];
    }
    
    if (s < 10) {
        sstr = [NSString stringWithFormat:@"0%d",time%60];
    }else {
        sstr = [NSString stringWithFormat:@"%d",time%60];
    }
    return [NSString stringWithFormat:@"%@:%@",mStr,sstr];
}

#pragma mark - 懒加载

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton new];
        [_playBtn setImage:[UIImage imageNamed:@"cm2_fm_btn_play"] forState:UIControlStateNormal];
        [_playBtn setImage:[UIImage imageNamed:@"cm2_fm_btn_play_prs"] forState:UIControlStateHighlighted];
        [_playBtn addTarget:self action:@selector(playBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:16.0];
    }
    return _timeLabel;
}

- (GKSliderView *)slider {
    if (!_slider) {
        _slider = [GKSliderView new];
        [_slider setBackgroundImage:[UIImage imageNamed:@"cm2_fm_playbar_btn"] forState:UIControlStateNormal];
        [_slider setBackgroundImage:[UIImage imageNamed:@"cm2_fm_playbar_btn"] forState:UIControlStateSelected];
        [_slider setBackgroundImage:[UIImage imageNamed:@"cm2_fm_playbar_btn"] forState:UIControlStateHighlighted];
        
        [_slider setThumbImage:[UIImage imageNamed:@"cm2_fm_playbar_btn_dot"] forState:UIControlStateNormal];
        [_slider setThumbImage:[UIImage imageNamed:@"cm2_fm_playbar_btn_dot"] forState:UIControlStateSelected];
        [_slider setThumbImage:[UIImage imageNamed:@"cm2_fm_playbar_btn_dot"] forState:UIControlStateHighlighted];
        _slider.maximumTrackImage = [UIImage imageNamed:@"cm2_fm_playbar_bg"];
        _slider.minimumTrackImage = [UIImage imageNamed:@"cm2_fm_playbar_curr"];
        _slider.bufferTrackImage  = [UIImage imageNamed:@"cm2_fm_playbar_ready"];
        _slider.delegate = self;
        _slider.sliderHeight = 2;
    }
    return _slider;
}

@end
