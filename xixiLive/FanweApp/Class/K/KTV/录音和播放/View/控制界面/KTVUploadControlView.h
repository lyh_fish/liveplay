//
//  KTVUploadControlView.h
//  FanweApp
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class KTVUploadControlView;
@protocol KTVUploadControlViewDelegate <NSObject>

- (void)restKtvWithKTVUploadControlView:(KTVUploadControlView *)controlView;

- (void)uploadWithKTVUploadControlView:(KTVUploadControlView *)controlView;

- (void)saveWithKTVUploadControlView:(KTVUploadControlView *)controlView;

@end

@interface KTVUploadControlView : UIView

@property (nonatomic, weak) id<KTVUploadControlViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *uploadButton;


@end

NS_ASSUME_NONNULL_END
