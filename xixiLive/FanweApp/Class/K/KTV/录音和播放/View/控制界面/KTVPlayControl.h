//
//  KTVPlayControl.h
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KTVPlayControl;
@protocol KTVPlayControlDelegate <NSObject>

- (void)pauseWithKTVPlayControl:(KTVPlayControl *)Control;

- (void)playWithKTVPlayControl:(KTVPlayControl *)Control;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVPlayControl : UIView

@property (nonatomic,assign) double duration;

@property (nonatomic,assign) double value;

@property (nonatomic,assign) BOOL isPlay;

@property (weak, nonatomic) IBOutlet UIButton *stateButton;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (nonatomic, weak) id<KTVPlayControlDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
