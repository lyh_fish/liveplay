//
//  GKWYMusicControlView.h
//  GKWYMusic
//
//  Created by gaokun on 2018/4/20.
//  Copyright © 2018年 gaokun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKSliderView.h"

@class KTVPlayControlView;

@protocol KTVPlayControlViewDelegate <NSObject>

// 按钮点击
- (void)controlView:(KTVPlayControlView *)controlView didClickPlay:(UIButton *)playBtn;

// 滑杆滑动及点击
- (void)controlView:(KTVPlayControlView *)controlView didSliderTouchBegan:(float)value;
- (void)controlView:(KTVPlayControlView *)controlView didSliderTouchEnded:(float)value;
- (void)controlView:(KTVPlayControlView *)controlView didSliderValueChange:(float)value;
- (void)controlView:(KTVPlayControlView *)controlView didSliderTapped:(float)value;

@end

@interface KTVPlayControlView : UIView

@property (nonatomic, weak) id<KTVPlayControlViewDelegate>    delegate;
// slider
@property (nonatomic, strong) GKSliderView                      *slider;

@property (nonatomic, assign) double                            currentTime;
@property (nonatomic, assign) double                            totalTime;
@property (nonatomic, assign) float                             progress;

- (void)updateTime;

- (void)setupPlayBtn;
- (void)setupPauseBtn;

@end
