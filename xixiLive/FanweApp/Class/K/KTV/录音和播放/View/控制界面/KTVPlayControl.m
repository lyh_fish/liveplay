//
//  KTVPlayControl.m
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPlayControl.h"

@implementation KTVPlayControl

- (void)awakeFromNib {
    [super awakeFromNib];
    self.slider.continuous = false;
    self.slider.thumbTintColor = [UIColor clearColor];
}

- (void)setDuration:(double)duration {
    self.slider.minimumValue = 0;
    self.slider.maximumValue = duration;
}

- (void)setValue:(double)value {
    _value = value;
    self.slider.value = (float)value;
}

- (IBAction)changeStateAction:(UIButton *)sender {
    if (self.isPlay) {
        if ([self.delegate respondsToSelector:@selector(pauseWithKTVPlayControl:)]) {
            [self.delegate pauseWithKTVPlayControl:self];
        }
    }else {
        if ([self.delegate respondsToSelector:@selector(playWithKTVPlayControl:)]) {
            [self.delegate playWithKTVPlayControl:self];
        }
    }
}

- (IBAction)sliderValueChange:(UISlider *)sender {
    
}

- (void)setIsPlay:(BOOL)isPlay {
    _isPlay = isPlay;
    if (isPlay) {
        [self.stateButton setImage:[UIImage imageNamed:@"KTV_recorder_pause"] forState:UIControlStateNormal];
    }else {
        [self.stateButton setImage:[UIImage imageNamed:@"KTV_recorder_play"] forState:UIControlStateNormal];
    }
}


@end
