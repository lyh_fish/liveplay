//
//  KTVUploadControlView.m
//  FanweApp
//
//  Created by lyh on 2019/1/26.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVUploadControlView.h"

@implementation KTVUploadControlView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.uploadButton.layer.cornerRadius = CGRectGetHeight(self.uploadButton.frame)/2;
    self.uploadButton.layer.borderWidth = 0.5;
    self.uploadButton.layer.borderColor = self.uploadButton.titleLabel.textColor.CGColor;
}

- (IBAction)resetButtonAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(restKtvWithKTVUploadControlView:)]) {
        [self.delegate restKtvWithKTVUploadControlView:self];
    }
}

- (IBAction)uploadButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(uploadWithKTVUploadControlView:)]) {
        [self.delegate uploadWithKTVUploadControlView:self];
    }
}

- (IBAction)saveButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(saveWithKTVUploadControlView:)]) {
        [self.delegate saveWithKTVUploadControlView:self];
    }
    
}




@end
