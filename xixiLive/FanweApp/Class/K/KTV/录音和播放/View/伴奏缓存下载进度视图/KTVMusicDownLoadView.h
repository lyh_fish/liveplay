//
//  KTVMusicDownLoadView.h
//  FanweApp
//
//  Created by lyh on 2019/2/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZWMusicDownLoadView.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVMusicDownLoadView : UIView

- (void)start;

- (void)updateProgress:(CGFloat)progress;
@end

NS_ASSUME_NONNULL_END
