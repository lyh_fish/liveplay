//
//  KTVMusicDownLoadView.m
//  FanweApp
//
//  Created by lyh on 2019/2/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVMusicDownLoadView.h"

@interface KTVMusicDownLoadView()

@property (weak, nonatomic) IBOutlet UIView *downLoadSubView;

@property (strong, nonatomic) ZWMusicDownLoadView *downLoadView;

@end

@implementation KTVMusicDownLoadView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ([super initWithCoder:aDecoder]) {
    
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.downLoadView == nil) {
        self.downLoadView =[[ZWMusicDownLoadView alloc]initWithFrame:self.downLoadSubView.bounds];
        [self.downLoadSubView addSubview:_downLoadView];
        self.downLoadView.musicalColor = KtvColor;
        self.backgroundColor = [UIColor clearColor];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
   
}

- (void)start {
    [self.downLoadView startGlowing];
}

- (void)updateProgress:(CGFloat)progress {
    self.downLoadView.musicalProgress = progress;
    if (progress == 1) {
        [self.downLoadView stopGlowing];
    }
}

@end
