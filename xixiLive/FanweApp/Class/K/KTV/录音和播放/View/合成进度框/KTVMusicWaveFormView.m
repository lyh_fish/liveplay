//
//  KTVMusicWaveFormView.m
//  YJMusicWaveView
//
//  Created by lyh on 2019/2/16.
//  Copyright © 2019 YangJing. All rights reserved.
//

#import "KTVMusicWaveFormView.h"

@interface KTVMusicWaveFormView ()

@end

@implementation KTVMusicWaveFormView {
    CALayer *maskLayer;
    UIImageView *picImageView;
    UILabel *tipLabel;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configSubview];
    }
    return self;
}

- (void)configSubview {
    
    self.backgroundColor = [UIColor colorWithWhite:100/255.0 alpha:0.3];
    self.layer.cornerRadius = 8;
   
    picImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KTV_wave"]];
    picImageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 20);
    [self addSubview:picImageView];
    
    tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, CGRectGetWidth(self.frame), 20)];
    tipLabel.text = @"正在合成中。。。";
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.font = [UIFont systemFontOfSize:14];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:tipLabel];
    
    maskLayer = [CALayer layer];
    maskLayer.anchorPoint = CGPointMake(0, 0.5);//注意，按默认的anchorPoint，width动画是同时像左右扩展的
    maskLayer.position = CGPointMake(0, CGRectGetHeight(self.bounds)/2);
    maskLayer.bounds = CGRectMake(0, 0, 0, CGRectGetHeight(self.bounds));
    maskLayer.backgroundColor = [UIColor whiteColor].CGColor;
    picImageView.layer.mask = maskLayer;
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    animation.values = @[@(0),@(picImageView.frame.size.width)];
    animation.duration = 1.0f;
    animation.calculationMode = kCAAnimationLinear;
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount = HUGE_VALF; //无限循环
    animation.removedOnCompletion = NO;
    [maskLayer addAnimation:animation forKey:@"kLyrcisAnimation"];
}

@end
