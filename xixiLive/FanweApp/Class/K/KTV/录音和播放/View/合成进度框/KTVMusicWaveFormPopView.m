//
//  MMDateView.m
//  MMPopupView
//
//  Created by Ralph Li on 9/7/15.
//  Copyright © 2015 LJC. All rights reserved.
//

#import "KTVMusicWaveFormPopView.h"
#import "KTVMusicWaveFormView.h"

@interface KTVMusicWaveFormPopView()

@end

@implementation KTVMusicWaveFormPopView


- (instancetype)init
{
    self = [super init];
    
    if ( self )
    {
        self.type = MMPopupTypeCustom;
        KTVMusicWaveFormView *waveView = [[KTVMusicWaveFormView alloc] initWithFrame:CGRectMake(0, 0, kScreenW/2.5, 60)];
        waveView.center = self.center;
        [self addSubview:waveView];
    }
    
    return self;
}

- (void)actionHide
{
    [self hide];
}

@end
