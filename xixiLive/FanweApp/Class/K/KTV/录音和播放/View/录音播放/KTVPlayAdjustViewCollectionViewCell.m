//
//  KTVPlayAdjustViewCollectionViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPlayAdjustViewCollectionViewCell.h"

@implementation KTVPlayAdjustViewCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.typeImageView.layer.cornerRadius = CGRectGetHeight(self.typeImageView.frame)/2;
    self.typeImageView.clipsToBounds = YES;
    self.maskView.backgroundColor = KtvColor;
    self.maskView.layer.cornerRadius = CGRectGetHeight(self.maskView.frame)/2;
    self.maskView.clipsToBounds = YES;
}

- (void)updateUIWithModel:(KTVPlayAdjustTypeModel *)model {
    if (model.isSelect) {
        self.typelabel.textColor = KtvColor;
        self.typeImageView.layer.borderWidth = 0.5;
        self.typeImageView.layer.borderColor = KtvColor.CGColor;
    }else {
        self.typelabel.textColor = [UIColor lightTextColor];
        self.typeImageView.layer.borderWidth = 0;
    }
    self.typelabel.text = model.type;
    self.typeImageView.image = [UIImage imageNamed:model.typeImageName];
    self.maskView.hidden = !model.isSelect;
}

@end

@implementation KTVPlayAdjustTypeModel


@end



