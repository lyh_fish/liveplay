//
//  KTVPlayAdjustView.m
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPlayAdjustView.h"
#import "KTVPlayAdjustViewCollectionViewCell.h"
#import "KTVControlPitchView.h"

@interface KTVPlayAdjustView()

@property (nonatomic, strong) NSArray *typeArr;

@property (nonatomic, strong) KTVPlayAdjustTypeModel *noewModel;

@end

@implementation KTVPlayAdjustView


- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"KTVPlayAdjustViewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView  reloadData];
    
    self.drywetButton.layer.cornerRadius = CGRectGetHeight(self.drywetButton.frame)/2;
    self.drywetButton.clipsToBounds = YES;
}

/// 音效和音色切换
- (IBAction)segmentedControlValueChange:(UISegmentedControl *)sender {
    self.view1.hidden = sender.selectedSegmentIndex != 0;
    self.view2.hidden = sender.selectedSegmentIndex == 0;
}

/// 混响还原
- (IBAction)drywetButtonAction:(UIButton *)sender {
    
}

/// 混响调节
- (IBAction)drywetSliderValueChange:(UISlider *)sender {
    if ([self.delegate respondsToSelector:@selector(drywetSelectWithValue:)]) {
        [self.delegate drywetSelectWithValue:(double)sender.value];
    }
    self.drywetLabel.text = [NSString stringWithFormat:@"%.2f",sender.value];
}

/// 人声声音调节
- (IBAction)soundSliderValueChange:(UISlider *)sender {
    if ([self.delegate respondsToSelector:@selector(soundWithValue:)]) {
        [self.delegate soundWithValue:(double)sender.value];
    }
    if (sender.value>0) {
        self.soundTipLabel.text = [NSString stringWithFormat:@"人声向前移动%.f毫秒",sender.value*1000];
    }else {
        self.soundTipLabel.text = [NSString stringWithFormat:@"人声向后移动%.f毫秒",sender.value*1000];
    }
    self.soundTipLabel.hidden = sender.value == 0;
}

/// 人声声音调节
- (IBAction)manSliderValueChange:(UISlider *)sender {
    if ([self.delegate respondsToSelector:@selector(manVolumeWithValue:)]) {
        [self.delegate manVolumeWithValue:(double)sender.value];
    }
    self.manLabel.text = [NSString stringWithFormat:@"%.2f",sender.value];
}

/// 伴奏声音调节
- (IBAction)AccompanimentSliderValueChange:(UISlider *)sender {
    if ([self.delegate respondsToSelector:@selector(accompanimentVolumeWithValue:)]) {
        [self.delegate accompanimentVolumeWithValue:(double)sender.value];
    }
    self.accompanimentLabel.text = [NSString stringWithFormat:@"%.2f",sender.value];
}

/// 降噪
- (IBAction)noiseSwitch:(UISwitch *)sender {
    
}

#pragma mark ---- UICollectionViewDelegate,UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.typeArr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KTVPlayAdjustViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell updateUIWithModel:self.typeArr[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    self.noewModel.isSelect  = NO;
    KTVPlayAdjustTypeModel *model = self.typeArr[indexPath.row];
    model.isSelect = YES;
    self.noewModel = model;
    [self.collectionView reloadData];
    
    self.drywetSlider.value = (float)model.value;
    self.drywetLabel.text = [NSString stringWithFormat:@"%.2f",model.value];

    if ([self.delegate respondsToSelector:@selector(drywetSelectWithValue:)]) {
        [self.delegate drywetSelectWithValue:model.value];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50, 75);
}


- (NSArray *)typeArr {
    if (!_typeArr) {
        NSMutableArray *arr = [NSMutableArray array];
        NSArray *names = [NSArray arrayWithObjects:@"原声",@"流行",@"新世纪",@"民谣",@"R&B",@"舞曲",@"现场",@"摇滚",@"魔音",@"复古", nil];
        NSArray *images = [NSArray arrayWithObjects:@"KTV_1",@"KTV_2",@"KTV_3",@"KTV_4",@"KTV_5",@"KTV_6",@"KTV_7",@"KTV_8",@"KTV_9",@"KTV_10", nil];
        NSArray *values = [NSArray arrayWithObjects:@(0.0),@(0.53),@(0.57),@(0.76),@(0.65),@(0.83),@(1.00),@(0.85),@(0.51),@(0.49), nil];
        
        for (int i=0; i<names.count; i++) {
            KTVPlayAdjustTypeModel *model = [[KTVPlayAdjustTypeModel alloc] init];
            model.typeImageName = images[i];
            model.type = names[i];
            model.value = [values[i] doubleValue];
            [arr addObject:model];
        }
        _typeArr = [NSArray arrayWithArray:arr];
    }
    return _typeArr;
}

@end
