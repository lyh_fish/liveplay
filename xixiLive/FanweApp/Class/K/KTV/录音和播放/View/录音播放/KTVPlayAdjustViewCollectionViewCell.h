//
//  KTVPlayAdjustViewCollectionViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVPlayAdjustTypeModel : NSObject

@property (nonatomic, assign) BOOL isSelect;

@property (nonatomic, copy) NSString *typeImageName;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, assign) double value;



@end


@interface KTVPlayAdjustViewCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;

@property (weak, nonatomic) IBOutlet UILabel *typelabel;

@property (nonatomic, assign) BOOL isSelect;

@property (weak, nonatomic) IBOutlet UIView *maskView;

- (void)updateUIWithModel:(KTVPlayAdjustTypeModel *)model;

@end

NS_ASSUME_NONNULL_END


