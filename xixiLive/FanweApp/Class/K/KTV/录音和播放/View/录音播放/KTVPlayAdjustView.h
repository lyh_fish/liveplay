//
//  KTVPlayAdjustView.h
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KTVPlayAdjustViewDelegate <NSObject>

- (void)drywetSelectWithValue:(double)value;

- (void)manVolumeWithValue:(double)value;

- (void)accompanimentVolumeWithValue:(double)value;

- (void)soundWithValue:(double)value;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVPlayAdjustView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

/// 音效
@property (weak, nonatomic) IBOutlet UIView *view1;

@property (weak, nonatomic) IBOutlet UIButton *drywetButton;

@property (weak, nonatomic) IBOutlet UISlider *drywetSlider;

@property (weak, nonatomic) IBOutlet UILabel *drywetLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

/// 音色
@property (weak, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UILabel *soundTipLabel;
@property (weak, nonatomic) IBOutlet UISlider *soundSlider;

@property (weak, nonatomic) IBOutlet UISlider *manSlider;
@property (weak, nonatomic) IBOutlet UILabel *manLabel;

@property (weak, nonatomic) IBOutlet UISlider *accompanimentSlider;
@property (weak, nonatomic) IBOutlet UILabel *accompanimentLabel;

/// 音调调节
@property (weak, nonatomic) IBOutlet UIView *view3;


@property (nonatomic, weak) id<KTVPlayAdjustViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
