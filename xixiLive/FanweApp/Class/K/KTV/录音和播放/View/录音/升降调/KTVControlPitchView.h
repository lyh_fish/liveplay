//
//  KTVControlPitchView.h
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KTVControlPitchViewDlegate <NSObject>

- (void)pitchSelectWithValue:(double)value;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVControlPitchView : UIView
@property (unsafe_unretained, nonatomic) IBOutlet UIView *sliderSupView;

@property (nonatomic, weak) id <KTVControlPitchViewDlegate> delegate;

@end

NS_ASSUME_NONNULL_END
