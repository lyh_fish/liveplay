//
//  KTVAdjustView.h
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KTVAdjustViewDelegate <NSObject>

- (void)recorderVolumeValueChange:(double)value;

- (void)accompanimentVolumeValueChange:(double)value;

- (void)dryWetValueChange:(double)value;

- (void)pitchValueChange:(double)value;

@end

@interface KTVAdjustView : UIView

@property (weak, nonatomic) IBOutlet UIView *sliderView;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@property (weak, nonatomic) IBOutlet UIButton *volumeButton;
@property (weak, nonatomic) IBOutlet UIButton *dryWetButton;
@property (weak, nonatomic) IBOutlet UIButton *toneButton;
@property (weak, nonatomic) IBOutlet UIStackView *stackView1;
@property (weak, nonatomic) IBOutlet UIStackView *stackView2;
@property (weak, nonatomic) IBOutlet UIStackView *stackView3;

/// 人声
@property (weak, nonatomic) IBOutlet UILabel *recorderLabel;
@property (weak, nonatomic) IBOutlet UISlider *recorderSilder;
/// 伴奏
@property (weak, nonatomic) IBOutlet UISlider *accompanimentSlider;
@property (weak, nonatomic) IBOutlet UILabel *accompanimentLabel;
/// 混响
@property (weak, nonatomic) IBOutlet UISlider *dryWetSlider;
@property (weak, nonatomic) IBOutlet UILabel *dryWetLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (nonatomic, weak) id<KTVAdjustViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *pitchSupView;


@end

NS_ASSUME_NONNULL_END
