//
//  KTVControlPitchView.m
//  FanweApp
//
//  Created by lyh on 2019/1/16.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVControlPitchView.h"
#import "SEFilterControl.h"

@interface KTVControlPitchView()

@property (nonatomic, strong) NSArray *titles;

@end

@implementation KTVControlPitchView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    SEFilterControl *slider = [[SEFilterControl alloc]initWithFrame:CGRectMake(25, 5, self.sliderSupView.frame.size.width-50, 15) Titles:self.titles];
    [slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventTouchUpInside];
    [slider setProgressColor:[UIColor groupTableViewBackgroundColor]];//设置滑杆的颜色
    [slider setTopTitlesColor:[UIColor orangeColor]];//设置滑块上方字体颜色
    [slider setSelectedIndex:4];//设置当前选中
    [self.sliderSupView addSubview:slider];
}

- (void)sliderValueChange:(SEFilterControl *)sender {
    if ([self.delegate respondsToSelector:@selector(pitchSelectWithValue:)]) {
        double value = [self.titles[sender.SelectedIndex] doubleValue];
        [self.delegate pitchSelectWithValue:value*200];
    }
}

- (NSArray *)titles {
    if (!_titles) {
        _titles = [NSArray arrayWithArray:[NSArray arrayWithObjects:@"-4",@"-3", @"-2", @"-1", @"0",@"1",@"2", @"3", @"4",nil]];
    }
    return _titles;
    
}

@end
