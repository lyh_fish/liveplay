//
//  KTVRecorderControlView.h
//  FanweApp
//
//  Created by lyh on 2019/1/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>


/// 播放状态
typedef enum : NSUInteger {
    KTVRecorderControlViewPlayStatePlay,
    KTVRecorderControlViewPlayStatePause,
} KTVRecorderControlViewPlayState;

@class KTVRecorderControlView;

@protocol KTVRecorderControlViewDelegate <NSObject>
/// 导唱/伴奏
- (void)guideWithOpen:(BOOL)open;
/// 均衡器
- (void)equalizerShowAction;
/// 暂停和播放
- (void)playStateChangeWithState:(KTVRecorderControlViewPlayState)State;
/// 耳返
- (void)earbackWithOpen:(BOOL)open;
/// 重唱
- (void)resetWithControlView:(KTVRecorderControlView *_Nullable)controlView;


@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVRecorderControlView : UIView

/// 是否打开导唱
@property (nonatomic, assign) BOOL openguide;
/// 是否打开耳返
@property (nonatomic, assign) BOOL openearback;

@property (nonatomic, assign) KTVRecorderControlViewPlayState playState;

@property (nonatomic, weak) id <KTVRecorderControlViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
