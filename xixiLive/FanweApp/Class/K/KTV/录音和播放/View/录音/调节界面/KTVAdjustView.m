//
//  KTVAdjustView.m
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVAdjustView.h"
#import "KTVControlPitchView.h"

@interface KTVAdjustView ()<KTVControlPitchViewDlegate>
{
    CGFloat lastlocation;
}
@end

@implementation KTVAdjustView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.recorderLabel.text = @"100%";
    self.recorderSilder.value = 1;
    
    self.accompanimentLabel.text = @"50%";
    self.accompanimentSlider.value = 0.5;
    
    self.dryWetLabel.text = @"0%";
    self.dryWetSlider.value = 0;
    
    self.resetButton.layer.cornerRadius = CGRectGetHeight(self.resetButton.frame)/2;
    self.resetButton.layer.borderWidth = 0.5;
    self.resetButton.layer.borderColor = self.resetButton.titleLabel.textColor.CGColor;
    
    [self selectButton:self.volumeButton];
    lastlocation = self.volumeButton.center.x;
    
    KTVControlPitchView *pitchView = [[NSBundle mainBundle] loadNibNamed:@"KTVControlPitchView" owner:self options:nil].lastObject;
    pitchView.frame = self.pitchSupView.bounds;
    pitchView.delegate = self;
    [self.pitchSupView addSubview:pitchView];
}

- (IBAction)volumeButtonAction:(UIButton *)sender {
    [self selectButton:sender];
    [self moveToButton:sender];
}

- (IBAction)dryWetButtonAction:(UIButton *)sender {
    [self selectButton:sender];
    [self moveToButton:sender];
}

- (IBAction)toneButtonAction:(UIButton *)sender {
    [self selectButton:sender];
    [self moveToButton:sender];
    
}

- (void)selectButton:(UIButton *)sender {
    self.stackView1.hidden = sender != self.volumeButton;
    self.stackView2.hidden = sender != self.dryWetButton;
    self.stackView3.hidden = sender != self.toneButton;
    for (UIButton *button in self.buttons) {
        if (button == sender) {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else {
            [button setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        }
    }
}

- (void)moveToButton:(UIButton *)sender {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.x"];
    animation.fromValue = @(lastlocation);
    animation.toValue = @(sender.center.x);
    animation.duration = 0.5;
    animation.removedOnCompletion = NO;//yes的话，又返回原位置了。
    animation.repeatCount = 0;
    animation.fillMode = kCAFillModeForwards;
    [self.sliderView.layer addAnimation:animation forKey:@"move"];
    lastlocation = sender.center.x;
}

- (IBAction)recorderSliderValueChanged:(UISlider *)sender {
    _recorderLabel.text = [NSString stringWithFormat:@"%.f%%",sender.value*100];
    if ([self.delegate respondsToSelector:@selector(recorderVolumeValueChange:)]) {
        [self.delegate recorderVolumeValueChange:sender.value];
    }
}

- (IBAction)accompanimentSliderValueChanged:(UISlider *)sender {
    _accompanimentLabel.text = [NSString stringWithFormat:@"%.f%%",sender.value*100];
    if ([self.delegate respondsToSelector:@selector(accompanimentVolumeValueChange:)]) {
        [self.delegate accompanimentVolumeValueChange:sender.value];
    }
}

- (IBAction)dryWetSliderValueChanged:(UISlider *)sender {
    _dryWetLabel.text = [NSString stringWithFormat:@"%.f%%",sender.value*100];
    if ([self.delegate respondsToSelector:@selector(dryWetValueChange:)]) {
        [self.delegate dryWetValueChange:sender.value];
    }
}

/// 还原
- (IBAction)resetAction:(UIButton *)sender {
    
}

/// 音调调节
- (void)pitchSelectWithValue:(double)value {
    if ([self.delegate respondsToSelector:@selector(pitchValueChange:)]) {
        [self.delegate pitchValueChange:value];
    }
}

@end
