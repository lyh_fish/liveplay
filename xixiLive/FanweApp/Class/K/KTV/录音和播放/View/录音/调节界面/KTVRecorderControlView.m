//
//  KTVRecorderControlView.m
//  FanweApp
//
//  Created by lyh on 2019/1/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVRecorderControlView.h"

@interface KTVRecorderControlView()
@property (weak, nonatomic) IBOutlet UIImageView *earBackImageView;
@property (weak, nonatomic) IBOutlet UILabel *earBackLabel;

@property (weak, nonatomic) IBOutlet UIImageView *smileImageView;
@property (weak, nonatomic) IBOutlet UILabel *smileLabel;

@property (weak, nonatomic) IBOutlet UIButton *stateButton;

@end

@implementation KTVRecorderControlView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.stateButton.layer.cornerRadius = CGRectGetHeight(self.stateButton.frame)/2;
    self.stateButton.clipsToBounds = YES;
    
    self.openearback = true;
    self.openguide = true;
    
    self.playState = KTVRecorderControlViewPlayStatePause;
    
}

- (void)setOpenguide:(BOOL)openguide  {
    _openguide = openguide;
    self.smileLabel.text = openguide?@"伴奏":@"导唱";
    if (openguide) {
        self.smileLabel.textColor = [UIColor whiteColor];
        self.smileImageView.image = [UIImage imageNamed:@"KTV_smile"];
    }else {
        self.smileLabel.textColor = RGB(185, 64, 58);
        self.smileImageView.image = [UIImage imageNamed:@"KTV_smile_selected"];
    }
}

- (void)setOpenearback:(BOOL)openearback {
    _openearback = openearback;
    if (openearback) {
        self.earBackLabel.textColor = RGB(185, 64, 58);
        self.earBackImageView.image = [UIImage imageNamed:@"KTV_earback_selected"];
    }else {
        self.earBackLabel.textColor = [UIColor whiteColor];
        self.earBackImageView.image = [UIImage imageNamed:@"KTV_earback"];
    }
        
}

- (void)setPlayState:(KTVRecorderControlViewPlayState)playState {
    _playState = playState;
    switch (playState) {
        case KTVRecorderControlViewPlayStatePlay:
            [self.stateButton setImage:[UIImage imageNamed:@"KTV_pause"] forState:UIControlStateNormal];
            break;
        case KTVRecorderControlViewPlayStatePause:
            [self.stateButton setImage:[UIImage imageNamed:@"KTV_play"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}


- (IBAction)smileButtonAction:(UIButton *)sender {
    self.openguide = !self.openguide;
    if ([self.delegate respondsToSelector:@selector(guideWithOpen:)]) {
        [self.delegate guideWithOpen:self.openguide];
    }
}

- (IBAction)equalizerButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(equalizerShowAction)]) {
        [self.delegate equalizerShowAction];
    }
}


- (IBAction)playStateButtonAction:(UIButton *)sender {
    switch (self.playState) {
        case KTVRecorderControlViewPlayStatePlay:
            self.playState = KTVRecorderControlViewPlayStatePause;
            break;
        case KTVRecorderControlViewPlayStatePause:
            self.playState = KTVRecorderControlViewPlayStatePlay;
            break;
        default:
            break;
    }
    if ([self.delegate respondsToSelector:@selector(playStateChangeWithState:)]) {
        [self.delegate playStateChangeWithState:self.playState];
    }
}

- (IBAction)earbackButtonAction:(UIButton *)sender {
    self.openearback = !self.openearback;
    if ([self.delegate respondsToSelector:@selector(earbackWithOpen:)]) {
        [self.delegate earbackWithOpen:self.openearback];
    }
}


- (IBAction)resetButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(resetWithControlView:)]) {
        [self.delegate resetWithControlView:self];
    }
}

@end
