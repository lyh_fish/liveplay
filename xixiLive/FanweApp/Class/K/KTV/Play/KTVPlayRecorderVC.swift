//
//  KTVPlayRecorderVC.swift
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

import UIKit

class KTVPlayRecorderVC: UIViewController,KTVPlayAdjustViewDelegate,KTVUploadControlViewDelegate,KTVPlayRecorderDelegate,KTVPlayControlViewDelegate {
    
    var ktvPlay:KTVPlayRecorder!
    
    var controlView:KTVPlayControlView!
    
    @IBOutlet weak var controlSupView: UIView!
    
    @IBOutlet weak var adjustSupView: UIView!
    
    @IBOutlet weak var uploadSupView: UIView!
    
    @IBOutlet weak var musicLabel: UILabel!
    
    @objc var music:KTVMusicModel!
    
    var popView:KTVMusicWaveFormPopView!
    
    var mp3Path:String!
    
    let now = Date()
    
    var isDrage = false

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.musicLabel.text = music.title
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        let dataStr = dateFormatter.string(from: now)
        /// 路径中不能带.否则阿里云oss上传会提示文件不存在
//        mp3Path = NSHomeDirectory() + "/Documents/" + dataStr + self.music.id  + ".mp3"
        let docPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        mp3Path = docPath + "/" + dataStr + self.music.id  + ".mp3"
        print("渲染路径" + mp3Path)
        
        ktvPlay = KTVPlayRecorder.init()
        ktvPlay.delegate = self
        ktvPlay.play()
        
        addControlView()
        addAdjustView()
        adduploadView()
    }
    
    func pop() -> Void {
        ktvPlay.finish()
        if self.navigationController!.viewControllers.count > 2 {
            let count = self.navigationController!.viewControllers.count - 3
            let vc = self.navigationController!.viewControllers[count]
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
    @IBAction func BackButtonAction(_ sender: UIButton) {
        self.showAlert()
    }
    
    func showAlert() -> Void {
        let alert = UIAlertController.init(title: "提示", message: "当前演唱尚未完成,确定要退出吗", preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "否", style: .cancel) { (action) in
            
        }
        
        let action2 = UIAlertAction.init(title: "是", style: .destructive) { (action) in
            self.pop()
        }
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true) {
            
        }
    }
    
    /// KTVPlayRecorderDelegate
    func playStateChange(state: KTVPlayRecorderState) {
        if (state == .play) {
            controlView.setupPlayBtn()
        }else {
            controlView.setupPauseBtn()
        }
    }
    
    func updateTime(time: Double, duration: Double) {
        if !isDrage {
            controlView.currentTime = time
            controlView.totalTime = duration
            controlView.progress = Float(time / duration)
            controlView.updateTime()
        }
    }
    
    func renderComplete(duration: Double) {
        popView.hide()
        ktvPlay.finish()
        
        let mp3Arr = mp3Path.components(separatedBy: "/")
        
        let song = KTVSong()
        song.id = self.music.id
        song.title = self.music.title
        song.local_path = mp3Arr.last ?? ""
        song.original_sing_path = self.music.original_sing_path;
        song.accompaniment_path = self.music.accompaniment_path;
        song.lyrics_path = self.music.lyrics_path
        song.local_time = now
        song.duration = duration
    
        do {
            let realm = RLMRealm.default()
            realm.beginWriteTransaction()
            realm.add(song)
            try  realm.commitWriteTransaction()
        } catch  {
            
        }
    
        let nowSong = KTVSong.allObjects().lastObject() as! KTVSong
        
        let push = KTVPushUpload()
        push.pushKTVUploadVC(with: nowSong)
    }
    
    /// KTVPlayRecorderDelegate
    
    func controlView(_ controlView: KTVPlayControlView!, didClickPlay playBtn: UIButton!) {
        if ktvPlay.recorderPlayer.isPlaying {
           ktvPlay.pause()
        }else {
           ktvPlay.resume()
        }
    }
    
    func controlView(_ controlView: KTVPlayControlView!, didSliderTouchBegan value: Float) {
        isDrage = true
    }
    
    func controlView(_ controlView: KTVPlayControlView!, didSliderTouchEnded value: Float) {
        isDrage = false
        let time = Double(value) * ktvPlay.recorderPlayer.duration
        ktvPlay.playAt(time: time)
    }
    
    func controlView(_ controlView: KTVPlayControlView!, didSliderValueChange value: Float) {
        if !isDrage {
            let time = Double(value) * ktvPlay.recorderPlayer.duration
            controlView.currentTime = time
        }
    }
    
    func controlView(_ controlView: KTVPlayControlView!, didSliderTapped value: Float) {
        let time = Double(value) * ktvPlay.recorderPlayer.duration
        ktvPlay.playAt(time: time)
    }
    
    /// KTVPlayAdjustViewDelegate
    func drywetSelect(withValue value: Double) {
        print(value)
        ktvPlay.setDryWetMix(dryWet: value)
    }
    
    func manVolume(withValue value: Double) {
        ktvPlay.setManVolume(volume: value)
    }
    
    func accompanimentVolume(withValue value: Double) {
        ktvPlay.setAccompanimentVolume(volume: value)
    }
    
    func sound(withValue value: Double) {
        ktvPlay.setDelay(time: value)
    }
    
    /// KTVUploadControlViewDelegate
    func restKtv(with controlView: KTVUploadControlView) {
        
    }
    
    func upload(with controlView: KTVUploadControlView) {
        popView = KTVMusicWaveFormPopView()
        popView.show()
        ktvPlay.render(mp3: mp3Path)
    }
    
    func save(with controlView: KTVUploadControlView) {
        
    }
    
    func addControlView() -> Void {
        controlView = KTVPlayControlView.init(frame: self.controlSupView.bounds)
        controlView.delegate = self
        controlView.setupPlayBtn()
        controlView.currentTime = 0.0
        controlView.totalTime = ktvPlay.recorderPlayer.duration
        self.controlSupView.addSubview(controlView);
    }
    
    func addAdjustView() -> Void {
        let adjustView = Bundle.main.loadNibNamed("KTVPlayAdjustView", owner: self, options: nil)?.last as! KTVPlayAdjustView
        adjustView.delegate = self as KTVPlayAdjustViewDelegate
        adjustView.frame = self.adjustSupView.bounds
        self.adjustSupView.addSubview(adjustView)
    }
    
    func adduploadView() -> Void {
        let uploadView = Bundle.main.loadNibNamed("KTVUploadControlView", owner: self, options: nil)?.last as! KTVUploadControlView
        uploadView.delegate = self
        uploadView.frame = self.uploadSupView.bounds
        self.uploadSupView.addSubview(uploadView)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
