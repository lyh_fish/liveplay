//
//  KTVPath.swift
//  FanweApp
//
//  Created by lyh on 2019/1/17.
//  Copyright © 2019 xfg. All rights reserved.
//

import UIKit

@objc enum KTVPathSupport:Int {
    case all  /// 全支持
    case music /// 仅支持音乐
    case amp /// 仅支持伴奏
}

open class KTVPath: NSObject {
    
    /// 原唱文件路径
    @objc var musicPath:String = ""
    /// 伴奏文件路径
    @objc var ampPath:String =  ""
    /// 歌词文件路径
    @objc var lyricePath:String =  ""
    /// 录音文件路径
    @objc var recorderPath:String = ""
    /// 导出录音文件路径 如果改后缀.m4a 需要修改KTVAudioMerge下的导出参数AVAssetExportPresetAppleM4A
    @objc let exportPath:String = NSHomeDirectory() + "/Documents/KTV.m4a"
    /// 渲染的caf
    @objc let renderPath:String = NSHomeDirectory() + "/Documents/render.caf"
    /// 录音文件需要分割的时间点
    @objc var recorderModels:[KTVRecorderModel] = []
    
    @objc var support:KTVPathSupport = .all
    
    @objc var music:KTVMusicModel!
    
    @objc static let ktv:KTVPath = KTVPath()
    
    @objc open class func share() -> KTVPath {
        return .ktv
    }
    
}

/// 只要一停止录音就会生成一个录音model
class KTVRecorderModel: NSObject {
    /// 录音开始音轨时间
    var start:Double = 0.0
    /// 录音结束音轨时间
    var end:Double = 0.0
    /// 音乐开始时间
    var musicstart:Double = 0.0
}
