//
//  KTVPlayRecorder.swift
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

import UIKit
import AudioKit

enum KTVPlayRecorderState {
    case play
    case pause
}

protocol KTVPlayRecorderDelegate {
    func playStateChange(state:KTVPlayRecorderState) -> Void
    func updateTime(time:Double,duration:Double) -> Void
    func renderComplete(duration:Double) -> Void
}

/// 播放录音和伴奏【【
class KTVPlayRecorder: NSObject {
    /// 播放伴奏
    var backPlayer:AKPlayer!
    /// 播放录音
    var recorderPlayer:AKPlayer!
    
    /// 混响调节 只调节录音
    var recorderReb:AKReverb?
    /// 音效混合
    var mainMixer: AKMixer!
    
    /// 录音音调调节
    var recorderPitch:AKTimePitch!
    
    var delay:AKDelay!
    
    private var progress:Double = 0
    
    let ktvPath:KTVPath = KTVPath.share()
    
    /// 歌词定时器
    var timer:CADisplayLink!
    
    var delegate:KTVPlayRecorderDelegate?
    
    private var playState:KTVPlayRecorderState = .pause
    
    var offlineNode:AKOfflineRenderNode!
    
    let queue = DispatchQueue(label: "render")
    
    override init() {
        super.init()
        AudioKit.disconnectAllInputs()
        
        do {
            try AKSettings.setSession(category: .playback, with: .mixWithOthers)
        } catch {
            AKLog("Could not set session category.")
        }
        AKSettings.playbackWhileMuted = true
        AKSettings.audioInputEnabled = false
        
        let recorderFile = ktvPath.exportPath
        let recorderUrl = URL.init(string: recorderFile)!
        
        recorderPlayer = AKPlayer.init(url: recorderUrl)
        recorderPlayer.volume = 1.0
        recorderPlayer.isLooping = false
        recorderPlayer.startTime = 0
        recorderPlayer.completionHandler = {
            self.recorderPlayer.stop()
            self.backPlayer.stop()
            
            self.backPlayer.startTime = 0
            self.backPlayer.endTime = self.recorderPlayer.duration
            self.backPlayer.play()
            self.recorderPlayer.play()
        }
        recorderPlayer.prepare()
        
        let backFile = ktvPath.ampPath
        let backUrl = URL.init(string: backFile)!
        
        backPlayer = AKPlayer.init(url: backUrl)
        backPlayer.volume = 0.5
        backPlayer.isLooping = false
        backPlayer.startTime = 0
        backPlayer.endTime = recorderPlayer.duration
        backPlayer.prepare()
        
        /// 音调调节
        let pitch1 = AKTimePitch(recorderPlayer)
        pitch1.rate = 1.0
        pitch1.pitch = -0.0
        pitch1.overlap = 8.0
        recorderPitch = pitch1
        
        /// 混响调节
        let reb = AKReverb(pitch1)
        reb.dryWetMix = 0;
        recorderReb = reb
        
        mainMixer = AKMixer(reb,backPlayer)
        
        if #available(iOS 11, *) {
            AudioKit.output = mainMixer
            do {
                try AudioKit.engine.start()
            } catch {
                print("AudioKit.start()失败")
            }
        } else {
            offlineNode = AKOfflineRenderNode(mainMixer)
            AudioKit.output = offlineNode
            do {
                try AudioKit.engine.start()
            } catch {
                print("AudioKit.start()失败")
            }
        }
       
    }
    
    @objc func finish() -> Void {
        self.removeTimer()
        do {
            try AudioKit.stop()
        } catch {
            print("AudioKit.stop() 失败" + error.localizedDescription)
        }
    }
    
    /// 添加定时器
    func addTimer() -> Void {
        self.timer = CADisplayLink.init(target: self, selector: #selector(updateTime))
        self.timer.add(to: RunLoop.main, forMode: .common)
    }
    
    /// 更新显示e歌词
    @objc func updateTime() -> Void {
        print(self.recorderPlayer.currentTime)
        print(self.backPlayer.currentTime)
        if self.timer != nil && self.playState == .play{
            if self.delegate != nil {
                let time = self.recorderPlayer.currentTime
                self.delegate?.updateTime(time: time, duration: self.recorderPlayer.duration)
            }
        }
    }
    
    
    /// 移除定时器
    func removeTimer() -> Void {
        if (self.timer != nil) {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    /// 播放伴奏和录音 只需调用一次即可
    @objc func play() -> Void {
        self.backPlayer.play()
        self.recorderPlayer.play()
        self.playState = .play
        self.addTimer()
    }
    
    /// 暂停
    @objc func pause() -> Void {
        self.backPlayer.pause()
        self.recorderPlayer.pause()
        self.playState = .pause
        if (self.delegate != nil){
            self.delegate?.playStateChange(state: self.playState)
        }
    }
    
    /// 继续
    @objc func resume() -> Void {
        self.backPlayer.resume()
        self.recorderPlayer.resume()
        self.playState = .play
        if (self.delegate != nil){
            self.delegate?.playStateChange(state: self.playState)
        }
    }
    
    /// 打开音乐/伴奏播放
    ///
    /// - Parameter time: 播放时间 continue:是否可以继续
    func playAt(time:Double) -> Void {
        print("打开音乐/伴奏播放")
        
        let isPlay = self.recorderPlayer.isPlaying && self.backPlayer.isPlaying
    
        if isPlay {
            self.recorderPlayer.pause()
            self.recorderPlayer.pause()
        }
        
        self.recorderPlayer.pauseTime = time
        self.backPlayer.pauseTime = time
        
        if isPlay {
            self.recorderPlayer.resume()
            self.backPlayer.resume()
        }
    }

    /// 调节混响强度
    @objc func setDryWetMix(dryWet:Double) -> Void {
        if dryWet < 0 || dryWet > 1{
            return
        }
        recorderReb?.dryWetMix = dryWet
    }
    
    @objc func setPitch(pitch:Double) -> Void {
        if pitch < -2400 || pitch > 2400{
            return
        }
        recorderPitch?.pitch = pitch
    }
    
    /// 调节伴奏音量
    @objc func setAccompanimentVolume(volume:Double) -> Void {
        if volume < 0 || volume > 1{
            return
        }
        backPlayer.volume = volume
    }
    
    /// 调节人声音量
    @objc func setManVolume(volume:Double) -> Void {
        if volume < 0 || volume > 1{
            return
        }
        recorderPlayer.volume = volume
    }
    
    @objc func setDelay(time:TimeInterval) -> Void {
        delay.stop()
        delay.time = time
        delay.start()
    }
    
    func render(mp3:String) -> Void {
        
        self.backPlayer.stop()
        self.recorderPlayer.stop()
        
            ///处理耗时操作的代码块...
            do {
                
                let renderSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 44100),//采样率
                    AVFormatIDKey: NSNumber(value: kAudioFormatLinearPCM),//音频格式
                    AVLinearPCMBitDepthKey: NSNumber(value: 16),//采样位数
                    AVNumberOfChannelsKey: NSNumber(value: 2),//通道数
                    AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.medium.rawValue)//录音质量
                ]
                
                let renderfiles = try AKAudioFile.init(writeIn: .temp, name: "render", settings: renderSetting)
                print(renderfiles.url.absoluteString)
                
                let time = 1.0
                if #available(iOS 11, *) {
                    try AudioKit.renderToFile(renderfiles, duration: self.recorderPlayer.duration, prerender: {
                        self.backPlayer.play(from: 0, to: self.recorderPlayer.duration)
                        self.recorderPlayer.play(from: 0, to: self.recorderPlayer.duration)
                    })
                    print("结束" + String(self.recorderPlayer.duration))
                } else {
                    self.offlineNode.internalRenderEnabled = true
                    self.backPlayer.play()
                    self.recorderPlayer.play()
                    do {
                        try self.offlineNode.renderToURL(renderfiles.url.absoluteURL, duration: self.recorderPlayer.duration)
                    } catch {
                        print(error)
                    }
                    self.backPlayer.stop()
                    self.recorderPlayer.stop()
                    self.offlineNode.internalRenderEnabled = false
                }
                print("开始转换为MP3")
                LameConver.conventToMp3(withCafFilePath: renderfiles.avAsset.url.path, mp3FilePath: mp3, callback: { (state) in
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time, execute: {
                        if (self.delegate != nil) {
                            print("完成渲染回到主线程")
                            self.delegate?.renderComplete(duration: self.recorderPlayer.duration)
                        }
                    })
                })
            } catch {
                print(error)
            }
    }
}
    

