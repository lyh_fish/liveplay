//
//  KTVAudioMerge.swift
//  KTV
//
//  Created by lyh on 2019/1/13.
//  Copyright © 2019 Sinopec. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol KTVAudioMergeDelegate {
    func KTVAudioMergeExportCompleted(path:String) -> Void
    
    func KTVAudioMergeExportFailed(error:String) -> Void
}

class KTVAudioMerge: NSObject {
    
    private let ktvPath:KTVPath = KTVPath.share()
    
    weak var delegate:KTVAudioMergeDelegate?
    
    private var exportSession:AVAssetExportSession!

    
    /// 合并录音
    func mergeRecorder() -> Void {
        print("正在合并音频")
        print(ktvPath.recorderModels)
        if ktvPath.recorderModels.count == 0 {
            if (self.delegate != nil) {
                DispatchQueue.main.async {
                    self.delegate?.KTVAudioMergeExportFailed(error: "录音不存在")
                }
            }
            print("录音文件数量为0")
            return
        }
    
        do {
            if FileManager.default.fileExists(atPath: self.ktvPath.exportPath) {
                print("文件存在，准备删除")
                try FileManager.default.removeItem(atPath: self.ktvPath.exportPath)
            }
        } catch {
            if (self.delegate != nil) {
                DispatchQueue.main.async {
                    self.delegate?.KTVAudioMergeExportFailed(error: error.localizedDescription)
                }
            }
            
            print("文件夹错误" + error.localizedDescription)
            return;
        }

        let composition = AVMutableComposition.init()
        
        if ktvPath.recorderModels.count > 1 {
            for model in ktvPath.recorderModels {
                print(model.musicstart,model.end)
                do {
                    if (model.musicstart < 0){
                        model.musicstart = 0;
                    }
                    if (model.end < 0){
                        model.end = 0;
                    }
                    /// 音频合并 - 插入音轨文件
                    let asset = AVURLAsset(url: URL.init(fileURLWithPath: ktvPath.recorderPath))
                    let audioTrack = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: 0)
                    let audioAssetTrack = asset.tracks(withMediaType: .audio).first!
                    let timescale = asset.duration.timescale
                    let at = CMTime.init(seconds: model.musicstart, preferredTimescale: timescale)
                    let start = CMTime.init(seconds: model.start, preferredTimescale: timescale)
                    let end = CMTime.init(seconds: model.end, preferredTimescale: timescale)
                    try audioTrack?.insertTimeRange(CMTimeRange.init(start: start, end: end), of: audioAssetTrack, at: at)
                    /// 合并后的文件导出 - `presetName`要和之后的`session.outputFileType`相对应。
                    exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetAppleM4A)!
                } catch {
                    if (self.delegate != nil) {
                        self.delegate?.KTVAudioMergeExportFailed(error: self.exportSession.error?.localizedDescription ?? "未知问题")
                        print("CMTime参数错误:"+(self.exportSession.error?.localizedDescription)!)
                        return;
                    }
                }
            }
        }else {
             let asset = AVURLAsset(url: URL.init(fileURLWithPath: ktvPath.recorderPath))
            print(ktvPath.recorderPath)
            /// 合并后的文件导出 - `presetName`要和之后的`session.outputFileType`相对应。
            exportSession = AVAssetExportSession.init(asset: asset, presetName: AVAssetExportPresetAppleM4A)!
        }

        exportSession.outputURL = URL.init(fileURLWithPath: ktvPath.exportPath)
        exportSession.outputFileType = AVFileType.m4a
        exportSession.shouldOptimizeForNetworkUse = true
      
        exportSession.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                switch self.exportSession.status {
                case .completed:
                    if (self.delegate != nil) {
                        self.delegate?.KTVAudioMergeExportCompleted(path: self.ktvPath.exportPath)
                        print("导出成功" + self.ktvPath.exportPath)
                    }
                    
                default:
                    if (self.delegate != nil) {
                        self.delegate?.KTVAudioMergeExportFailed(error: self.exportSession.error?.localizedDescription ?? "未知问题")
                        print("导出其他问题"+(self.exportSession.error?.localizedDescription)! )
                    }
                }
            }
        })
    }
}

