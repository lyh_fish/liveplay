//
//  KTVPlayVC.swift
//  KTV
//
//  Created by lyh on 2019/1/8.
//  Copyright © 2019 Sinopec. All rights reserved.
//

import UIKit
import Schedule


class KTVPlayVC: UIViewController,KTVAdjustViewDelegate,KTVAudioMergeDelegate,KTVRecorderControlViewDelegate,KTVPlayDelegate,KRCLyricsShowViewDelegate {
    
    var ktvPlay:KTVPlay!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var completeButton: UIButton!
    
    @IBOutlet weak var adjustSupView: UIView!

    @IBOutlet weak var controlSupView: UIView!
    
    var recorderControlView:KTVRecorderControlView!
    
    var krcShowView:KRCLyricsShowView!
    /// 歌曲名称
    @IBOutlet weak var musicnameLabel: UILabel!
    /// 歌曲进度时间
    @IBOutlet weak var musictimeLabel: UILabel!
    /// 歌曲时间
    @IBOutlet weak var musicdurationLabel: UILabel!
    
    @objc var music:KTVMusicModel!
    
    @objc var backRoot:Bool = false
    
    /// 导出合成录音类
    var audioMerge:KTVAudioMerge = KTVAudioMerge()
    
    let ktvPath:KTVPath = KTVPath.share()
    
    var downLoadView:KTVMusicDownLoadView!
    
    deinit {
        print("KTVPlayVC释放")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        UIApplication.shared.isIdleTimerDisabled =  false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        UIApplication.shared.isIdleTimerDisabled =  true;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// 发送已点请求
        KTVMusicClick.clickMusic(withID: music.id)
        
        downLoadView = Bundle.main.loadNibNamed("KTVMusicDownLoadView", owner: self, options: nil)?.last as? KTVMusicDownLoadView
        downLoadView.frame = self.view.bounds
        self.view.addSubview(downLoadView)
        downLoadView.start()
        downLoadView.isHidden = true
    
        if music != nil {

            ktvPath.music = music

            musicnameLabel.text = music.title

            KTVDownload.download(model: music) { (progress, state) in
                switch state {
                case .download:
                    self.completeButton.isEnabled = false
                    self.downLoadView.isHidden = false
                    self.downLoadView.updateProgress(CGFloat(progress))
                case .success,.exist:
                    self.downLoadView.isHidden = true
                    self.completeButton.isEnabled = true
                    self.addadjustView()
                    self.addControlView()
                    self.prepare()
                case .failure:
                    self.downLoadView.isHidden = true
                    SVProgressHUD.showError(withStatus: "下载失败,请退出重试")
                case .other:
                    self.downLoadView.isHidden = true
                    SVProgressHUD.showError(withStatus: "未知问题,请退出重试")
                case .empty:
                    self.downLoadView.isHidden = true
                    SVProgressHUD.showError(withStatus: "没有找到相关歌曲文件")
                default:
                    break
                }
            }
        }else {
            SVProgressHUD.showError(withStatus: "音乐不存在")
        }
    }
    
    func prepare() -> Void {
        
        self.addKrcShowView()
        self.view.bringSubviewToFront(self.adjustSupView)
        self.view.bringSubviewToFront(self.controlSupView)
        
        ktvPlay = KTVPlay.init()
        ktvPlay.delegate = self
        musicdurationLabel.text = ktvPlay.durationstr
        
        audioMerge.delegate = self
        
        WZBCountdownLabel.play(withNumber: 3, endTitle: "", begin: { (label) in
            
        }, success: { (label) in
            self.ktvPlay.playAll(time: 0, canContinue: false)
            self.ktvPlay.playRecord()
        })
    }
    
    func request() -> Void {
        
    }
    
    /// 停止播放和录音并且导出录音内容
    func exportAudio() -> Void {
        completeButton.isEnabled = false
        SVProgressHUD.show()
        if ktvPlay != nil {
            ktvPlay.finish()
            krcShowView.pauseLyricAnimation()
        }
        /// 导出前必须停止录音
        audioMerge.mergeRecorder()
    }
    
    /// KRCLyricsShowViewDelegate 歌词滑动/跳过代理
    
    func beginScroll(with showView: KRCLyricsShowView) {
        self.adjustSupView.isHidden = true
        ktvPlay.pauseAll()
    }
    
    func endScrollendPoint(with showView: KRCLyricsShowView, time: Double) {
        ktvPlay.playRecord()
    }
    
    func endScrollStartPoint(with showView: KRCLyricsShowView, time: Double) {
        ktvPlay.playAll(time: time, canContinue: false)
    }
    
    /// KTVRecorderControlViewDelegate 录音配置代理
    func guide(withOpen open: Bool) {
        if open { /// 打开伴奏
            ktvPlay.closeOriginalMusic()
        }else {
            ktvPlay.openOriginalMusic()
        }
    }
    
    func earback(withOpen open: Bool) {
        ktvPlay.changeMIC(open: open)
    }
    
    func equalizerShowAction() {
        self.adjustSupView.isHidden = !self.adjustSupView.isHidden;
    }
    
    func reset(with controlView: KTVRecorderControlView?) {
        
        let alert = UIAlertController.init(title: "音乐还未完成", message: "确定重置吗?", preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "取消", style: .cancel) { (action) in
            
        }
        
        let action2 = UIAlertAction.init(title: "确定", style: .destructive) { (action) in
            self.ktvPlay.resetAll()
            self.krcShowView.clearLyricAnimation()
        }
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true) {
            
        }
    }
    
    /// KTVPlayDelegate 播放器播放状态改变
    func earBack(open: Bool) {
        
    }
    
    func playStateChange(state: KTVPlayState) {
        switch state {
        case .play:
            recorderControlView.playState = .init(rawValue: 0)
       case .pause:
            recorderControlView.playState = .init(rawValue: 1)
        default:
            break
        }
    }
    
    func updateTime(time: Double, timestr: String) {
        krcShowView.currentTime = time
        self.musictimeLabel.text = timestr
    }
    
    func playCompletion() {
        self.exportAudio()
    }
    
    /// KTVRecorderControlViewDelegate 录音配置代理
    func playStateChange(with State: KTVRecorderControlViewPlayState) {
        if State == KTVRecorderControlViewPlayStatePlay {
            self.krcShowView.continueLyricAnimation()
            self.ktvPlay.playAll(time: 0, canContinue: true)
            self.ktvPlay.playRecord()
        }else {
            krcShowView.pauseLyricAnimation()
            self.ktvPlay.pauseAll()
        }
    }
    
    /// KTVAdjustViewDelegate
    func recorderVolumeValueChange(_ value: Double) {
        ktvPlay?.setMICVolume(volume: value)
    }
    
    func accompanimentVolumeValueChange(_ value: Double) {
        ktvPlay?.setAccompanimentVolume(volume: value)
    }
    
    func dryWetValueChange(_ value: Double) {
        ktvPlay?.setDryWetMix(dryWet:value)
    }
    
    func pitchValueChange(_ value: Double) {
        ktvPlay?.setPitch(pitch: value)
    }
    
    /// KTVAudioMergeDelegate
    func KTVAudioMergeExportCompleted(path: String) {
        SVProgressHUD.showSuccess(withStatus: "导出成功")
        SVProgressHUD.dismiss(withDelay: 1.0) {
            let vc = UIStoryboard.init(name: "KTV", bundle: nil).instantiateViewController(withIdentifier: "KTVPlayRecorderVC") as! KTVPlayRecorderVC
            vc.music = self.music
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func KTVAudioMergeExportFailed(error: String) {
        SVProgressHUD.dismiss(withDelay: 1.0) {
            self.completeButton.isEnabled = true
            SVProgressHUD.showError(withStatus: error)
        }
    }
    
    func addKrcShowView() -> Void {
        /// 默认歌词高度为44，最多显示7个
        krcShowView = KRCLyricsShowView.init(frame: CGRect.init(x: 0, y: 100, width: self.view.frame.size.width, height:44*7))
        krcShowView.delegate = self
        krcShowView.lyricPath = ktvPath.lyricePath
        self.view.addSubview(krcShowView);
    }
    
    func addadjustView() -> Void {
        let adjustView = Bundle.main.loadNibNamed("KTVAdjustView", owner: self, options: nil)?.last as! KTVAdjustView
        adjustView.delegate = self
        adjustView.frame = self.adjustSupView.bounds
        self.adjustSupView.addSubview(adjustView)
        self.adjustSupView.isHidden = true
        
    }
    
    func addControlView() -> Void {
        let controlView = Bundle.main.loadNibNamed("KTVRecorderControlView", owner: self, options: nil)?.last as! KTVRecorderControlView
        controlView.delegate = self
        controlView.frame = self.controlSupView.bounds
        controlView.playState = KTVRecorderControlViewPlayStatePlay
        self.controlSupView.addSubview(controlView)
        recorderControlView = controlView
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        if backRoot {
            self.navigationController?.popToRootViewController(animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        ktvPlay.finish()
    }
    
    @IBAction func completeButtonAction(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "音乐还未完成", message: "确定终止吗", preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "取消", style: .cancel) { (action) in
            
        }
        
        let action2 = UIAlertAction.init(title: "确定", style: .destructive) { (action) in
            self.exportAudio()
        }
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true) {
            
        }
    }
    
    /// 精仿唱吧APP音乐下载按钮
    func setUpChangBaMusicalView() -> Void {
        
    }
    

}
