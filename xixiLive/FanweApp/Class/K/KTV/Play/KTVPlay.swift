//
//  KTVPlay.swift
//  KTV
//
//  Created by lyh on 2019/1/8.
//  Copyright © 2019 Sinopec. All rights reserved.
//

import UIKit
import AudioKit


class KTVModel: NSObject {
    /// 原声路径
    var musicPath:String = ""
    /// 伴奏路径
    var backPath:String = ""
    /// 录音路径
    var recorderPath:String = ""
}

extension Double {
    func toMusicTime() -> String {
        let time = Int(self)
        let h =  time/3600
        let hstr = String(format: "%02ld", h)
        let mstr = String(format: "%02ld",time%3600/60)
        let sstr = String(format: "%02ld",time%60)
        if h == 0 {
            return  mstr + ":" + sstr
        }else {
            return hstr + ":" + mstr + ":" + sstr
        }
    }
}

enum KTVPlayState {
    case play
    case pause
}

protocol KTVPlayDelegate: NSObjectProtocol {
    func playStateChange(state:KTVPlayState) -> Void
    func updateTime(time:Double,timestr:String) -> Void
    func playCompletion() -> Void
    func earBack(open:Bool) -> Void
}

class KTVPlay: NSObject {
    
    /// 音效混合
    var mainMixer: AKMixer!

    var moogLadder: AKMoogLadder!
    /// 录音
    var recorder: AKNodeRecorder!
    /// 麦克风扩音
    var micBooster: AKBooster!
    /// 麦克风混合类
    var micMixer: AKMixer!
    
    var mic: AKMicrophone!
    
    var micVolume:Double = 1
    /// 播放原声或者伴奏的声音（0-1）
    var playVolume:Double = 0.5
    /// 混响强度（0-1）
    var dryWetMix:Double = 0
    
    /// 原声音调调节
    var musicPitch:AKTimePitch!
    /// 伴奏音调调节
    var backPitch:AKTimePitch!
    
    /// 播放原声
    var musicPlayer:AKPlayer!
    /// 播放伴奏
    var backPlayer:AKPlayer!
    /// 播放录音
    /// 是否伴奏默认打开伴奏关闭原声
    var isAccompaniment:Bool = true
    
    var recorderReb:AKReverb?

    private var nowRecorderModel = KTVRecorderModel.init()

    private var recorderModels:[KTVRecorderModel] = []
    /// 暂停时间
    private var pausetime:Double = 0.0
    
    let ktvPath:KTVPath = KTVPath.share()
    
    weak var delegate:KTVPlayDelegate?
    
    private var playState:KTVPlayState = .pause
    
    public var durationstr:String = "00:00"
    
    public var isOpenEarBack:Bool = false;
    
    /// 歌词定时器
    var lrcTimer:CADisplayLink!
    
    deinit {
        print("KTVPlay释放")
        NotificationCenter.default.removeObserver(self)
    }
    
    override init() {
        super.init()
        
        // Clean tempFiles !
        AKAudioFile.cleanTempDirectory()
        AudioKit.disconnectAllInputs()
        
        if AKSettings.headPhonesPlugged {
            do {
                try AKSettings.setSession(category: .playAndRecord)
                try AKSettings.session.setMode(.gameChat)
                try AKSettings.session.setActive(true, options: .notifyOthersOnDeactivation)
            } catch {
                print("Couldn't override output audio port")
            }
        }else {
            do {
                try AKSettings.setSession(category: .playAndRecord)
                AKSettings.defaultToSpeaker = true
            } catch {
                print("Couldn't override output audio port")
            }
        }
    
        /// 麦克风
        mic = AKMicrophone()
        let monoToStereo = AKStereoFieldLimiter(mic, amount: 1)
        micMixer = AKMixer(monoToStereo)
        micBooster = AKBooster(micMixer)
        routeChange()
    
        /// 混响调节
        let reb = AKReverb(micBooster)
        reb.dryWetMix = dryWetMix;
        recorderReb = reb
        
        do {
            let recordSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 44100),//采样率
                AVFormatIDKey: NSNumber(value: kAudioFormatLinearPCM),//音频格式
                AVLinearPCMBitDepthKey: NSNumber(value: 16),//采样位数
                AVNumberOfChannelsKey: NSNumber(value: 2),//通道数
                AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.medium.rawValue)//录音质量
            ]
            let file = try AKAudioFile.init(writeIn: .temp, name: "recorder", settings: recordSetting)
            recorder = try? AKNodeRecorder(node: micMixer,file:file)
            ktvPath.recorderPath = file.url.absoluteString
        } catch {
            print("初始化录音失败")
        }
        
        let musicFile = ktvPath.musicPath
        let musicUrl = URL.init(string: musicFile)!
        
        musicPlayer = AKPlayer.init(url: musicUrl)
        if musicPlayer == nil {
            SVProgressHUD.showError(withStatus: "音频格式有误,请联系管理员解决")
            return;
        }
        musicPlayer.volume = isAccompaniment ? 0:0.5
        musicPlayer.isLooping = false
        musicPlayer.prepare()
        musicPlayer.completionHandler = {
            if self.delegate != nil {
                self.pauseAll()
                self.delegate?.playCompletion()
            }
        }
        durationstr = musicPlayer.duration.toMusicTime()
        
        /// 音调调节
        let pitch1 = AKTimePitch(musicPlayer)
        pitch1.rate = 1.0
        pitch1.pitch = -0.0
        pitch1.overlap = 8.0
        musicPitch = pitch1
        
        let backFile = ktvPath.ampPath
        let backUrl = URL.init(string: backFile)!
        
        backPlayer = AKPlayer.init(url: backUrl)
        if backPlayer == nil {
            SVProgressHUD.showError(withStatus: "音频格式有误,请联系管理员解决")
            return;
        }
        backPlayer.volume = isAccompaniment ? 0.5:0
        backPlayer.isLooping = false
        backPlayer.prepare()
        backPlayer.completionHandler = {
            print("播放完毕")
        }
        
        /// 音调调节
        let pitch2 = AKTimePitch(backPlayer)
        pitch2.rate = 1.0
        pitch2.pitch = 0
        pitch2.overlap = 8.0
        backPitch = pitch2
        
        mainMixer = AKMixer(pitch1,reb,pitch2)
    
        AudioKit.output = mainMixer
        
        do {
            try AudioKit.start()
        } catch {
            print("AudioKit.start() 失败")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(routeChange), name: AVAudioSession.routeChangeNotification, object: nil)
        
        self.addLrcTimer()
    }
    
    /// 添加定时器
    func addLrcTimer() -> Void {
        self.lrcTimer = CADisplayLink.init(target: self, selector: #selector(updateTime))
        self.lrcTimer.add(to: RunLoop.main, forMode: .common)
    }
    
    /// 移除定时器
    func removeLrcTimer() -> Void {
        if (self.lrcTimer != nil) {
            self.lrcTimer.invalidate()
            self.lrcTimer = nil
        }
    }
    
    /// 更新显示e歌词
    @objc func updateTime() -> Void {
        if self.lrcTimer != nil && self.playState == .play{
            if self.delegate != nil {
                let time = self.musicPlayer.currentTime
                self.delegate?.updateTime(time: time, timestr: time.toMusicTime())
            }
        }
    }
    
    
    /// 完成并导出录音导出录音
    func finish() -> Void {
        if self.musicPlayer.isPlaying {
            self.pauseAll()
        }
    
        do {
            try AudioKit.shutdown()
            try AudioKit.stop()
        } catch {
            print("AudioKit.stop() 失败" + error.localizedDescription)
        }
        self.removeLrcTimer()
        ktvPath.recorderModels = recorderModels
    }
    
    /// 打开音乐/伴奏播放
    ///
    /// - Parameter time: 播放时间 continue:是否可以继续
    func playAll(time:Double?,canContinue:Bool) -> Void {
        
        if musicPlayer == nil {
            SVProgressHUD.showError(withStatus: "音频格式有误,请联系管理员解决")
            return;
        }
        
        if backPlayer == nil {
            SVProgressHUD.showError(withStatus: "音频格式有误,请联系管理员解决")
            return;
        }
        print("打开音乐/伴奏播放")
        if self.musicPlayer.isPlaying || self.backPlayer.isPlaying {
            self.musicPlayer.stop()
            self.backPlayer.stop()
        }
        
        var playtime = 0.0
        if canContinue {
            playtime = pausetime
        }else {
            playtime = time ?? 0
        }
        self.musicPlayer.startTime = playtime
        self.backPlayer.startTime = playtime
        self.musicPlayer.play()
        self.backPlayer.play()
        if self.delegate != nil {
            self.playState = .play
            self.delegate?.playStateChange(state: .play)
        }
    }
    
    /// 打开录音
    func playRecord() -> Void {
        print("准备打开")
        do {
            print("打开录音")
            try recorder.record()
        } catch {
            print("录音打开失败")
        }
        nowRecorderModel.musicstart = self.musicPlayer.currentTime
    }
    
    /// 暂停音乐、伴奏和录音:不释放录音资源
    @objc func pauseAll() -> Void {
        pausetime = self.musicPlayer.currentTime
        nowRecorderModel.end = recorder.recordedDuration
        if recorderModels.count != 0 {
            let lastRecorderModel = recorderModels.last!
            if (lastRecorderModel.start > nowRecorderModel.start) {
                recorderModels.removeLast()
            }
        }
        recorderModels.append(nowRecorderModel)
        self.musicPlayer.stop()
        self.backPlayer.stop()
        if self.recorder.isRecording {
            print("暂停录音")
            self.recorder.stop()
        }
        if self.delegate != nil {
            self.playState = .pause
            self.delegate?.playStateChange(state: .pause)
        }
    }
    
    /// 重置音乐,伴奏和录音:释放录音资源
    @objc func resetAll() -> Void {
        recorderModels.removeAll()
        if self.recorder.isRecording {
            do {
                try recorder.reset()
            } catch {
                print("录音重置失败")
            }
        }
        do {
            try recorder.record()
        } catch {
            print("打开录音失败")
        }
        self.playAll(time: 0, canContinue: false)
    }
    
    /// 打开原声
    @objc func openOriginalMusic() -> Void {
        self.isAccompaniment = !self.isAccompaniment
        self.musicPlayer?.volume = self.playVolume
        self.backPlayer?.volume = 0
    }
    
    /// 关闭原声
    @objc func closeOriginalMusic() -> Void {
        self.isAccompaniment = !self.isAccompaniment
        self.musicPlayer?.volume = 0
        self.backPlayer?.volume = self.playVolume
    }
    
    
    /// 调节麦克风音量
    @objc func setMICVolume(volume:Double) -> Void {
        if volume < 0 || volume > 1{
            return
        }
        if (AKSettings.headPhonesPlugged) {
            mic.volume = volume
        }
        micVolume = volume
    }
    
    /// 打开或关闭耳返
    @objc func changeMIC(open: Bool) -> Void {
        if (AKSettings.headPhonesPlugged) {
            if open {
                micBooster.gain = micVolume
                SVProgressHUD.showInfo(withStatus: "耳返打开")
            }else {
                micBooster.gain = 0
                SVProgressHUD.showInfo(withStatus: "耳返关闭")
            }
        }else {
            SVProgressHUD.showInfo(withStatus: "耳返需要戴耳机才能激活哟!!!")
        }
    }
    
    /// 调节伴奏音量
    @objc func setAccompanimentVolume(volume:Double) -> Void {
        if volume < 0 || volume > 1{
            return
        }
        playVolume = volume
        if isAccompaniment {
            backPlayer.volume = volume
        }else {
            musicPlayer.volume = volume
        }
    }
    
    /// 调节混响强度
    @objc func setDryWetMix(dryWet:Double) -> Void {
        if dryWet < 0 || dryWet > 1{
            return
        }
        dryWetMix = dryWet
        recorderReb?.dryWetMix = dryWet
    }
    
    /// 升降调
    @objc func setPitch(pitch:Double) -> Void {
        if pitch < -2400 || pitch > 2400{
            return
        }
        backPitch.pitch = pitch
        musicPitch.pitch = pitch
    }
    
    @objc private func routeChange() -> Void {
        if micBooster != nil {
            if (AKSettings.headPhonesPlugged) {
                micBooster.gain = micVolume
            }else {
                micBooster.gain = 0
            }
        }
    }

}
