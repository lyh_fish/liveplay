//
//  KTVOSS.h
//  FanweApp
//
//  Created by lyh on 2019/1/31.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KTVSong.h"

NS_ASSUME_NONNULL_BEGIN
@class KTVOSS;
@protocol KTVOSSDelegate <NSObject>
- (void)uploadOSSCompleteWithOSS:(KTVOSS*)oss;
@end

@interface KTVOSS : NSObject

@property (nonatomic, strong) KTVSong *song;

@property (nonatomic, weak) id<KTVOSSDelegate> delegate;

+ (instancetype)share;

- (void)uploadWithCoverpath:(NSArray *)coverpath Title:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
