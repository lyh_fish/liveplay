//
//  KTVOSS.m
//  FanweApp
//
//  Created by lyh on 2019/1/31.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVOSS.h"
#import "FWOssManager.h"

@interface KTVOSS() <OssUploadImageDelegate>

@end

@implementation KTVOSS
static KTVOSS *oss;
+ (instancetype)share {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        oss = [[KTVOSS alloc] init];
    });
    return oss;
}

- (void)uploadWithCoverpath:(NSArray *)coverpath Title:(NSString *)title{
    
    [[FWHUDHelper sharedInstance]syncLoading:@"正在上传"];
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *song = [NSString stringWithFormat:@"%@/%@",path,self.song.local_path];
    NSMutableDictionary * mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"app" forKey:@"ctl"];
    [mDict setObject:@"aliyun_sts" forKey:@"act"];
    FWWeakify(self)
    [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson) {
        FWStrongify(self)
        NSString *bucket = [responseJson objectForKey:@"bucket"];
        NSString *dir = [responseJson objectForKey:@"dir"];
        NSString *objectKey = [self getObjectKeyStringWithDir:dir];
        NSString *oss_domain = [responseJson objectForKey:@"oss_domain"];;
        NSString *endpoint = [responseJson objectForKey:@"endpoint"];
        NSString *AccessKeyId = [responseJson objectForKey:@"AccessKeyId"];
        NSString *AccessKeySecret = [responseJson objectForKey:@"AccessKeySecret"];
        NSString *SecurityToken = [responseJson objectForKey:@"SecurityToken"];
        NSString *callbackUrl = [responseJson objectForKey:@"callbackUrl"];
        NSString *ossPath = [NSString stringWithFormat:@"%@/%@",oss_domain,objectKey];
       
        // 移动端建议使用STS方式初始化OSSClient。可以通过sample中STS使用说明了解更多(https://github.com/aliyun/aliyun-oss-ios-sdk/tree/master/DemoByOC)
        id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:AccessKeyId secretKeyId:AccessKeySecret securityToken:SecurityToken];
        
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        // 必填字段
        put.bucketName = bucket;
        put.objectKey = objectKey;
        put.uploadingFileURL = [NSURL fileURLWithPath:song];
        put.callbackParam = @{
                                      @"callbackUrl": callbackUrl,
                                      // callbackBody可自定义传入的信息
                                      @"callbackBody": @"filename=${object}"
                                      };
        // put.uploadingData = <NSData *>; // 直接上传NSData
        //    // 设置Content-Type，可选
        //    put.contentType = @"application/octet-stream";
        //    // 设置MD5校验，可选
        //    put.contentMd5 = [OSSUtil base64Md5ForFilePath:@"<filePath>"]; // 如果是文件路径
        //    // put.contentMd5 = [OSSUtil base64Md5ForData:<NSData *>]; // 如果是二进制数据
        // 进度设置，可选
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            // 当前上传段长度、当前已经上传总长度、一共需要上传的总长度
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        };
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self uploadMusicWithOssPath:ossPath Coverpath:coverpath Title:title];
                    NSLog(@"upload object success!");
                }];
            } else {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    NSLog(@"upload object failed, error: %@" , task.error);
                    [[FWHUDHelper sharedInstance] syncStopLoadingMessage:task.error.description];
                }];
            }
            
            return nil;
        }];
    } FailureBlock:^(NSError *error){
        [[FWHUDHelper sharedInstance] syncStopLoadingMessage:error.description];
    }];
}


- (void)uploadMusicWithOssPath:(NSString *)ossPath Coverpath:(NSArray *)coverpath Title:(NSString *)title{
   

    if (self.song) {
        NSMutableDictionary * mDict = [NSMutableDictionary dictionary];
        [mDict setObject:@"audio" forKey:@"ctl"];
        [mDict setObject:@"add" forKey:@"act"];
        if (title) {
            [mDict setObject:title forKey:@"title"];
        }
        [mDict setObject:ossPath forKey:@"sing_path"];
        [mDict setObject:self.song.id forKey:@"audio_info_id"];
        /// chorus-song 或k歌: k-song , 不传为:k-song
        [mDict setObject:@"k-song" forKey:@"mix_type"];
        if (coverpath) {
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:coverpath options:NSJSONWritingPrettyPrinted error:&error];
            NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [mDict setObject:json forKey:@"cover_path"];
        }
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson) {
            FWStrongify(self)
            [[FWHUDHelper sharedInstance] syncStopLoading];
            if ([responseJson toInt:@"status"] == 1)
            {
                [FanweMessage alertHUD:@"发布成功"];
                if ([self.delegate respondsToSelector:@selector(uploadOSSCompleteWithOSS:)]) {
                    [self.delegate uploadOSSCompleteWithOSS:self];
                }
            }else
            {
                [FanweMessage alertHUD:[responseJson toString:@"error"]];
            }
        } FailureBlock:^(NSError *error){
            [[FWHUDHelper sharedInstance] syncStopLoading];
        }];
    }else {
        [[FWHUDHelper sharedInstance] syncStopLoading];
        [FanweMessage alertHUD:@"无任何歌曲相关信息"];
    }
}

- (NSString *)getObjectKeyStringWithDir:(NSString *)dir
{
    NSDate *currentDate = [NSDate date];//获取当前时间日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYYMMddhhmmssSSS"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    NSString *nameString = [NSString stringWithFormat:@"%@%d.mp3",dateString,arc4random()%10000/1000];
    NSString * objectKey = [NSString stringWithFormat:@"%@%@",dir,nameString];
    return objectKey;
}

@end
