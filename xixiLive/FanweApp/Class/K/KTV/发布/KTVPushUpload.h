//
//  KTVPushUpload.h
//  FanweApp
//
//  Created by lyh on 2019/2/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KTVSong.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVPushUpload : NSObject

- (void)pushKTVUploadVCWithSong:(KTVSong *)song;

@end

NS_ASSUME_NONNULL_END
