//
//  KTVUploadVC.m
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVUploadVC.h"
#import "KTVPhoto.h"
#import "KTVPhotoVC.h"
#import "KTVOSS.h"

@interface KTVUploadVC ()<UITextViewDelegate,KTVPhotoVCDelegate,KTVOSSDelegate>

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *placeholderLabel;

@property (nonatomic, strong) UIView *photoView;
@property (nonatomic, strong) UILabel *photoTipLabel;
@property (nonatomic, strong) UILabel *photoTipDetailLabel;
@property (nonatomic, strong) UIButton *photoButton;

@property (nonatomic, strong) NSArray *photos;
@end

@implementation KTVUploadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"发布";
    self.view.backgroundColor = kBackGroundColor;
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(completeBarItemAction)];
    rightBarItem.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = rightBarItem;

    [self setupBackBtnWithBlock:^{
        [self showBackAlert];
    }];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectZero];
    self.textView.delegate = self;
    self.textView.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:self.textView];
    
    self.placeholderLabel = [[UILabel alloc] init];
    self.placeholderLabel.font = [UIFont systemFontOfSize:13];
    self.placeholderLabel.text = @"对于这首歌,不说点什么吗?";
    self.placeholderLabel.textColor =  myTextColor;
    self.placeholderLabel.userInteractionEnabled = false;
    [self.view addSubview:self.placeholderLabel];
    
    
    self.photoView = [[UIView alloc] init];
    self.photoView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.photoView];

    self.photoTipLabel = [[UILabel alloc] init];
    self.photoTipLabel.text = @"设置背景图片";
    self.photoTipLabel.font = [UIFont systemFontOfSize:15];
    [self.photoView addSubview:self.photoTipLabel];
    
    self.photoTipDetailLabel = [[UILabel alloc] init];
    self.photoTipDetailLabel.font = [UIFont systemFontOfSize:12];
    self.photoTipDetailLabel.textColor =  myTextColor;
    self.photoTipDetailLabel.text = @"选中的图片将以幻灯片的形式播放";
    [self.photoView addSubview:self.photoTipDetailLabel];
    
    self.photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.photoButton.userInteractionEnabled = false;
    self.photoButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.photoButton setTitleColor:myTextColor forState:UIControlStateNormal];
    [self.photoButton setTitle:@"0张" forState:UIControlStateNormal];
    [self.photoView addSubview:self.photoButton];
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view).offset(-24);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@(120));
        make.top.equalTo(self.view).offset(16);
    }];
    
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.textView).offset(8);
        make.top.equalTo(self.textView).offset(4);
    }];
    
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@60);
        make.width.centerX.equalTo(self.textView);
        make.top.equalTo(self.textView.mas_bottom).offset(12);
    }];
    
    [self.photoTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.photoView.mas_centerY);
        make.left.equalTo(self.photoView).offset(8);
    }];
    
    [self.photoTipDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.photoView);
        make.top.equalTo(self.photoTipLabel.mas_bottom);
        make.left.equalTo(self.photoTipLabel);
    }];
    
    [self.photoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.photoView);
        make.right.equalTo(self.photoView).offset(-8);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPhotoView)];
    [self.photoView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)initFWUI {
    [super initFWUI];
}

- (void)initFWData {
    [super initFWData];
}

- (void)tapPhotoView {
    KTVPhotoVC *photoVC = [[KTVPhotoVC alloc] init];
    photoVC.isRelease = true;
    photoVC.delegate = self;
    [[AppDelegate sharedAppDelegate] pushViewController:photoVC withBackTitle:nil];
}

- (void)completeBarItemAction {
    
    [KTVOSS share].delegate = self;
    NSMutableArray *coverpath = [NSMutableArray array];
    for (KTVPhoto *photo in self.photos) {
        [coverpath addObject:photo.src];
    }
    [KTVOSS share].song = self.song;
    [[KTVOSS share] uploadWithCoverpath:coverpath Title:self.textView.text];
//    if (self.photos.count == 0) {
//        [FanweMessage alertHUD:@"快去添加一张图片吧"];
//    }else if (self.textView.text.length == 0) {
//        [FanweMessage alertHUD:@"你还没说什么喲！！！"];
//    }else {
//
//    }
}

- (void)showBackAlert {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"作品未发布,确定退出吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self pop];
    }];
    [alertVC addAction:action1];
    [alertVC addAction:action2];
    [self presentViewController:alertVC animated:true completion:^{
        
    }];
}

-  (void)pop {
    if (self.backRoot) {
         [[AppDelegate sharedAppDelegate] popToRootViewController];
    }else {
         [[AppDelegate sharedAppDelegate] popViewController];
    }
}

#pragma mark KTVOSSDelegate

- (void)uploadOSSCompleteWithOSS:(KTVOSS*)oss {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm transactionWithBlock:^{
        _song.isRelease = true;
    }];
    
    [[FWHUDHelper sharedInstance]syncStopLoading];
    [FanweMessage alertHUD:@"发布成功"];
    [self pop];
}

#pragma mark KTVPhotoVCDelegate

- (void)selectPhotosWithPhotos:(NSArray<KTVPhoto *> *)photos {
    self.photos = [NSArray arrayWithArray:photos];
    [self.photoButton setTitle:[NSString stringWithFormat:@"%ld张",photos.count] forState:UIControlStateNormal];
}

#pragma mark UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.placeholderLabel.hidden = true;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.placeholderLabel.hidden = textView.text.length != 0;
}

- (NSArray *)photos {
    if (!_photos) {
        _photos = [NSArray array];
    }
    return _photos;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end



