//
//  KTVPushUpload.m
//  FanweApp
//
//  Created by lyh on 2019/2/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPushUpload.h"
#import "KTVUploadVC.h"

#import "KTVLocalVC.h"

@implementation KTVPushUpload

- (void)pushKTVUploadVCWithSong:(KTVSong *)song{
    KTVLocalVC *localVC = [[KTVLocalVC alloc] init];
    localVC.song = song;
    [[AppDelegate sharedAppDelegate] pushViewController:localVC withBackTitle:@""];
}

@end
