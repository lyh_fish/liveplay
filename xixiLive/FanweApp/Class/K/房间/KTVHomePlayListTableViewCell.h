//
//  KTVHomePlayListTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/3/3.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKMusic.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVHomePlayListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *musicLabel;

@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

- (void)updateWithMusic:(GKMusic *)music IsMusic:(BOOL)isMusic;

@end

NS_ASSUME_NONNULL_END
