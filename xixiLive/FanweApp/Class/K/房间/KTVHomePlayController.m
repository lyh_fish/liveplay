//
//  FWTKTVPlayController.m
//  FanweApp
//
//  Created by lyh on 2019/2/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomePlayController.h"
#import "KTVHomePlayView.h"
#import "HMHotItemModel.h"

@interface KTVHomePlayController ()

@property (nonatomic, strong) KTVHomePlayView *playView;

@property (nonatomic, strong) UIImageView     *bgImageView;

@property (nonatomic, strong) UILabel          *titleLabel;

@property (nonatomic, strong) UIButton         *ktvButton;

@property (nonatomic, strong) UIButton         *backButton;

@end

@implementation KTVHomePlayController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:true animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:NO];
    [[GKMusicTool sharedInstance] stopMusic];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.playView prepareWithMusic:self.music];
}

- (id)init {
    if (self= [super init]) {
        
        [self.view addSubview:self.bgImageView];
        [self.view addSubview:self.backButton];
        [self.view addSubview:self.titleLabel];
//        [self.view addSubview:self.ktvButton];
        [self.view addSubview:self.playView];
        
        [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(12);
            make.top.equalTo(@(40));
            make.width.equalTo(@(44));
            make.height.equalTo(@(30));
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.bottom.equalTo(self.backButton);
        }];
        
//        [self.ktvButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self.backButton);
//            make.right.equalTo(self.view).offset(-12);
//            make.width.equalTo(@(60));
//            make.height.equalTo(@(30));
//        }];

        [self.playView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.equalTo(self.view);
            make.height.equalTo(@(140));
            make.bottom.equalTo(@(-16));
        }];
    }
    return self;
}

- (void)viewDidLoad {   
    [super viewDidLoad];
}

- (void)backButtonAction {
    [[AppDelegate sharedAppDelegate] popViewController];
}

- (void)ktvButtonAction {
    GKMusic *music = [GKMusicTool sharedInstance].music;
    if ([music.other isKindOfClass:[HMHotItemModel class]]) {
        HMHotItemModel *model = music.other;
        KTVHotSong *song = model.song.audio_info.song;
        KTVMusicModel *music = [[KTVMusicModel alloc] init];
        music.id = song.id;
        music.title = song.title;
        music.cover_path = song.cover_path;
        music.original_sing_path = song.original_sing_path;
        music.accompaniment_path = song.accompaniment_path;
        music.lyrics_path = song.lyrics_path;
        music.sort = song.sort;
        music.created_at = song.created_at;
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = music;
        vc.backRoot = YES;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
    }
    
    if ([music.other isKindOfClass:[KTVSong class]]) {
        KTVSong *song = music.other;
        KTVMusicModel *music = [[KTVMusicModel alloc] init];
        music.id = song.id;
        music.title = song.title;
        music.cover_path = @"";
        music.original_sing_path = song.original_sing_path;
        music.accompaniment_path = song.accompaniment_path;
        music.lyrics_path = song.lyrics_path;
        music.sort = @"";
        music.created_at = @"";
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = music;
        vc.backRoot = YES;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
        
    }
}

- (void)updateUI {
    
}

- (KTVHomePlayView *)playView {
    if (!_playView) {
        _playView = [[KTVHomePlayView alloc] init];
    }
    return _playView;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cm2_fm_bg-ip6"]];
    }
    return _bgImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}

- (UIButton *)ktvButton {
    if (!_ktvButton) {
        _ktvButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _ktvButton.layer.borderWidth = 0.5;
        _ktvButton.layer.borderColor = [UIColor whiteColor].CGColor;
        _ktvButton.titleLabel.font = [UIFont systemFontOfSize:13];
        _ktvButton.layer.cornerRadius = 15;
        [_ktvButton setTitle:@"演唱" forState:UIControlStateNormal];
        [_ktvButton addTarget:self action:@selector(ktvButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ktvButton;
}

- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton setImage:[UIImage imageNamed:@"ic_muisc_back"] forState:UIControlStateNormal];
        [_backButton setImage:[UIImage imageNamed:@"ic_muisc_back"] forState:UIControlStateHighlighted];
        [_backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

@end
