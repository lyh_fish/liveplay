//
//  KTVHomePlayListTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/3/3.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomePlayListTableViewCell.h"

@implementation KTVHomePlayListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)updateWithMusic:(GKMusic *)music IsMusic:(BOOL)isMusic{
    self.musicLabel.text = music.title;
    self.subtitleLabel.text = music.subtitle;
    
    if (isMusic) {
        self.musicLabel.textColor  = [UIColor redColor];
        self.subtitleLabel.textColor  = [UIColor redColor];
    }else {
        self.musicLabel.textColor  = [UIColor darkTextColor];
        self.subtitleLabel.textColor  = [UIColor darkTextColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
