//
//  FWTKTVPlayController.h
//  FanweApp
//
//  Created by lyh on 2019/2/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVHomePlayView.h"
#import "GKMusic.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVHomePlayController : FWBaseViewController

@property (nonatomic, strong) GKMusic *music;

@end

NS_ASSUME_NONNULL_END
