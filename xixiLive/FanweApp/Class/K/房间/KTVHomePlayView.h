//
//  KTVHomePlayView.h
//  FanweApp
//
//  Created by lyh on 2019/3/1.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKMusicTool.h"
#import "GKWYMusicControlView.h"

@class KTVHomePlayView;
@protocol KTVHomePlayViewDelegate <NSObject>

- (void)toKTVWithHomePlayView:(KTVHomePlayView *)playView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVHomePlayView : UIView

@property (nonatomic, strong) GKMusic *music;

@property (nonatomic, weak) id <KTVHomePlayViewDelegate> delegate;

- (void)prepareWithMusic:(GKMusic *)music;

@end

NS_ASSUME_NONNULL_END
