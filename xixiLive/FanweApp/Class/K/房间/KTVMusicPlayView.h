//
//  KTVMusicPlayView.h
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVMusicPlayView : UIView

@property (nonatomic, strong) UIViewController *vc;

@end

NS_ASSUME_NONNULL_END
