//
//  KTVLyricsView.m
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLyricsView.h"
#import "KRCLyricsLabel.h"
#import "KRC.h"
#import "KRCLyricsUtil.h"

@interface KTVLyricsView()

{
    int index;
    
    BOOL isScrolL;
}

@property (nonatomic,strong) KRCLyricsLabel *firstLabel;

@property (nonatomic,strong) KRCLyricsLabel *secondLabel;

@property (nonatomic,strong) NSArray *lyrics;

@property (nonatomic,strong) NSArray *lyricTimes;

@property (nonatomic,strong) NSArray *starsTimes;

@property (nonatomic,strong) NSArray *durationTimes;
/// 持续时间
@property (nonatomic,strong) NSArray *intervalTimes;

@property (nonatomic, strong) NSArray *animationTimesArray;

@property (nonatomic, strong) NSArray *animationlocationsArray;

@end

@implementation KTVLyricsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        index = -1;
        isScrolL = false;
        [self addSubview:self.firstLabel];
        [self addSubview:self.secondLabel];
        [self updateUI];
    }
    return self;
}

- (void)updateUI {
    [_firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(16));
        make.bottom.equalTo(self.mas_centerY);
    }];
    [_secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-16));
        make.top.equalTo(self.mas_centerY);
    }];
}

- (void)setFirstLyric:(NSString *)firstLyric SecondLyric:(NSString *)secondLyric {
    [_firstLabel setText:firstLyric];
    [_secondLabel setText:secondLyric];
}

- (void)startWithCurrentTime:(NSTimeInterval)currentTime {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            // 处理耗时操作的代码块...
            for (int i = 0; i < self.starsTimes.count; i++) {
                int start = [self.starsTimes[i] intValue];
                int duration = [self.durationTimes[i] intValue];
                
                int end = start + duration;
                
                if ((start < currentTime - 20 ||start == currentTime) && end > currentTime && index != i) {
                    NSString *firstText = @"";
                    NSString *secondText = @"";
                    firstText = self.lyrics[i];
                    secondText = @"";
                    if (self.lyrics.count - 1 > i) {
                        secondText = self.lyrics[i+1];
                    }
                    NSArray *times = self.lyricTimes[i];
                    if (i%2 == 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_secondLabel setText:secondText];
                            _secondLabel.alpha = 0;
                            [UIView animateWithDuration:0.2 animations:^{
                                _secondLabel.alpha = 1;
                            }];
                            [_secondLabel stopAnimation];
                            [_firstLabel startLyricsAnimationWithTimeArray:times Duration:duration];
                        });
                    }else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_firstLabel setText:secondText];
                            _firstLabel.alpha = 0;
                            [UIView animateWithDuration:0.2 animations:^{
                                _firstLabel.alpha = 1;
                            }];
                            [_firstLabel stopAnimation];
                            [_secondLabel startLyricsAnimationWithTimeArray:times Duration:duration];
                        });
                    }
                    index = i;
                    break;
                }
            }
        });
}

- (void)scrollWithCurrentTime:(NSTimeInterval)currentTime {
    [self stopAnimation];
//    isScrolL = true;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 处理耗时操作的代码块...
        for (int i = 0; i < self.starsTimes.count; i++) {
            int nextStart = [self.starsTimes[i] intValue];
            if (nextStart > currentTime) {
                if (i==0) {
                    [self resetData];
                    return ;
                }
                ///证明现在动画在上一句
                int nowIndex = i-1;
                int start = [self.starsTimes[nowIndex] intValue];
                int duration = [self.durationTimes[nowIndex] intValue];
                NSTimeInterval location = start + duration - currentTime;
                NSString *firstText = @"";
                NSString *secondText = @"";
                firstText = self.lyrics[nowIndex];
                secondText = @"";
                if (self.lyrics.count - 1 > nowIndex) {
                    secondText = self.lyrics[nowIndex+1];
                }
                NSArray *times = self.lyricTimes[nowIndex];
                if (nowIndex%2 == 0) {
                    isScrolL = false;
                    dispatch_async(dispatch_get_main_queue(), ^{

                        [_secondLabel setText:secondText];
                        [_firstLabel setText:firstText];
                        [_firstLabel startLyricsAnimationWithTimeArray:times Duration:duration Location:location];
//                        NSLog(@"%@",times);
                        NSLog(@"%@",_firstLabel.textLabel.text);
                    });
                }else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_firstLabel setText:secondText];
                        [_secondLabel setText:firstText];
                        [_secondLabel startLyricsAnimationWithTimeArray:times Duration:duration  Location:location];
//                        NSLog(@"%@",times);
                        NSLog(@"%@",_secondLabel.textLabel.text);
                    });
                }
                NSLog(@"hhhhhhhhhhhhhh")
                index = nowIndex;
                break;
            }
        }
    });
}


- (void)pauseAnimation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_firstLabel pauseAnimation];
        [_secondLabel pauseAnimation];
    });
}

- (void)reuseAnimation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_firstLabel continueAnimation];
        [_secondLabel continueAnimation];
    });
   
}

- (void)stopAnimation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_firstLabel stopAnimation];
        [_secondLabel stopAnimation];
    });
}

- (void)resetData {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.lyrics.count > 1) {
            [_firstLabel setText:self.lyrics[0]];
            [_secondLabel setText:self.lyrics[1]];
        }
    });
}

- (void)setLyricPath:(NSString *)lyricPath {
    _lyricPath = lyricPath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:lyricPath]) {
        if ([lyricPath containsString:@".krc"]) {
            KRC * krc = [KRC new];
            NSString * lyric = [krc Decode:lyricPath];
            self.lyrics = [KRCLyricsUtil getLyricSArrayWithLyric:lyric];
            self.lyricTimes = [KRCLyricsUtil timeArrayWithLineLyric:lyric];
            self.starsTimes = [KRCLyricsUtil startTimeArrayWithLineLyric:lyric];
            self.durationTimes = [KRCLyricsUtil durationTimeArrayWithLineLyric:lyric];
            [self resetData];
        }else if ([lyricPath containsString:@".lrc"]) {
            NSError *error;
            NSString *lyric = [NSString stringWithContentsOfFile:lyricPath encoding:NSUTF8StringEncoding error:&error];
            LyricsTool *tool = [[LyricsTool alloc] init];
            [tool lyricAnalyzeWithLyric:lyric];
            [tool setAnalyzeBlock:^(NSArray * _Nonnull lyricModels) {
                NSMutableArray *lyrics = [NSMutableArray array];
                NSMutableArray *lyricTimes = [NSMutableArray array];
                NSMutableArray *starsTimes = [NSMutableArray array];
                for (LyricModel *model in lyricModels) {
                    [lyrics addObject:model.lyric];
                    NSMutableArray *times = [NSMutableArray array];
                    for (int i=0; i<model.lyric.length; i++) {
                        [times addObject:@((int)(model.duration*1000*i/model.lyric.length))];
                    }
                    [lyricTimes addObject:times];
                    [starsTimes addObject:@((int)(model.start*1000))];
                }
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    self.lyrics = [NSArray arrayWithArray:lyrics];
                    self.lyricTimes = [NSArray arrayWithArray:lyricTimes];
                    self.starsTimes = [NSArray arrayWithArray:starsTimes];
                    [self resetData];
                }];
            }];
        }else {
            
        }
    }
    else {
        
    }
}

- (KRCLyricsLabel *)firstLabel {
    if (!_firstLabel) {
        _firstLabel = [[KRCLyricsLabel alloc] init];
        [_firstLabel setFont:[UIFont boldSystemFontOfSize:20]];
       
        [_firstLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _firstLabel;
}

- (KRCLyricsLabel *)secondLabel {
    if (!_secondLabel) {
        _secondLabel = [[KRCLyricsLabel alloc] init];
        [_secondLabel setFont:[UIFont boldSystemFontOfSize:20]];
        
        [_secondLabel setTextAlignment:NSTextAlignmentRight];
    }
    return _secondLabel;
}

@end
