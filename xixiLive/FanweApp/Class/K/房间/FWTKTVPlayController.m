//
//  FWTKTVPlayController.m
//  FanweApp
//
//  Created by lyh on 2019/2/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomePlayController.h"
#import "GKWYMusicControlView.h"
#import "SDCycleScrollView.h"
#import "GKAudioPlayer.h"
#import "GKWYMusicTool.h"

@interface KTVHomePlayController ()<GKWYMusicControlViewDelegate,SDCycleScrollViewDelegate,GKAudioPlayerDelegate>

@property (nonatomic, strong) GKWYMusicControlView  *controlView;

@property (nonatomic, strong) SDCycleScrollView     *cycleScrollView;

@property (nonatomic, strong) UIImageView     *backImageView;

@property (nonatomic, assign) BOOL                  isAutoPlay;     // 是否自动播放，用于切换歌曲
@property (nonatomic, assign) BOOL                  isDraging;      // 是否正在拖拽进度条
@property (nonatomic, assign) BOOL                  isSeeking;      // 是否在快进快退，锁屏时操作
@property (nonatomic, assign) BOOL                  isChanged;      // 是否正在切换歌曲，点击上一曲下一曲按钮
@property (nonatomic, assign) BOOL                  isCoverScroll;  // 是否转盘在滑动
@property (nonatomic, assign) BOOL                  isPaused;       // 是否点击暂停
@property (nonatomic, assign) float                 toSeekProgress; // seek进度

@property (nonatomic, assign) NSTimeInterval        duration;       // 总时间
@property (nonatomic, assign) NSTimeInterval        currentTime;    // 当前时间
@property (nonatomic, assign) NSTimeInterval        positionTime;   // 锁屏时的滑杆时间

@end

@implementation KTVHomePlayController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)viewDidLoad {   
    [super viewDidLoad];
    

    self.title = @"";
    self.view.backgroundColor = kBackGroundColor;
    
    self.backImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cm2_fm_bg-ip6"]];
    [self.view addSubview:self.backImageView];
    
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenW,kScreenH) delegate:self placeholderImage:nil];
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.cycleScrollView.currentPageDotColor = kAppMainColor; // 自定义分页控件小圆标颜色
    self.cycleScrollView.autoScrollTimeInterval = 0.3;
    self.cycleScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.cycleScrollView];
    
    self.controlView = [[GKWYMusicControlView alloc] init];
    self.controlView.delegate = self;
    [self.view addSubview:self.controlView];
    [self.controlView bringSubviewToFront:self.view];
    
    [self.backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(170);
    }];
    
    [self.controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(170);
    }];
    
    [self setupBackBtnWithBlock:^{
        [[AppDelegate sharedAppDelegate] popViewController];
    }];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithTitle:@"演唱" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarItemAction)];
    rightBarItem.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    [self initPlay];
    // Do any additional setup after loading the view.
}

- (void)rightBarItemAction {
    
}

- (void)initPlay {
    NSString *url = [[NSBundle mainBundle] pathForResource:@"1" ofType:@"mp3"];
    KTVPLAY.playUrlStr = url;
    KTVPLAY.delegate = self;
    [self  playMusic];
    
    self.controlView.totalTime = [NSString stringWithFormat:@"%u:%u",KTVPLAY.duration.minute,KTVPLAY.duration.second];;
    [self.controlView setupPlayBtn];
}

- (void)playMusic {
    // 首先检查网络状态
    if (!KTVPLAY.playUrlStr) { // 没有播放地址
        // 需要重新请求
//        [self getMusicInfo];
        
    }else {
        if (KTVPLAY.playerState == GKAudioPlayerStateStopped) {
            [KTVPLAY play];
        }else if (KTVPLAY.playerState == GKAudioPlayerStatePaused) {
            [KTVPLAY resume];
        }else {
            [KTVPLAY play];
        }
    }
}

- (void)pauseMusic {
    [KTVPLAY pause];
}

- (void)stopMusic {
    [KTVPLAY stop];
}

#pragma mark SDCycleScrollViewDelegate


#pragma mark GKAudioPlayerDelegate

// 播放器状态改变
- (void)gkPlayer:(GKAudioPlayer *)player statusChanged:(GKAudioPlayerState)status {
    switch (status) {
        case GKAudioPlayerStateLoading:{    // 加载中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView showLoadingAnim];
                [self.controlView setupPauseBtn];
//                [self.coverView playedWithAnimated:YES];
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateBuffering: { // 缓冲中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPlayBtn];
//                [self.coverView playedWithAnimated:YES];
                
//                if (self.isAppear) {
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        [self.nameLabel scrollLabelIfNeeded];
//                    });
//                }
            });
            
            self.isPlaying = YES;
        }
            break;
        case GKAudioPlayerStatePlaying: {   // 播放中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPlayBtn];
//                [self.coverView playedWithAnimated:YES];
            });
            
            if (self.toSeekProgress > 0) {
                [KTVPLAY setPlayerProgress:self.toSeekProgress];
                
                self.toSeekProgress = 0;
            }
            
            self.isPlaying = YES;
        }
            break;
        case GKAudioPlayerStatePaused:{     // 暂停
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
                if (self.isChanged) {
                    self.isChanged = NO;
                }else {
//                    [self.coverView pausedWithAnimated:YES];
                }
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateStoppedBy:{  // 主动停止
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
                
                if (self.isChanged) {
                    self.isChanged = NO;
                }else {
                    NSLog(@"播放停止后暂停动画");
//                    [self.coverView pausedWithAnimated:YES];
                }
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateStopped:{    // 打断停止
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
//                [self.coverView pausedWithAnimated:YES];
                
                [self pauseMusic];
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateEnded: {     // 播放结束
            NSLog(@"播放结束了");
            if (self.isPlaying) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.controlView hideLoadingAnim];
                    [self.controlView setupPauseBtn];
//                    [self.coverView pausedWithAnimated:YES];
                    self.controlView.currentTime = self.controlView.totalTime;
                });
                
                self.isPlaying = NO;
                
                // 播放结束，自动播放下一首
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.isAutoPlay = YES;
                    
//                    [self playNextMusic];
                });
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.controlView hideLoadingAnim];
                    [self.controlView setupPauseBtn];
//                    [self.coverView pausedWithAnimated:YES];
                });
                
                self.isPlaying = NO;
            }
        }
            break;
        case GKAudioPlayerStateError: {     // 播放出错
            NSLog(@"播放出错了");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
//                [self.coverView pausedWithAnimated:YES];
            });
            self.isPlaying = NO;
        }
            break;
        default:
            break;
    }
    
//    [kNotificationCenter postNotificationName:GKWYMUSIC_PLAYSTATECHANGENOTIFICATION object:nil];
}

// 播放时间（单位：毫秒)、总时间（单位：毫秒）、进度（播放时间 / 总时间）
- (void)gkPlayer:(GKAudioPlayer *)player currentTime:(NSTimeInterval)currentTime totalTime:(NSTimeInterval)totalTime progress:(float)progress {
    self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:currentTime];
    self.controlView.progress    = progress;
}

// 总时间（单位：毫秒）
- (void)gkPlayer:(GKAudioPlayer *)player totalTime:(NSTimeInterval)totalTime {
    self.controlView.totalTime = [GKWYMusicTool timeStrWithMsTime:totalTime];
    self.duration = totalTime;
//    self.duration               = totalTime;
}


#pragma mark GKWYMusicControlViewDelegate
// 按钮点击
- (void)controlView:(GKWYMusicControlView *)controlView didClickLoop:(UIButton *)loopBtn {
    
}
- (void)controlView:(GKWYMusicControlView *)controlView didClickPrev:(UIButton *)prevBtn {
    
}
- (void)controlView:(GKWYMusicControlView *)controlView didClickPlay:(UIButton *)playBtn {
    if (self.isPlaying) {
        [self pauseMusic];
    }else {
        [self playMusic];
    }
}
- (void)controlView:(GKWYMusicControlView *)controlView didClickNext:(UIButton *)nextBtn {
    if (self.isCoverScroll) return;
    
    self.isAutoPlay = NO;
    
    if (self.isPlaying) {
        [self stopMusic];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.isChanged  = YES;
        
//        [self playNextMusic];
    });
}

- (void)controlView:(GKWYMusicControlView *)controlView didClickList:(UIButton *)listBtn {
    
}

// 滑杆滑动及点击
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTouchBegan:(float)value {
    self.isDraging = YES;
}
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTouchEnded:(float)value {
    self.isDraging = NO;

    if (self.isPlaying) {
        [KTVPLAY setPlayerProgress:value];
    }else {
        self.toSeekProgress = value;
    }

    // 滚动歌词到对应位置
//    [self.lyricView scrollLyricWithCurrentTime:(self.duration * value) totalTime:self.duration];
}
- (void)controlView:(GKWYMusicControlView *)controlView didSliderValueChange:(float)value {
    self.isDraging = YES;
    self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:(self.duration * value)];
}
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTapped:(float)value {
    self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:(self.duration * value)];

    if (self.isPlaying) {
        [KTVPLAY setPlayerProgress:value];
    }else {
        self.toSeekProgress = value;
    }

    // 滚动歌词到对应位置
    ///[self.lyricView scrollLyricWithCurrentTime:(self.duration * value) totalTime:self.duration];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
