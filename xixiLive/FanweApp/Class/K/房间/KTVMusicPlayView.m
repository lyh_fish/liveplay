//
//  KTVMusicPlayView.m
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVMusicPlayView.h"
#import "GKAudioPlayer.h"
#import "KTVHomePlayListController.h"
#import "GKMusicTool.h"

@interface KTVMusicPlayView()

@property (weak, nonatomic) IBOutlet UILabel *musicLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *playImageView;

@property (weak, nonatomic) IBOutlet UIButton *controlButton;

@property (weak, nonatomic) IBOutlet UIButton *listButton;

@property (weak, nonatomic) IBOutlet UIView *progressView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLayoutConstraint;


@end

@implementation KTVMusicPlayView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)coder {
    if ([super initWithCoder:coder]) {
        
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
        self.frame = frame;
        [self.progressView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.centerY.height.equalTo(self);
            make.width.equalTo(@(0));
        }];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateState) name:GKWYMUSIC_PLAYSTATECHANGENOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTime) name:GKWYMUSIC_PLAYTIMECHANGENOTIFICATION object:nil];
}

- (IBAction)controlButtonAction:(UIButton *)sender {
     [[GKMusicTool sharedInstance]playMusic];
}

- (IBAction)listButtonAction:(UIButton *)sender {
    KTVHomePlayListController *listVC = [[KTVHomePlayListController alloc] init];
    listVC.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    listVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.vc presentViewController:listVC animated:true completion:^{
        
    }];
}


- (void)updateState {
    GKAudioPlayerState status = [GKAudioPlayer sharedInstance].playerState;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (status == GKAudioPlayerStatePlaying) {
            NSMutableArray *images = [NSMutableArray new];
            for (NSInteger i = 0; i < 6; i++) {
                NSString *imageName = [NSString stringWithFormat:@"cm2_topbar_icn_playing%ld_prs", i + 1];
                [images addObject:[UIImage imageNamed:imageName]];
            }
            
            for (NSInteger i = 6; i > 0; i--) {
                NSString *imageName = [NSString stringWithFormat:@"cm2_topbar_icn_playing%ld_prs", i];
                [images addObject:[UIImage imageNamed:imageName]];
            }
            self.playImageView.animationImages = images;
            self.playImageView.animationDuration = 0.75;
            [self.playImageView startAnimating];
            [self.controlButton setTitle:@"暂停" forState:UIControlStateNormal];
        }
        else {
            if (self.playImageView.isAnimating) {
                [self.playImageView stopAnimating];
            }
            [self.playImageView setImage:[UIImage imageNamed:@"cm2_topbar_icn_playing1_prs"]];
            [self.controlButton setTitle:@"播放" forState:UIControlStateNormal];
        }
    });
}

- (void)updateTime {
    float progress = [GKAudioPlayer sharedInstance].progress;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.widthLayoutConstraint.constant = progress*CGRectGetWidth(self.frame);
        self.musicLabel.text = [GKMusicTool sharedInstance].music.title;
        self.nameLabel.text = [GKMusicTool sharedInstance].music.subtitle;
    });
}

@end
