//
//  KTVHomePlayListController.m
//  FanweApp
//
//  Created by lyh on 2019/3/3.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomePlayListController.h"
#import "KTVHomePlayListTableViewCell.h"
#import "FWTabBarController.h"
#import "GKMusicTool.h"

@interface KTVHomePlayListController ()<UITableViewDelegate,UITableViewDataSource>

{
    NSInteger index;
}

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) UIButton *closeButton;

@property (nonatomic,strong) UIView *setView;

@property (nonatomic,strong) NSArray *musics;

@end

@implementation KTVHomePlayListController

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:false];
    FWTabBarController.sharedInstance.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated  {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:false];
    FWTabBarController.sharedInstance.tabBar.hidden = false;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.closeButton];
    
    /// 设置view
    [self.view addSubview:self.setView];
    
    UIView *topLine = [[UIView alloc] init];
    topLine.backgroundColor = [UIColor redColor];
    [self.setView addSubview:topLine];
    
    UIView *bottomLine = [[UIView alloc] init];
    bottomLine.backgroundColor = [UIColor colorWithWhite:200/255.0 alpha:1];
    [self.setView addSubview:bottomLine];
    
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"KTVHomePlayListTableViewCell" bundle:nil] forCellReuseIdentifier:@"KTVHomePlayListTableViewCell"];
    [self.view addSubview:self.tableView];
    
    [self.setView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        make.height.equalTo(@(0));
        make.top.equalTo(self.view.mas_centerY);
    }];
    
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        make.top.equalTo(self.setView);
        make.height.equalTo(@(0.5));
    }];
    
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        make.bottom.equalTo(self.setView);
        make.height.equalTo(@(0.5));
    }];
    
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.bottom.equalTo(self.view);
        make.height.equalTo(@(44));
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        make.bottom.equalTo(self.closeButton.mas_top);
        make.top.equalTo(self.setView.mas_bottom);
    }];
    
    self.musics = [NSArray arrayWithArray:[GKMusicTool sharedInstance].musics];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTableView) name:GKWYMUSIC_MUSICTIMECHANGENOTIFICATION object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)closeButtonAction {
    [self dismissViewControllerAnimated:true completion:^{
        
    }];
}

- (void)updateTableView {
    [self.tableView reloadData];
}


#pragma mark UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVHomePlayListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KTVHomePlayListTableViewCell" forIndexPath:indexPath];
    GKMusic *music = self.musics[indexPath.row];
    BOOL isMusic = [[GKMusicTool sharedInstance].music.song_path isEqualToString:music.song_path];
    [cell updateWithMusic:music IsMusic:isMusic];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.musics.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GKMusic *music = self.musics[indexPath.row];
    BOOL isMusic = [[GKMusicTool sharedInstance].music.song_path isEqualToString:music.song_path];
    if (!isMusic) {
        [[GKMusicTool sharedInstance] downloadWithGKMusic:music];
        [[GKMusicTool sharedInstance] playMusic];
    }
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton new];
        [_closeButton setTitle:@"关闭" forState:UIControlStateNormal];
        _closeButton.backgroundColor = RGB(180, 180, 180);
        _closeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_closeButton addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIView *)setView {
    if (!_setView) {
        _setView = [[UIView alloc] init];
        _setView.backgroundColor = [UIColor whiteColor];
    }
    return _setView;
}

- (NSArray *)musics {
    if (!_musics) {
        _musics = [NSArray array];
    }
    return _musics;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
