//
//  KTVLyricsView.h
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVLyricsView : UIView

@property (nonatomic,copy) NSString *lyricPath;


- (void)startWithCurrentTime:(NSTimeInterval)currentTime;

- (void)scrollWithCurrentTime:(NSTimeInterval)currentTime;

- (void)pauseAnimation;

- (void)reuseAnimation;

- (void)resetData;

@end

NS_ASSUME_NONNULL_END
