//
//  KTVHomePlayView.m
//  FanweApp
//
//  Created by lyh on 2019/3/1.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomePlayView.h"
#import "KTVLyricsView.h"
#import "GKAudioPlayer.h"
#import "GKWYMusicTool.h"
#import "FWUtils.h"
#import "HMHotItemModel.h"

@interface KTVHomePlayView ()<GKWYMusicControlViewDelegate>

@property (nonatomic, strong) GKWYMusicControlView  *controlView;

@property (nonatomic, strong) KTVLyricsView *showView;

@property (nonatomic, strong) UIButton *ktvButton;

@property (nonatomic, assign) BOOL                  isDraging;      // 是否正在拖拽进度条
@property (nonatomic, assign) BOOL                  isSeeking;      // 是否在快进快退，锁屏时操作
@property (nonatomic, assign) BOOL                  isChanged;      // 是否正在切换歌曲，点击上一曲下一曲按钮
@property (nonatomic, assign) BOOL                  isPaused;       // 是否点击暂停
@property (nonatomic, assign) float                 toSeekProgress; // seek进度

@property (nonatomic, assign) NSTimeInterval        duration;       // 总时间
@property (nonatomic, assign) NSTimeInterval        currentTime;    // 当前时间
@property (nonatomic, assign) NSTimeInterval        positionTime;   // 锁屏时的滑杆时间

/** 是否正在播放 */
@property (nonatomic, assign) BOOL isPlaying;

/** 本地歌曲路径 */
@property (nonatomic, copy) NSString * song;
/** 本地歌词路径 */
@property (nonatomic, copy) NSString * lyric;

@property (nonatomic, assign) GKWYPlayerPlayStyle   playStyle;      // 循环类型



@end

@implementation KTVHomePlayView


- (id)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    self.isPlaying = KTVPLAY.playerState == GKAudioPlayerStatePlaying;
    self.playStyle = GKWYPlayerPlayStyleLoop;
    
    [self addSubview:self.showView];

    [self addSubview:self.controlView];
    
    [self addSubview:self.ktvButton];
    
    [self.showView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.equalTo(@(88));
    }];
    
    [self.controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.equalTo(self.showView.mas_bottom);
    }];
    
    [self.ktvButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-12));
        make.top.equalTo(self);
        make.height.equalTo(@(30));
        make.width.equalTo(@(60));
    }];
}

- (void)ktvButtonAction {
    [[GKMusicTool sharedInstance] stopMusic];
    if ([self.delegate respondsToSelector:@selector(toKTVWithHomePlayView:)]) {
        [self.delegate toKTVWithHomePlayView:self];
    }

    GKMusic *music = self.music;
    if ([music.other isKindOfClass:[KTVHotSong class]]) {
        KTVHotSong *song = music.other;
        KTVMusicModel *music = [[KTVMusicModel alloc] init];
        music.id = song.id;
        music.title = song.title;
        music.cover_path = song.cover_path;
        music.original_sing_path = song.original_sing_path;
        music.accompaniment_path = song.accompaniment_path;
        music.lyrics_path = song.lyrics_path;
        music.sort = song.sort;
        music.created_at = song.created_at;
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = music;
        vc.backRoot = YES;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
    }
    
    if ([music.other isKindOfClass:[KTVSong class]]) {
        KTVSong *song = music.other;
        KTVMusicModel *music = [[KTVMusicModel alloc] init];
        music.id = song.id;
        music.title = song.title;
        music.cover_path = @"";
        music.original_sing_path = song.original_sing_path;
        music.accompaniment_path = song.accompaniment_path;
        music.lyrics_path = song.lyrics_path;
        music.sort = @"";
        music.created_at = @"";
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = music;
        vc.backRoot = YES;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
        
    }
    
}

- (void)prepareWithMusic:(GKMusic *)music {
    
    self.music = music;
    
    GKMusicTool *tool = [GKMusicTool sharedInstance];
    __block GKMusicTool * blockTool = tool;
    __block KTVHomePlayView *blockSelf = self;
    tool.prepareblock = ^(NSString *song, NSString *lyric) {
        blockSelf.showView.lyricPath = lyric;
        [blockTool playMusic];
    };
    
    tool.stateblock = ^(GKAudioPlayerState state) {
        dispatch_async(dispatch_get_main_queue(), ^{
             [blockSelf updateState:state];
        });
    };
    
    tool.updatetimeblock = ^(NSTimeInterval currentTime, NSTimeInterval totalTime, float progress) {
        [blockSelf updateTimeWithCurrentTime:currentTime TotalTime:totalTime Progress:progress];
    };
    
    tool.updatetotaltimeblock = ^(NSTimeInterval totalTime) {
        dispatch_async(dispatch_get_main_queue(), ^{
            blockSelf.controlView.totalTime = [GKWYMusicTool timeStrWithMsTime:totalTime];
            blockSelf.duration = totalTime;
        });
    };
    
    tool.bufferblock = ^(float progress) {
        
    };

    dispatch_async(dispatch_get_main_queue(), ^{
        [tool downloadWithGKMusic:music];
    });
}

- (void)updateTimeWithCurrentTime:(NSTimeInterval)currentTime TotalTime:(NSTimeInterval)totalTime Progress:(float)progress {
    
    if (self.isDraging) return;
    if (self.isSeeking) return;
    
    if (self.toSeekProgress > 0) {
        progress = self.toSeekProgress;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:currentTime];
        self.controlView.progress    = progress;
        // 歌词滚动
        if (!self.isPlaying) return;
        [self.showView startWithCurrentTime:currentTime];
    });
}

#pragma mark GKAudioPlayerDelegate

// 播放器状态改变
- (void)updateState:(GKAudioPlayerState)state {
    switch (state) {
        case GKAudioPlayerStateLoading:{    // 加载中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView showLoadingAnim];
                [self.controlView setupPauseBtn];
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateBuffering: { // 缓冲中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPlayBtn];
            });
            
            self.isPlaying = YES;
        }
            break;
        case GKAudioPlayerStatePlaying: {   // 播放中
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPlayBtn];
            });
            
            if (self.toSeekProgress > 0) {
                [KTVPLAY setPlayerProgress:self.toSeekProgress];
                self.toSeekProgress = 0;
            }
            self.isPlaying = YES;
        }
            break;
        case GKAudioPlayerStatePaused:{     // 暂停
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
                if (self.isChanged) {
                    self.isChanged = NO;
                }else {

                }
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateStoppedBy:{  // 主动停止
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
                
                if (self.isChanged) {
                    self.isChanged = NO;
                }else {
                    NSLog(@"播放停止后暂停动画");
                    [self.showView pauseAnimation];
                }
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateStopped:{    // 打断停止
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
            });
            self.isPlaying = NO;
        }
            break;
        case GKAudioPlayerStateEnded: {     // 播放结束
            NSLog(@"播放结束了");
            if (self.isPlaying) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.controlView hideLoadingAnim];
                    [self.controlView setupPauseBtn];
                    [self.showView resetData];
                    self.controlView.currentTime = self.controlView.totalTime;
                });
                self.isPlaying = NO;
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.controlView hideLoadingAnim];
                    [self.controlView setupPauseBtn];
                });
                
                self.isPlaying = NO;
            }
        }
            break;
        case GKAudioPlayerStateError: {     // 播放出错
            NSLog(@"播放出错了");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlView hideLoadingAnim];
                [self.controlView setupPauseBtn];
            });
            self.isPlaying = NO;
        }
            break;
        default:
            break;
    }
}

#pragma mark GKWYMusicControlViewDelegate
// 按钮点击
- (void)controlView:(GKWYMusicControlView *)controlView didClickLoop:(UIButton *)loopBtn {
    if (self.playStyle == GKWYPlayerPlayStyleLoop) {  // 循环->单曲
        self.playStyle = GKWYPlayerPlayStyleOne;
    }else if (self.playStyle == GKWYPlayerPlayStyleOne) { // 单曲->随机
        self.playStyle = GKWYPlayerPlayStyleRandom;
    }else { // 随机-> 循环
        self.playStyle = GKWYPlayerPlayStyleLoop;
    }
    self.controlView.style = self.playStyle;
    [[GKMusicTool sharedInstance] switchPlayStyleWithStyle:self.playStyle];
}

- (void)controlView:(GKWYMusicControlView *)controlView didClickPrev:(UIButton *)prevBtn {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.isChanged  = YES;
        [[GKMusicTool sharedInstance] playLastMusic];
    });
}

- (void)controlView:(GKWYMusicControlView *)controlView didClickNext:(UIButton *)nextBtn {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.isChanged  = YES;
       [[GKMusicTool sharedInstance] playNextMusic];
    });
}


- (void)controlView:(GKWYMusicControlView *)controlView didClickPlay:(UIButton *)playBtn {
    if (self.isPlaying) {
        [[GKMusicTool sharedInstance] pauseMusic];
        [self.showView pauseAnimation];
    }else {
         [[GKMusicTool sharedInstance] playMusic];
        [self.showView reuseAnimation];
    }
}

// 滑杆滑动及点击
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTouchBegan:(float)value {
    self.isDraging = YES;
    NSLog(@"开始滑动了")
}
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTouchEnded:(float)value {
    self.isDraging = NO;
    // 滚动歌词到对应位置
    [self.showView scrollWithCurrentTime:(self.duration * value)];
    if (self.isPlaying) {
        [KTVPLAY setPlayerProgress:value];
    }else {
        self.toSeekProgress = value;
    }
    NSLog(@"结束滑动了")
}

- (void)controlView:(GKWYMusicControlView *)controlView didSliderValueChange:(float)value {
    self.isDraging = YES;
    self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:(self.duration * value)];
}
- (void)controlView:(GKWYMusicControlView *)controlView didSliderTapped:(float)value {
    self.controlView.currentTime = [GKWYMusicTool timeStrWithMsTime:(self.duration * value)];
    // 滚动歌词到对应位置
    [self.showView scrollWithCurrentTime:(self.duration * value)];
    if (self.isPlaying) {
        [KTVPLAY setPlayerProgress:value];
    }else {
        self.toSeekProgress = value;
    }
}

- (KTVLyricsView *)showView {
    if (!_showView) {
        _showView = [[KTVLyricsView alloc] init];
    }
    return _showView;
}

- (GKWYMusicControlView *)controlView {
    if (!_controlView) {
        _controlView = [[GKWYMusicControlView alloc] init];
        self.controlView.delegate = self;
    }
    return _controlView;
}

- (UIButton *)ktvButton {
    if (!_ktvButton) {
        _ktvButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_ktvButton setTitle:@"演唱" forState:UIControlStateNormal];
        _ktvButton.titleLabel.font = [UIFont systemFontOfSize:13];
        _ktvButton.clipsToBounds = YES;
        _ktvButton.layer.cornerRadius = 15;
        _ktvButton.layer.borderWidth = 0.5;
        _ktvButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [_ktvButton addTarget:self action:@selector(ktvButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ktvButton;
}

@end
