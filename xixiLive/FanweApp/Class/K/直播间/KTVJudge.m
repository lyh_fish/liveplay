//
//  KTVJudge.m
//  FanweApp
//
//  Created by lyh on 2019/5/9.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVJudge.h"

@implementation KTVJudge

+ (BOOL)isKTVWithAble:(id<FWShowLiveRoomAble>)able {
    TCShowLiveListItem *item = (TCShowLiveListItem *)able;
    return [item.type isEqualToString:@"2"];
}

@end
