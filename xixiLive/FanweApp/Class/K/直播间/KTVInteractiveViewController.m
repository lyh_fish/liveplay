//
//  KTVInteractiveViewController.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVInteractiveViewController.h"
#import <JXCategoryView/JXCategoryView.h>
#import <JXCategoryView/JXCategoryListContainerView.h>
#import "KTVCommentsViewController.h"
#import "KTVLikesViewController.h"
#import "KTVInputCommentsView.h"

@interface KTVInteractiveViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate,KTVInputCommentsViewDelegate>

@property (nonatomic,strong) JXCategoryTitleView *categoryView;

@property (nonatomic,strong) JXCategoryListContainerView *listContainerView;

@property (nonatomic,strong) NSArray *viewControllers;

@property (nonatomic, strong) KTVInputCommentsView *commentsView;

@property (nonatomic, copy) NSString *ID;

@end

@implementation KTVInteractiveViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 0.0f;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"评论|点赞";

    self.ID = [[NSUserDefaults standardUserDefaults] stringForKey:KTV_SONG_ID];
    
    [self setupBackBtnWithBlock:nil];
    
    [self.view addSubview:self.commentsView];
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.contentScrollView = self.listContainerView.scrollView;
    
    [self.commentsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
        make.height.equalTo(@(60));
    }];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerX.top.equalTo(self.view);
        make.right.equalTo(self.categoryView.mas_left);
        make.height.equalTo(@(50));
    }];
    
    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        make.bottom.equalTo(self.commentsView.mas_top);
        make.top.equalTo(self.categoryView.mas_bottom);
    }];
    // Do any additional setup after loading the view.
}

- (void)sendCommentsWithKTVInputText:(NSString *)inputText  {
    if ([FWUtils isBlankString:inputText]) {
        [FanweMessage alertTWMessage:@"请输入评论"];
        return;
    }
    
    if (self.ID) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"user" forKey:@"ctl"];
        [parmDict setObject:@"publish_comment" forKey:@"act"];
        [parmDict setObject:self.ID forKey:@"audio_user_id"];
        [parmDict setObject:inputText forKey:@"content"];
        [parmDict setObject:@"1" forKey:@"type"];
        [parmDict setObject:@"xr" forKey:@"itype"];
        
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 [self.view endEditing:true];
                 self.commentsView.inputTF.text = @"";
                 [FanweMessage alertTWMessage:@"评论成功"];
                 KTVCommentsViewController *commentsViewController = self.viewControllers.firstObject;
                 [commentsViewController headerRefresh];
                 [self refresh];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
             [FanweMessage alertHUD:error.description];
         }];
    }else {
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

- (void)refresh {
    [[NSNotificationCenter defaultCenter] postNotificationName:ROOM_UPDATE object:nil];
}

#pragma mark JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    [self.listContainerView didClickSelectedItemAtIndex:index];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView scrollingFromLeftIndex:(NSInteger)leftIndex toRightIndex:(NSInteger)rightIndex ratio:(CGFloat)ratio {
    [self.listContainerView scrollingFromLeftIndex:leftIndex toRightIndex:rightIndex ratio:ratio selectedIndex:categoryView.selectedIndex];
}

#pragma mark JXCategoryListContainerViewDelegate

//返回列表的数量
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return 3;
}
//返回遵从`JXCategoryListContentViewDelegate`协议的实例
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    id<JXCategoryListContentViewDelegate> list = self.viewControllers[index];
    return list;
}

- (JXCategoryTitleView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryTitleView alloc] init];
        _categoryView.delegate = self;
        _categoryView.titles = @[@"评论", @"点赞"];
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleSelectedColor = [UIColor redColor];
        _categoryView.titleFont = [UIFont systemFontOfSize:15];
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorLineViewColor = [UIColor redColor];
        lineView.indicatorLineWidth = JXCategoryViewAutomaticDimension;
        _categoryView.indicators = @[lineView];
    }
    return _categoryView;
}

- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithDelegate:self];
    }
    return _listContainerView;
}

- (NSArray *)viewControllers {
    if (!_viewControllers) {
        KTVCommentsViewController *commentsViewController = [[KTVCommentsViewController alloc] init];
        KTVLikesViewController *likesViewController = [[KTVLikesViewController alloc] init];
        _viewControllers = [NSArray arrayWithObjects:commentsViewController,likesViewController, nil];
    }
    return _viewControllers;
}

- (KTVInputCommentsView *)commentsView {
    if (!_commentsView) {
        _commentsView = [[NSBundle mainBundle] loadNibNamed:@"KTVInputCommentsView" owner:self options:nil].lastObject;
        _commentsView.delegate = self;
    }
    return _commentsView;
}
@end
