//
//  KTVCommentLikeView.m
//  FanweApp
//
//  Created by lyh on 2019/3/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVCommentLikeView.h"

@implementation KTVCommentLikeView

- (IBAction)commentButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(commentWithKTVCommentLikeView:)]) {
        [self.delegate commentWithKTVCommentLikeView:self];
    }
}

- (IBAction)likeButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(likeWithKTVCommentLikeView:)]) {
        [self.delegate likeWithKTVCommentLikeView:self];
    }
}
@end
