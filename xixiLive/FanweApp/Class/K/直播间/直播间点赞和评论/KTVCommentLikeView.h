//
//  KTVCommentLikeView.h
//  FanweApp
//
//  Created by lyh on 2019/3/23.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KTVCommentLikeView;
@protocol KTVCommentLikeViewDelegate <NSObject>

- (void)commentWithKTVCommentLikeView:(KTVCommentLikeView *)commentLikeView;

- (void)likeWithKTVCommentLikeView:(KTVCommentLikeView *)commentLikeView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVCommentLikeView : UIView

@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@property (nonatomic,weak) id <KTVCommentLikeViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
