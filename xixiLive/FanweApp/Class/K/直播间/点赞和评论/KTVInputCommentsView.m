//
//  KTVInputCommentsView.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVInputCommentsView.h"

@implementation KTVInputCommentsView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.sendButton.layer.borderWidth = 0.5;
    self.sendButton.layer.borderColor = self.sendButton.titleLabel.textColor.CGColor;
    self.sendButton.layer.cornerRadius = CGRectGetHeight(self.sendButton.frame)/2;
}

- (IBAction)sendButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(sendCommentsWithKTVInputText:)]) {
        [self.delegate sendCommentsWithKTVInputText:self.inputTF.text];
    }
}


@end
