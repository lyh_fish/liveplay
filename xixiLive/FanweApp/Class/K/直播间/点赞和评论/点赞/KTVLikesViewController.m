//
//  KTVLikesViewController.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLikesViewController.h"
#import "KTVLikesTableViewCell.h"

@interface KTVLikesViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    int currentPage;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, strong) NSMutableArray *likes;

@end

@implementation KTVLikesViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 5.0f;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ID = [[NSUserDefaults standardUserDefaults] stringForKey:KTV_SONG_ID];
    
    currentPage = 1;
    
    [self setupBackBtnWithBlock:nil];
    
    [FWMJRefreshManager refresh:self.tableView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:@selector(footerReresh)];
    
}

- (void)initFWUI {
    [super initFWUI];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

}

- (void)initFWData {
    [super initFWData];
}

- (void)headerRefresh {
    currentPage = 1;
    [self requestNetDataWithPage:1];
}

- (void)footerReresh {
    currentPage ++;
    [self requestNetDataWithPage:currentPage];
}

+ (void)like  {
    
    NSString *ID = [[NSUserDefaults standardUserDefaults] stringForKey:KTV_SONG_ID];
    
    if (ID) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"user" forKey:@"ctl"];
        [parmDict setObject:@"publish_comment" forKey:@"act"];
        [parmDict setObject:ID forKey:@"audio_user_id"];
        [parmDict setObject:@"2" forKey:@"type"];
        [parmDict setObject:@"xr" forKey:@"itype"];
        
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 [FanweMessage alertTWMessage:@"点赞成功"];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
         }];
    }else {
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

- (void)requestNetDataWithPage:(int)Page
{
    FWWeakify(self)
    if (self.ID) {
        
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:self.ID forKey:@"audio_user_id"];
        [parmDict setObject:@"audio" forKey:@"ctl"];
        [parmDict setObject:@"my_song_by_id" forKey:@"act"];
        [parmDict setObject:self.ID forKey:@"id"];
        
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson) {
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                NSArray *comments = [KTVLikeModel mj_objectArrayWithKeyValuesArray:responseJson[@"song"][@"digg_list"]];
                if (Page == 1) {
                    self.likes = [NSMutableArray arrayWithArray:comments];
                }else {
                    [self.likes addObjectsFromArray:comments];
                }
                [self.tableView reloadData];
            }else
            {
                [FanweMessage alertHUD:[responseJson toString:@"error"]];
            }
            [FWMJRefreshManager endRefresh:self.tableView];
        } FailureBlock:^(NSError *error) {
            FWStrongify(self)
            [FWMJRefreshManager endRefresh:self.tableView];
        }];
    }else {
        [FWMJRefreshManager endRefresh:self.tableView];
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

#pragma mark ----  UITableViewDelegate,UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVLikesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.comments[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (NSMutableArray *)comments {
    if (!_likes) {
        _likes = [NSMutableArray array];
    }
    return _likes;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"KTVLikesTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

@end
