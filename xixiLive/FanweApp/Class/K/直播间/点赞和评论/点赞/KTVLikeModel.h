//
//  KTVLikeModel.h
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVLikeModel : NSObject

@property (nonatomic,copy) NSString *user_id;

@property (nonatomic,copy) NSString *nick_name;

@property (nonatomic,copy) NSString *head_image;

@property (nonatomic,copy) NSString *is_authentication;

@property (nonatomic,copy) NSString *create_time;

@end

NS_ASSUME_NONNULL_END
