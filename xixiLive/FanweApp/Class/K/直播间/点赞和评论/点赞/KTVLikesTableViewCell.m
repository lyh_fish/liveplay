//
//  KTVLikesTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVLikesTableViewCell.h"

@implementation KTVLikesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImageView.layer.cornerRadius = CGRectGetHeight(self.photoImageView.frame)/2;
    self.photoImageView.clipsToBounds = true;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setModel:(KTVLikeModel *)model {
    _model = model;
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:model.head_image] placeholderImage:kDefaultPreloadHeadImg];
    if ([FWUtils isBlankString:model.nick_name]) {
        self.nameLabel.text = model.user_id;
    }else {
        self.nameLabel.text = model.nick_name;
    }
    self.timeLabel.text = model.create_time;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
