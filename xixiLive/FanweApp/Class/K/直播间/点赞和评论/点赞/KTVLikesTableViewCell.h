//
//  KTVLikesTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVLikeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVLikesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic,strong) KTVLikeModel *model;

@end

NS_ASSUME_NONNULL_END
