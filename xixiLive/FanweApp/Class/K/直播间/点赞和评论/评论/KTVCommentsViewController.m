//
//  KTVCommentsViewController.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVCommentsViewController.h"
#import "FWReplyCell.h"
#import "CommentModel.h"

@interface KTVCommentsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    int currentPage;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, strong) NSMutableArray *comments;

@end

@implementation KTVCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ID = [[NSUserDefaults standardUserDefaults] stringForKey:KTV_SONG_ID];

    currentPage = 1;
    
    [self setupBackBtnWithBlock:nil];
    [FWMJRefreshManager refresh:self.tableView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:nil];
    
}

- (void)initFWUI {
    [super initFWUI];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)initFWData {
    [super initFWData];
}

- (void)headerRefresh {
    currentPage = 1;
    [self requestNetDataWithPage:1];
}

- (void)footerReresh {
    currentPage ++;
    [self requestNetDataWithPage:currentPage];
}

- (void)sendCommentsWithKTVInputText:(NSString *)inputText  {
    if ([FWUtils isBlankString:inputText]) {
        [FanweMessage alertTWMessage:@"请输入评论"];
        return;
    }
    
    if (self.ID) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"user" forKey:@"ctl"];
        [parmDict setObject:@"publish_comment" forKey:@"act"];
        [parmDict setObject:self.ID forKey:@"audio_user_id"];
        [parmDict setObject:inputText forKey:@"content"];
        [parmDict setObject:@"1" forKey:@"type"];
        [parmDict setObject:@"xr" forKey:@"itype"];
    
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 [FanweMessage alertTWMessage:@"评论成功"];
                 [self requestNetDataWithPage:1];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
             [FWMJRefreshManager endRefresh:self.tableView];
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
             [FWMJRefreshManager endRefresh:self.tableView];
         }];
    }else {
        [FWMJRefreshManager endRefresh:self.tableView];
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

- (void)requestNetDataWithPage:(int)Page
{
    FWWeakify(self)
    if (self.ID) {
        
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"audio" forKey:@"ctl"];
        [parmDict setObject:@"my_song_by_id" forKey:@"act"];
        [parmDict setObject:self.ID forKey:@"id"];
        
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson) {
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                NSArray *comments = [CommentModel mj_objectArrayWithKeyValuesArray:responseJson[@"song"][@"comment_list"]];
                if (Page == 1) {
                    self.comments = [NSMutableArray arrayWithArray:comments];
                }else {
                    [self.comments addObjectsFromArray:comments];
                }
                [self.tableView reloadData];
            }else
            {
                [FanweMessage alertHUD:[responseJson toString:@"error"]];
            }
            [FWMJRefreshManager endRefresh:self.tableView];
        } FailureBlock:^(NSError *error) {
            FWStrongify(self)
            [FWMJRefreshManager endRefresh:self.tableView];
        }];
    }else {
        [FWMJRefreshManager endRefresh:self.tableView];
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}

#pragma mark ----  UITableViewDelegate,UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FWReplyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FWReplyCell" forIndexPath:indexPath];
    [cell creatCellWithModel:self.comments[indexPath.row] andRow:(int)indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"FWReplyCell";
    FWReplyCell *cell = (FWReplyCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (cell == nil) {
        cell = [[FWReplyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return [cell creatCellWithModel:self.comments[indexPath.row] andRow:(int)indexPath.row];;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (NSMutableArray *)comments {
    if (!_comments) {
        _comments = [NSMutableArray array];
    }
    return _comments;
}

//- (KTVInputCommentsView *)commentsView {
//    if (!_commentsView) {
//        _commentsView = [[NSBundle mainBundle] loadNibNamed:@"KTVInputCommentsView" owner:self options:nil].lastObject;
//        _commentsView.delegate = self;
//    }
//    return _commentsView;
//}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[FWReplyCell class] forCellReuseIdentifier:@"FWReplyCell"];
    }
    return _tableView;
}

@end
