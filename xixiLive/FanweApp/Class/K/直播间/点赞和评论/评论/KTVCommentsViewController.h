//
//  KTVCommentsViewController.h
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVCommentsViewController : FWBaseViewController<JXCategoryListContentViewDelegate>

- (void)headerRefresh;
@end

NS_ASSUME_NONNULL_END
