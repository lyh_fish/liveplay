//
//  KTVInputCommentsView.h
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KTVInputCommentsViewDelegate <NSObject>

- (void)sendCommentsWithKTVInputText:(NSString *)inputText;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVInputCommentsView : UIView
@property (weak, nonatomic) IBOutlet UITextField *inputTF;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (nonatomic,weak) id <KTVInputCommentsViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
