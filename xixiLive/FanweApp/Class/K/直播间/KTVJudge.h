//
//  KTVJudge.h
//  FanweApp
//
//  Created by lyh on 2019/5/9.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppAble.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVJudge : NSObject

+ (BOOL)isKTVWithAble:(id<FWShowLiveRoomAble>)able;

@end

NS_ASSUME_NONNULL_END
