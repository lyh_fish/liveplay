//
//  GKWYMusicTool.h
//  GKWYMusic
//
//  Created by gaokun on 2018/4/20.
//  Copyright © 2018年 gaokun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GKWYMusicTool : NSObject

// 时间转字符串：毫秒-> string
+ (NSString *)timeStrWithMsTime:(NSTimeInterval)msTime;
// 时间转字符串：秒 -> string
+ (NSString *)timeStrWithSecTime:(NSTimeInterval)secTime;

+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font;
+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxW:(CGFloat)maxW;


@end
