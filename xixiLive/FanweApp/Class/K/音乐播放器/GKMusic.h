//
//  GKMusic.h
//  FanweApp
//
//  Created by lyh on 2019/3/1.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GKMusic : NSObject
/// 歌曲名称
@property (nonatomic,copy) NSString *title;
/// 副标题
@property (nonatomic,copy) NSString *subtitle;
/// 封面地址,是数组
@property (nonatomic,copy) NSString  *cover_path;
/// 歌曲地址
@property (nonatomic,copy) NSString *song_path;
/// 歌曲歌词地址
@property (nonatomic,copy) NSString *lyric_path;
///// 时间
@property (nonatomic,strong) id other;

@end

NS_ASSUME_NONNULL_END
