//
//  GKMusicTool.h
//  FanweApp
//
//  Created by lyh on 2019/3/1.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKMusic.h"
#import "GKAudioPlayer.h"

#define GKWYMUSIC_PLAYSTATECHANGENOTIFICATION   @"playStateChangeNotification" // 播放状态改变

#define GKWYMUSIC_PLAYTIMECHANGENOTIFICATION   @"playTimeChangeNotification" // 播放状态改变

#define GKWYMUSIC_MUSICTIMECHANGENOTIFICATION   @"playMusicChangeNotification" // 播放状态改变

typedef NS_ENUM(NSUInteger, GKWYPlayerPlayStyle) {
    GKWYPlayerPlayStyleLoop,        // 循环播放
    GKWYPlayerPlayStyleOne,         // 单曲循环
    GKWYPlayerPlayStyleRandom       // 随机播放
};

typedef void(^PrepareBlock)(NSString *song,NSString *lyric);

typedef void(^StateBlock)(GKAudioPlayerState state);

typedef void(^UpdateTimeBlock)(NSTimeInterval currentTime,NSTimeInterval totalTime,float progress);

typedef void(^UpdateTotalTimeBlock)(NSTimeInterval totalTime);

typedef void(^BufferBlock)(float progress);

NS_ASSUME_NONNULL_BEGIN

@interface GKMusicTool : NSObject
/**
 正在播放的音乐
 */
@property (nonatomic, strong) GKMusic *music;

@property (nonatomic, strong) NSMutableArray <GKMusic *> *musics;

@property (nonatomic, assign,readonly) GKWYPlayerPlayStyle style;

@property (nonatomic, copy) PrepareBlock prepareblock;

@property (nonatomic, copy) StateBlock stateblock;

@property (nonatomic, copy) UpdateTimeBlock updatetimeblock;

@property (nonatomic, copy) UpdateTotalTimeBlock updatetotaltimeblock;

@property (nonatomic, copy) BufferBlock bufferblock;

+ (instancetype)sharedInstance;

- (void)downloadWithGKMusic:(GKMusic *)music;

- (void)playMusic;

- (void)pauseMusic;

- (void)stopMusic;
/**
 移除全部音乐
 */
- (void)removeAllMusics;

/**
 上一首音乐
 */
- (void)playLastMusic;

/**
 下一首音乐
 */
- (void)playNextMusic;

- (void)switchPlayStyleWithStyle:(GKWYPlayerPlayStyle)style;

@end

NS_ASSUME_NONNULL_END
