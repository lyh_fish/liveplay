//
//  GKMusicTool.m
//  FanweApp
//
//  Created by lyh on 2019/3/1.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "GKMusicTool.h"

@interface GKMusicTool()<GKAudioPlayerDelegate>

@property (nonatomic, strong) NSMutableArray *playedMusics;

@property (nonatomic, assign,readwrite) GKWYPlayerPlayStyle style;

@property (nonatomic, assign) BOOL isStop;

@end

@implementation GKMusicTool

static GKMusicTool *tool = nil;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[GKMusicTool alloc] init];
        
    });
    return tool;
}

- (void)downloadWithGKMusic:(GKMusic *)music {
    self.music = music;
    KTVPLAY.delegate = self;
    [self stopMusic];
    self.isStop = false;
    
    if ([music.song_path containsString:@"http://"]) {
        [KTVDownload downloadMusicWithMusic:music musicDownloadBlock:^(double p, enum KTVDownloadStatus state, NSString * song, NSString * lyric) {
            switch (state) {
                case KTVDownloadStatusSuccess:
                    [SVProgressHUD dismiss];
                    if (!self.isStop) {
                        [self preparePlayWithSong:song];
                    }
                    
                    if (self.prepareblock) {
                        self.prepareblock(song, lyric);
                    }
                    break;
                case KTVDownloadStatusExist:
                    [SVProgressHUD dismiss];
                    if (!self.isStop) {
                        [self preparePlayWithSong:song];
                    }
                    if (self.prepareblock) {
                        self.prepareblock(song, lyric);
                    }
                    break;
                case KTVDownloadStatusDownload:
                    [SVProgressHUD showProgress:p];
                    break;
                default:
                    [SVProgressHUD dismiss];
                    [FanweMessage alertTWMessage:@"音乐加载失败,请重试"];
                    break;
            }
        }];
    }else {
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *song = [NSString stringWithFormat:@"%@/%@",path,music.song_path];
        NSString *lyric = [KTVDownload cachePathWithUrl:music.lyric_path];
        if (!self.isStop) {
            [self preparePlayWithSong:song];
        }
        
        if (self.prepareblock) {
            self.prepareblock(song, lyric);
        }
    }
}

- (void)preparePlayWithSong:(NSString *)song {
    [KTVPLAY stop];
    KTVPLAY.playUrlStr = song;
    _isStop = true;
}

- (void)playMusic {
    NSLog(@"%ld",KTVPLAY.playerState);
    if (KTVPLAY.playerState == GKAudioPlayerStateStopped || KTVPLAY.playerState == GKAudioPlayerStateEnded) {
        [KTVPLAY play];
    }else if (KTVPLAY.playerState == GKAudioPlayerStatePaused) {
        [KTVPLAY resume];
    }else if (KTVPLAY.playerState == GKAudioPlayerStatePlaying ) {
        [KTVPLAY pause];
    }
    else {
        [KTVPLAY play];
    }
}

- (void)pauseMusic {
    [KTVPLAY pause];
}

- (void)stopMusic {
    [KTVPLAY stop];
    self.musics = [NSMutableArray array];
    self.music = nil;
}

- (void)playLastMusic {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (KTVPLAY.playerState == GKAudioPlayerStatePlaying) {
            [self stopMusic];
        }
       [self downloadWithGKMusic:[self lastMusic]];
    });
}

- (void)playNextMusic {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (KTVPLAY.playerState == GKAudioPlayerStatePlaying) {
            [self stopMusic];
        }
        [self downloadWithGKMusic:[self nextMusic]];
    });
}

- (void)removeAllMusics {
    [self.musics removeAllObjects];
}

- (GKMusic *)lastMusic {
    NSInteger index = [self.musics indexOfObject:self.music];
    if (index == 0) {
        return self.musics.lastObject;
    }else {
        return self.musics[index - 1];
    }
}

- (GKMusic *)nextMusic {
    NSInteger index = [self.musics indexOfObject:self.music];
    if (index > self.musics.count) {
        return self.musics.firstObject;
    }else {
        return self.musics[index + 1];
    }
}

- (void)switchPlayStyleWithStyle:(GKWYPlayerPlayStyle)style {
    _style = style;
}

#pragma mark ------ GKAudioPlayerDelegate

// 播放器状态改变
- (void)gkPlayer:(GKAudioPlayer *)player statusChanged:(GKAudioPlayerState)status {
    if (status == GKAudioPlayerStateEnded) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[GKMusicTool sharedInstance]playMusic];
        });
    }
    
    if (self.stateblock) {
        self.stateblock(status);
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:GKWYMUSIC_PLAYSTATECHANGENOTIFICATION object:nil];
}

// 播放时间（单位：毫秒)、总时间（单位：毫秒）、进度（播放时间 / 总时间）
- (void)gkPlayer:(GKAudioPlayer *)player currentTime:(NSTimeInterval)currentTime totalTime:(NSTimeInterval)totalTime progress:(float)progress {
    if (self.updatetimeblock) {
        self.updatetimeblock(currentTime, totalTime, progress);
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:GKWYMUSIC_PLAYTIMECHANGENOTIFICATION object:nil];
}

// 总时间（单位：毫秒）
- (void)gkPlayer:(GKAudioPlayer *)player totalTime:(NSTimeInterval)totalTime {
    if (self.updatetotaltimeblock) {
        self.updatetotaltimeblock(totalTime);
    }
}

// 缓冲进度
- (void)gkPlayer:(GKAudioPlayer *)player bufferProgress:(float)bufferProgress {
    if (self.bufferblock) {
        self.bufferblock(bufferProgress);
    }
}

- (NSMutableArray *)musics {
    if (!_musics) {
        _musics = [NSMutableArray array];
    }
    return _musics;
}

- (NSMutableArray *)playedMusics {
    if (!_playedMusics) {
        _playedMusics = [NSMutableArray array];
    }
    return _playedMusics;
}

@end
