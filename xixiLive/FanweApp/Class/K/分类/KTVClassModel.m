//
//  KTVClassModel.m
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVClassModel.h"

@implementation KTVClassModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"_child" : @"KTVClassChildModel",
             };
}

@end

@implementation KTVClassChildModel

@end
