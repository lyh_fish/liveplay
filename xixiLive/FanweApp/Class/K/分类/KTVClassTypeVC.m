//
//  KTVClassTypeVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/17.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVClassTypeVC.h"
#import "KTVPointMusicListVC.h"
#import "KTVClassModel.h"

@interface KTVClassTypeVC ()

@property (nonatomic, strong) NSArray *musicClasses;
@end

@implementation KTVClassTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView reloadData];
    
    [self setupBackBtnWithBlock:nil];
}

- (void)initFWUI {
    [super initFWUI];
}

- (void)initFWData {
    [super initFWData];
    [self loadNetData];
}

- (void)loadNetData
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"music_classified" forKey:@"act"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             self.musicClasses = [KTVClassModel mj_objectArrayWithKeyValuesArray:responseJson[@"music_classified"]];
            [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}


#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    KTVClassModel *class = self.musicClasses[indexPath.section];
    KTVClassChildModel *child = class._child[indexPath.row];
    cell.textLabel.text = child.cat_name;
    cell.textLabel.textColor = [UIColor darkTextColor];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.musicClasses.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    KTVClassModel *class = self.musicClasses[section];
    return class._child.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    KTVClassModel *class = self.musicClasses[section];
    if (section <  class._child.count-1) {
        return 32;
    }else {
        return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    KTVClassModel *class = self.musicClasses[section];
    return class.cat_name;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    KTVClassModel *class = self.musicClasses[indexPath.section];
    KTVClassChildModel *child = class._child[indexPath.row];
    
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"song_by_music_classified_id" forKey:@"act"];
    [parmDict setObject:child.id forKey:@"music_classified_id"];
    
    KTVPointMusicListVC *listVC = [[KTVPointMusicListVC alloc] init];
    listVC.title = child.cat_name;
    listVC.parmDict = [NSDictionary dictionaryWithDictionary:parmDict];
    [[AppDelegate sharedAppDelegate] pushViewController:listVC];
}

- (NSArray *)musicClasses {
    if (!_musicClasses) {
        _musicClasses = [NSArray array];
    }
    return _musicClasses;
}

@end
