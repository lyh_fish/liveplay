//
//  KTVClassModel.h
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class KTVClassChildModel;

@interface KTVClassModel : NSObject

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *parent_id;
/// 语言
@property (nonatomic,copy) NSString *cat_name;

@property (nonatomic,copy) NSString *is_effect;

@property (nonatomic,copy) NSString *order;

@property (nonatomic,copy) NSString *created_at;

@property (nonatomic,strong) NSMutableArray <KTVClassChildModel *> *_child;

@end


@interface KTVClassChildModel : NSObject

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *parent_id;
/// 语言
@property (nonatomic,copy) NSString *cat_name;

@property (nonatomic,copy) NSString *is_effect;

@property (nonatomic,copy) NSString *order;

@property (nonatomic,copy) NSString *created_at;

@end

NS_ASSUME_NONNULL_END
