//
//  KTVDownload2.swift
//  FanweApp
//
//  Created by lyh on 2019/1/22.
//  Copyright © 2019 xfg. All rights reserved.
//

import UIKit
import Tiercel

@objc public enum KTVDownloadStatus:NSInteger {

    case exist
    
    case success
    
    case failure
    
    case other
    
    case download
    
    case empty
}


typealias DownloadBlock = (_ progress:Double,_ state:KTVDownloadStatus) -> Void

typealias musicDownloadBlock = (_ progress:Double,_ state:KTVDownloadStatus,_ musicpath:String,_ lyricpath:String) -> Void

class KTVDownload: NSObject {
    
    static let muscicDownload = TRManager()
    
    @objc static func download(model:KTVMusicModel,downloadBlock:@escaping DownloadBlock) -> Void {
        
        var URLStrings:[String] = []
        
        /// 原唱和伴奏都是空的
        if model.accompaniment_path.isEmpty && model.original_sing_path.isEmpty{
            downloadBlock(0,.empty)
            return;
        }else if  model.accompaniment_path.isEmpty && !model.original_sing_path.isEmpty {
            model.accompaniment_path = model.original_sing_path
            KTVPath.share().support = .music
            print("该歌曲不支持伴奏")
        }else if !model.accompaniment_path.isEmpty && model.original_sing_path.isEmpty {
            model.original_sing_path = model.accompaniment_path
            KTVPath.share().support = .amp
            print("该歌曲不支持原唱")
        }else {
            KTVPath.share().support = .all
            print("该歌曲支持伴奏/原唱")
        }
    
        if !model.accompaniment_path.isEmpty {
            if (TRCache.default.fileExists(URLString: model.accompaniment_path)) {
                 print("文件存在")
                KTVPath.share().ampPath = TRCache.default.filePtah(URLString:model.accompaniment_path) ?? ""
            }else {
                URLStrings.append(model.accompaniment_path)
            }
        }
        
        if !model.original_sing_path.isEmpty {
            if (TRCache.default.fileExists(URLString: model.original_sing_path)) {
                KTVPath.share().musicPath = TRCache.default.filePtah(URLString:model.original_sing_path) ?? ""
                print("文件存在")
            }else {
                URLStrings.append(model.original_sing_path)
            }
        }
        
        if !model.lyrics_path.isEmpty {
            if (TRCache.default.fileExists(URLString: model.lyrics_path)) {
                print("文件存在")
                KTVPath.share().lyricePath = TRCache.default.filePtah(URLString:model.lyrics_path) ?? ""
            }else {
                URLStrings.append(model.lyrics_path)
            }
        }
        
        
        if URLStrings.count == 0 {
            downloadBlock(1,.exist)
            return;
        }
        
        let downloadManager = TRManager()
    
        var tasks = downloadManager.multiDownload(URLStrings, fileNames: nil)
        
        downloadManager.totalStart()
        
        downloadManager.progress { (manager) in
            let progress = manager.progress.fractionCompleted
            downloadBlock(progress,.download)
            print("downloadManager运行中, 总进度：\(progress)")
            }.success { (manager) in
                if manager.status == .suspend {
                    downloadBlock(0,.failure)
                    print("manager暂停了")
                } else if manager.status == .completed {
                    
                    ///
                    KTVPath.share().musicPath = TRCache.default.filePtah(URLString:model.original_sing_path) ?? ""
                    KTVPath.share().ampPath = TRCache.default.filePtah(URLString:model.accompaniment_path) ?? ""
                    KTVPath.share().lyricePath = TRCache.default.filePtah(URLString:model.lyrics_path) ?? ""
                
                    print("所有下载任务都下载成功")
                    downloadBlock(1,.success)
                    tasks.removeAll()
                }
            }.failure { (manager) in
                downloadBlock(0,.failure)
                if manager.status == .failed {
                    print("存在下载失败的任务")
                } else if manager.status == .cancel {
                    print("manager取消了")
                } else if manager.status == .remove {
                    print("manager移除了")
                }
        }
        
    }
    
    @objc static func downloadMusic(music:GKMusic,musicDownloadBlock:@escaping musicDownloadBlock) -> Void {
        
        var song = ""
        var lyric = ""
        
        if muscicDownload.tasks.count != 0 {
            print("取消下载")
            muscicDownload.totalCancel()
        }
        
        var URLStrings:[String] = []
        
        /// 原唱和伴奏都是空的
        if music.song_path.isEmpty && music.lyric_path.isEmpty{
            musicDownloadBlock(0,.empty,"","")
            return;
        }
        
        if !music.song_path.isEmpty {
            if (TRCache.default.fileExists(URLString: music.song_path)) {
                print("文件存在")
                song = TRCache.default.filePtah(URLString:music.song_path)!
            }else {
                URLStrings.append(music.song_path)
            }
        }
    
        if !music.lyric_path.isEmpty {
            if (TRCache.default.fileExists(URLString: music.lyric_path)) {
                print("文件存在")
                lyric = TRCache.default.filePtah(URLString:music.lyric_path)!
            }else {
                URLStrings.append(music.lyric_path)
            }
        }
        
        if URLStrings.count == 0 {
            musicDownloadBlock(1,.exist,song,lyric)
            return;
        }
    
        var tasks = muscicDownload.multiDownload(URLStrings, fileNames: nil)
        
        muscicDownload.totalStart()
        
        muscicDownload.progress { (manager) in
            let progress = manager.progress.fractionCompleted
            musicDownloadBlock(progress,.download,"","")
            print("downloadManager运行中, 总进度：\(progress)")
            }.success { (manager) in
                if manager.status == .suspend {
                    musicDownloadBlock(0,.failure,"","")
                    print("manager暂停了")
                } else if manager.status == .completed {
                    song = TRCache.default.filePtah(URLString:music.song_path) ?? ""
                    lyric = TRCache.default.filePtah(URLString:music.lyric_path) ?? ""
                    print("所有下载任务都下载成功")
                    musicDownloadBlock(1,.success,song,lyric)
                    tasks.removeAll()
                }
            }.failure { (manager) in
                musicDownloadBlock(0,.failure,"","")
                if manager.status == .failed {
                    print("存在下载失败的任务")
                } else if manager.status == .cancel {
                    print("manager取消了")
                } else if manager.status == .remove {
                    print("manager移除了")
                }
        }
        
    }
    
    @objc static func cachePath(url:String) -> String {
        return TRCache.default.filePtah(URLString:url) ?? ""
    }

}
