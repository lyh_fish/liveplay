//
//  KTVSignerTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVSignerTableViewCell.h"

@implementation KTVSignerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(KTVSignerModel *)model {
    _model = model;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:model.cover_img]placeholderImage:[UIImage imageNamed:@"KTV_signer_default"]];
    self.nameLabel.text = model.singer_name;
    self.instructionLabel.text = @" ";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
