//
//  KTVSignerModel.h
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVSignerModel : NSObject

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *singer_classified_id;

@property (nonatomic,copy) NSString *singer_name;

@property (nonatomic,copy) NSString *cover_img;

@property (nonatomic,copy) NSString *created_at;

@end

NS_ASSUME_NONNULL_END
