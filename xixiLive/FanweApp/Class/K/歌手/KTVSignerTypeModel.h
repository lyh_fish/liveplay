//
//  KTVSignerTypeModel.h
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVSignerTypeModel : NSObject

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *singer_cat_name;

@property (nonatomic,copy) NSString *sort;

@end

NS_ASSUME_NONNULL_END
