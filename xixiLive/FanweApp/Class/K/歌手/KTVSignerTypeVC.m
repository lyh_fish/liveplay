//
//  KTVSignerTypeVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/17.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVSignerTypeVC.h"
#import "KTVSignersVC.h"
#import "KTVSignerTypeModel.h"

@interface KTVSignerTypeVC ()

@property (nonatomic, strong) NSArray *classes;

@property (nonatomic, strong) KTVSignerTypeModel *allModel;

@end

@implementation KTVSignerTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView reloadData];
    
    [self setupBackBtnWithBlock:nil];
}

- (void)initFWUI {
    [super initFWUI];
}

- (void)initFWData {
    [super initFWData];
    [self loadNetData];
}

- (void)loadNetData
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"singer_classified" forKey:@"act"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             ///NSArray *music_classified = [NSArray arrayWithObject:responseJson[@"music_classified"]];
             self.classes = [KTVSignerTypeModel mj_objectArrayWithKeyValuesArray:responseJson[@"singer_classified"]];
             [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}


#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.textColor = [UIColor darkTextColor];
    if (indexPath.section == 0) {
        cell.textLabel.text = @"全部歌手";
    }else {
        KTVSignerTypeModel *model = self.classes[indexPath.row];
        cell.textLabel.text = model.singer_cat_name;
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else {
        return self.classes.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==0) {
        return 16;
    }else {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        KTVSignersVC *signersVC = [[KTVSignersVC alloc] init];
        signersVC.title = @"全部歌手";
        signersVC.classify_id = @"";
        [[AppDelegate sharedAppDelegate] pushViewController:signersVC];
    }else {
        KTVSignerTypeModel *model = self.classes[indexPath.row];
        KTVSignersVC *signersVC = [[KTVSignersVC alloc] init];
        signersVC.title = model.singer_cat_name;
        signersVC.classify_id = model.id;
        [[AppDelegate sharedAppDelegate] pushViewController:signersVC];
    }
}

- (NSArray *)classes {
    if (!_classes) {
        _classes = [NSArray array];
    }
    return _classes;
}

@end
