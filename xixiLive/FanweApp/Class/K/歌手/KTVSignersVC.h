//
//  KTVSignersVC.h
//  FanweApp
//
//  Created by lyh on 2019/1/18.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVSignersVC : FWBaseTableViewController

@property (nonatomic, copy) NSString *classify_id;

@end

NS_ASSUME_NONNULL_END
