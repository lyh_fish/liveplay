//
//  KTVSignerTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVSignerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVSignerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

@property (nonatomic, strong) KTVSignerModel *model;

@end

NS_ASSUME_NONNULL_END
