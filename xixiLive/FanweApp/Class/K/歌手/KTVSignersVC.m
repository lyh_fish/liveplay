//
//  KTVSignersVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/18.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVSignersVC.h"
#import "KTVPointMusicListVC.h"
#import "KTVSignerModel.h"
#import "KTVSignerTableViewCell.h"
#import "ICPinyinGroup.h"
#import "FanweApp-Swift.h"

@interface KTVSignersVC ()
@property (nonatomic, strong) NSArray *classes;
@property (nonatomic, strong) NSArray *letters;
@end

@implementation KTVSignersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"KTVSignerTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
    
    [self setupBackBtnWithBlock:nil];
}

- (instancetype)initWithStyle:(UITableViewStyle)style {
    return [super initWithStyle:UITableViewStylePlain];
}

- (void)initFWUI {
    [super initFWUI];
}

- (void)initFWData {
    [super initFWData];
    [self loadNetData];
}

- (void)loadNetData
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"singer_by_classify_id" forKey:@"act"];
    [parmDict setObject:@"0" forKey:@"p"];
    [parmDict setObject:self.classify_id forKey:@"classify_id"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *singers = [KTVSignerModel mj_objectArrayWithKeyValuesArray:responseJson[@"singers"]];
             NSDictionary *dict  = [ICPinyinGroup group:singers key:@"singer_name"];
             self.letters = [NSArray arrayWithArray:dict[LEOPinyinGroupCharKey]];
             self.classes = [NSArray arrayWithArray:dict[LEOPinyinGroupResultKey]];
             [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVSignerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSArray *signers = self.classes[indexPath.section];
    KTVSignerModel *model = signers[indexPath.row];
    cell.model = model;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.classes.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *signers = self.classes[section];
    return signers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.letters;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.letters objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   
    
    NSArray *signers = self.classes[indexPath.section];
    KTVSignerModel *model = signers[indexPath.row];
    
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"song_by_singer_id" forKey:@"act"];
    [parmDict setObject:model.id forKey:@"singer_id"];

    KTVPointMusicListVC *listVC = [[KTVPointMusicListVC alloc] init];
    listVC.title = model.singer_name;
    listVC.parmDict = [NSDictionary dictionaryWithDictionary:parmDict];
    [[AppDelegate sharedAppDelegate] pushViewController:listVC];
}

- (NSArray *)classes {
    if (!_classes) {
        _classes = [NSArray array];
    }
    return _classes;
}
@end
