//
//  KTVMusicClicke.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVMusicClick.h"

@implementation KTVMusicClick

+ (void)clickMusicWithID:(NSString *)ID {
    if (ID) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"audio" forKey:@"ctl"];
        [parmDict setObject:@"song_by_song_id" forKey:@"act"];
        [parmDict setObject:ID forKey:@"song_id"];
        FWWeakify(self)
        [[NetHttpsManager manager] POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             
         } FailureBlock:^(NSError *error)
         {
         }];
    }
}


@end
