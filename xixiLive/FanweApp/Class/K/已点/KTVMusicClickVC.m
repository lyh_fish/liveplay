//
//  KTVMusicClickVC.m
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVMusicClickVC.h"
#import "KTVPointTableView.h"

@interface KTVMusicClickVC ()

{
    int currentPage;
}

@property (nonatomic, strong) NSMutableArray *songs;

@property (nonatomic, strong) KTVPointTableView *pointTableView;

@end

@implementation KTVMusicClickVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"已点";
    
    self.pointTableView = [[KTVPointTableView alloc] init];
    [self.view addSubview:self.pointTableView];
    
    [FWMJRefreshManager refresh:self.pointTableView.tableView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:@selector(footerReresh)];
    
    [self.pointTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [self setupBackBtnWithBlock:nil];
    // Do any additional setup after loading the view.
}

- (void)headerRefresh {
    currentPage = 1;
    [self loadNetDataWithPage:1];
}

- (void)footerReresh {
    currentPage ++;
    [self loadNetDataWithPage:currentPage];
}

- (void)loadNetDataWithPage:(int)page
{
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@(page) forKey:@"p"];
        [parmDict setObject:@"audio" forKey:@"ctl"];
        [parmDict setObject:@"song_clicked" forKey:@"act"];
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 NSArray *songs = [KTVSongModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
                 if (page == 1) {
                     self.songs = [NSMutableArray arrayWithArray:songs];
                 }else {
                     [self.songs addObjectsFromArray:songs];
                 }
                 [self.pointTableView  reloadDataWithDataSource:self.songs];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
             [FWMJRefreshManager endRefresh:self.pointTableView.tableView];
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
             [FWMJRefreshManager endRefresh:self.pointTableView.tableView];
         }];
}


@end
