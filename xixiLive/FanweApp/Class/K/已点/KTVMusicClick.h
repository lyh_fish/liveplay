//
//  KTVMusicClicke.h
//  FanweApp
//
//  Created by lyh on 2019/3/22.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVMusicClick : NSObject
+ (void)clickMusicWithID:(NSString *)ID;
@end

NS_ASSUME_NONNULL_END
