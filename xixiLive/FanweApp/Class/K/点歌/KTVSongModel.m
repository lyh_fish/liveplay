//
//  KTVSongModel.m
//  FanweApp
//
//  Created by lyh on 2019/1/28.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVSongModel.h"

@implementation KTVSongModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"classified" : @"KTVSongClassified",
             @"singers" : @"KTVSongSingers",
             };
}

@end


@implementation KTVSongClassified

//+ (NSDictionary *)mj_objectClassInArray
//{
//    return @{
//             @"classify" : @"KTVSongClassify",
//             };
//}

@end

@implementation KTVSongClassify


@end

@implementation KTVSongSingers

//+ (NSDictionary *)mj_objectClassInArray
//{
//    return @{
//             @"singer": @"KTVSongSinger",
//             };
//}

@end



@implementation KTVSongSinger

@end
