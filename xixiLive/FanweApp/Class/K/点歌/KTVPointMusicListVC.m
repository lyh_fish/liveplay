//
//  KTVPointMusicListVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/17.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPointMusicListVC.h"
#import "KTVPointMusicTableViewCell.h"
#import "KTVMusicModel.h"

@interface KTVPointMusicListVC ()
{
    int currentPage;
}

@property (nonatomic, strong) NSMutableArray *songs;

@end

@implementation KTVPointMusicListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentPage = 1;
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"KTVPointMusicTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView reloadData];
    
    [self setupBackBtnWithBlock:nil];
    
    [FWMJRefreshManager refresh:self.tableView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:@selector(footerReresh)];
    
}

- (void)initFWUI {
    [super initFWUI];
}

- (void)initFWData {
    [super initFWData];
}

- (void)headerRefresh {
    currentPage = 1;
    [self loadNetDataWithPage:1];
}

- (void)footerReresh {
    currentPage ++;
    [self loadNetDataWithPage:currentPage];
}

- (void)loadNetDataWithPage:(int)page
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionaryWithDictionary:self.parmDict];
    [parmDict setObject:@(page) forKey:@"p"];
//    [parmDict setObject:@"audio" forKey:@"ctl"];
//    [parmDict setObject:@"song_by_singer_id" forKey:@"act"];
//    [parmDict setObject:@(page) forKey:@"p"];
//    [parmDict setObject:self.singer_id forKey:@"singer_id"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *songs = [KTVMusicModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
             if (page == 1) {
                 self.songs = [NSMutableArray arrayWithArray:songs];
             }else {
                 [self.songs addObjectsFromArray:songs];
             }
             [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}

#pragma mark ----  UITableViewDelegate,UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVPointMusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    KTVMusicModel *model = self.songs[indexPath.row];
    cell.model = model;
    cell.isChorus = [self.title containsString:@"合唱"];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.songs.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (NSMutableArray *)songs {
    if (!_songs) {
        _songs = [NSMutableArray array];
    }
    return _songs;
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
