//
//  TestListBaseView.h
//  JXCategoryView
//
//  Created by jiaxin on 2018/8/27.
//  Copyright © 2018年 jiaxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"
#import "KTVSongModel.h"

@interface KTVPointTableView : UIView <JXPagerViewListViewDelegate>

@property (nonatomic, strong) UITableView *tableView;


- (void)reloadDataWithDataSource:(NSArray <KTVSongModel *>*)dataSource;

@end
