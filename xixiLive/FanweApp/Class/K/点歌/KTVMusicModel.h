//
//  KTVMusicModel.h
//  FanweApp
//
//  Created by lyh on 2019/1/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVMusicModel : NSObject
/// 歌曲id
@property (nonatomic,copy) NSString *id;
/// 演员
@property (nonatomic,copy) NSString *title;
/// 封面图片地址
@property (nonatomic,copy) NSString *cover_path;
/// 原唱下载地址
@property (nonatomic,copy) NSString *original_sing_path;
/// 伴奏下载地址
@property (nonatomic,copy) NSString *accompaniment_path;
/// 歌词下载地址
@property (nonatomic,copy) NSString *lyrics_path;

@property (nonatomic,copy) NSString *sort;

@property (nonatomic,copy) NSString *created_at;
/// 这个参数有可能是空值
@property (nonatomic,copy) NSString *singer_id;

@end

NS_ASSUME_NONNULL_END
