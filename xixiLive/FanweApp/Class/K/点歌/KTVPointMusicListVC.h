//
//  KTVPointMusicListVC.h
//  FanweApp
//
//  Created by lyh on 2019/1/17.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseTableViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface KTVPointMusicListVC : FWBaseTableViewController

/// 根据人去查歌曲
@property (nonatomic, copy) NSString *singer_id;
/// 根据分类去查歌曲
@property (nonatomic, copy) NSString *song_by_music_classified_id;

@property (nonatomic, strong) NSDictionary *parmDict;

@end

NS_ASSUME_NONNULL_END
