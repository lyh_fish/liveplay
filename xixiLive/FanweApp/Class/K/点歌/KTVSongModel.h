//
//  KTVSongModel.h
//  FanweApp
//
//  Created by lyh on 2019/1/28.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class KTVSongClassified,KTVSongSingers,KTVSongSinger,KTVSongClassify;
@interface KTVSongModel : NSObject
/// 歌曲id
@property (nonatomic,copy) NSString *id;
/// 歌曲名称
@property (nonatomic,copy) NSString *title;
/// 封面图片地址
@property (nonatomic,copy) NSString *cover_path;
/// 原唱下载地址
@property (nonatomic,copy) NSString *original_sing_path;
/// 伴奏下载地址
@property (nonatomic,copy) NSString *accompaniment_path;
/// 歌词下载地址
@property (nonatomic,copy) NSString *lyrics_path;

@property (nonatomic,copy) NSString *sort;

@property (nonatomic,copy) NSString *created_at;
/// 歌手地址
@property (nonatomic,copy) NSString *sing_path;
/// 房间id
@property (nonatomic,copy) NSString *room_id;

@property (nonatomic,strong)NSArray <KTVSongClassified *>*classified;

@property (nonatomic,strong)NSArray <KTVSongSingers *>*singers;

@end

@interface KTVSongClassified : NSObject
///
@property (nonatomic,copy) NSString *id;
///
@property (nonatomic,copy) NSString *audio_info_id;
///
@property (nonatomic,copy) NSString *music_classified_id;
///
@property (nonatomic,copy) NSString *created_at;
///
@property (nonatomic,strong) KTVSongClassify * classify;

@end

@interface KTVSongClassify : NSObject
///
@property (nonatomic,copy) NSString *id;
///
@property (nonatomic,copy) NSString *parent_id;
///
@property (nonatomic,copy) NSString *cat_name;
///
@property (nonatomic,copy) NSString *is_effect;
///
@property (nonatomic,copy) NSString *order;
///
@property (nonatomic,copy) NSString *created_at;

@end


@interface KTVSongSingers : NSObject
///
@property (nonatomic,copy) NSString *id;
///
@property (nonatomic,copy) NSString *audio_info_id;
///
@property (nonatomic,copy) NSString *singer_id;
///
@property (nonatomic,copy) NSString *created_at;
///
@property (nonatomic,strong)KTVSongSinger * singer;
///
@end

@interface KTVSongSinger : NSObject
///
@property (nonatomic,copy) NSString *id;
///
@property (nonatomic,copy) NSString *singer_classified_id;
///周杰伦
@property (nonatomic,copy) NSString *singer_name;
///
@property (nonatomic,copy) NSString *cover_img;
///
@property (nonatomic,copy) NSString *created_at;
///
@end

NS_ASSUME_NONNULL_END


