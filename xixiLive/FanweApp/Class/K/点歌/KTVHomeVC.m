//
//  KTVHomeVC.m
//  FanweApp
//
//  Created by lyh on 2019/1/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHomeVC.h"
#import "KTVPointMusicTableViewCell.h"
#import "KTVPointHeadView.h"
#import "KTVPointMusicListVC.h"
#import "KTVSignerTypeVC.h"
#import "KTVClassTypeVC.h"
#import "JXCategoryView.h"
#import "KTVPointTableView.h"
#import "KTVMusicClickVC.h"

static const CGFloat JXTableHeaderViewHeight = 0;
static const CGFloat JXheightForHeaderInSection = 50;

@interface KTVHomeVC ()<KTVPointHeadViewDelegate,JXPagerViewDelegate, JXCategoryViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerSupView;

@property (nonatomic, strong) KTVPointHeadView *headerView;

@property (nonatomic, strong) JXPagerView *pagingView;

@property (weak, nonatomic) IBOutlet UIView *pageSupView;

@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property (nonatomic, strong) NSArray <KTVPointTableView *> *listViewArray;

@property (nonatomic, strong) NSArray <NSString *> *titles;

@property (nonatomic, strong) NSArray <NSString *> *sort_bys;

@end

@implementation KTVHomeVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController  setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController  setNavigationBarHidden:NO];
}

- (void)setupBackBtnWithBlock:(BackBlock)backBlock {
    [super setupBackBtnWithBlock:backBlock];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.headerSupView addSubview:self.headerView];
    
     _titles = @[@"点唱榜", @"飙升榜", @"新歌榜"];
    
    KTVPointTableView *listView1 = [[KTVPointTableView alloc] init];
    
    KTVPointTableView *listView2 = [[KTVPointTableView alloc] init];
    
    KTVPointTableView *listView3 = [[KTVPointTableView alloc] init];
    
     _listViewArray = @[listView1, listView2, listView3];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, JXheightForHeaderInSection)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = KtvColor;
    self.categoryView.titleColor = [UIColor blackColor];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.titleFont = [UIFont boldSystemFontOfSize:16];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.lineStyle = JXCategoryIndicatorLineStyle_Normal;
    lineView.indicatorLineViewColor = KtvColor;
    lineView.indicatorLineWidth = 45;
    self.categoryView.indicators = @[lineView];
    
    _pagingView = [[JXPagerView alloc] initWithDelegate:self];
    [self.pageSupView addSubview:self.pagingView];
    
    self.categoryView.contentScrollView = self.pagingView.listContainerView.collectionView;
    
    [self loadNetDataWithsort_by:self.sort_bys[0] Tableview:self.listViewArray[0]];
}

- (void)loadNetDataWithsort_by:(NSString *)sort_by Tableview:(KTVPointTableView *)tableview
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"tab_list" forKey:@"act"];
    [parmDict setObject:sort_by forKey:@"sort_by"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *songs =  [KTVSongModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
             [tableview  reloadDataWithDataSource:songs];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
     }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.pagingView.frame = self.pageSupView.bounds;
}

- (void)headerReresh {
    
}

#pragma mark - JXPagingViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return nil;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return JXTableHeaderViewHeight;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return JXheightForHeaderInSection;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSArray<UIView<JXPagerViewListViewDelegate> *> *)listViewsInPagerView:(JXPagerView *)pagerView {
    return self.listViewArray;
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    [self loadNetDataWithsort_by:self.sort_bys[index] Tableview:self.listViewArray[index]];
}


- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[NSBundle mainBundle] loadNibNamed:@"KTVPointHeadView" owner:self options:nil].lastObject;
        _headerView.delegate = self;
        _headerView.frame = self.headerSupView.bounds;
    }
    return _headerView;
}

#pragma mark ----  KTVPointHeadViewDelegate

- (void)searchWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVSearchVC"];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)singerWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    KTVSignerTypeVC *signerVC = [[KTVSignerTypeVC alloc] init];
    signerVC.title = @"歌手";
    [[AppDelegate sharedAppDelegate] pushViewController:signerVC];
}

- (void)classWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    KTVClassTypeVC *classTypeVC = [[KTVClassTypeVC alloc] init];
    classTypeVC.title = @"分类";
    [[AppDelegate sharedAppDelegate] pushViewController:classTypeVC];
}

- (void)pointedWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    KTVMusicClickVC *clickVC = [[KTVMusicClickVC alloc] init];
    [[AppDelegate sharedAppDelegate] pushViewController:clickVC];
}

- (void)liveWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    PopMenuCenter *popMenuCenter = [PopMenuCenter sharePopMenuCenter];
    [popMenuCenter setTabBarC:self];
    [popMenuCenter setStPopMenuShowState:STPopMenuShow];
}

- (void)chorusWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    KTVPointMusicListVC *listVC = [[KTVPointMusicListVC alloc] init];
    listVC.title = @"加入合唱";
    [[AppDelegate sharedAppDelegate] pushViewController:listVC];
}

- (void)backWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView {
    [[AppDelegate sharedAppDelegate]popViewController];
}

- (NSArray *)sort_bys {
    if (!_sort_bys) {
        _sort_bys = [NSArray arrayWithObjects:@"click_song",@"up_song",@"new_song", nil];
    }
    return _sort_bys;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
