//
//  KTVPointMusicTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/1/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPointMusicTableViewCell.h"
#import "GKAudioPlayer.h"

@implementation KTVPointMusicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.pointButton.layer.cornerRadius = CGRectGetHeight(self.pointButton.frame)/2;
    self.pointButton.clipsToBounds = true;
    self.pointButton.layer.borderWidth = 1;
    self.pointButton.layer.borderColor = KtvColor.CGColor;
}

- (void)setModel:(KTVMusicModel *)model {
    _model = model;
    [self.musicImageView sd_setImageWithURL:[NSURL URLWithString:model.cover_path] placeholderImage:[UIImage imageNamed:@"KTV_signer_default"]];
    self.musicNameLabel.text = model.title;
    self.musicArtNameLabel.text = @"";
}

- (void)setSong:(KTVSongModel *)song {
    _song = song;
    [self.musicImageView sd_setImageWithURL:[NSURL URLWithString:song.cover_path] placeholderImage:[UIImage imageNamed:@"KTV_signer_default"]];
    self.musicNameLabel.text = song.title;
    if (song.singers.count > 0) {
        KTVSongSingers *singers = (KTVSongSingers *)song.singers.firstObject;
        self.musicArtNameLabel.text = singers.singer.singer_name;
    }else {
        self.musicArtNameLabel.text = @"未知";
    }
}

- (IBAction)pointMusicAction:(UIButton *)sender {
    
    if (_song != nil ) {
        KTVMusicModel *music = [[KTVMusicModel alloc] init];
        music.id = _song.id;
        music.title = _song.title;
        music.cover_path = _song.cover_path;
        music.original_sing_path = _song.original_sing_path;
        music.accompaniment_path = _song.accompaniment_path;
        music.lyrics_path = _song.lyrics_path;
        music.sort = _song.sort;
        music.created_at = _song.created_at;
        
        [[GKAudioPlayer sharedInstance] stop];
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = music;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
        return;
    }
    if (_model != nil ) {
        
        [[GKAudioPlayer sharedInstance] stop];
        
        KTVPlayVC *vc = [[UIStoryboard storyboardWithName:@"KTV" bundle:nil] instantiateViewControllerWithIdentifier:@"KTVPlayVC"];
        vc.music = _model;
        [[AppDelegate sharedAppDelegate] pushViewController:vc];
        return;
    }
}

- (void)setIsChorus:(BOOL)isChorus {
    _isChorus = isChorus;
    if (_isChorus) {
        [self.pointButton setTitle:@"合唱" forState:UIControlStateNormal];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
