//
//  KTVPointHeadView.h
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>


@class KTVPointHeadView;
@protocol KTVPointHeadViewDelegate <NSObject>
/// 搜索
- (void)searchWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 歌手
- (void)singerWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 分类
- (void)classWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 已点
- (void)pointedWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 直播
- (void)liveWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 合唱
- (void)chorusWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;
/// 返回
- (void)backWithKTVPointHeadView:(KTVPointHeadView *)pointHeadView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface KTVPointHeadView : UIView

@property (nonatomic, weak) id<KTVPointHeadViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end

NS_ASSUME_NONNULL_END
