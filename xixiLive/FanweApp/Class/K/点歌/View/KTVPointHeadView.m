//
//  KTVPointHeadView.m
//  FanweApp
//
//  Created by lyh on 2019/1/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPointHeadView.h"

@implementation KTVPointHeadView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITextField * searchField = [_searchBar valueForKey:@"_searchField"];
    [searchField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [searchField setValue:[UIFont boldSystemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
}

- (IBAction)searchButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(searchWithKTVPointHeadView:)]) {
        [self.delegate searchWithKTVPointHeadView:self];
    }
}


/// 歌手
- (IBAction)singerButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(singerWithKTVPointHeadView:)]) {
        [self.delegate singerWithKTVPointHeadView:self];
    }
}

/// 分类
- (IBAction)classButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(classWithKTVPointHeadView:)]) {
        [self.delegate classWithKTVPointHeadView:self];
    }
}

/// 已点
- (IBAction)pointedButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(pointedWithKTVPointHeadView:)]) {
        [self.delegate pointedWithKTVPointHeadView:self];
    }
}

- (IBAction)liveButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(liveWithKTVPointHeadView:)]) {
        [self.delegate liveWithKTVPointHeadView:self];
    }
}

- (IBAction)chorusButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(chorusWithKTVPointHeadView:)]) {
        [self.delegate chorusWithKTVPointHeadView:self];
    }
}

- (IBAction)backButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(backWithKTVPointHeadView:)]) {
        [self.delegate backWithKTVPointHeadView:self];
    }
}

@end
