//
//  KTVPointMusicTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/1/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVMusicModel.h"
#import "KTVSongModel.h"

NS_ASSUME_NONNULL_BEGIN

//@class KTVPointMusicTableViewCell;
//@protocol KTVPointMusicTableViewCellDelegate <NSObject>
//- (void)pointMusicWithTableViewCell:(KTVPointMusicTableViewCell *)tableViewCell;
//@end

@interface KTVPointMusicTableViewCell : UITableViewCell
/// 图像
@property (weak, nonatomic) IBOutlet UIImageView *musicImageView;
/// 歌名
@property (weak, nonatomic) IBOutlet UILabel *musicNameLabel;
/// 作曲人
@property (weak, nonatomic) IBOutlet UILabel *musicArtNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *pointButton;

@property (nonatomic,assign) BOOL isChorus;

//@property (nonatomic, weak) id <KTVPointMusicTableViewCellDelegate> delegate;

@property (nonatomic, strong) KTVMusicModel *model;

@property (nonatomic, strong) KTVSongModel *song;

@end

NS_ASSUME_NONNULL_END
