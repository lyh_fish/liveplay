//
//  KTVSong.h
//  FanweApp
//
//  Created by lyh on 2019/2/26.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import <YYModel/YYModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVSong : RLMObject<YYModel>
/// 歌曲id
@property (nonatomic,copy) NSString *id;
/// 歌曲名称
@property (nonatomic,copy) NSString *title;
/// 原唱下载地址
@property (nonatomic,copy) NSString *original_sing_path;
/// 伴奏下载地址
@property (nonatomic,copy) NSString *accompaniment_path;
/// 歌曲歌词地址
@property (nonatomic,copy) NSString *lyrics_path;
/// 本地录音时间
@property (nonatomic,strong) NSDate *local_time;
/// 本地录音地址:默认只带后缀 2019030516313317.mp3
@property (nonatomic,copy) NSString *local_path;
/// 是否发布
@property (nonatomic,assign) BOOL isRelease;
/// 歌曲持续时间
@property (nonatomic,assign) double duration;
@end

NS_ASSUME_NONNULL_END
