//
//  MPFunctionView.m
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "MPFunctionView.h"

@implementation MPFunctionView

- (IBAction)localButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(localWithMPFunctionView:)]) {
        [self.delegate localWithMPFunctionView:self];
    }
}

- (IBAction)pictureButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(pictureWithMPFunctionView:)]) {
        [self.delegate pictureWithMPFunctionView:self];
    }
}

- (IBAction)collectionButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(collectionWithMPFunctionView:)]) {
        [self.delegate collectionWithMPFunctionView:self];
    }
}

- (IBAction)worksButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(worksWithMPFunctionView:)]) {
        [self.delegate worksWithMPFunctionView:self];
    }
}

@end
