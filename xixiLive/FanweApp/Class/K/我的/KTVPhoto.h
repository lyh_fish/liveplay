//
//  KTVPhoto.h
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVPhoto : NSObject

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *src;
/// 0:未选中 1:已选中
@property (nonatomic,assign) int state;

@end

NS_ASSUME_NONNULL_END
