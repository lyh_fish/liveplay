//
//  KTVPhotoVC.h
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseViewController.h"

@class KTVPhotoVC;
@class KTVPhoto;
@protocol KTVPhotoVCDelegate <NSObject>

- (void)selectPhotosWithPhotos:(NSArray<KTVPhoto *> *)photos;

@end


NS_ASSUME_NONNULL_BEGIN

@interface KTVPhotoVC : FWBaseViewController
/// 是否是发布
@property (nonatomic, assign) BOOL isRelease;

@property (nonatomic, weak) id<KTVPhotoVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
