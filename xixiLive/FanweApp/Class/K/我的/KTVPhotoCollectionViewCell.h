//
//  KTVPhotoCollectionViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTVPhoto.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UIImageView *selectImageView;

@property (nonatomic,assign) BOOL isAdd;

@property (nonatomic,strong) KTVPhoto *photo;

@end

NS_ASSUME_NONNULL_END
