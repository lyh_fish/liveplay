//
//  KTVWorksTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userPageModel.h"
#import "KTVHotSongModel.h"

NS_ASSUME_NONNULL_BEGIN
@class KTVWorksTableViewCell;
@protocol KTVWorksTableViewCellDelegate <NSObject>

- (void)goKTVWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model;

- (void)giftWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model;

- (void)commentWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model;

- (void)shareWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model;

- (void)otherWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model;

@end

@interface KTVWorksTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIButton *musicnameButton;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIButton *playTimeButton;

@property (weak, nonatomic) IBOutlet UIButton *zanButton;

@property (weak, nonatomic) IBOutlet UIButton *giftButton;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (nonatomic, strong) KTVHotSongModel *model;

@property (nonatomic, strong) id <KTVWorksTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *actionView;
@end

NS_ASSUME_NONNULL_END
