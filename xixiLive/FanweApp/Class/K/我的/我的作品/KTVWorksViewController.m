//
//  KTVWorksViewController.m
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVWorksViewController.h"
#import "KTVWorksTableViewCell.h"
#import "KTVHotSongModel.h"
#import "KTVHomePlayController.h"
#import "FWMD5UTils.h"
#import "KTVInputCommentsView.h"

@interface KTVWorksViewController ()<UITableViewDelegate,UITableViewDataSource,KTVWorksTableViewCellDelegate,KTVInputCommentsViewDelegate>

{
    int currentPage;
}

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray *works;

@property (nonatomic,strong) KTVInputCommentsView *commentsView;

@property (nonatomic,strong) KTVHotSongModel *songModel;

@end

@implementation KTVWorksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)initFWUI {
    [super initFWUI];
    
    self.title = @"我的作品";

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"KTVWorksTableViewCell" bundle:nil] forCellReuseIdentifier:@"KTVWorksTableViewCell"];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.top.bottom.equalTo(self.view);
    }];
    
    [self.view addSubview:self.commentsView];
    self.commentsView.hidden = true;
    
    [self.commentsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.centerX.equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.bottom.equalTo(self.view);
        }
        make.height.equalTo(@(60));
    }];
    
    [self setupBackBtnWithBlock:nil];
    
    [FWMJRefreshManager refresh:self.tableView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:@selector(footerReresh)];
}

- (void)initFWData {
    [super initFWData];
    currentPage = 1;
    [self loadNetData];
}

- (void)headerRefresh {
    currentPage = 1;
    [self loadNetData];
}

- (void)footerReresh {
    currentPage ++;
    [self loadNetData];
}

- (void)loadNetData
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"my_song" forKey:@"act"];
    [parmDict setObject:@(currentPage) forKey:@"p"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *works = [KTVHotSongModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
             if (currentPage == 1) {
                 self.works = [NSMutableArray arrayWithArray:works];
             }else {
                 [self.works addObjectsFromArray:works];
             }
             [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}

- (void)deleteNetWithModel:(KTVHotSongModel *)model IndexPath:(NSIndexPath *)path;
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定删除该作品吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"正在删除"];
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"audio" forKey:@"ctl"];
        [parmDict setObject:@"my_song_del" forKey:@"act"];
        [parmDict setObject:model.id forKey:@"ids"];
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 [SVProgressHUD showSuccessWithStatus:@"删除成功"];
                 [self.works removeObject:model];
                 [self.tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
             }else
             {
                 [SVProgressHUD dismiss];
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
             [FWMJRefreshManager endRefresh:self.tableView];
         } FailureBlock:^(NSError *error)
         {
             [SVProgressHUD dismiss];
             FWStrongify(self)
             [FWMJRefreshManager endRefresh:self.tableView];
         }];
    }];
    [alertVC addAction:action1];
    [alertVC addAction:action2];
    [self presentViewController:alertVC animated:true completion:^{
        
    }];

}

#pragma mark UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTVWorksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KTVWorksTableViewCell" forIndexPath:indexPath];
    cell.model = self.works[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return self.works.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 124;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteNetWithModel:self.works[indexPath.row] IndexPath:indexPath];
    }
}

#pragma mark KTVWorksTableViewCellDelegate

- (void)goKTVWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model {
    GKMusic *music = [[GKMusic alloc] init];
    music.cover_path = model.cover_path;
    music.song_path = model.sing_path;
    music.lyric_path = model.audio_info.song.lyrics_path;
    music.other = model.audio_info.song;
    
    [GKMusicTool sharedInstance].music = music;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:model.group_id forKey:@"group_id"];
    [dic setObject:model.room_id forKey:@"room_id"];
    [dic setObject:self.userModel.head_image forKey:@"head_image"];
    [dic setObject:@(false) forKey:@"has_ad_password"];
    [dic setObject:@"2" forKey:@"type"];
    [dic setObject:self.userModel.user_id forKey:@"user_id"];
    [dic setObject:@"3" forKey:@"live_in"];
    [dic setObject:@(false) forKey:@"has_ad_password"];
    
    BOOL isSusWindow = [[LiveCenterManager sharedInstance] judgeIsSusWindow];
    [[LiveCenterManager sharedInstance] showLiveOfAudienceLiveofPramaDic:dic.mutableCopy isSusWindow:isSusWindow isSmallScreen:NO block:^(BOOL finished) {
    }];
}

- (void)giftWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model {
    
}

- (void)commentWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model {
//    self.songModel = model;
//    self.commentsView.hidden = false;
//    [self.commentsView.inputTF becomeFirstResponder];
}

- (void)shareWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model {
    
//    //创建分享消息对象
//    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    if (UMSocialPlatformType_Sina == platformType)
//    {
//        //设置文本
//        messageObject.text = [NSString stringWithFormat:@"%@,%@",shareModel.share_title,shareModel.share_url];
//
//        //创建图片内容对象
//        UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
//        //如果有缩略图，则设置缩略图
//        shareObject.thumbImage = shareModel.share_imageUrl;
//        [shareObject setShareImage:shareModel.share_imageUrl];
//
//        //分享消息对象设置分享内容对象
//        messageObject.shareObject = shareObject;
//    }
//    else
//    {
//        //创建网页内容对象
//        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:shareModel.share_title descr:shareModel.share_content thumImage:shareModel.share_imageUrl];
//        //设置网页地址
//        shareObject.webpageUrl = shareModel.share_url;
//
//        shareObject.thumbImage = shareModel.share_imageUrl;
//
//        //分享消息对象设置分享内容对象
//        messageObject.shareObject = shareObject;
//    }
//
//    __weak typeof(self) ws = self;
//    //调用分享接口
//    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:vc completion:^(id data, NSError *error) {
//        if (error) {
//            UMSocialLogInfo(@"************Share fail with error %@*********",error);
//            if (failed)
//            {
//                failed(FWCode_Normal_Error, @"分享失败");
//            }
//        }
//        else
//        {
//            if ([data isKindOfClass:[UMSocialShareResponse class]])
//            {
//                UMSocialShareResponse *resp = data;
//                //分享结果消息
//                UMSocialLogInfo(@"response message is %@",resp.message);
//                //第三方原始返回的数据
//                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
//
//                if (shareModel.isNotifiService)
//                {
//                    [ws didFinishGetUMSocialDataInViewController:resp shareModel:shareModel];
//                }
//
//                if (succ)
//                {
//                    succ(resp);
//                    [self refresh];
//                }
//            }
//            else
//            {
//                UMSocialLogInfo(@"response data is %@",data);
//                if (failed)
//                {
//                    failed(FWCode_Normal_Error, @"分享失败");
//                }
//            }
//        }
//    }];
    
}

- (void)otherWithCell:(KTVWorksTableViewCell *)cell Model:(KTVHotSongModel *)model {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"置顶作品" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"修改作品描述" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"设为私密" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSInteger row = [self.works indexOfObject:model];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self deleteNetWithModel:model IndexPath:indexPath];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:action1];
    [alertVC addAction:action2];
    [alertVC addAction:action3];
    [alertVC addAction:action4];
    [alertVC addAction:cancelAction];
    [self presentViewController:alertVC animated:true completion:nil];
}

#pragma mark

- (void)sendCommentsWithKTVInputText:(NSString *)inputText {
    if ([FWUtils isBlankString:inputText]) {
        [FanweMessage alertTWMessage:@"请输入评论"];
        return;
    }
    self.commentsView.hidden = true;
    [self.view endEditing:false];
    if (self.songModel.audio_info_id) {
        NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
        [parmDict setObject:@"user" forKey:@"ctl"];
        [parmDict setObject:@"publish_comment" forKey:@"act"];
        [parmDict setObject:self.songModel.audio_info_id forKey:@"audio_user_id"];
        [parmDict setObject:inputText forKey:@"content"];
        [parmDict setObject:@"1" forKey:@"type"];
        [parmDict setObject:@"xr" forKey:@"itype"];
        
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
         {
             FWStrongify(self)
             if ([responseJson toInt:@"status"] == 1)
             {
                 [FanweMessage alertTWMessage:@"评论成功"];
//                 [self requestNetDataWithPage:1];
             }else
             {
                 [FanweMessage alertHUD:[responseJson toString:@"error"]];
             }
             [FWMJRefreshManager endRefresh:self.tableView];
         } FailureBlock:^(NSError *error)
         {
             FWStrongify(self)
             [FWMJRefreshManager endRefresh:self.tableView];
         }];
    }else {
        [FWMJRefreshManager endRefresh:self.tableView];
        [FanweMessage alertHUD:@"歌曲id不能为nil"];
    }
}


- (NSMutableArray *)works {
    if (!_works) {
        _works = [NSMutableArray array];
    }
    return _works;
}

- (KTVInputCommentsView *)commentsView {
    if (!_commentsView) {
        _commentsView = [[NSBundle mainBundle] loadNibNamed:@"KTVInputCommentsView" owner:self options:nil].lastObject;
        _commentsView.delegate = self;
    }
    return _commentsView;
}

@end
