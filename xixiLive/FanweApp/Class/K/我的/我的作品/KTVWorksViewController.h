//
//  KTVWorksViewController.h
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "FWBaseViewController.h"
#import "userPageModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface KTVWorksViewController : FWBaseViewController
@property (nonatomic,strong) userPageModel          *userModel;
@end

NS_ASSUME_NONNULL_END
