//
//  KTVWorksTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/2/24.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVWorksTableViewCell.h"

@implementation KTVWorksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImageView.layer.cornerRadius = CGRectGetHeight(self.photoImageView.frame)/2;
    self.photoImageView.clipsToBounds = true;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.actionView addGestureRecognizer:tap];
}

- (void)tapAction {
    if ([self.delegate respondsToSelector:@selector(goKTVWithCell:Model:)]) {
        [self.delegate goKTVWithCell:self Model:self.model];
    }
}

- (void)setModel:(KTVHotSongModel *)model {
    _model = model;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];//创建一个日期格式化器
    dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:model.created_at];
    
    self.nameLabel.text =  model.title;
    [self.musicnameButton setTitle:model.audio_info.song.title forState:UIControlStateNormal];
    
    [self.playTimeButton setTitle:[NSString stringWithFormat:@"  %@",model.play_times] forState:UIControlStateNormal];
    [self.zanButton setTitle:[NSString stringWithFormat:@"  %@",model.digg_num] forState:UIControlStateNormal];
    [self.giftButton setTitle:[NSString stringWithFormat:@"  %@",model.gift_num] forState:UIControlStateNormal];
    [self.commentButton setTitle:[NSString stringWithFormat:@"  %@",model.comment_num] forState:UIControlStateNormal];
    [self.shareButton setTitle:[NSString stringWithFormat:@"  %@",model.share_num] forState:UIControlStateNormal];
    
    self.timeLabel.text = [date timeTextOfDate];
    
    NSString *path = model.cover_path;
    NSData * jsonData = [path dataUsingEncoding:NSUTF8StringEncoding];
    //json解析
    id obj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    if ([obj isKindOfClass:[NSArray class]]) {
        NSArray *paths = [NSArray arrayWithArray:obj];
        if (paths.count > 0) {
            [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:paths.firstObject] placeholderImage:kDefaultPreloadImgSquare];
        }else {
            [self.photoImageView setImage:kDefaultPreloadImgSquare];
        }
    }else {
        [self.photoImageView setImage:kDefaultPreloadImgSquare];
    }
    
}

- (IBAction)giftButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(giftWithCell:Model:)]) {
        [self.delegate giftWithCell:self Model:self.model];
    }
}

- (IBAction)commentButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(commentWithCell:Model:)]) {
        [self.delegate commentWithCell:self Model:self.model];
    }
}

- (IBAction)shareButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(shareWithCell:Model:)]) {
        [self.delegate shareWithCell:self Model:self.model];
    }
}

- (IBAction)otherButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(otherWithCell:Model:)]) {
        [self.delegate otherWithCell:self  Model:self.model];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
