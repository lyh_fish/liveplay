//
//  KTVPhotoVC.m
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPhotoVC.h"
#import "KTVPhotoCollectionViewCell.h"
#import "KTVPhoto.h"
#import <Photos/Photos.h>
#import "TZImageManager.h"

@interface KTVPhotoVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TZImagePickerControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,OssUploadImageDelegate>
{
    NSInteger p;
}
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) UILabel *editLabel;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *selectPhotos;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) UIImagePickerController *imagePickerVc;

@property (nonatomic, strong) FWOssManager            *ossManager;
@end

@implementation KTVPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ossManager];
    // Do any additional setup after loading the view.
}


- (void)initFWUI {
    [super initFWUI];
    
    self.title = @"我的相册";
    self.view.backgroundColor = kBackGroundColor;
    
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    self.layout.minimumLineSpacing = 5;
    self.layout.minimumInteritemSpacing = 5;
    self.layout.itemSize = CGSizeMake((kScreenW-15)/3, (kScreenW-15)/3);
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:self.layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = kBackGroundColor;
    [self.collectionView registerNib:[UINib nibWithNibName:@"KTVPhotoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"KTVPhotoCollectionViewCell"];
    [self.view addSubview:self.collectionView];

    self.editLabel = [[UILabel alloc] init];
    self.editLabel.text = @"已选择0张图片";
    self.editLabel.textColor = [UIColor blackColor];
    self.editLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:self.editLabel];
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setImage:[UIImage imageNamed:@"ktv_photo_deleteable"] forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(deleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.deleteButton];
    
    [self.editLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(4);
        make.bottom.equalTo(self.view).offset(-16);
    }];
    
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-12);
        make.bottom.equalTo(self.editLabel);
    }];
    
    self.isEdit = !self.isRelease;
    
    if (self.isRelease) {
        UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(completeBarItemAction)];
        rightBarItem.tintColor = [UIColor blackColor];
        self.navigationItem.rightBarButtonItem = rightBarItem;
        self.editLabel.hidden = YES;
        self.deleteButton.hidden = YES;
    }else {
        UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarItemAction)];
        rightBarItem.tintColor = [UIColor blackColor];
        self.navigationItem.rightBarButtonItem = rightBarItem;
    }
    
    [self setupBackBtnWithBlock:^{
         [[AppDelegate sharedAppDelegate] popViewController];
    }];
}

- (void)initFWData {
    [super initFWData];
    
    [FWMJRefreshManager refresh:self.collectionView target:self headerRereshAction:nil footerRereshAction:@selector(footerRefresh)];
    p = 1;
    [self loadNetData];
    self.isEdit = false;
}

- (void)completeBarItemAction {
    if (self.selectPhotos.count == 0) {
        [FanweMessage alertHUD:@"请先选择图片"];
        return;
    }
    if ([self.delegate respondsToSelector:@selector(selectPhotosWithPhotos:)]) {
        [[AppDelegate sharedAppDelegate] popViewController];
        [self.delegate selectPhotosWithPhotos:self.selectPhotos];
    }
}


- (void)rightBarItemAction {
    self.isEdit = !self.isEdit;
}

- (void)deleteButtonAction {
    [self deleteImages];
}

- (void)footerRefresh {
    p = p+1;
    [self loadNetData];
}

/// 获取相册
- (void)loadNetData
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"photos" forKey:@"act"];
    [parmDict setObject:@(p) forKey:@"p"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *photos = [KTVPhoto mj_objectArrayWithKeyValuesArray:[responseJson objectForKey:@"photos"]];
             if (p==1) {
                 self.photos = [photos mutableCopy];
             }else {
                 [self.photos addObjectsFromArray:photos];
             }
             if (photos.count == 0) {
                 [self.collectionView.mj_footer resetNoMoreData];
             }else {
                 [self.collectionView reloadData];
             }
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.collectionView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.collectionView];
     }];
}

/// 上传图片
- (void)uploadImages:(NSArray *)images {
    [[FWHUDHelper sharedInstance]syncLoading:@"正在上传图片中..."];
    [self.ossManager showUploadOfOssServiceOfDataMarray:[images mutableCopy]
                                 andSTDynamicSelectType:STDynamicSelectPhoto
                                            andComplete:^(BOOL finished,
                                                          NSMutableArray<NSString *> *urlStrMArray) {
                                                [self uploadImageUrls:urlStrMArray];
                                            }];
    
}

/// 上传图片到相册
- (void)uploadImageUrls:(NSArray *)urls {
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"photos_add" forKey:@"act"];
    [parmDict setObject:urls forKey:@"srcs"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         [[FWHUDHelper sharedInstance] syncStopLoading];
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             p = 1;
             [self loadNetData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.collectionView];
     } FailureBlock:^(NSError *error)
     {
         [[FWHUDHelper sharedInstance] syncStopLoading];
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.collectionView];
     }];
}

/// 删除上传的图片
- (void)deleteImages {
    if (self.selectPhotos.count==0) {
        [FanweMessage alertHUD:@"请先选择删除的图片"];
        return;
    }
    [[FWHUDHelper sharedInstance]syncLoading:@"删除图片中。。。"];
    NSMutableArray *imageIDs = [NSMutableArray array];
    for (KTVPhoto *photo in self.selectPhotos) {
        [imageIDs addObject:photo.id];
    }
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"photos_del" forKey:@"act"];
    [parmDict setObject:imageIDs forKey:@"ids"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         [[FWHUDHelper sharedInstance] syncStopLoading];
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             [FanweMessage alertHUD:@"删除成功"];
             [self.photos removeObjectsInArray:self.selectPhotos];
             self.editLabel.text = [NSString stringWithFormat:@"已选择%ld张图片",self.selectPhotos.count];
             [self.collectionView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
         [FWMJRefreshManager endRefresh:self.collectionView];
     } FailureBlock:^(NSError *error)
     {
         [[FWHUDHelper sharedInstance] syncStopLoading];
         FWStrongify(self)
         [FWMJRefreshManager endRefresh:self.collectionView];
     }];
}

- (void)setIsEdit:(BOOL)isEdit {
     _isEdit = isEdit;
    if (self.isRelease) {
        self.editLabel.hidden = YES;
        self.deleteButton.hidden = YES;
    }else {
        self.editLabel.hidden = !isEdit;
        self.deleteButton.hidden = !isEdit;
        self.navigationItem.rightBarButtonItem.title = isEdit?@"取消":@"编辑";
        for (KTVPhoto *photo in self.selectPhotos) {
            photo.state = 0;
        }
        [self.selectPhotos removeAllObjects];
        [self.collectionView reloadData];
    }
}

#pragma mark ------- UICollectionViewDelegate,UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    KTVPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KTVPhotoCollectionViewCell" forIndexPath:indexPath];
    if (!_isEdit) {
        if (indexPath.row==0) {
            cell.isAdd = true;
        }else {
            cell.photo = self.photos[indexPath.row-1];
        }
    }else {
        cell.photo = self.photos[indexPath.row];
    }
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
     return _isEdit?self.photos.count:self.photos.count+1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!_isEdit) {
        if (indexPath.row==0) {
            [self showAlert];
        }else {
            KTVPhoto *photo = self.photos[indexPath.row-1];
            if (photo.state == 0) {
                [self.selectPhotos addObject:photo];
            }else {
                if ([self.selectPhotos containsObject:photo]) {
                    [self.selectPhotos removeObject:photo];
                }
            }
            photo.state = photo.state == 1 ? 0:1;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            self.editLabel.text = [NSString stringWithFormat:@"已选择%ld张图片",self.selectPhotos.count];
        }
    }else {
        KTVPhoto *photo = self.photos[indexPath.row];
        if (photo.state == 0) {
            [self.selectPhotos addObject:photo];
        }else {
            if ([self.selectPhotos containsObject:photo]) {
                [self.selectPhotos removeObject:photo];
            }
        }
        photo.state = photo.state == 1 ? 0:1;
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        self.editLabel.text = [NSString stringWithFormat:@"已选择%ld张图片",self.selectPhotos.count];
    }
}

- (void)showAlert {
    
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takePhoto];
    }];
    [alertVc addAction:takePhotoAction];
    UIAlertAction *imagePickerAction = [UIAlertAction actionWithTitle:@"去相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self pushTZImagePickerController];
    }];
    [alertVc addAction:imagePickerAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertVc addAction:cancelAction];

    [self presentViewController:alertVc animated:YES completion:nil];
}

- (void)pushTZImagePickerController {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowPickingVideo = false;
    imagePickerVc.title = @"选择图片";
    imagePickerVc.barItemTextColor = [UIColor blueColor];
    imagePickerVc.navigationBar.backgroundColor = [UIColor blueColor];
    [imagePickerVc.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            NSMutableArray *images = [NSMutableArray array];
            for (UIImage *image in photos) {
                NSData *data =  UIImagePNGRepresentation(image);
                [images addObject:data];
            }
            [self uploadImages:images];
        }
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController

- (void)takePhoto {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        // 无相机权限 做一个友好的提示
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"无法使用相机" message:@"请在iPhone的""设置-隐私-相机""中允许访问相机" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"设置", nil];
        [alert show];
    } else if (authStatus == AVAuthorizationStatusNotDetermined) {
        // fix issue 466, 防止用户首次拍照拒绝授权时相机页黑屏
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self takePhoto];
                });
            }
        }];
    } else {
        [self pushImagePickerController];
    }
}

// 调用相机
- (void)pushImagePickerController {
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        self.imagePickerVc.sourceType = sourceType;
        [self presentViewController:_imagePickerVc animated:YES completion:nil];
    } else {
        NSLog(@"模拟器中无法打开照相机,请在真机中使用");
    }
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *data =  UIImagePNGRepresentation(image);
        [self uploadImages:@[data]];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
}

- (NSMutableArray *)photos {
    if (!_photos) {
        _photos = [NSMutableArray array];
    }
    return _photos;
}

- (NSMutableArray *)selectPhotos {
    if (!_selectPhotos) {
        _selectPhotos = [NSMutableArray array];
    }
    return _selectPhotos;
}

- (UIImagePickerController *)imagePickerVc {
    if (_imagePickerVc == nil) {
        _imagePickerVc = [[UIImagePickerController alloc] init];
        _imagePickerVc.delegate = self;
        _imagePickerVc.sourceType = UIImagePickerControllerSourceTypeCamera;
        // set appearance / 改变相册选择页的导航栏外观
        _imagePickerVc.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
        _imagePickerVc.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        UIBarButtonItem *tzBarItem, *BarItem;
        if (@available(iOS 9, *)) {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[TZImagePickerController class]]];
            BarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UIImagePickerController class]]];
        } else {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedIn:[TZImagePickerController class], nil];
            BarItem = [UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil];
        }
        NSDictionary *titleTextAttributes = [tzBarItem titleTextAttributesForState:UIControlStateNormal];
        [BarItem setTitleTextAttributes:titleTextAttributes forState:UIControlStateNormal];
        
    }
    return _imagePickerVc;
}

-(FWOssManager *)ossManager
{
    if (!_ossManager)
    {
        _ossManager = [[FWOssManager alloc]initWithDelegate:self];
    }
    return _ossManager;
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
