//
//  MPFunctionView.h
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPFunctionView;
@protocol MPFunctionViewDelegate <NSObject>

- (void)localWithMPFunctionView:(MPFunctionView *)functionView;

- (void)pictureWithMPFunctionView:(MPFunctionView *)functionView;

- (void)collectionWithMPFunctionView:(MPFunctionView *)functionView;

- (void)worksWithMPFunctionView:(MPFunctionView *)functionView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface MPFunctionView : UIView

@property (nonatomic, weak) id <MPFunctionViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
