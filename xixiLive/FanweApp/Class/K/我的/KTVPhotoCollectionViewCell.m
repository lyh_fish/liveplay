//
//  KTVPhotoCollectionViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/2/21.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVPhotoCollectionViewCell.h"

@implementation KTVPhotoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = kAppMainColor;
}

- (void)setIsAdd:(BOOL)isAdd {
    _isAdd = isAdd;
    if (isAdd) {
        self.photoImageView.image = [UIImage imageNamed:@"ktv_photo_add"];
        self.selectImageView.hidden = true;
    }
}

- (void)setPhoto:(KTVPhoto *)photo {
    _photo = photo;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:photo.src] placeholderImage:kDefaultPreloadImgSquare];
    self.selectImageView.hidden = photo.state == 0;
}

@end
