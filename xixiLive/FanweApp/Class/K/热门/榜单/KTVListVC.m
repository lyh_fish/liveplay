//
//  KTVListVC.m
//  FanweApp
//
//  Created by lyh on 2019/2/20.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVListVC.h"
#import "KTVPointTableView.h"

@interface KTVListVC ()

@end

@implementation KTVListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"榜单";
    
    KTVPointTableView *tableView = [[KTVPointTableView alloc] init];
    [self.view addSubview:tableView];
    
    [self loadNetDataWithsort_by:@"up_song" Tableview:tableView];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    // Do any additional setup after loading the view.
}

- (void)loadNetDataWithsort_by:(NSString *)sort_by Tableview:(KTVPointTableView *)tableview
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"audio" forKey:@"ctl"];
    [parmDict setObject:@"tab_list" forKey:@"act"];
    [parmDict setObject:sort_by forKey:@"sort_by"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             NSArray *songs =  [KTVSongModel mj_objectArrayWithKeyValuesArray:responseJson[@"songs"]];
             [tableview  reloadDataWithDataSource:songs];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
