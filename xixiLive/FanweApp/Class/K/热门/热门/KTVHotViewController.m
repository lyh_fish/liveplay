//
//  KTVHotViewController.m
//  FanweApp
//
//  Created by lyh on 2019/2/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHotViewController.h"
#import "KTVHotCollectionViewCell.h"
#import "HMHotModel.h"
#import "HMHotTableViewCell.h"
#import "SDCycleScrollView.h"
#import "AdJumpViewModel.h"
#import "KTVHotFunctionCollectionViewCell.h"
#import "KTVListVC.h"
#import "userPageModel.h"
#import "EditFamilyViewController.h"
#import "FamilyDesViewController.h"
#import "KTVHomePlayController.h"
#import "GKMusic.h"
#import "GKMusicTool.h"
#import "LeaderboardViewController.h"

// 广告图默认滚动时间
static float const bannerAutoScrollTimeInterval = 7;

@interface KTVHotViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SDCycleScrollViewDelegate>

{
    
}

@property (nonatomic, strong) NSMutableArray            *mainDataMArray;
@property (nonatomic, strong) HMHotModel                *hotModel;
@property (nonatomic, strong) SDCycleScrollView         *cycleScrollView;
@property (nonatomic, assign) BOOL                      canClickItem;       // 防止视频列表被重复点击
@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) NSArray *functionTitles;
@property (nonatomic, strong) NSArray *functionImages;
@property (nonatomic,strong) userPageModel          *userModel;               //userModel
@property (nonatomic,strong) NSMutableArray          *musics;

@property (nonatomic,strong) UIView *closeView;
@end

@implementation KTVHotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)initFWUI
{
    [super initFWUI];
    
    self.view.backgroundColor = kBackGroundColor;
    
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    self.layout.minimumLineSpacing = 10;
    self.layout.minimumInteritemSpacing = 10;
    self.layout.itemSize = CGSizeMake((kScreenW-32)/2, (kScreenW-32)/2+48);
    self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.collectionView = [[UICollectionView alloc] initWithFrame:_collectionViewFrame collectionViewLayout:self.layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = kBackGroundColor;
    [self.collectionView registerNib:[UINib nibWithNibName:@"KTVHotCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"KTVHotCollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"KTVHotFunctionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"KTVHotFunctionCollectionViewCell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.view addSubview:self.collectionView];
}

- (void)setType:(KTVHotViewControllerType)type {
    _type = type;
    
    if (type == KTVHotViewControllerTypeSwitchRoom) {
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.bottom.equalTo(self.view);
            make.height.equalTo(@360);
        }];
        
        self.closeView = [UIView new];
        self.closeView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.closeView];
        [self.closeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.centerX.equalTo(self.view);
            make.bottom.equalTo(self.collectionView.mas_top);
            make.height.equalTo(@(40));
        }];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"关闭" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.closeView);
            make.right.equalTo(self.view).offset(-12);
        }];
    }
}

- (void)initFWVariables
{
    [super initFWVariables];
    
    self.canClickItem = YES;
}

- (void)initFWData
{
    [super initFWData];
    [FWMJRefreshManager refresh:self.collectionView target:self headerRereshAction:@selector(headerRefresh) footerRereshAction:@selector(footerRefresh)];
}

- (void)refreshHome:(NSNotification *)noti
{
    if (noti)
    {
        NSDictionary *tmpDict = (NSDictionary *)noti.object;
        NSString *room_id = [tmpDict toString:@"room_id"];

        @synchronized (self.mainDataMArray)
        {
            NSMutableArray *tmpArray = self.mainDataMArray;
            for (HMHotItemModel *model in tmpArray)
            {
                if ([model.room_id isEqualToString:room_id])
                {
                    [tmpArray removeObject:model];
                    self.mainDataMArray = tmpArray;
                    [self.collectionView reloadData];
                    return;
                }
            }
        }
    }
}

- (void)headerRefresh
{
    [self loadDataFromPage:1];
}

- (void)footerRefresh
{
    [self loadDataFromPage:1];
}

#pragma mark 加载热门页数据
- (void)loadDataFromPage:(int)page
{
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"index" forKey:@"ctl"];
    [parmDict setObject:@"index_k" forKey:@"act"];
    [parmDict setObject:@"2" forKey:@"type"];
    
//    if (![FWUtils isBlankString:_sexString])
//    {
//        [parmDict setObject:_sexString forKey:@"sex"];
//    }
//    if (![FWUtils isBlankString:_areaString])
//    {
//        [parmDict setObject:_areaString forKey:@"city"];
//    }
//    if (![FWUtils isBlankString:self.cate_id])
//    {
//        [parmDict setObject:self.cate_id forKey:@"cate_id"];
//    }
    
    [parmDict setObject:@(page) forKey:@"p"];
    
    FWWeakify(self)
    
    [NetWorkManager asyncPostWithParameters:parmDict successBlock:^(NSDictionary *responseJson, AppNetWorkModel *netWorkModel) {
        
        FWStrongify(self)
        if ([responseJson toInt:@"status"] == 1)
        {
            self.hotModel = [HMHotModel mj_objectWithKeyValues:responseJson];
            self.mainDataMArray = self.hotModel.list;
            
            [self.musics removeAllObjects];
            for (HMHotItemModel *model in self.mainDataMArray) {
                GKMusic *music = [[GKMusic alloc] init];
                music.title = model.song.audio_info.song.title;
                music.subtitle = model.song.title;
                music.song_path = model.song.sing_path;
                music.cover_path = model.song.cover_path;
                music.lyric_path = model.song.audio_info.song.lyrics_path;
                music.other = model.song.audio_info.song;
                [self.musics addObject:music];
            }
            [GKMusicTool sharedInstance].musics = self.musics;
        
            if (self.type != KTVHotViewControllerTypeSwitchRoom) {
                [self initBanner];
            }
            
            [self.collectionView reloadData];
        }
        
        [FWMJRefreshManager endRefresh:self.collectionView];
        
    } failureBlock:^(NSError *error, AppNetWorkModel *netWorkModel) {
        
        FWStrongify(self)
        [FWMJRefreshManager endRefresh:self.collectionView];
        
    }];
}

#pragma mark 加载家族数据
- (void)loadNetData
{
    if (![IMAPlatform isAutoLogin])// 如果没有登录，就不需要后续操作
    {
        return;
    }
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"user" forKey:@"ctl"];
    [parmDict setObject:@"userinfo" forKey:@"act"];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson)
     {
         FWStrongify(self)
         if ([responseJson toInt:@"status"] == 1)
         {
             _userModel = [userPageModel mj_objectWithKeyValues:[responseJson objectForKey:@"user"]];
//             self.fanweApp.appModel.h5_url = [AppUrlModel mj_objectWithKeyValues:[responseJson objectForKey:@"h5_url"]];
//             [self sectionTitle];
//             [self.tableHeadView setCellWithModel:_userModel];
//             self.tableView.tableHeaderView = self.tableHeadView;
//             [self.personCModel creatUIWithModel:_userModel andMArr:self.detailArray andMyView:self.myLFFView];
//             [self.tableView reloadData];
         }else
         {
             [FanweMessage alertHUD:[responseJson toString:@"error"]];
         }
//         [FWMJRefreshManager endRefresh:self.tableView];
     } FailureBlock:^(NSError *error)
     {
         FWStrongify(self)
//         [FWMJRefreshManager endRefresh:self.tableView];
     }];
}

- (void)initBanner
{
    if (!_cycleScrollView)
    {
        // 网络加载 创建带标题的图片轮播器
        self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenW, [self bannerHeight]) delegate:self placeholderImage:nil];
        self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        self.cycleScrollView.currentPageDotColor = kAppMainColor; // 自定义分页控件小圆标颜色
        self.cycleScrollView.autoScrollTimeInterval = bannerAutoScrollTimeInterval;
        self.cycleScrollView.backgroundColor = kWhiteColor;
    }
    
    NSMutableArray *tmpMArray = [NSMutableArray array];
    for (HMHotBannerModel *bannerModel in self.hotModel.banner)
    {
        [tmpMArray addObject:bannerModel.image];
    }
    
    // 加载延迟
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.cycleScrollView.imageURLStringsGroup = tmpMArray;
        
    });
}

#pragma mark 点击图片回调
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    HMHotBannerModel *hotBannerModel = [self.hotModel.banner objectAtIndex:index];
    if ([AdJumpViewModel adToOthersWith:hotBannerModel])
    {
        [[AppDelegate sharedAppDelegate]pushViewController:[AdJumpViewModel adToOthersWith:hotBannerModel]];
    }
}

- (CGFloat)bannerHeight
{
    if (self.hotModel.banner)
    {
        if ([self.hotModel.banner count])
        {
            HMHotBannerModel *bannerModel = [self.hotModel.banner firstObject];
            return bannerModel.bannerHeight;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}


#pragma mark ------- UICollectionViewDelegate,UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0&&self.type == KTVHotViewControllerTypeCustom) {
        KTVHotFunctionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KTVHotFunctionCollectionViewCell" forIndexPath:indexPath];
        cell.functionLabel.text = self.functionTitles[indexPath.row];
        cell.functionImageView.image = [UIImage imageNamed:self.functionImages[indexPath.row]];
        return cell;
    }else {
        KTVHotCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KTVHotCollectionViewCell" forIndexPath:indexPath];
        cell.itemModel = self.mainDataMArray[indexPath.row];
        return cell;
    }
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0&&self.type == KTVHotViewControllerTypeCustom) {
        return self.functionTitles.count;
    }else {
        return self.mainDataMArray.count;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.type == KTVHotViewControllerTypeCustom) {
        return 2;
    }else {
        return 1;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0&&self.type == KTVHotViewControllerTypeCustom) {
        return CGSizeMake((kScreenW-60)/4, 70);
    }else {
        return CGSizeMake((kScreenW-32)/2, (kScreenW-32)/2+48);
    }
}

- (UICollectionReusableView *) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        [headerView addSubview:self.cycleScrollView];
        return headerView;
    }else {
        return nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (self.type == KTVHotViewControllerTypeCustom) {
            return CGSizeMake(kScreenW, [self bannerHeight]);
        }else {
            return  CGSizeMake(0, 0);
        }
    }else {
        return  CGSizeMake(0, 0);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0&&self.type == KTVHotViewControllerTypeCustom) {
        switch (indexPath.row) {
                case 0: // 榜单
                {
                    LeaderboardViewController *leadVC = [[LeaderboardViewController alloc] init];
                    [[AppDelegate sharedAppDelegate] pushViewController:leadVC];
//                    KTVListVC *listVC = [[KTVListVC alloc] init];
//                    [[AppDelegate sharedAppDelegate] pushViewController:listVC];
                }
                break;
                case 1: // MV
                {
                   
                }
                break;
                case 2: // 活动
                {
                    
                }
                break;
                case 3: // 家族
                {
                    [self goFamily];
                }
                break;
                
            default:
                break;
        }
    }else {
        [GKMusicTool sharedInstance].music = self.musics[indexPath.row];
        HMHotItemModel *model = self.mainDataMArray[indexPath.row];
        [self joinLivingRoom:model];
//        [KTVHomePlayController sharedInstance].music = self.musics[indexPath.row];
//        [[AppDelegate sharedAppDelegate] pushViewController:[KTVHomePlayController sharedInstance]];
    }
}

#pragma mark 加入直播间
- (void)joinLivingRoom:(HMHotItemModel *)model
{
    if (model.song.id) {
        [[NSUserDefaults standardUserDefaults] setObject:model.song.id forKey:KTV_SONG_ID];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    // 防止重复点击
    if (self.canClickItem)
    {
        self.canClickItem = NO;

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            self.canClickItem = YES;

        });
    }
    else
    {
        return;
    }

    if (![FWUtils isNetConnected])
    {
        return;
    }
    // model  转为 dic
    NSDictionary *dic = model.mj_keyValues;
    // 直播管理中心开启观众直播
    BOOL isSusWindow = [[LiveCenterManager sharedInstance] judgeIsSusWindow];
    [[LiveCenterManager sharedInstance] showLiveOfAudienceLiveofPramaDic:dic.mutableCopy isSusWindow:isSusWindow isSmallScreen:NO block:^(BOOL finished) {
    }];
}

#pragma mark 关闭页面
- (void)closeButtonAction {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark 进入家族
- (void)goFamily {
    if ([_userModel.family_id intValue] == 0)
    {
        [self createBtnFamily];
    }
    else
    {
        [self goToFamilyDesVCWithModel:_userModel];
    }
}

//创建家族
- (void)createBtnFamily
{
    EditFamilyViewController * editFamilyVC = [[EditFamilyViewController alloc] init];
    editFamilyVC.hidesBottomBarWhenPushed = YES;
    //创建家族时type=0;
    editFamilyVC.type = 0;
    editFamilyVC.user_id = self.userModel.user_id;
    [[AppDelegate sharedAppDelegate] pushViewController:editFamilyVC];
}

//家族详情
- (void)goToFamilyDesVCWithModel:(userPageModel *)userModel
{
    FamilyDesViewController * familyDesVc = [[FamilyDesViewController alloc] init];
    //是否是族长
    familyDesVc.isFamilyHeder = [userModel.family_chieftain intValue];
    familyDesVc.jid =userModel.family_id;
    familyDesVc.user_id =userModel.user_id;
    [[AppDelegate sharedAppDelegate] pushViewController:familyDesVc];
}


#pragma mark - ----------------------- GET方法 -----------------------
- (NSMutableArray *)mainDataMArray
{
    if (!_mainDataMArray)
    {
        _mainDataMArray = [NSMutableArray array];
    }
    return _mainDataMArray;
}

- (NSArray *)functionTitles {
    if (!_functionTitles) {
        _functionTitles = [NSArray arrayWithObjects:@"榜单",@"MV",@"活动",@"家族", nil];
    }
    return _functionTitles;
}

- (NSArray *)functionImages {
    if (!_functionImages) {
        _functionImages = [NSArray arrayWithObjects:@"KTV_hot_list",@"KTV_hot_mv",@"KTV_hot_act",@"KTV_hot_home", nil];
    }
    return _functionImages;
}

- (NSMutableArray *)musics {
    if (!_musics) {
        _musics = [NSMutableArray array];
    }
    return _musics;
}


@end
