//
//  KTVHotFunctionCollectionViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/2/15.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KTVHotFunctionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *functionImageView;

@property (weak, nonatomic) IBOutlet UILabel *functionLabel;

@end

NS_ASSUME_NONNULL_END
