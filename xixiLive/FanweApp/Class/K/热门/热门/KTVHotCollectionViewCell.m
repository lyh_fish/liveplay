//
//  KTVHotCollectionViewCell.m
//  FanweApp
//
//  Created by lyh on 2019/2/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import "KTVHotCollectionViewCell.h"

@implementation KTVHotCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.layer.cornerRadius = 5;
}

- (void)setItemModel:(HMHotItemModel *)itemModel {
    _itemModel = itemModel;
    
    KTVHotSongModel *model = itemModel.song;
    KTVHotSong *song = model.audio_info.song;

    if (song) {
        NSString *path = model.cover_path;
        NSData * jsonData = [path dataUsingEncoding:NSUTF8StringEncoding];
        //json解析
        id obj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        if ([obj isKindOfClass:[NSArray class]]) {
            NSArray *paths = [NSArray arrayWithArray:obj];
            if (paths.count > 0) {
                [self.musicImageView sd_setImageWithURL:[NSURL URLWithString:paths.firstObject] placeholderImage:kDefaultPreloadHeadImg];
            }else {
                [self.musicImageView setImage:kDefaultPreloadImgSquare];
            }
        }else {
            [self.musicImageView setImage:kDefaultPreloadImgSquare];
        }
        self.musicLabel.text = song.title;
        self.showLabel.text = itemModel.nick_name;
        
    }else {
        self.musicLabel.text = @"";
        self.showLabel.text = @"";
        [self.musicImageView setImage:kDefaultPreloadImgSquare];
    }
}

@end
