//
//  KTVHotSongModel.h
//  FanweApp
//
//  Created by lyh on 2019/2/28.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class KTVHotSongInfo, KTVHotSong;
@interface KTVHotSongModel : NSObject

/// 歌曲id
@property (nonatomic,copy) NSString *id;
/// 歌曲标题
@property (nonatomic,copy) NSString *title;
/// 封面图片地址,数组格式
@property (nonatomic,copy) NSString *cover_path;
/// 歌曲地址
@property (nonatomic,copy) NSString *sing_path;
///
@property (nonatomic,copy) NSString *audio_info_id;
/// 用户id
@property (nonatomic,copy) NSString *user_id;
/// 排列
@property (nonatomic,copy) NSString *sort;
/// 歌曲类型 k-song:单唱
@property (nonatomic,copy) NSString *mix_type;

@property (nonatomic,copy) NSString *created_at;
/// 歌曲类型 房间号
@property (nonatomic,copy) NSString *room_id;

/// 聊天id
@property (nonatomic,copy) NSString *group_id;

@property (nonatomic,strong) KTVHotSongInfo *audio_info;

@property (nonatomic,copy) NSString *comment_num;

@property (nonatomic,copy) NSString *digg_num;

@property (nonatomic,copy) NSString *share_num;

@property (nonatomic,copy) NSString *gift_num;

@property (nonatomic,copy) NSString *play_times;

@end

NS_ASSUME_NONNULL_END

@interface KTVHotSongInfo : NSObject

@property (nonatomic,strong) KTVHotSong *song;

@end

@interface KTVHotSong : NSObject

/// 歌曲id
@property (nonatomic,copy) NSString *id;
/// 歌曲名称
@property (nonatomic,copy) NSString *title;
/// 封面地址,是数组
@property (nonatomic,copy) NSString *cover_path;
/// 原唱地址
@property (nonatomic,copy) NSString *original_sing_path;
/// 伴奏地址
@property (nonatomic,copy) NSString *accompaniment_path;
/// 歌曲歌词地址
@property (nonatomic,copy) NSString *lyrics_path;
/// 排序
@property (nonatomic,copy) NSString * sort;
/// 时间
@property (nonatomic,copy) NSString * created_at;

@end

