//
//  KTVHotCollectionViewCell.h
//  FanweApp
//
//  Created by lyh on 2019/2/14.
//  Copyright © 2019 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMHotItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVHotCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) HMHotItemModel *itemModel;

@property (weak, nonatomic) IBOutlet UIImageView *musicImageView;

@property (weak, nonatomic) IBOutlet UILabel *musicLabel;

@property (weak, nonatomic) IBOutlet UILabel *showLabel;

@end

NS_ASSUME_NONNULL_END
