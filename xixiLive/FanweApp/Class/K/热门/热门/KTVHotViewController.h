//
//  KTVHotViewController.h
//  FanweApp
//
//  Created by lyh on 2019/2/14.
//  Copyright © 2019 xfg. All rights reserved.
//

typedef enum : NSUInteger {
    KTVHotViewControllerTypeCustom,
    KTVHotViewControllerTypeSwitchRoom,
} KTVHotViewControllerType;

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface KTVHotViewController : FWBaseViewController

@property (nonatomic, strong) UICollectionView   *collectionView;

@property (nonatomic, assign) CGRect        collectionViewFrame;

@property (nonatomic, assign) KTVHotViewControllerType type;

/**
 加载热门页数据
 
 @param page 页码
 */
- (void)loadDataFromPage:(int)page;

@end

NS_ASSUME_NONNULL_END
