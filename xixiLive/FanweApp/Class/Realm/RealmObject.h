//
//  RealmObject.h
//  qingchu
//
//  Created by ZhuXiaoyan on 2017/2/28.
//  Copyright © 2017年 whtriples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
@interface RealmObject : NSObject
//配置当前数据库
+ (void)setDefaultRealmForUser:(NSString *)username;
@end
