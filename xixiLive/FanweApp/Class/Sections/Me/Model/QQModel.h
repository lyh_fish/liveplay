//
//  QQModel.h
//  FanweApp
//
//  Created by lyh on 2018/12/23.
//  Copyright © 2018 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QQModel : NSObject
@property (nonatomic, copy)NSString * qq_number;// QQ号
@property (nonatomic, copy)NSString * desc;// 描述
@property (nonatomic, copy)NSString * image;// 图片地址
@end

NS_ASSUME_NONNULL_END
