//
//  QQTableViewCell.m
//  FanweApp
//
//  Created by lyh on 2018/12/23.
//  Copyright © 2018 xfg. All rights reserved.
//

#import "QQTableViewCell.h"

@implementation QQTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImageView.layer.cornerRadius = CGRectGetHeight(self.photoImageView.frame)/2;
    self.photoImageView.layer.masksToBounds = YES;
}

- (void)setModel:(QQModel *)model {
    _model = model;
    self.qqLabel.text = [NSString stringWithFormat:@"客服QQ:%@",model.qq_number];
    self.decLabel.text = model.desc;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:model.image]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
