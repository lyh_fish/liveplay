//
//  QQTableViewCell.h
//  FanweApp
//
//  Created by lyh on 2018/12/23.
//  Copyright © 2018 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QQModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QQTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *qqLabel;
@property (weak, nonatomic) IBOutlet UILabel *decLabel;

@property (nonatomic, strong) QQModel *model;

@end

NS_ASSUME_NONNULL_END
