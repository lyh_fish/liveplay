//
//  QQVC.m
//  FanweApp
//
//  Created by lyh on 2018/12/23.
//  Copyright © 2018 xfg. All rights reserved.
//

#import "QQVC.h"
#import "QQModel.h"
#import "QQTableViewCell.h"

@interface QQVC () <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *lists;
@end

@implementation QQVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"客服";
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(backpProfitVC) image:@"com_arrow_vc_back" highImage:@"com_arrow_vc_back"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.estimatedRowHeight = 55;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerNib:[UINib nibWithNibName:@"QQTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self loadNet];
    // Do any additional setup after loading the view from its nib.
}

#pragma marlk 返回
- (void)backpProfitVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadNet
{
    [self showMyHud];
    NSMutableDictionary *parmDict = [NSMutableDictionary dictionary];
    [parmDict setObject:@"index" forKey:@"ctl"];
    [parmDict setObject:@"qq" forKey:@"act"];
    
    __weak QQVC *weakSelf = self;
    [self.httpsManager POSTWithParameters:parmDict SuccessBlock:^(NSDictionary *responseJson) {
         [weakSelf hideMyHud];
        self.lists = [QQModel mj_objectArrayWithKeyValuesArray:responseJson[@"list"]];
        [weakSelf.tableView reloadData];
        
    } FailureBlock:^(NSError *error) {
         [weakSelf hideMyHud];
    }];
}


#pragma mark ---- UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    QQModel *qq = self.lists[indexPath.row];
    cell.model = qq;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.lists.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    QQModel *qq = self.lists[indexPath.row];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web",qq.qq_number]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (NSArray *)lists {
    if (!_lists) {
        _lists = [NSArray array];
    }
    return _lists;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
