//
//  AdJumpViewModel.m
//  FanweApp
//
//  Created by xfg on 16/10/26.
//  Copyright © 2016年 xfg. All rights reserved.
//

#import "AdJumpViewModel.h"
#import "LeaderboardViewController.h"

@implementation AdJumpViewModel

+ (id)adToOthersWith:(HMHotBannerModel *)bannerModel
{
    if (bannerModel.type == 0)      // 跳转到普通webview
    {
        if(bannerModel.in_browser == 0) { // app里面打开
            FWMainWebViewController *tmpController = [FWMainWebViewController webControlerWithUrlStr:bannerModel.url isShowIndicator:YES isShowNavBar:YES isShowBackBtn:YES isShowCloseBtn:YES];
            tmpController.navTitleStr = bannerModel.title;
            return tmpController;
        }else { // safari里面打开
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:bannerModel.url]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:bannerModel.url]];
                return nil;
            }else {
                [FanweMessage alertTWMessage:@"url不能为空"];
                return nil;
            }
        }
    }
    else if(bannerModel.type == 1)      // 跳转到排行榜
    {
        LeaderboardViewController *lbVCtr = [[LeaderboardViewController alloc] init];
        return lbVCtr;
    }
    
    LeaderboardViewController *lbVCtr = [[LeaderboardViewController alloc] init];
    return lbVCtr;
    
    return nil;
}

@end
