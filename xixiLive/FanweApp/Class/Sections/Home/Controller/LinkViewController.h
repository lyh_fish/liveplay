//
//  LinkViewController.h
//  FanweApp
//
//  Created by lyh on 2018/12/12.
//  Copyright © 2018 xfg. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LinkViewController : FWBaseViewController

@property (nonatomic, strong) VideoClassifiedModel *tmpModel;

@end

NS_ASSUME_NONNULL_END
