//
//  LinkViewController.m
//  FanweApp
//
//  Created by lyh on 2018/12/12.
//  Copyright © 2018 xfg. All rights reserved.
//

#import "LinkViewController.h"

@interface LinkViewController ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation LinkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)initFWUI
{
    [super initFWUI];
    if (self.tmpModel.value&&self.tmpModel.image_url) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.tmpModel.image_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
        }];
        self.imageView.userInteractionEnabled = true;
        [self.view addSubview:self.imageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        
        [self.imageView addGestureRecognizer:tap];
    }
}

- (void)tapAction {
    if(self.tmpModel.in_browser == 0) { // app里面打开
        FWMainWebViewController *tmpController = [FWMainWebViewController webControlerWithUrlStr:self.tmpModel.value isShowIndicator:YES isShowNavBar:YES isShowBackBtn:YES isShowCloseBtn:YES];
        tmpController.navTitleStr = self.tmpModel.title;
        [[AppDelegate sharedAppDelegate] pushViewController:tmpController];
    }else { // safari里面打开
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.tmpModel.value]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.tmpModel.value]];
        }else {
            [FanweMessage alertTWMessage:@"url不能为空"];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
